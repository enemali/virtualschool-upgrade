﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Queries;
using VirtualSchool.Application.Applicant.Invoice.Commands;
using VirtualSchool.Application.Applicant.Invoice.Models;
using VirtualSchool.Application.Applicant.Invoice.Queries;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/undergraduate/[controller]")]
    public class ApplicantController:BaseController
    {
        [HttpGet]
        public Task<IEnumerable<UndergraduateInvoice>> Get()
        {
            return Mediator.Send(new GetAllUndergraduateInvoiceQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetUndergraduateInvoiceQuery() { Id = id }));
        }

        [HttpGet("/api/undergraduate/GetByDepartmentProgrammeSession")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetByDepartment(int departmentId, int programmeId, int sessionId)
        {
            return Ok(await Mediator.Send(new GetAllApplicationFormFilterQeury() { departmentId = departmentId, programmeId = programmeId, sessionId = sessionId }));
        }


        [HttpGet("/api/undergraduate/GetByInvoiceNumber")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetByInvoiceNumber(string invoice_number)
        {
            return Ok(await Mediator.Send(new GetUndergraduateInvoiceQuery() { InvoiceNumber = invoice_number }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUndergraduateInvoiceCommand command)
        {
            var paymentId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = paymentId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateUndergraduateInvoiceCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteUndergraduateInvoiceCommand { Id = id });
            return NoContent();
        }

    }
}
