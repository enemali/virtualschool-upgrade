﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Applicant.ELearning.Commands;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Application.Applicant.ELearning.Queries;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/[controller]")]
    [ApiController]
    public class ELearningController : BaseController
    {
        [HttpGet("{id}/GetMediaFile")]
        [ProducesResponseType(typeof(EContentTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetEContentTypeQuery() { Id = id }));
        }
        [HttpPost("/api/Elearning/CreateMediaFile")]
        public async Task<IActionResult> Post([FromBody] CreateEContentTypeCommand command)
        {
            var eContentTypeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = eContentTypeId });
        }

        [HttpGet("/api/Elearning/ViewECourse")]
        [ProducesResponseType(typeof(ECourseDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ViewECourse(int PersonId, int EContentTypeId)
        {
            return Ok(await Mediator.Send(new GetECourseQuery() { PersonId = PersonId, EContentTypeId = EContentTypeId }));
        }

        [HttpPost("/api/Elearning/CreateECourse")]
        public async Task<IActionResult> CreateECourse([FromBody] CreateECourseCommand command)
        {
            var eCourseId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = eCourseId });
        }

        [HttpPost("/api/Elearning/CreateAssignment")]
        public async Task<IActionResult> CreateAssignment([FromBody] CreateEAssignmentCommand command)
        {
            var CourseId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = CourseId });
        }

        [HttpGet("/api/Elearning/GetAssignment")]
        [ProducesResponseType(typeof(AssignmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAssignment(int CourseId)
        {
            return Ok(await Mediator.Send(new GetAssignmentQuery() { CourseId = CourseId}));
        }

        [HttpPost("/api/Elearning/SubmitAssignment")]
        public async Task<IActionResult> SubmitAssignment([FromBody] CreateAssignmentSubmissionCommand command)
        {
            var AssignmentId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = AssignmentId });
        }

        [HttpGet("/api/Elearning/ViewSubmittedAssignment")]
        [ProducesResponseType(typeof(AssignmentSubmissionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ViewSubmittedAssignment(int CourseId, int PersonId)
        {
            return Ok(await Mediator.Send(new GetAssignmentSubmissionQuery() {CourseId = CourseId, PersonId = PersonId }));
        }
    }
}