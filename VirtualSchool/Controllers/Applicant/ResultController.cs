﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Applicant.Result.Queries;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/student/[controller]")]
    public class ResultController : BaseController
    {
        [HttpPost("SemesterResult")]
        public async Task<IActionResult> SemesterResult([FromBody] GetStudentSemesterResultQuery command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("SessionResult")]
        public async Task<IActionResult> SessionResult([FromBody] GetStudentSessionResultQuery command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("Transcript")]
        public async Task<IActionResult> Transcript([FromBody] GetStudentTranscriptQuery command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}