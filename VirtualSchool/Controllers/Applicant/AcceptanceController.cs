﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Acceptance.Commands;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Application.Applicant.Acceptance.Queries;
using VirtualSchool.Application.Applicant.Invoice.Models;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/applicant/[controller]")]
    public class AcceptanceController : BaseController
    {
        [HttpGet("AcceptanceStatusByApplicationFormNumber/{formNumber}")]
        [ProducesResponseType(typeof(AcceptanceView), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AcceptanceStatusByApplicationFormNumber(string formNumber)
        {
            return Ok(await Mediator.Send(new GetApplicantAcceptanceStatusQuery() { FormNumber = formNumber }));
        }

        [HttpGet("AcceptanceStatusByPersonID/{id}")]
        [ProducesResponseType(typeof(AcceptanceView), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AcceptanceStatusByPersonID(int id)
        {
            return Ok(await Mediator.Send(new GetApplicantAcceptanceStatusQuery() { PersonId = id }));
        }

        [HttpPost("GenerateAcceptanceInvoice")]
        [ProducesResponseType(typeof(AcceptanceInvoiceView), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GenerateAcceptanceInvoice(GenerateAcceptanceInvoiceDTO generateAcceptanceInvoiceDTO)
        {
            return Ok(await Mediator.Send(new GenerateAcceptanceInvoiceCommand() { GenerateAcceptanceInvoiceDTO = generateAcceptanceInvoiceDTO }));
        }
        [HttpGet("AcceptAdmissionByPersonId/{id}")]
        [ProducesResponseType(typeof(AcceptAdmissionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AcceptAdmissionByPersonId(int id)
        {
            return Ok(await Mediator.Send(new AcceptAdmissionQuery() { PersonId = id }));
        }
        [HttpPost("GenerateAcceptanceReceipt")]
        [ProducesResponseType(typeof(AcceptanceReceiptDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GenerateAcceptanceReceipt(GenerateAcceptanceReceiptObject generateAcceptanceReceiptObject)
        {
            return Ok(await Mediator.Send(new GenerateAcceptanceReceiptCommand() { GenerateAcceptanceReceiptObject =generateAcceptanceReceiptObject}));
        }

        [HttpPost]
        [ProducesResponseType(typeof(AcceptanceReceiptDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] GenerateReceiptWithInvoiceNumberCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
