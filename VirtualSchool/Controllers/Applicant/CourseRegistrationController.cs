﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.CourseRegistration.Commands;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;
using VirtualSchool.Application.Applicant.CourseRegistration.Queries;

namespace VirtualSchool.Controllers.Student
{
    [Route("api/undergraduate/[controller]")]
    public class CourseRegistrationController: BaseController
    {
        [HttpGet("GetRegisteredCourseByPersonIdAndSessionId")]
        [ProducesResponseType(typeof(IEnumerable<CourseRegistrationDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRegisteredCourseByPersonIdAndSessionId(int personId,int sessionId)
        {
            return Ok(await Mediator.Send(new GetRegisteredCourseQuery() { PersonId = personId, SessionId = sessionId }));
        }

        [HttpGet("RegisterCourses")]
        [ProducesResponseType(typeof(PullCoursesForRegistrationDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> RegisterCourses(int personId, int sessionId, int semesterId)
        {
            return Ok(await Mediator.Send(new RegisterCourseQuery() { PersonId = personId, SessionId = sessionId, SemesterId=semesterId }));
        }

        [HttpPost]
        [ProducesResponseType(typeof(ReturnedStudentCourseRegistration), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] CourseRegistrationCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpPost("UpdateCourseRegistration")]
        [ProducesResponseType(typeof(UpdateCourseRegistrationDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateCourseRegistration([FromBody] UpdateStudentCourseRegistrationCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
