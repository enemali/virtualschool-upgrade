﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Command;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.Applicant.Form.Queries;
using VirtualSchool.Application.Applicant.Invoice.Queries;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/undergraduate/[controller]")]
    public class FormController : BaseController
    {
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UndergraduateForm), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetApplicationFormQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateApplicationFormCommand command)
        {
            var formId = await Mediator.Send(command);
            //return CreatedAtAction("Get", new { id = formId });
            return Ok(new { id = formId });
        }


    }
}
