﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Invoice.Commands;
using VirtualSchool.Application.Applicant.Invoice.Models;
using VirtualSchool.Application.Applicant.Invoice.Queries;

namespace VirtualSchool.Controllers.Applicant
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class InvoiceController: BaseController
    {

        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<UndergraduateInvoice>> Get()
        {

            return Mediator.Send(new GetAllUndergraduateInvoiceQuery());
        }

        [HttpGet("GetUndergraduateInvoice/{id}")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetUndergraduateInvoiceQuery() { Id = id }));
        }
        [HttpGet("GetInvoiceByPaymentIdOrInvoiceNumberQuery/{Input}")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(string Input)
        {
            return Ok(await Mediator.Send(new GetInvoiceByPaymentIdOrInvoiceNumberQuery() { Input = Input }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUndergraduateInvoiceCommand command)
        {
            var InvoiceTypeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = InvoiceTypeId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UndergraduateInvoice), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateUndergraduateInvoiceCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteUndergraduateInvoiceCommand { Id = id });
            return NoContent();
        }
    }
}
