﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Admission.Command;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Applicant.Admission.Queries;
using VirtualSchool.Controllers;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/Admin/[controller]")]
    [ApiController]
    public class AdmissionCriteriaController : BaseController
    {

        [HttpPost("/api/Admin/CreateAdmissionCriteria")]
        public async Task<IActionResult> Post([FromBody] CreateAdmissionCriteriaCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }
        [HttpGet("/api/Admin/GetAdmissionCriteria")]
        [ProducesResponseType(typeof(AdmissionCriteriaDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCriteria(int programmeId, int departmentId )
        {
            return Ok(await Mediator.Send(new GetAdmissionCriteriaQuery() { DepartmentId = departmentId, ProgrammeId = programmeId }));
        }

        [HttpGet("/api/Admin/GetAllAdmissionCriteria")]
        [ProducesResponseType(typeof(AdmissionCriteriaDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllCriteria()
        {
            return Ok(await Mediator.Send(new GetAllAdmissionCriteriaQuery() { }));
        }
       

        [HttpPut("/api/Admin/UpdateAdmissionCriteria")]
        public async Task<IActionResult> UpdateCriteria([FromBody] UpdateAdmissionCriteriaCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }

        [HttpPost("/api/Admin/CreateAdmissionCriteriaForOlevelSubject")]
        public async Task<IActionResult> Post([FromBody] CreateAdmissionCriteriaForOLevelSubjectCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }

        [HttpGet("/api/Admin/GetAdmissionCriteriaForOlevelSubject")]
        [ProducesResponseType(typeof(AdmissionCriteriaForOLevelSubjectDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCriteriaOlevelSubject(int admissionCriteriaId, int oLevelSubjectId)
        {
            return Ok(await Mediator.Send(new GetAdmissionCriteriaForOLevelSubjectQuery() { AdmissionCriteriaId = admissionCriteriaId, OLevelSubjectId = oLevelSubjectId }));
        }

        [HttpPut("/api/Admin/UpdateAdmissionCriteriaForOlevelSubject")]
        public async Task<IActionResult> UpdateCriteriaOlevel([FromBody] UpdateOlevelSubjectCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }


        [HttpPost("/api/Admin/CreateAdmissionCriteriaForOlevelSubjectAlternative")]
        public async Task<IActionResult> Post([FromBody] CreateAdmissionCriteriaForOLevelSubjectAlternativeCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }

        [HttpGet("/api/Admin/GetAdmissionCriteriaForOlevelSubjectAlt")]
        [ProducesResponseType(typeof(AdmissionCriteriaForOLevelSubjectAlternativeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCriteriaOlevelSubjectAlt(int admissionCriteriaIdSubjectId, int oLevelSubjectId)
        {
            return Ok(await Mediator.Send(new GetAdmissionCriteriaForOLevelSubjectAlternativeQuery() { AdmissionCriteriaForOLevelSubjectId = admissionCriteriaIdSubjectId, OLevelSubjectId = oLevelSubjectId }));
        }

        [HttpPut("/api/Admin/UpdateAdmissionCriteriaForOlevelSubjectAlt")]
        public async Task<IActionResult> UpdateCriteriaOlevelAlt([FromBody] UpdateOlevelSubjectAlternativeCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }
        [HttpPost("/api/Admin/CreateAdmissionCriteriaForOlevelType")]
        public async Task<IActionResult> Post([FromBody] CreateAdmissionCriteriaForOLevelTypeCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }

        [HttpGet("/api/Admin/GetAdmissionCriteriaForOlevelType")]
        [ProducesResponseType(typeof(AdmissionCriteriaForOLevelTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCriteriaOlevelType(int departmentId, int oLevelSubjectId, int programmeId)
        {
            return Ok(await Mediator.Send(new GetAdmissionCriteriaForOLevelTypeQuery() { DepartmentId = departmentId, OlevelTypeId = oLevelSubjectId, ProgrammeId = programmeId }));
        }

        [HttpPut("/api/Admin/UpdateAdmissionCriteriaForOlevelSubjectType")]
        public async Task<IActionResult> UpdateCriteriaOlevelType([FromBody] UpdateOlevelTypeCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = admissionCriteria });
        }
    }
}