﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
//using VirtualSchool.Application.Applicant.Student.Payment.Command;
using VirtualSchool.Application.Applicant.SchoolFee.Models;
using VirtualSchool.Application.Applicant.SchoolFee.Queries;
using VirtualSchool.Application.Applicant.SchoolFee.Commands;
using VirtualSchool.Controllers;

namespace VirtualSchool.Controllers.Applicant
{
  
        [Route("api/[controller]")]
        public class SchoolFeesController : BaseController
        {

            [HttpGet()]
            [ProducesResponseType(typeof(SchoolFeesStatusDTO), (int)HttpStatusCode.OK)]
            public async Task<IActionResult> GetStatus(int PersonId, int SessionId)
            {
                return Ok(await Mediator.Send(new SchoolFeesStatusQuery() { PersonId = PersonId, SessionId = SessionId }));
            }

            [HttpGet("Invoice")]
            [ProducesResponseType(typeof(SchoolFeesInvoiceDTO), (int)HttpStatusCode.OK)]
            public async Task<IActionResult> GetInvoice(int PersonId, int SessionId)
            {
                return Ok(await Mediator.Send(new SchoolFeesInvoiceQuery() { PersonId = PersonId, SessionId = SessionId }));
            }

            [HttpGet("Receipt")]
            [ProducesResponseType(typeof(SchoolFeesReceiptDTO), (int)HttpStatusCode.OK)]
            public async Task<IActionResult> GetReceipt(int PersonId, int SessionId)
            {
                return Ok(await Mediator.Send(new SchoolFeesReceiptQuery() { PersonId = PersonId, SessionId = SessionId }));
            }

            [HttpPost("/api/SchoolFees/GenerateInvoice")]
            public async Task<IActionResult> Post([FromBody] CreateSchoolFeesCommand command)
             {
            var paymentId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = paymentId });
            }

            [HttpPost("/api/SchoolFees/GenerateReceipt")]
            public async Task<IActionResult> Post([FromBody] CreateSchoolFeesReceiptCommand command)
            {
                var paymentId = await Mediator.Send(command);
                return CreatedAtAction("Get", new { id = paymentId });
            }

    }



}

