﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Admission.Command;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Applicant.Admission.Queries;
using VirtualSchool.Controllers;


namespace VirtualSchool.Controllers.Admission
{
    [Route("api/Applicant/[controller]")]
    public class AdmissionController : BaseController
    {

        [HttpGet("{PersonId}/Status")]
        [ProducesResponseType(typeof(CheckAdmissionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetStatus(int PersonId)
        {
            return Ok(await Mediator.Send(new CheckAdmissionStatusQuery() { PersonId = PersonId }));
        }

        [HttpGet("{PersonId}/Slip")]
        [ProducesResponseType(typeof(AdmissionSlipDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSlip(int PersonId)
        {
            return Ok(await Mediator.Send(new GetAdmissionSlipQuery() { Id = PersonId }));
        }

        [HttpPost("/api/Applicant/CreateAdmission")]
        public async Task<IActionResult> Post([FromBody] CreateAdmissionCommand command)
        {
            var admissionCriteria = await Mediator.Send(command);
            //return CreatedAtAction("Get", new { id = admissionCriteria });
            return Ok(new { id = admissionCriteria });
        }

        [HttpGet("/api/Applicant/GetAdmissionListByDepartmentProgrammeSession")]
        [ProducesResponseType(typeof(AdmissionListDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAdmittedStudentsByDepartment(int departmentId, int programmeId, int sessionId)
        {
            return Ok(await Mediator.Send(new GetAdmittedStudentQuery() { departmentId = departmentId, programmeId = programmeId, sessionId = sessionId }));
        }
        [HttpGet("/api/Applicant/GetAdmissionStatusByApplicationNumber")]
        [ProducesResponseType(typeof(CheckAdmissionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAdmissionStatusByApplicationNumber(string AdmissionNumber)
        {
            return Ok(await Mediator.Send(new CheckAdmissionStatusQuery() { ApplicationNumber = AdmissionNumber }));
        }

    }
}
