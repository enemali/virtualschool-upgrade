﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Applicant.OLevelVerification.Command;
using VirtualSchool.Application.Applicant.OLevelVerification.Queries;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;
using VirtualSchool.Application.Applicant.Form.Models;

namespace VirtualSchool.Controllers.Applicant
{
    [Route("api/undergraduate/[controller]")]
    public class OlevelVerificationController : BaseController
    {
        [HttpPost("Verify")]
        public async Task<IActionResult> Verify([FromBody] VerifyOLevelByDepartmentCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("{formId}/AlternateDepartments")]
        [ProducesResponseType(typeof(List<AlternateDepartment>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAlternateDepartment(int formId)
        {
            return Ok(await Mediator.Send(new GetAlternateDepartmentQuery() { FormId = formId }));
        }

        [HttpGet("{personId}")]
        [ProducesResponseType(typeof(OlevelResultDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ViewOlevelResult(int personId)
        {
            return Ok(await Mediator.Send(new GetOlevelResultQuery() { PersonId = personId }));
        }

        [HttpPut("{olevelResulId}")]
        [ProducesResponseType(typeof(OlevelResultDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int olevelResulId, [FromBody] UpdateOLevelResultCommand command)
        {
            if (olevelResulId != command.OlevelResulId)
            {
                return BadRequest();
            }
            int personId = await Mediator.Send(command);

            return Ok(await Mediator.Send(new GetOlevelResultQuery() { PersonId = personId }));

        }
    }
}