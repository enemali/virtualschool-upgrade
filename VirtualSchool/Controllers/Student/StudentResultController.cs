﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.StudentResult.Queries;

namespace VirtualSchool.Controllers.Student
{
    [Route("api/student/[controller]")]
    public class StudentResultController:BaseController
    {
        [HttpPost("SemesterResult")]
        public async Task<IActionResult> SemesterResult([FromBody] ReturningStudentSemesterResultQuery command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpPost("SessionResult")]
        public async Task<IActionResult> SessionResult([FromBody] ReturningStudentSessionResultQuery command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Transcript")]
        public async Task<IActionResult> Transcript([FromBody] ReturningStudentTranscriptQuery command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
