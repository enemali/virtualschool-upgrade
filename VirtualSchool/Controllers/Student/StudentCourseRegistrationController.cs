﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Commands;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Queries;

namespace VirtualSchool.Controllers.Student
{
    [Route("api/undergraduate/[controller]")]
    public class StudentCourseRegistrationController : BaseController
    {
        [HttpGet("GetRegisteredCourseByPersonIdAndSessionId")]
        [ProducesResponseType(typeof(IEnumerable<StudentCourseRegistrationDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRegisteredCourseByPersonIdAndSessionId(int personId, int sessionId)
        {
            return Ok(await Mediator.Send(new GetReturningStudentRegisteredCourseQuery() { PersonId = personId, SessionId = sessionId }));
        }
        [HttpGet("RegisterCourses")]
        [ProducesResponseType(typeof(StudentPullCoursesForRegistrationDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> RegisterCourses(int personId, int sessionId, int semesterId)
        {
            return Ok(await Mediator.Send(new ReturningStudentRegisterCourseQuery() { PersonId = personId, SessionId = sessionId, SemesterId = semesterId }));
        }
        //[HttpGet("GeCourses")]
        //[ProducesResponseType(typeof(StudentPullCoursesForRegistrationDTO), (int)HttpStatusCode.OK)]
        //public async Task<IActionResult> FetchCoursesForRegistere(int programmeId, int departmentId, int departmentOptionId, int sessionId, int semesterId)
        //{
        //    return Ok(await Mediator.Send(new ReturningStudentRegisterCourseQuery() { PersonId = personId, SessionId = sessionId, SemesterId = semesterId }));
        //}
        [HttpPost]
        [ProducesResponseType(typeof(StudentCourseRegistrationList), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] ReturningStudentCourseRegistrationCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPut("UpdateCourseRegistration")]
        [ProducesResponseType(typeof(StudentUpdateCourseRegistrationDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateCourseRegistration([FromBody] UpdateReturningStudentCourseRegistrationCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
