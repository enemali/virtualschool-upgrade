﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.StudentSchoolFee.Models;
using VirtualSchool.Application.Student.StudentSchoolFee.Queries;

namespace VirtualSchool.Controllers.Student
{
    [Route("api/[controller]")]
    public class StudentSchoolFeesController:BaseController
    {
        [HttpGet()]
        [ProducesResponseType(typeof(StudentSchoolFeesStatusDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetStatus(int PersonId, int SessionId)
        {
            try
            {
                return Ok(await Mediator.Send(new StudentSchoolFeesStatusQuery() { PersonId = PersonId, SessionId = SessionId }));
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        [HttpGet("Invoice")]
        [ProducesResponseType(typeof(StudentSchoolFeesInvoiceDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetInvoice(int PersonId, int SessionId)
        {
            try
            {
                return Ok(await Mediator.Send(new StudentSchoolFeesInvoiceQuery() { PersonId = PersonId, SessionId = SessionId }));
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("Receipt")]
        [ProducesResponseType(typeof(StudentSchoolFeesReceiptDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetReceipt(int PersonId, int SessionId)
        {
            try
            {
                return Ok(await Mediator.Send(new StudentSchoolFeesReceiptQuery() { PersonId = PersonId, SessionId = SessionId }));
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
