﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.StudentLogin.Command;
using VirtualSchool.Application.Student.StudentLogin.Model;

namespace VirtualSchool.Controllers.Student
{
    public class StudentLoginController : BaseController
    {
      
        [HttpPost]
       
        [ProducesResponseType(typeof(StudentLoginReturnDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] StudentLoginCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }

}