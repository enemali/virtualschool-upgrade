﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.DepartmentOption.Models;
using System.Net;
using VirtualSchool.Application.Utilities.DepartmentOption.Command;
using VirtualSchool.Application.Utilities.DepartmentOption.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class DepartmentOptionController : BaseController
    {
        [HttpGet]
        public Task<IEnumerable<DepartmentOptionDTO>> Get()
        {
            return Mediator.Send(new GetAllDepartmentOtionQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DepartmentOptionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetDepartmentOptionQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateDepartmentOptionCommand command)
        {
            var departmentOptionId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = departmentOptionId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(DepartmentOptionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateDepartmentOptionCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteDepartmentOptionCommand { Id = id });
            return NoContent();
        }
    }
}
