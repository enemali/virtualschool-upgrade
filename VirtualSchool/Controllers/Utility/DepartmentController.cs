﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.FeeType.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.FeeType.Command;
using VirtualSchool.Application.Exceptions;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class DepartmentController : BaseController
    {
        [HttpGet]
        public Task<IEnumerable<DepartmentDTO>> Get()
        {
            return Mediator.Send(new GetAllDepartmentQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DepartmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var data = await Mediator.Send(new GetDepartmentQuery() { Id = id });
                return Ok(data);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateDepartmentCommand command)
        {
            try
            {
                var departmentId = await Mediator.Send(command);
                return CreatedAtAction("Get", new { id = departmentId });
            }
            catch (ExistingItemException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(DepartmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateDepartmentCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteDepartmentCommand { Id = id });
            return NoContent();
        }
    }
}
