﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Programme.Commands;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Application.Utilities.Programme.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class ProgrammeController : BaseController
    {
        // GET api/values
        [HttpGet]
        public Task<IEnumerable<ProgrammeDTO>> Get()
        {
            return Mediator.Send(new GetAllProgrammesQuery());
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProgrammeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetProgrammeQuery() { Id = id})) ;
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateProgrammeCommand command)
        {
            var programmeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = programmeId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ProgrammeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id,[FromBody] UpdateProgrammeCommand command)
        {
            if (id != command.Id)
            {
               return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteProgrammeCommand { Id = id });
            return NoContent();
        }

    }
}
