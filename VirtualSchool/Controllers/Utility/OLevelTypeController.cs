﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.OlevelType.Command;
using VirtualSchool.Application.Utilities.OlevelType.Models;
using VirtualSchool.Application.Utilities.OlevelType.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class OLevelTypeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<OLevelTypeDTO>> Get()
        {
            return Mediator.Send(new GetAllOlevelTypeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(OLevelTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetOlevelTypeQuery() { Id = id }));
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateOlevelTypeCommand command)
        {
            var OlevelId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = OlevelId });
        }

        //[HttpPut("{id}")]
        //[ProducesResponseType(typeof(OLevelTypeDTO), (int)HttpStatusCode.OK)]
        //public async Task<IActionResult> Put([FromRoute] int id, [FromBody]  UpdateOlevelTypeCommand command)
        //{
        //    if (id != command.Id)
        //    {
        //        return BadRequest();
        //    }

        //    return Ok(await Mediator.Send(command));
        //}


        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteOlevelTypeCommand { Id = id });
            return NoContent();
        }
    }
}
