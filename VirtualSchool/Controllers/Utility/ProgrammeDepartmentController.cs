﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries;
using MediatR;
using System.Net;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class ProgrammeDepartmentController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<ProgrammeDepartmentDTO>> Get()
        {
            return Mediator.Send(new GetAllProgrammeDepartmentQuery());
        }

        [HttpGet("{programmeId}")]
        [ProducesResponseType(typeof(ProgrammeDepartmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int programmeId)
        {
            return Ok(await Mediator.Send(new GetProgrammeDepartmentQuery() { ProgrammeId = programmeId}));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateProgrammeDepartmentCommand command)
        {
            var programmeDepartmentId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = programmeDepartmentId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ProgrammeDepartmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateProgrammeDepartmentCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteProgrammeDepartmentCommand { Id = id });
            return NoContent();
        }
        
    }
}
