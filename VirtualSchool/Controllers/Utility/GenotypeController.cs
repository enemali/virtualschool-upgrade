﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Genotype.Models;
using VirtualSchool.Application.Utilities.Genotype.Queries;
using MediatR;
using System.Net;
using VirtualSchool.Application.Utilities.Genotype.Command;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class GenotypeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<GenotypeDTO>> Get()
        {
            return Mediator.Send(new GetAllGenotypeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GenotypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetGenotypeQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateGenotypeCommand command)
        {
            var genoTypeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = genoTypeId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(GenotypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateGenotypeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGenotypeCommand { Id = id });
            return NoContent();
        }
    }
}
