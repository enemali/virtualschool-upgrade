﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;
using VirtualSchool.Application.Utilities.LocalGovernment.Queries;
using MediatR;
using System.Net;
using VirtualSchool.Application.Utilities.LocalGovernment.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class LocalGovermentController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<LocalGovernmentDTO>> Get()
        {
            return Mediator.Send(new GetAllLocalGovernmentQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LocalGovernmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetLocalGovernmentQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateLocalGovernmentCommand command)
        {
            var levelId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = levelId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(LocalGovernmentDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateLocalGovernmentCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteLocalGovernmentCommand { Id = id });
            return NoContent();
        }
    }
}

