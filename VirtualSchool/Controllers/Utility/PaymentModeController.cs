﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.PaymentMode.Models;
using VirtualSchool.Application.Utilities.PaymentMode.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.PaymenMode.Queries;
using VirtualSchool.Application.Utilities.PaymentMode.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class PaymentModeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<PaymentModeDTO>> Get()
        {
            return Mediator.Send(new GetAllPaymentModeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PaymentModeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetPaymentModeQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreatePaymentModeCommand command)
        {
            var levelId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = levelId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PaymentModeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdatePaymentModeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeletePaymentModeCommand { Id = id });
            return NoContent();
        }
    }
}

