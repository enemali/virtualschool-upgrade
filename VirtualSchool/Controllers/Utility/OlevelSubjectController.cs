﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Subject.Command;
using VirtualSchool.Application.Utilities.Subject.Models;
using VirtualSchool.Application.Utilities.Subject.Queries;

namespace VirtualSchool.Controllers.Utility
{
  
    [Route("api/[controller]")]
    public class OlevelSubjectController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<SubjectDTO>> Get()
        {
            return Mediator.Send(new GetAllSubjectQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SubjectDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetSubjectQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateSubjectCommand command)
        {
            var countryId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = countryId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(SubjectDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateSubjectCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteSubjectCommand { Id = id });
            return NoContent();
        }
    }


}