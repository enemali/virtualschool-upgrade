﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Command;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class ApplicationFormSettingController : BaseController
    {
        // GET api/values
        [HttpGet]
        public Task<IEnumerable<ApplicationFormSettingDTO>> Get()
        {
            return Mediator.Send(new GetAllApplicationFormSettingQuery());
        }


        [HttpGet("GetFormTypeByProgrammeId")]
        [ProducesResponseType(typeof(ApplicationFormSettingDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetApplicationFormSettingByProgrammeIdQuery() { Id = id }));
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateApplicationFormSettingCommand command)
        {
            var ApplicationFormSettingId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = ApplicationFormSettingId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ApplicationFormSettingDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateApplicationFormSettingCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteApplicationFormSettingCommand { Id = id });
            return NoContent();
        }
    }
}
