﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.PaymentType.Models;
using MediatR;
using VirtualSchool.Application.Utilities.PaymentType.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.PaymentType.Command;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class PaymentTypeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<PaymentTypeDTO>> Get()
        {
            return Mediator.Send(new GetAllPaymentTypeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PaymentTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetPaymentTypeQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreatePaymentTypeCommand command)
        {
            var paymentTyoeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = paymentTyoeId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PaymentTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdatePaymnetTypeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeletePaymentTypeCommand { Id = id });
            return NoContent();
        }
    }
}
