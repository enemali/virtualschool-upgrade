﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Religion.Models;
using MediatR;
using VirtualSchool.Application.Utilities.Religion.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.Religion.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class ReligionController :BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<ReligionDTO>> Get()
        {
            return Mediator.Send(new GetAllReglionQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ReligionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetReligionQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateReligionCommand command)
        {
            var reglionId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = reglionId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ReligionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateReligionCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteReligionCommand { Id = id });
            return NoContent();
        }
    }
}
