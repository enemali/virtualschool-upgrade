﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.State.Models;
using MediatR;
using VirtualSchool.Application.Utilities.State.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.State.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class StateController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<StateDTO>> Get()
        {
            return Mediator.Send(new GetAllStateQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(StateDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetStateQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateStateCommand command)
        {
            var stateId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = stateId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(StateDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateStateCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteStateCommand { Id = id });
            return NoContent();
        }
    }
}
