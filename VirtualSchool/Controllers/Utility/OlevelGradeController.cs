﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Grade.Command;
using VirtualSchool.Application.Utilities.Grade.Models;
using VirtualSchool.Application.Utilities.Grade.Queries;

namespace VirtualSchool.Controllers.Utility
{
    
    [Route("api/[controller]")]
    public class OlevelGradeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<GradeDTO>> Get()
        {
            return Mediator.Send(new GetAllGradeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GradeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetGradeQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateGradeCommand command)
        {
            var countryId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = countryId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(GradeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateGradeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGradeCommand { Id = id });
            return NoContent();
        }
    }

}
