﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Session.Models;
using VirtualSchool.Application.Utilities.Session.Queries;
using MediatR;
using System.Net;
using VirtualSchool.Application.Utilities.Session.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class SessionController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<SessionDTO>> Get()
        {
            return Mediator.Send(new GetAllSessionQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SessionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetSessionQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateSessionCommand command)
        {
            var reglionId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = reglionId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(SessionDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateSessionCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteSessionCommand { Id = id });
            return NoContent();
        }
    }
}
