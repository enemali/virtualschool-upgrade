﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Country.Models;
using MediatR;
using VirtualSchool.Application.Utilities.Country.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.Country.Command;


namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class CountryController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<CountryDTO>> Get()
        {
            return Mediator.Send(new GetAllCountryQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CountryDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetCountryQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateGountryCommand command)
        {
            var countryId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = countryId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CountryDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateCountryCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteCountryCommand { Id = id });
            return NoContent();
        }
    }

}
