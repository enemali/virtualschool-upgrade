﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Relationship.Command;
using VirtualSchool.Application.Utilities.Relationship.Models;
using VirtualSchool.Application.Utilities.Relationship.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class RelationshipController: BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<RelationshipDTO>> Get()
        {
            return Mediator.Send(new GetAllRelationshipQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RelationshipDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetRelationshipQuery() { Id = id }));
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateRelationshipCommand command)
        {
            var RelationshipId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = RelationshipId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(RelationshipDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateRelationshipCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteRelationshipCommand { Id = id });
            return NoContent();
        }
    }
}
