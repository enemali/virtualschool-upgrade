﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using VirtualSchool.Application.Utilities.Course.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.Course.Command;
using VirtualSchool.Application.Utilities.Course.Models;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class CourseController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<CourseDTO>> Get()
        {
            return Mediator.Send(new GetAllCourseQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CourseDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetCourseQuery() { Id = id }));
        }

        [HttpGet("CoursesToBeRegistered")]
        [ProducesResponseType(typeof(CourseDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> RegisterCourses(int programmeId, int departmentId, int semesterId, int levelId, int optionId)
        {
            return Ok(await Mediator.Send(new GetCourseByQuery() { programmeId = programmeId, departmentId = departmentId, semesterId = semesterId, levelId= levelId, optionId=optionId}));
        }



        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCourseCommand command)
        {
            var countryId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = countryId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CourseDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateCourseCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        //[HttpDelete("{id}")]
        //[ProducesResponseType((int)HttpStatusCode.NoContent)]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    await Mediator.Send(new DeleteCourseCommand { Id = id });
        //    return NoContent();
        //}
    }

}
