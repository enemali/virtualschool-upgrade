﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.FeeType.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.FeeType.Command;
using VirtualSchool.Application.Utilities.Level.Queries;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class FeeTypeController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<FeeTypeDTO>> Get()
        {

            return Mediator.Send(new GetAllFeeTypeQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(FeeTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetFeeTypeQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateFeeTypeCommand command)
        {
            var feeTypeId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = feeTypeId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(FeeTypeDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateFeeTypeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteFeeTypeCommand { Id = id });
            return NoContent();
        }
    }
}
