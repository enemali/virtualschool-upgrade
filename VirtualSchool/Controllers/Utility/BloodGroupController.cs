﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.BloodGroup.Models;
using VirtualSchool.Application.Utilities.BloodGroup.Queries;
using System.Net;
using VirtualSchool.Application.Utilities.BloodGroup.Command;

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class BloodGroupController : BaseController
    {
        // GET api/values
        [HttpGet]
        public Task<IEnumerable<BloodGroupDTO>> Get()
        {
            return Mediator.Send(new GetAllBloodGroupQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BloodGroupDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetBloodGroupQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateBloodGroupCommand command)
        {
            var bloodGroupId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = bloodGroupId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BloodGroupDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateBlooddGroupCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteBloodGroupCommand { Id = id });
            return NoContent();
        }
    }
}
