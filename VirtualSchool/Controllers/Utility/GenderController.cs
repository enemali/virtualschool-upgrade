﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualSchool.Application.Utilities.Gender.Models;
using VirtualSchool.Application.Utilities.Gender.Queries;
using MediatR;
using System.Net;
using VirtualSchool.Application.Utilities.Gender.Command;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VirtualSchool.Controllers.Utility
{
    [Route("api/[controller]")]
    public class GenderController : BaseController
    {
        // GET: api/values
        [HttpGet]
        public Task<IEnumerable<GenderDTO>> Get()
        {
            return Mediator.Send(new GetAllGenderQuery());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GenderDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetGenderQuery() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateGenderCommand command)
        {
            var genderId = await Mediator.Send(command);
            return CreatedAtAction("Get", new { id = genderId });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(GenderDTO), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UpdateGenderCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGenderCommand { Id = id });
            return NoContent();
        }
    }
}
