﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionCriteriaForOLevelSubjectConfiguration : IEntityTypeConfiguration<AdmissionCriteriaForOLevelSubject>
    {
        public void Configure(EntityTypeBuilder<AdmissionCriteriaForOLevelSubject> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne<AdmissionCriteria>(s => s.AdmissionCriteria).WithMany(s => s.AdmissionCriteriaForOLevelSubjects).HasConstraintName("Fk_AdmissionCriteriaForOLevelSubject_AdmissionCriteria").HasForeignKey(s => s.AdmissionCriteriaId).IsRequired();
            builder.HasOne<OlevelSubject>(s => s.OlevelSubject).WithMany(s => s.AdmissionCriteriaForOLevelSubjects).HasConstraintName("Fk_AdmissionCriteriaForOLevelSubject_OlevelSubject").HasForeignKey(s => s.OLevelSubjectId).IsRequired();
            builder.HasOne<OlevelGrade>(s => s.OlevelGrade).WithMany(s => s.AdmissionCriteriaForOLevelSubject).HasConstraintName("Fk_AdmissionCriteriaForOLevelSubject_OlevelGrade").HasForeignKey(s => s.MinimumOLevelGradeId).IsRequired();
            builder.Property(e => e.IsCompulsory).HasColumnName("IsCompulsory").IsRequired();
        }
    }
}
