﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ProgrammeDepartmentConfiguration : IEntityTypeConfiguration<ProgrammeDepartment>
    {
        public void Configure(EntityTypeBuilder<ProgrammeDepartment> builder)
        {
            builder.HasKey(table => new {
                table.ProgrammeId,
                table.DepartmentId
            });

            
            builder.Property(e => e.ProgrammeId).HasColumnName("Programme_Id");
            builder.Property(e => e.DepartmentId).HasColumnName("Department_Id");
            builder.Property(e => e.Active).HasColumnName("Active").HasDefaultValue(1);

        }

    }
}
