﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class PaymentChannelConfiguration : IEntityTypeConfiguration<PaymentChannel>
    {

        public void Configure(EntityTypeBuilder<PaymentChannel> builder)
        {
            builder.HasKey(e => e.PaymentChannnelId);
            builder.Property(e => e.PaymentChannelStatus).HasColumnName("PaymentChannelStatus").IsRequired().HasMaxLength(20).IsRequired();
            builder.Property(e => e.PaymentChannelStatus).HasColumnName("PaymentStatus").IsRequired();
            builder.Property(e => e.PaymentChannelDescription).HasColumnName("PaymentDesciption").IsRequired(false);
        }
    }
}
