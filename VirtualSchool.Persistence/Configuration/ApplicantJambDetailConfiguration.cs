﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantJambDetailConfiguration : IEntityTypeConfiguration<ApplicantJambDetail>
    {
        public void Configure(EntityTypeBuilder<ApplicantJambDetail> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.JambNumber).HasColumnName("Jamb_Number").IsRequired().HasMaxLength(10).IsRequired();
            builder.Property(e => e.JambScore).HasColumnName("Jamb_Score").IsRequired(false);
            builder.Property(e => e.SchoolChoice).HasColumnName("School_Choice").IsRequired(false);

        }
    }
}
