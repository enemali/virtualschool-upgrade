﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ScoreGradeConfiguration : IEntityTypeConfiguration<ScoreGrade>
    {
        public void Configure(EntityTypeBuilder<ScoreGrade> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.SessionStart).IsRequired(false);
        }
    }
}
