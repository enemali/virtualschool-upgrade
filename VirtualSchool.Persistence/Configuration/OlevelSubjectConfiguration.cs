﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class OlevelSubjectConfiguration : IEntityTypeConfiguration<OlevelSubject>
    {
        public void Configure(EntityTypeBuilder<OlevelSubject> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Slug).IsRequired();
            builder.Property(e => e.Code).IsRequired();
            builder.Property(e => e.Active).IsRequired();
        }
    }
}
