﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ProgrammeConfiguration : IEntityTypeConfiguration<Programme>
    {
        public void Configure(EntityTypeBuilder<Programme> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("Id");
            builder.Property(e => e.Active).HasColumnName("Active").HasDefaultValue(1);
            builder.Property(e => e.Code).HasColumnName("Code").IsRequired();
            builder.Property(e => e.Description).HasColumnName("Description").IsRequired();
            builder.Property(e => e.Name).HasColumnName("Name").IsRequired();

            //builder.HasMany<ApplicantAppliedCourse>().WithOne(e => e.Programme);
            //builder.HasMany<ProgrammeDepartment>().WithOne(e => e.Programme);
            //builder.HasMany<ProgrammeLevel>().WithOne(e => e.Programme);

        }

    }
}
