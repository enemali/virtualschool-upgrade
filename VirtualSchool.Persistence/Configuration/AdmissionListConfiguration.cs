﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionListConfiguration : IEntityTypeConfiguration<AdmissionList>
    {
        public void Configure(EntityTypeBuilder<AdmissionList> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne<ApplicationForm>(s => s.ApplicationForm).WithMany().HasForeignKey(s => s.ApplicationFormId).OnDelete(DeleteBehavior.Restrict);
           
        }
    }
}
