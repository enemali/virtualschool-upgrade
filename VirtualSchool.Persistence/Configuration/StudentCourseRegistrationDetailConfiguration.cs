﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentCourseRegistrationDetailConfiguration : IEntityTypeConfiguration<StudentCourseRegistrationDetail>
    {
        public void Configure(EntityTypeBuilder<StudentCourseRegistrationDetail> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CourseUnit).HasColumnName("CourseUnit").IsRequired();
            builder.Property(e => e.TestScore).HasColumnName("TestScore").IsRequired(false);
            builder.Property(e => e.ExamScore).HasColumnName("ExamScore").IsRequired(false);
            builder.Property(e => e.SpecialCase).HasColumnName("SpecialCase").IsRequired(false);
            builder.Property(e => e.DateUploaded).HasColumnName("DateUploaded").IsRequired(false);
            builder.HasOne(s => s.Semester).WithMany().HasForeignKey(s => s.SemesterId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(s => s.StudentCourseRegistration).WithMany().HasForeignKey(s => s.StudentCourseRegistrationId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}