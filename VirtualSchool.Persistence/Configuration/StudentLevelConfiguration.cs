﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentLevelConfiguration : IEntityTypeConfiguration<StudentLevel>
    {
        public void Configure(EntityTypeBuilder<StudentLevel> builder)
        {
            builder.HasKey(e => e.StudentLevelId);
            builder.HasOne<Session>(s => s.Session).WithMany().HasConstraintName("Fk_StudentLevel_Session").HasForeignKey(s => s.SessionId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<Student>(s => s.Student).WithMany().HasConstraintName("Fk_StudentLevel_Student").HasForeignKey(s => s.StudentId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<Programme>(s => s.Programme).WithMany().HasConstraintName("Fk_StudentLevel_Programme").HasForeignKey(s => s.ProgrammeId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<Level>(s => s.Level).WithMany().HasConstraintName("Fk_StudentLevel_Level").HasForeignKey(s => s.LevelId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<Department>(s => s.Department).WithMany().HasConstraintName("Fk_StudentLevel_Department").HasForeignKey(s => s.DepartmentId).IsRequired().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<DepartmentOption>(s => s.DepartmentOption).WithMany().HasConstraintName("Fk_StudentLevel_DepartmentOption").HasForeignKey(s => s.DepartmentOptionId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
