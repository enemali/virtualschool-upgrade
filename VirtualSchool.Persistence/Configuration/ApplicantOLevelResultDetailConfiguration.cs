﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantOLevelResultDetailConfiguration : IEntityTypeConfiguration<OlevelResultDetail>
    {
        public void Configure(EntityTypeBuilder<OlevelResultDetail> builder)
        {
            builder.HasKey(e => e.Id);
        }
    }
}
