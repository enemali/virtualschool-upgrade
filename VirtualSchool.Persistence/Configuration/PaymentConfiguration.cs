﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("Id");
            builder.Property(e => e.Amount).HasColumnName("Amount").HasDefaultValue(0);
            builder.Property(e => e.DateGenerated).HasColumnName("Date_Generated").HasDefaultValueSql("GetDate()");
            builder.Property(e => e.DatePaid).HasColumnName("Date_Paid");
            builder.Property(e => e.InvoiceNumber).HasColumnName("Invoice_Number").IsRequired();
            builder.Property(e => e.SerialNumber).HasColumnName("Serial_Number").IsRequired();

            //builder.HasOne<FeeType>().WithMany(e => e.Payments).HasForeignKey(e => e.FeeTypeId);
            //builder.HasOne<Level>().WithMany(e => e.Payments).HasForeignKey(e => e.LevelId);
            //builder.HasOne<PaymentMode>().WithMany(e => e.Payments).HasForeignKey(e => e.PaymentModeId);
            //builder.HasOne<PaymentType>().WithMany(e => e.Payments).HasForeignKey(e => e.PaymentTypeId);
            //builder.HasOne<Person>().WithMany(e => e.Payments).HasForeignKey(e => e.PersonId);
            //builder.HasOne<Session>().WithMany(e => e.Payments).HasForeignKey(e => e.SessionId);

        }

    }
}
