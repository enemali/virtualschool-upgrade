﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionCriteriaForOLevelSubjectAlternativeConfiguration : IEntityTypeConfiguration<AdmissionCriteriaForOLevelSubjectAlternative>
    {
        public void Configure(EntityTypeBuilder<AdmissionCriteriaForOLevelSubjectAlternative> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne<AdmissionCriteriaForOLevelSubject>(s => s.AdmissionCriteriaForOLevelSubject).WithMany(s => s.AdmissionCriteriaForOLevelSubjectAlternatives).HasConstraintName("Fk_AdmissionCriteriaForOLevelSubjectAlternative_AdmissionCriteriaForOLevelSubject").HasForeignKey(s => s.AdmissionCriteriaForOLevelSubjectId).IsRequired();
            builder.HasOne<OlevelSubject>(s => s.OlevelSubject).WithMany(s => s.AdmissionCriteriaForOLevelSubjectAlternatives).HasConstraintName("Fk_AdmissionCriteriaForOLevelSubjectAlternative_OlevelSubject").HasForeignKey(s => s.OLevelSubjectId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        }
    }
}
