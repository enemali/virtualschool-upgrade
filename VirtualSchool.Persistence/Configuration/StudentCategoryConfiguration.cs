﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentCategoryConfiguration : IEntityTypeConfiguration<StudentCategory>
    {
        public void Configure(EntityTypeBuilder<StudentCategory> builder)
        {
            builder.HasKey(e => e.StudentCateoryId);
            builder.Property(e => e.StudentCategoryName).HasColumnName("StudentCategoryName").IsRequired();
            builder.Property(e => e.StudentCategoryDescription).HasColumnName("StudentCategoryDescription").IsRequired(false);
        }
    }
}
