﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{

    public class PaymentTerminalConfiguration : IEntityTypeConfiguration<PaymentTerminal>
    {
        public void Configure(EntityTypeBuilder<PaymentTerminal> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Terminal).HasColumnName("Terminal").IsRequired().HasMaxLength(20).IsRequired();
          


        }
    }
    
}
