﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
  
        public class ECourseConfiguration : IEntityTypeConfiguration<ECourse>
        {
            public void Configure(EntityTypeBuilder<ECourse> builder)
            {
                builder.HasKey(e => e.Id);
                builder.HasOne<Course>(s => s.Course).WithMany(s => s.ECourses).HasConstraintName("FK_ECourse_Course").HasForeignKey(s => s.CourseId).IsRequired();
                builder.HasOne<EContentType>(s => s.EContentType).WithMany(s => s.ECourses).HasConstraintName("FK_ECourse_EContentType").HasForeignKey(s => s.EContentTypeId).IsRequired();
            }
        }
    
}
