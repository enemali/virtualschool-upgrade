﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentCourseRegistrationConfiguration : IEntityTypeConfiguration<StudentCourseRegistration>
    {
        public void Configure(EntityTypeBuilder<StudentCourseRegistration> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne(s => s.Student).WithMany().HasForeignKey(s => s.StudentId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
