﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class CourseModeConfiguration : IEntityTypeConfiguration<CourseMode>
    {
        public void Configure(EntityTypeBuilder<CourseMode> builder)
        {
            builder.HasKey(e => e.CourseModeId);
            builder.Property(e => e.CourseModeId).HasColumnName("CourseModeId");
            builder.Property(e => e.CourseModeName).HasColumnName("CourseModeName").IsRequired();
            builder.Property(e => e.CourseModeDescription).HasColumnName("CourseModeDescription");
        }
    }
}
