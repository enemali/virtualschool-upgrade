﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasKey(e => e.PersonId);
            builder.HasOne(s => s.Person).WithMany().HasForeignKey(s => s.PersonId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
