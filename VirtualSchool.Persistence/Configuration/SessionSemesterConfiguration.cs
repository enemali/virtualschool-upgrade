﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class SessionSemesterConfiguration : IEntityTypeConfiguration<SessionSemester>
    {
        public void Configure(EntityTypeBuilder<SessionSemester> builder)
        {
            builder.HasKey(e => e.SessionSemesterId);
        }
    }
}
