﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("Id");
            builder.Property(e => e.Active).HasColumnName("Active").HasDefaultValue(1);
            builder.Property(e => e.Code).HasColumnName("Code").IsRequired();
            builder.Property(e => e.Description).HasColumnName("Description").IsRequired();
            builder.Property(e => e.Name).HasColumnName("Name").IsRequired();

           // builder.HasOne<Faculty>().WithMany(e => e.Departments).HasForeignKey(e => e.FacultyId);
            //builder.HasMany<ApplicantAppliedCourse>().WithOne(e => e.Department);

        }
    }
}
