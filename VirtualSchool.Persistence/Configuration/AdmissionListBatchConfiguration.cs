﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionListBatchConfiguration : IEntityTypeConfiguration<AdmissionListBatch>
    {
        public void Configure(EntityTypeBuilder<AdmissionListBatch> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne<AdmissionListType>(s => s.AdmissionListType).WithMany(s => s.AdmissionListBatches).HasConstraintName("Fk_AdmissionListBatch_AdmissionListType").HasForeignKey(s => s.AdmissionListTypeId).IsRequired();
            builder.Property(e => e.DateUploaded).HasColumnName("DateUploaded").IsRequired();
            builder.Property(e => e.UploadedFilePath).HasColumnName("UploadedFilePath").IsRequired(false);
        }
    }
}
