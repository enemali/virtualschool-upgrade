﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantConfiguration : IEntityTypeConfiguration<Applicant>
    {
        public void Configure(EntityTypeBuilder<Applicant> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne(s => s.Person).WithMany().HasForeignKey(s => s.PersonId).OnDelete(DeleteBehavior.Restrict);


        }
    }
}
