﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;


namespace VirtualSchool.Persistence.Configuration
{
    public class OnlinePaymentConfiguration : IEntityTypeConfiguration<OnlinePayment>
    {
        public void Configure(EntityTypeBuilder<OnlinePayment> builder)
        {
            builder.HasKey(e => e.PaymentId);
            builder.Property(e => e.TransactionNumber).HasColumnName("TransactionNumber").IsRequired(false);
            builder.Property(e => e.TransactionDate).HasColumnName("TransactionDate").IsRequired(false);
            builder.HasOne<Payment>(s => s.Payment).WithMany(s => s.OnlinePayments).HasConstraintName("Fk_OnlinePayments_Payment").HasForeignKey(s => s.PaymentId).IsRequired();
            builder.HasOne<PaymentChannel>(s => s.PaymentChannel).WithMany(s => s.OnlinePayments).HasConstraintName("Fk_OnlinePayments_PaymentChannel").HasForeignKey(s => s.PaymentChannelId).IsRequired();
        }
    }
}
