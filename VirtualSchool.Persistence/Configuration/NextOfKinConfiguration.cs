﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class NextOfKinConfiguration : IEntityTypeConfiguration<NextOfkin>
    {
        public void Configure(EntityTypeBuilder<NextOfkin> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Address).IsRequired();
            builder.Property(e => e.PhoneNumber).IsRequired();
            builder.Property(e => e.Name).IsRequired();
        }
    }
}
