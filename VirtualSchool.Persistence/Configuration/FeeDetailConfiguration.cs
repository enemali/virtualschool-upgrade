﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class FeeDetailConfiguration : IEntityTypeConfiguration<FeeDetail>
    {
        public void Configure(EntityTypeBuilder<FeeDetail> builder)
        {
            builder.HasKey(e => e.Id);
            //builder.HasOne<Department>(s => s.Department).WithMany(s => s.FeeDetails).HasConstraintName("Fk_FeeDetail_Department").HasForeignKey(s => s.DepartmentId);
            //builder.HasOne<Fee>(s => s.Fee).WithMany(s => s.FeeDetails).HasConstraintName("Fk_FeeDetail_Fee").HasForeignKey(s => s.FeeId).IsRequired();
            //builder.HasOne<FeeType>(s => s.FeeType).WithMany(s => s.FeeDetails).HasConstraintName("Fk_FeeDetail_FeeType").HasForeignKey(s => s.FeeTypeId).IsRequired();
            //builder.HasOne<Session>(s => s.Session).WithMany(s => s.FeeDetails).HasConstraintName("Fk_FeeDetail_Session").HasForeignKey(s => s.SessionId).IsRequired();
            //builder.HasOne<Level>(s => s.Level).WithMany(s => s.FeeDetail).HasConstraintName("Fk_FeeDetail_Level").HasForeignKey(s => s.LevelId).IsRequired();
        }
    }
}
