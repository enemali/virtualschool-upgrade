﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ProgrammeLevelConfiguration : IEntityTypeConfiguration<ProgrammeLevel>
    {
        public void Configure(EntityTypeBuilder<ProgrammeLevel> builder)
        {
            builder.HasKey(table => new {
                table.ProgrammeId,
                table.LevelId
            });

            builder.Property(e => e.ProgrammeId).HasColumnName("Programme_Id");
            builder.Property(e => e.LevelId).HasColumnName("Level_Id");
            builder.Property(e => e.Active).HasColumnName("Active").HasDefaultValue(1);

            //builder.HasOne<Programme>().WithMany(a => a.ProgrammeLevel).HasForeignKey(a => a.ProgrammeId);
            //builder.HasOne<Level>().WithMany(a => a.ProgrammeLevel).HasForeignKey(a => a.LevelId);


        }

    }
}
