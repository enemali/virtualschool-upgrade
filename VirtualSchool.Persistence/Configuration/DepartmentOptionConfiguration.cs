﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class DepartmentOptionConfiguration : IEntityTypeConfiguration<DepartmentOption>
    {
        public void Configure(EntityTypeBuilder<DepartmentOption> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("Id");
            builder.Property(e => e.Active).HasColumnName("Active").HasDefaultValue(1);
            builder.Property(e => e.Code).HasColumnName("Code").IsRequired();
            builder.Property(e => e.Description).HasColumnName("Description").IsRequired();
            builder.Property(e => e.Name).HasColumnName("Name").IsRequired();

           // builder.HasOne<Department>().WithMany(e => e.DepartmentOptions).HasForeignKey(e => e.DepartmentId);
            //builder.HasMany<ApplicantAppliedCourse>().WithOne(e => e.DepartmentOption);

        }
    }
}
