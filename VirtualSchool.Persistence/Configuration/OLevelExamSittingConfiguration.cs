﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class OLevelExamSittingConfiguration : IEntityTypeConfiguration<OLevelExamSitting>
    {
        public void Configure(EntityTypeBuilder<OLevelExamSitting> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).HasColumnName("Name").IsRequired();
        }
    }
}
