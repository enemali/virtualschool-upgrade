﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("Id");
            builder.Property(e => e.Surname).HasColumnName("Surname").IsRequired();
            builder.Property(e => e.Firstname).HasColumnName("Firstname").IsRequired();
            builder.Property(e => e.Othername).HasColumnName("Othername").IsRequired();
            builder.Property(e => e.PhoneNumber).HasColumnName("Phone_Number").IsRequired();
            builder.Property(e => e.ContactAddress).HasColumnName("Contact_Address").IsRequired(false);
            builder.Property(e => e.Email).HasColumnName("Email").IsRequired(false);
            builder.Property(e => e.Picture).HasColumnName("Picture").IsRequired(false);
            builder.Property(e => e.DateOfBirth).HasColumnName("Date_Of_Birth").IsRequired(false);

            //builder.HasOne<Gender>().WithMany(e => e.People).HasForeignKey(e => e.GenderId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<State>().WithMany(e => e.People).HasForeignKey(e => e.StateId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<LocalGovernment>().WithMany(e => e.People).HasForeignKey(e => e.LocalGovernmentId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<Country>().WithMany(e => e.People).HasForeignKey(e => e.CountryId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<BloodGroup>().WithMany(e => e.People).HasForeignKey(e => e.BloodGroupId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<Genotype>().WithMany(e => e.People).HasForeignKey(e => e.GenotypeId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
            //builder.HasOne<Religion>().WithMany(e => e.People).HasForeignKey(e => e.ReligionId).OnDelete(DeleteBehavior.Restrict).IsRequired(false);
           
            
            //builder.HasMany<Payment>().WithOne(e => e.Person).OnDelete(DeleteBehavior.Restrict);

        }

    }
}
