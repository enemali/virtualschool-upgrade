﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class CourseUnitConfiguration : IEntityTypeConfiguration<CourseUnit>
    {
        public void Configure(EntityTypeBuilder<CourseUnit> builder)
        {
            builder.HasKey(e => e.CourseUnitId);
            builder.Property(e => e.CourseUnitId).HasColumnName("CourseUnitId");
            builder.Property(e => e.MaximumUnit).HasColumnName("MaximumUnit").IsRequired();
            builder.Property(e => e.MinimumUnit).HasColumnName("MinimumUnit").IsRequired();
            builder.HasOne<Semester>(s => s.Semester).WithMany().HasConstraintName("Fk_CourseUnits_Semester").HasForeignKey(s => s.SemesterId).IsRequired();
            builder.HasOne<Programme>(s => s.Programme).WithMany().HasConstraintName("Fk_CourseUnits_Programme").HasForeignKey(s => s.ProgrammeId).IsRequired();
            builder.HasOne<Level>(s => s.Level).WithMany().HasConstraintName("Fk_CourseUnits_Level").HasForeignKey(s => s.LevelId).IsRequired();
            builder.HasOne<Department>(s => s.Department).WithMany().HasConstraintName("Fk_CourseUnits_Department").HasForeignKey(s => s.DepartmentId).IsRequired();
        }
    }
}
