﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionCriteriaForOLevelTypeConfiguration : IEntityTypeConfiguration<AdmissionCriteriaForOLevelType>
    {
        public void Configure(EntityTypeBuilder<AdmissionCriteriaForOLevelType> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasOne<AdmissionCriteria>(s => s.AdmissionCriteria).WithMany(s => s.AdmissionCriteriaForOLevelTypes).HasConstraintName("Fk_AdmissionCriteriaForOLevelType_AdmissionCriteria").HasForeignKey(s => s.AdmissionCriteriaId).IsRequired();
            builder.HasOne<OlevelType>(s => s.OlevelType).WithMany(s => s.AdmissionCriteriaForOLevelTypes).HasConstraintName("Fk_AdmissionCriteriaForOLevelType_OlevelType").HasForeignKey(s => s.OLevelTypeId).IsRequired();
        }
    }
}
