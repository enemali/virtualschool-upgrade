﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
   
        public class EAssignmentSubmissionConfiguration : IEntityTypeConfiguration<EAssignmentSubmission>
        {
            public void Configure(EntityTypeBuilder<EAssignmentSubmission> builder)
            {
                builder.HasKey(e => e.Id);
                builder.HasOne<Student>(s => s.Student).WithMany(s => s.EAssignmentSubmissions).HasConstraintName("FK_EAssignmentSubmissions_Student").HasForeignKey(s => s.PersonId).OnDelete(DeleteBehavior.Restrict).IsRequired();
                builder.HasOne<EAssignment>(s => s.EAssignment).WithMany(s => s.EAssignmentSubmissions).HasConstraintName("FK_EAssignmentSubmissions_EAssignment").HasForeignKey(s => s.EAssignmentId).IsRequired();
            
            }
        }
    
}
