﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class StudentTypeConfiguration : IEntityTypeConfiguration<StudentType>
    {
        public void Configure(EntityTypeBuilder<StudentType> builder)
        {
            builder.HasKey(e => e.StudentTypeId);
            builder.Property(e => e.StudentTypeName).HasColumnName("StudentTypeName").IsRequired();
            builder.Property(e => e.StudentTypeDescription).HasColumnName("StudentTypeDescription").IsRequired(false);
        }
    }
}
