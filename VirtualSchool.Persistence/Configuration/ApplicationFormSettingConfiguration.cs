﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicationFormSettingConfiguration : IEntityTypeConfiguration<ApplicationFormSetting>
    {
        public void Configure(EntityTypeBuilder<ApplicationFormSetting> builder)
        {
            builder.HasKey(e => e.Id);
        }
    }
}
