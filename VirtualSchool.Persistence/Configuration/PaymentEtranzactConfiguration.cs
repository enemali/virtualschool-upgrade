﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class PaymentEtranzactConfiguration : IEntityTypeConfiguration<PaymentEtranzact>
    {
        public void Configure(EntityTypeBuilder<PaymentEtranzact> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.TransactionAmount).HasColumnName("TransactionAmount");
            builder.Property(e => e.BankCode).HasColumnName("BankCode");
            builder.Property(e => e.BranchCode).HasColumnName("BranchCode");
            builder.Property(e => e.ConfirmationNo).HasColumnName("ConfirmationNo").IsRequired();
            builder.Property(e => e.CustomerAddress).HasColumnName("CustomerAddress");
            builder.Property(e => e.CustomerID).HasColumnName("CustomerID");
            builder.Property(e => e.CustomerName).HasColumnName("CustomerName").IsRequired();
            builder.Property(e => e.MerchantCode).HasColumnName("MerchantCode");
            builder.Property(e => e.PaymentCode).HasColumnName("PaymentCode");
            builder.Property(e => e.ReceiptNo).HasColumnName("ReceiptNo");
            builder.Property(e => e.TransactionDate).HasColumnName("TransactionDate");
            builder.Property(e => e.TransactionDescription).HasColumnName("TransactionDescription");
            builder.HasOne(s => s.PaymentEtranzactType).WithMany().HasForeignKey(s => s.PaymentEtranzactTypeId).OnDelete(DeleteBehavior.Restrict).IsRequired();
            builder.HasOne(s => s.PaymentTerminal).WithMany().HasForeignKey(s => s.PaymenTerminalId).OnDelete(DeleteBehavior.Restrict).IsRequired();
            builder.HasOne(s => s.Payment).WithMany().HasForeignKey(s => s.PaymentId).OnDelete(DeleteBehavior.Restrict).IsRequired();
        }
    }
}
