﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantClearanceConfiguration : IEntityTypeConfiguration<ApplicantClearance>
    {
        public void Configure(EntityTypeBuilder<ApplicantClearance> builder)
        {
            builder.HasKey(e => e.Id);
          
        }
    }
}
