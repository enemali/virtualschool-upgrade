﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
  
        public class EAssignmentConfiguration : IEntityTypeConfiguration<EAssignment>
        {
            public void Configure(EntityTypeBuilder<EAssignment> builder)
            {
                builder.HasKey(e => e.Id);
                builder.HasOne<Course>(s => s.Course).WithMany(s => s.EAssignments).HasConstraintName("FK_EAssignmenta_Course").HasForeignKey(s => s.CourseId).IsRequired();


        }
    }
    
}
