﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class OlevelResultConfiguration : IEntityTypeConfiguration<OlevelResult>
    {
        public void Configure(EntityTypeBuilder<OlevelResult> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.ExamNumber).IsRequired();
            builder.Property(e => e.ExamYear).IsRequired();
        }
    }
}
