﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantOLevelResultConfiguration : IEntityTypeConfiguration<OlevelResult>
    {
        public void Configure(EntityTypeBuilder<OlevelResult> builder)
        {
            builder.HasKey(e => e.Id);
        }
    }
}
