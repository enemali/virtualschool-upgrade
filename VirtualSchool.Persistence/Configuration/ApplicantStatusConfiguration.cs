﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantStatusConfiguration : IEntityTypeConfiguration<ApplicantStatus>
    {
        public void Configure(EntityTypeBuilder<ApplicantStatus> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).HasColumnName("Name").IsRequired(true);
            builder.Property(e => e.Description).HasColumnName("Description").IsRequired();
        }
    }
}
