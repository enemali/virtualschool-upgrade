﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class ApplicantAppliedCourseConfiguration : IEntityTypeConfiguration<ApplicantAppliedCourse>
    {
        public void Configure(EntityTypeBuilder<ApplicantAppliedCourse> builder)
        {
            builder.HasKey(e => e.Id);
            //builder.HasOne<Person>(s => s.Person).WithMany(s => s.ApplicantAppliedCourses).HasConstraintName("Fk_AppliedCourse_Person").HasForeignKey(s => s.PersonId);
            //builder.HasOne<Programme>(s => s.Programme).WithMany(s => s.ApplicantAppliedCourses).HasConstraintName("Fk_AppliedCourse_Programme").HasForeignKey(s => s.ProgrammeId);
            //builder.HasOne<Department>(s => s.Department).WithMany(s => s.ApplicantAppliedCourses).HasConstraintName("Fk_AppliedCourse_Department").HasForeignKey(s => s.DepartmentId);
            //builder.HasOne<DepartmentOption>(s => s.DepartmentOption).WithMany(s => s.ApplicantAppliedCourses).HasConstraintName("Fk_AppliedCourse_Department_Option").HasForeignKey(s => s.DepartmentOptionId).IsRequired(false);
            //builder.HasOne<ApplicationForm>(s => s.ApplicationForm).WithMany(s => s.ApplicantAppliedCourses).HasConstraintName("Fk_AppliedCourse_Application_Form").HasForeignKey(s => s.ApplicationFormId).IsRequired(false);

        }
    }
}
