﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence.Configuration
{
    public class AdmissionCriteriaConfiguration : IEntityTypeConfiguration<AdmissionCriteria>
    {
        public void Configure(EntityTypeBuilder<AdmissionCriteria> builder)
        {
            builder.HasKey(e => e.Id);
          
        }
    }
}
