﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualSchool.Persistence.Migrations
{
    public partial class SchoolModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdmissionListType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionListType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BloodGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BloodGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CourseMode",
                columns: table => new
                {
                    CourseModeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseModeName = table.Column<string>(nullable: false),
                    CourseModeDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseMode", x => x.CourseModeId);
                });

            migrationBuilder.CreateTable(
                name: "CourseType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EContentType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Slug = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EContentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Faculty",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faculty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Fee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FeeName = table.Column<string>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    FeeDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fee", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FeeType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gender",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gender", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genotype",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genotype", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Level",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Level", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OLevelExamSitting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OLevelExamSitting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OlevelGrade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OlevelGrade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OlevelSubject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OlevelSubject", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OlevelType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OlevelType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentChannel",
                columns: table => new
                {
                    PaymentChannnelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PaymentChannelName = table.Column<string>(nullable: true),
                    PaymentDesciption = table.Column<string>(nullable: true),
                    PaymentStatus = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentChannel", x => x.PaymentChannnelId);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Programme",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programme", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RelationShip",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelationShip", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Religion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Religion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScoreGrade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ScoreFrom = table.Column<int>(nullable: false),
                    ScoreTo = table.Column<int>(nullable: false),
                    Grade = table.Column<string>(nullable: true),
                    GradePoint = table.Column<decimal>(nullable: false),
                    Performance = table.Column<string>(nullable: true),
                    GradeDescription = table.Column<string>(nullable: true),
                    SessionStart = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScoreGrade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Semester",
                columns: table => new
                {
                    SemesterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SemesterName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Semester", x => x.SemesterId);
                });

            migrationBuilder.CreateTable(
                name: "Session",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Session", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StudentCategory",
                columns: table => new
                {
                    StudentCateoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentCategoryName = table.Column<string>(nullable: false),
                    StudentCategoryDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCategory", x => x.StudentCateoryId);
                });

            migrationBuilder.CreateTable(
                name: "StudentType",
                columns: table => new
                {
                    StudentTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentTypeName = table.Column<string>(nullable: false),
                    StudentTypeDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentType", x => x.StudentTypeId);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionListBatch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdmissionListTypeId = table.Column<int>(nullable: false),
                    DateUploaded = table.Column<DateTime>(nullable: false),
                    UploadedFilePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionListBatch", x => x.Id);
                    table.ForeignKey(
                        name: "Fk_AdmissionListBatch_AdmissionListType",
                        column: x => x.AdmissionListTypeId,
                        principalTable: "AdmissionListType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true),
                    CountryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.Id);
                    table.ForeignKey(
                        name: "FK_State_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true),
                    FacultyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Department_Faculty_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "Faculty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProgrammeLevel",
                columns: table => new
                {
                    Programme_Id = table.Column<int>(nullable: false),
                    Level_Id = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgrammeLevel", x => new { x.Programme_Id, x.Level_Id });
                    table.ForeignKey(
                        name: "FK_ProgrammeLevel_Level_Level_Id",
                        column: x => x.Level_Id,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgrammeLevel_Programme_Programme_Id",
                        column: x => x.Programme_Id,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationFormSetting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicationFormSettingName = table.Column<string>(nullable: true),
                    FeeTypeId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationFormSetting", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationFormSetting_FeeType_FeeTypeId",
                        column: x => x.FeeTypeId,
                        principalTable: "FeeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationFormSetting_Level_LevelId",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationFormSetting_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentEtranzactType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    FeetypeId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    PaymentModeId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentEtranzactType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzactType_FeeType_FeetypeId",
                        column: x => x.FeetypeId,
                        principalTable: "FeeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzactType_PaymentMode_PaymentModeId",
                        column: x => x.PaymentModeId,
                        principalTable: "PaymentMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzactType_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzactType_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentTerminal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Terminal = table.Column<string>(maxLength: 20, nullable: false),
                    FeetypeId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTerminal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTerminal_FeeType_FeetypeId",
                        column: x => x.FeetypeId,
                        principalTable: "FeeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentTerminal_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SessionSemester",
                columns: table => new
                {
                    SessionSemesterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(nullable: false),
                    SemesterId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionSemester", x => x.SessionSemesterId);
                    table.ForeignKey(
                        name: "FK_SessionSemester_Semester_SemesterId",
                        column: x => x.SemesterId,
                        principalTable: "Semester",
                        principalColumn: "SemesterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SessionSemester_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LocalGovernment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true),
                    stateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalGovernment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LocalGovernment_State_stateId",
                        column: x => x.stateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCriteria",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    MinimumRequiredNumberOfSubject = table.Column<int>(nullable: false),
                    DateEntered = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCriteria", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdmissionCriteria_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionCriteria_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourseUnit",
                columns: table => new
                {
                    CourseUnitId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartmentId = table.Column<int>(nullable: false),
                    SemesterId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    MinimumUnit = table.Column<byte>(nullable: false),
                    MaximumUnit = table.Column<byte>(nullable: false),
                    LevelId1 = table.Column<int>(nullable: true),
                    SemesterId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseUnit", x => x.CourseUnitId);
                    table.ForeignKey(
                        name: "Fk_CourseUnits_Department",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_CourseUnits_Level",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseUnit_Level_LevelId1",
                        column: x => x.LevelId1,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_CourseUnits_Programme",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_CourseUnits_Semester",
                        column: x => x.SemesterId,
                        principalTable: "Semester",
                        principalColumn: "SemesterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseUnit_Semester_SemesterId1",
                        column: x => x.SemesterId1,
                        principalTable: "Semester",
                        principalColumn: "SemesterId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentOption",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true),
                    DepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentOption", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartmentOption_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeeDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FeeId = table.Column<int>(nullable: false),
                    FeeTypeId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeeDetail_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeeDetail_Fee_FeeId",
                        column: x => x.FeeId,
                        principalTable: "Fee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeeDetail_FeeType_FeeTypeId",
                        column: x => x.FeeTypeId,
                        principalTable: "FeeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeeDetail_Level_LevelId",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FeeDetail_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProgrammeDepartment",
                columns: table => new
                {
                    Programme_Id = table.Column<int>(nullable: false),
                    Department_Id = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgrammeDepartment", x => new { x.Programme_Id, x.Department_Id });
                    table.ForeignKey(
                        name: "FK_ProgrammeDepartment_Department_Department_Id",
                        column: x => x.Department_Id,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgrammeDepartment_Programme_Programme_Id",
                        column: x => x.Programme_Id,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Surname = table.Column<string>(nullable: false),
                    Firstname = table.Column<string>(nullable: false),
                    Othername = table.Column<string>(nullable: false),
                    Phone_Number = table.Column<string>(nullable: false),
                    Contact_Address = table.Column<string>(nullable: true),
                    Hometown = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Date_Of_Birth = table.Column<DateTime>(nullable: true),
                    GenderId = table.Column<int>(nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    LocalGovernmentId = table.Column<int>(nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    BloodGroupId = table.Column<int>(nullable: true),
                    GenotypeId = table.Column<int>(nullable: true),
                    ReligionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Person_BloodGroup_BloodGroupId",
                        column: x => x.BloodGroupId,
                        principalTable: "BloodGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Gender_GenderId",
                        column: x => x.GenderId,
                        principalTable: "Gender",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Genotype_GenotypeId",
                        column: x => x.GenotypeId,
                        principalTable: "Genotype",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_LocalGovernment_LocalGovernmentId",
                        column: x => x.LocalGovernmentId,
                        principalTable: "LocalGovernment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Religion_ReligionId",
                        column: x => x.ReligionId,
                        principalTable: "Religion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCriteriaForOLevelSubject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdmissionCriteriaId = table.Column<int>(nullable: false),
                    OLevelSubjectId = table.Column<int>(nullable: false),
                    MinimumOLevelGradeId = table.Column<int>(nullable: false),
                    IsCompulsory = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCriteriaForOLevelSubject", x => x.Id);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelSubject_AdmissionCriteria",
                        column: x => x.AdmissionCriteriaId,
                        principalTable: "AdmissionCriteria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelSubject_OlevelGrade",
                        column: x => x.MinimumOLevelGradeId,
                        principalTable: "OlevelGrade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelSubject_OlevelSubject",
                        column: x => x.OLevelSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCriteriaForOLevelType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdmissionCriteriaId = table.Column<int>(nullable: false),
                    OLevelTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCriteriaForOLevelType", x => x.Id);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelType_AdmissionCriteria",
                        column: x => x.AdmissionCriteriaId,
                        principalTable: "AdmissionCriteria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelType_OlevelType",
                        column: x => x.OLevelTypeId,
                        principalTable: "OlevelType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseTypeId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    LevelId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    DepartmentOptionId = table.Column<int>(nullable: true),
                    Unit = table.Column<int>(nullable: false),
                    SemesterId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    Activated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Course_CourseType_CourseTypeId",
                        column: x => x.CourseTypeId,
                        principalTable: "CourseType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Course_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Course_DepartmentOption_DepartmentOptionId",
                        column: x => x.DepartmentOptionId,
                        principalTable: "DepartmentOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Course_Level_LevelId",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Course_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Course_Semester_SemesterId",
                        column: x => x.SemesterId,
                        principalTable: "Semester",
                        principalColumn: "SemesterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationForm",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    ApplicationFormSettingId = table.Column<int>(nullable: false),
                    SerialNumber = table.Column<int>(nullable: false),
                    FormNumber = table.Column<string>(nullable: true),
                    ExamNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationForm", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationForm_ApplicationFormSetting_ApplicationFormSettingId",
                        column: x => x.ApplicationFormSettingId,
                        principalTable: "ApplicationFormSetting",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationForm_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    PaymentModeId = table.Column<int>(nullable: false),
                    PaymentTypeId = table.Column<int>(nullable: false),
                    FeeTypeId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    Serial_Number = table.Column<long>(nullable: false),
                    Invoice_Number = table.Column<string>(nullable: false),
                    Date_Generated = table.Column<DateTime>(nullable: false, defaultValueSql: "GetDate()"),
                    Date_Paid = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payment_FeeType_FeeTypeId",
                        column: x => x.FeeTypeId,
                        principalTable: "FeeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payment_Level_LevelId",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payment_PaymentMode_PaymentModeId",
                        column: x => x.PaymentModeId,
                        principalTable: "PaymentMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payment_PaymentType_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payment_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payment_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCriteriaForOLevelSubjectAlternative",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdmissionCriteriaForOLevelSubjectId = table.Column<int>(nullable: false),
                    OLevelSubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCriteriaForOLevelSubjectAlternative", x => x.Id);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelSubjectAlternative_AdmissionCriteriaForOLevelSubject",
                        column: x => x.AdmissionCriteriaForOLevelSubjectId,
                        principalTable: "AdmissionCriteriaForOLevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_AdmissionCriteriaForOLevelSubjectAlternative_OlevelSubject",
                        column: x => x.OLevelSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EAssignment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseId = table.Column<int>(nullable: false),
                    Assignment = table.Column<string>(nullable: true),
                    URL = table.Column<string>(nullable: true),
                    Instructions = table.Column<string>(nullable: true),
                    DateSet = table.Column<DateTime>(nullable: false),
                    DueDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EAssignment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EAssignmenta_Course",
                        column: x => x.CourseId,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ECourse",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseId = table.Column<int>(nullable: false),
                    EContentTypeId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    views = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ECourse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ECourse_Course",
                        column: x => x.CourseId,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ECourse_EContentType",
                        column: x => x.EContentTypeId,
                        principalTable: "EContentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdmissionListBatchId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    DepartmentOptionId = table.Column<int>(nullable: true),
                    ApplicationFormId = table.Column<int>(nullable: true),
                    Activated = table.Column<bool>(nullable: false),
                    SessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdmissionList_AdmissionListBatch_AdmissionListBatchId",
                        column: x => x.AdmissionListBatchId,
                        principalTable: "AdmissionListBatch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionList_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionList_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionList_DepartmentOption_DepartmentOptionId",
                        column: x => x.DepartmentOptionId,
                        principalTable: "DepartmentOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionList_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionList_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdmissionList_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Applicant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExtraCurricularActivities = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    ApplicantStatusId = table.Column<int>(nullable: false),
                    ApplicationFormId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applicant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Applicant_ApplicantStatus_ApplicantStatusId",
                        column: x => x.ApplicantStatusId,
                        principalTable: "ApplicantStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applicant_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applicant_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantAppliedCourse",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    DepartmentOptionId = table.Column<int>(nullable: true),
                    ApplicationFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantAppliedCourse", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantAppliedCourse_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantAppliedCourse_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicantAppliedCourse_DepartmentOption_DepartmentOptionId",
                        column: x => x.DepartmentOptionId,
                        principalTable: "DepartmentOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantAppliedCourse_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicantAppliedCourse_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantClearance",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicationFormId = table.Column<int>(nullable: false),
                    Cleared = table.Column<bool>(nullable: false),
                    DateCleared = table.Column<DateTime>(nullable: false),
                    Rejected = table.Column<bool>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantClearance", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantClearance_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantJambDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    Jamb_Number = table.Column<string>(maxLength: 10, nullable: false),
                    Jamb_Score = table.Column<int>(nullable: true),
                    School_Choice = table.Column<int>(nullable: true),
                    FirstSubjectId = table.Column<int>(nullable: true),
                    FirstSubjectScore = table.Column<int>(nullable: true),
                    SecondSubjectId = table.Column<int>(nullable: true),
                    SecondSubjectScore = table.Column<int>(nullable: true),
                    ThirdSubjectId = table.Column<int>(nullable: true),
                    ThirdSubjectScore = table.Column<int>(nullable: true),
                    FourthSubjectId = table.Column<int>(nullable: true),
                    FourthSubjectScore = table.Column<int>(nullable: true),
                    ApplicationFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantJambDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_OlevelSubject_FirstSubjectId",
                        column: x => x.FirstSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_OlevelSubject_FourthSubjectId",
                        column: x => x.FourthSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_OlevelSubject_SecondSubjectId",
                        column: x => x.SecondSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicantJambDetail_OlevelSubject_ThirdSubjectId",
                        column: x => x.ThirdSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NextOfkin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    RelationShipId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false),
                    ApplicationFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NextOfkin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NextOfkin_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NextOfkin_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NextOfkin_RelationShip_RelationShipId",
                        column: x => x.RelationShipId,
                        principalTable: "RelationShip",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OlevelResult",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PersonId = table.Column<int>(nullable: false),
                    ExamNumber = table.Column<string>(nullable: false),
                    ExamYear = table.Column<int>(nullable: false),
                    ScannedResultUrl = table.Column<string>(nullable: true),
                    OlevelSittingId = table.Column<int>(nullable: false),
                    OLevelExamSittingId = table.Column<int>(nullable: true),
                    OlevelTypeId = table.Column<int>(nullable: false),
                    ApplicationFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OlevelResult", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OlevelResult_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OlevelResult_OLevelExamSitting_OLevelExamSittingId",
                        column: x => x.OLevelExamSittingId,
                        principalTable: "OLevelExamSitting",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OlevelResult_OlevelType_OlevelTypeId",
                        column: x => x.OlevelTypeId,
                        principalTable: "OlevelType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OlevelResult_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    PersonId = table.Column<int>(nullable: false),
                    ApplicationFormId = table.Column<int>(nullable: false),
                    StudentCategoryId = table.Column<int>(nullable: false),
                    StudentTypeId = table.Column<int>(nullable: false),
                    MatricNumber = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.PersonId);
                    table.ForeignKey(
                        name: "FK_Student_ApplicationForm_ApplicationFormId",
                        column: x => x.ApplicationFormId,
                        principalTable: "ApplicationForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Student_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Student_StudentCategory_StudentCategoryId",
                        column: x => x.StudentCategoryId,
                        principalTable: "StudentCategory",
                        principalColumn: "StudentCateoryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Student_StudentType_StudentTypeId",
                        column: x => x.StudentTypeId,
                        principalTable: "StudentType",
                        principalColumn: "StudentTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OnlinePayment",
                columns: table => new
                {
                    PaymentId = table.Column<int>(nullable: false),
                    PaymentChannelId = table.Column<int>(nullable: false),
                    TransactionNumber = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OnlinePayment", x => x.PaymentId);
                    table.ForeignKey(
                        name: "Fk_OnlinePayments_PaymentChannel",
                        column: x => x.PaymentChannelId,
                        principalTable: "PaymentChannel",
                        principalColumn: "PaymentChannnelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Fk_OnlinePayments_Payment",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OlevelResultDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    olevelResultId = table.Column<int>(nullable: false),
                    olevelGradeId = table.Column<int>(nullable: false),
                    OlevelSubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OlevelResultDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OlevelResultDetail_OlevelSubject_OlevelSubjectId",
                        column: x => x.OlevelSubjectId,
                        principalTable: "OlevelSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OlevelResultDetail_OlevelGrade_olevelGradeId",
                        column: x => x.olevelGradeId,
                        principalTable: "OlevelGrade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OlevelResultDetail_OlevelResult_olevelResultId",
                        column: x => x.olevelResultId,
                        principalTable: "OlevelResult",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EAssignmentSubmission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EAssignmentId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    AssignmentContent = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Score = table.Column<decimal>(nullable: true),
                    DateSubmitted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EAssignmentSubmission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EAssignmentSubmissions_EAssignment",
                        column: x => x.EAssignmentId,
                        principalTable: "EAssignment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EAssignmentSubmissions_Student",
                        column: x => x.PersonId,
                        principalTable: "Student",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourseRegistration",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    DateApproved = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourseRegistration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistration_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistration_Level_LevelId",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistration_Programme_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistration_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistration_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentLevel",
                columns: table => new
                {
                    StudentLevelId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    DepartmentOptionId = table.Column<int>(nullable: false),
                    DepartmentId1 = table.Column<int>(nullable: true),
                    DepartmentOptionId1 = table.Column<int>(nullable: true),
                    LevelId1 = table.Column<int>(nullable: true),
                    ProgrammeId1 = table.Column<int>(nullable: true),
                    SessionId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentLevel", x => x.StudentLevelId);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_Department",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentLevel_Department_DepartmentId1",
                        column: x => x.DepartmentId1,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_DepartmentOption",
                        column: x => x.DepartmentOptionId,
                        principalTable: "DepartmentOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentLevel_DepartmentOption_DepartmentOptionId1",
                        column: x => x.DepartmentOptionId1,
                        principalTable: "DepartmentOption",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_Level",
                        column: x => x.LevelId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentLevel_Level_LevelId1",
                        column: x => x.LevelId1,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_Programme",
                        column: x => x.ProgrammeId,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentLevel_Programme_ProgrammeId1",
                        column: x => x.ProgrammeId1,
                        principalTable: "Programme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_Session",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentLevel_Session_SessionId1",
                        column: x => x.SessionId1,
                        principalTable: "Session",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "Fk_StudentLevel_Student",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PaymentEtranzact",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PaymentId = table.Column<int>(nullable: false),
                    PaymenTerminalId = table.Column<int>(nullable: false),
                    PaymentEtranzactTypeId = table.Column<int>(nullable: false),
                    OnlinePaymentId = table.Column<int>(nullable: false),
                    ReceiptNo = table.Column<string>(nullable: true),
                    PaymentCode = table.Column<string>(nullable: true),
                    MerchantCode = table.Column<string>(nullable: true),
                    TransactionAmount = table.Column<decimal>(nullable: true),
                    TransactionDescription = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: true),
                    ConfirmationNo = table.Column<string>(nullable: false),
                    BankCode = table.Column<string>(nullable: true),
                    BranchCode = table.Column<string>(nullable: true),
                    CustomerID = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: false),
                    CustomerAddress = table.Column<string>(nullable: true),
                    Used = table.Column<bool>(nullable: false),
                    UsedBy = table.Column<long>(nullable: false),
                    PaymentEtranzactTypeId1 = table.Column<int>(nullable: true),
                    PaymentId1 = table.Column<int>(nullable: true),
                    PaymentTerminalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentEtranzact", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_OnlinePayment_OnlinePaymentId",
                        column: x => x.OnlinePaymentId,
                        principalTable: "OnlinePayment",
                        principalColumn: "PaymentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_PaymentTerminal_PaymenTerminalId",
                        column: x => x.PaymenTerminalId,
                        principalTable: "PaymentTerminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_PaymentEtranzactType_PaymentEtranzactTypeId",
                        column: x => x.PaymentEtranzactTypeId,
                        principalTable: "PaymentEtranzactType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_PaymentEtranzactType_PaymentEtranzactTypeId1",
                        column: x => x.PaymentEtranzactTypeId1,
                        principalTable: "PaymentEtranzactType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_Payment_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_Payment_PaymentId1",
                        column: x => x.PaymentId1,
                        principalTable: "Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentEtranzact_PaymentTerminal_PaymentTerminalId",
                        column: x => x.PaymentTerminalId,
                        principalTable: "PaymentTerminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourseRegistrationDetail",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentCourseRegistrationId = table.Column<long>(nullable: false),
                    CourseId = table.Column<long>(nullable: false),
                    CourseModeId = table.Column<int>(nullable: false),
                    CourseUnit = table.Column<int>(nullable: false),
                    SemesterId = table.Column<int>(nullable: false),
                    TestScore = table.Column<decimal>(nullable: true),
                    ExamScore = table.Column<decimal>(nullable: true),
                    SpecialCase = table.Column<string>(nullable: true),
                    DateUploaded = table.Column<DateTime>(nullable: true),
                    CourseId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourseRegistrationDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistrationDetail_Course_CourseId1",
                        column: x => x.CourseId1,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistrationDetail_CourseMode_CourseModeId",
                        column: x => x.CourseModeId,
                        principalTable: "CourseMode",
                        principalColumn: "CourseModeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistrationDetail_Semester_SemesterId",
                        column: x => x.SemesterId,
                        principalTable: "Semester",
                        principalColumn: "SemesterId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentCourseRegistrationDetail_StudentCourseRegistration_StudentCourseRegistrationId",
                        column: x => x.StudentCourseRegistrationId,
                        principalTable: "StudentCourseRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteria_DepartmentId",
                table: "AdmissionCriteria",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteria_ProgrammeId",
                table: "AdmissionCriteria",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelSubject_AdmissionCriteriaId",
                table: "AdmissionCriteriaForOLevelSubject",
                column: "AdmissionCriteriaId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelSubject_MinimumOLevelGradeId",
                table: "AdmissionCriteriaForOLevelSubject",
                column: "MinimumOLevelGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelSubject_OLevelSubjectId",
                table: "AdmissionCriteriaForOLevelSubject",
                column: "OLevelSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelSubjectAlternative_AdmissionCriteriaForOLevelSubjectId",
                table: "AdmissionCriteriaForOLevelSubjectAlternative",
                column: "AdmissionCriteriaForOLevelSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelSubjectAlternative_OLevelSubjectId",
                table: "AdmissionCriteriaForOLevelSubjectAlternative",
                column: "OLevelSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelType_AdmissionCriteriaId",
                table: "AdmissionCriteriaForOLevelType",
                column: "AdmissionCriteriaId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionCriteriaForOLevelType_OLevelTypeId",
                table: "AdmissionCriteriaForOLevelType",
                column: "OLevelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_AdmissionListBatchId",
                table: "AdmissionList",
                column: "AdmissionListBatchId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_ApplicationFormId",
                table: "AdmissionList",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_DepartmentId",
                table: "AdmissionList",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_DepartmentOptionId",
                table: "AdmissionList",
                column: "DepartmentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_PersonId",
                table: "AdmissionList",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_ProgrammeId",
                table: "AdmissionList",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionList_SessionId",
                table: "AdmissionList",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionListBatch_AdmissionListTypeId",
                table: "AdmissionListBatch",
                column: "AdmissionListTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_ApplicantStatusId",
                table: "Applicant",
                column: "ApplicantStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_ApplicationFormId",
                table: "Applicant",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_PersonId",
                table: "Applicant",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_ApplicationFormId",
                table: "ApplicantAppliedCourse",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_DepartmentId",
                table: "ApplicantAppliedCourse",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_DepartmentOptionId",
                table: "ApplicantAppliedCourse",
                column: "DepartmentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_PersonId",
                table: "ApplicantAppliedCourse",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_ProgrammeId",
                table: "ApplicantAppliedCourse",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantClearance_ApplicationFormId",
                table: "ApplicantClearance",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_ApplicationFormId",
                table: "ApplicantJambDetail",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_FirstSubjectId",
                table: "ApplicantJambDetail",
                column: "FirstSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_FourthSubjectId",
                table: "ApplicantJambDetail",
                column: "FourthSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_PersonId",
                table: "ApplicantJambDetail",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_SecondSubjectId",
                table: "ApplicantJambDetail",
                column: "SecondSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantJambDetail_ThirdSubjectId",
                table: "ApplicantJambDetail",
                column: "ThirdSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationForm_ApplicationFormSettingId",
                table: "ApplicationForm",
                column: "ApplicationFormSettingId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationForm_PersonId",
                table: "ApplicationForm",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationFormSetting_FeeTypeId",
                table: "ApplicationFormSetting",
                column: "FeeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationFormSetting_LevelId",
                table: "ApplicationFormSetting",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationFormSetting_SessionId",
                table: "ApplicationFormSetting",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_CourseTypeId",
                table: "Course",
                column: "CourseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_DepartmentId",
                table: "Course",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_DepartmentOptionId",
                table: "Course",
                column: "DepartmentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_LevelId",
                table: "Course",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_ProgrammeId",
                table: "Course",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_SemesterId",
                table: "Course",
                column: "SemesterId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_DepartmentId",
                table: "CourseUnit",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_LevelId",
                table: "CourseUnit",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_LevelId1",
                table: "CourseUnit",
                column: "LevelId1");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_ProgrammeId",
                table: "CourseUnit",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_SemesterId",
                table: "CourseUnit",
                column: "SemesterId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseUnit_SemesterId1",
                table: "CourseUnit",
                column: "SemesterId1");

            migrationBuilder.CreateIndex(
                name: "IX_Department_FacultyId",
                table: "Department",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentOption_DepartmentId",
                table: "DepartmentOption",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EAssignment_CourseId",
                table: "EAssignment",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_EAssignmentSubmission_EAssignmentId",
                table: "EAssignmentSubmission",
                column: "EAssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EAssignmentSubmission_PersonId",
                table: "EAssignmentSubmission",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_ECourse_CourseId",
                table: "ECourse",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_ECourse_EContentTypeId",
                table: "ECourse",
                column: "EContentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_DepartmentId",
                table: "FeeDetail",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_FeeId",
                table: "FeeDetail",
                column: "FeeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_FeeTypeId",
                table: "FeeDetail",
                column: "FeeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_LevelId",
                table: "FeeDetail",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_SessionId",
                table: "FeeDetail",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalGovernment_stateId",
                table: "LocalGovernment",
                column: "stateId");

            migrationBuilder.CreateIndex(
                name: "IX_NextOfkin_ApplicationFormId",
                table: "NextOfkin",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_NextOfkin_PersonId",
                table: "NextOfkin",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_NextOfkin_RelationShipId",
                table: "NextOfkin",
                column: "RelationShipId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResult_ApplicationFormId",
                table: "OlevelResult",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResult_OLevelExamSittingId",
                table: "OlevelResult",
                column: "OLevelExamSittingId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResult_OlevelTypeId",
                table: "OlevelResult",
                column: "OlevelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResult_PersonId",
                table: "OlevelResult",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResultDetail_OlevelSubjectId",
                table: "OlevelResultDetail",
                column: "OlevelSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResultDetail_olevelGradeId",
                table: "OlevelResultDetail",
                column: "olevelGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_OlevelResultDetail_olevelResultId",
                table: "OlevelResultDetail",
                column: "olevelResultId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlinePayment_PaymentChannelId",
                table: "OnlinePayment",
                column: "PaymentChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_FeeTypeId",
                table: "Payment",
                column: "FeeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_LevelId",
                table: "Payment",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_PaymentModeId",
                table: "Payment",
                column: "PaymentModeId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_PaymentTypeId",
                table: "Payment",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_PersonId",
                table: "Payment",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_SessionId",
                table: "Payment",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_OnlinePaymentId",
                table: "PaymentEtranzact",
                column: "OnlinePaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymenTerminalId",
                table: "PaymentEtranzact",
                column: "PaymenTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymentEtranzactTypeId",
                table: "PaymentEtranzact",
                column: "PaymentEtranzactTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymentEtranzactTypeId1",
                table: "PaymentEtranzact",
                column: "PaymentEtranzactTypeId1");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymentId",
                table: "PaymentEtranzact",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymentId1",
                table: "PaymentEtranzact",
                column: "PaymentId1");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzact_PaymentTerminalId",
                table: "PaymentEtranzact",
                column: "PaymentTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzactType_FeetypeId",
                table: "PaymentEtranzactType",
                column: "FeetypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzactType_PaymentModeId",
                table: "PaymentEtranzactType",
                column: "PaymentModeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzactType_ProgrammeId",
                table: "PaymentEtranzactType",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentEtranzactType_SessionId",
                table: "PaymentEtranzactType",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTerminal_FeetypeId",
                table: "PaymentTerminal",
                column: "FeetypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTerminal_SessionId",
                table: "PaymentTerminal",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_BloodGroupId",
                table: "Person",
                column: "BloodGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_CountryId",
                table: "Person",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_GenderId",
                table: "Person",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_GenotypeId",
                table: "Person",
                column: "GenotypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_LocalGovernmentId",
                table: "Person",
                column: "LocalGovernmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_ReligionId",
                table: "Person",
                column: "ReligionId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_StateId",
                table: "Person",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_ProgrammeDepartment_Department_Id",
                table: "ProgrammeDepartment",
                column: "Department_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProgrammeLevel_Level_Id",
                table: "ProgrammeLevel",
                column: "Level_Id");

            migrationBuilder.CreateIndex(
                name: "IX_SessionSemester_SemesterId",
                table: "SessionSemester",
                column: "SemesterId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionSemester_SessionId",
                table: "SessionSemester",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_State_CountryId",
                table: "State",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_ApplicationFormId",
                table: "Student",
                column: "ApplicationFormId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_StudentCategoryId",
                table: "Student",
                column: "StudentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_StudentTypeId",
                table: "Student",
                column: "StudentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistration_DepartmentId",
                table: "StudentCourseRegistration",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistration_LevelId",
                table: "StudentCourseRegistration",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistration_ProgrammeId",
                table: "StudentCourseRegistration",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistration_SessionId",
                table: "StudentCourseRegistration",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistration_StudentId",
                table: "StudentCourseRegistration",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistrationDetail_CourseId1",
                table: "StudentCourseRegistrationDetail",
                column: "CourseId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistrationDetail_CourseModeId",
                table: "StudentCourseRegistrationDetail",
                column: "CourseModeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistrationDetail_SemesterId",
                table: "StudentCourseRegistrationDetail",
                column: "SemesterId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourseRegistrationDetail_StudentCourseRegistrationId",
                table: "StudentCourseRegistrationDetail",
                column: "StudentCourseRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_DepartmentId",
                table: "StudentLevel",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_DepartmentId1",
                table: "StudentLevel",
                column: "DepartmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_DepartmentOptionId",
                table: "StudentLevel",
                column: "DepartmentOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_DepartmentOptionId1",
                table: "StudentLevel",
                column: "DepartmentOptionId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_LevelId",
                table: "StudentLevel",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_LevelId1",
                table: "StudentLevel",
                column: "LevelId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_ProgrammeId",
                table: "StudentLevel",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_ProgrammeId1",
                table: "StudentLevel",
                column: "ProgrammeId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_SessionId",
                table: "StudentLevel",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_SessionId1",
                table: "StudentLevel",
                column: "SessionId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentLevel_StudentId",
                table: "StudentLevel",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdmissionCriteriaForOLevelSubjectAlternative");

            migrationBuilder.DropTable(
                name: "AdmissionCriteriaForOLevelType");

            migrationBuilder.DropTable(
                name: "AdmissionList");

            migrationBuilder.DropTable(
                name: "Applicant");

            migrationBuilder.DropTable(
                name: "ApplicantAppliedCourse");

            migrationBuilder.DropTable(
                name: "ApplicantClearance");

            migrationBuilder.DropTable(
                name: "ApplicantJambDetail");

            migrationBuilder.DropTable(
                name: "CourseUnit");

            migrationBuilder.DropTable(
                name: "EAssignmentSubmission");

            migrationBuilder.DropTable(
                name: "ECourse");

            migrationBuilder.DropTable(
                name: "FeeDetail");

            migrationBuilder.DropTable(
                name: "NextOfkin");

            migrationBuilder.DropTable(
                name: "OlevelResultDetail");

            migrationBuilder.DropTable(
                name: "PaymentEtranzact");

            migrationBuilder.DropTable(
                name: "ProgrammeDepartment");

            migrationBuilder.DropTable(
                name: "ProgrammeLevel");

            migrationBuilder.DropTable(
                name: "ScoreGrade");

            migrationBuilder.DropTable(
                name: "SessionSemester");

            migrationBuilder.DropTable(
                name: "StudentCourseRegistrationDetail");

            migrationBuilder.DropTable(
                name: "StudentLevel");

            migrationBuilder.DropTable(
                name: "AdmissionCriteriaForOLevelSubject");

            migrationBuilder.DropTable(
                name: "AdmissionListBatch");

            migrationBuilder.DropTable(
                name: "ApplicantStatus");

            migrationBuilder.DropTable(
                name: "EAssignment");

            migrationBuilder.DropTable(
                name: "EContentType");

            migrationBuilder.DropTable(
                name: "Fee");

            migrationBuilder.DropTable(
                name: "RelationShip");

            migrationBuilder.DropTable(
                name: "OlevelResult");

            migrationBuilder.DropTable(
                name: "OnlinePayment");

            migrationBuilder.DropTable(
                name: "PaymentTerminal");

            migrationBuilder.DropTable(
                name: "PaymentEtranzactType");

            migrationBuilder.DropTable(
                name: "CourseMode");

            migrationBuilder.DropTable(
                name: "StudentCourseRegistration");

            migrationBuilder.DropTable(
                name: "AdmissionCriteria");

            migrationBuilder.DropTable(
                name: "OlevelGrade");

            migrationBuilder.DropTable(
                name: "OlevelSubject");

            migrationBuilder.DropTable(
                name: "AdmissionListType");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "OLevelExamSitting");

            migrationBuilder.DropTable(
                name: "OlevelType");

            migrationBuilder.DropTable(
                name: "PaymentChannel");

            migrationBuilder.DropTable(
                name: "Payment");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "CourseType");

            migrationBuilder.DropTable(
                name: "DepartmentOption");

            migrationBuilder.DropTable(
                name: "Programme");

            migrationBuilder.DropTable(
                name: "Semester");

            migrationBuilder.DropTable(
                name: "PaymentMode");

            migrationBuilder.DropTable(
                name: "PaymentType");

            migrationBuilder.DropTable(
                name: "ApplicationForm");

            migrationBuilder.DropTable(
                name: "StudentCategory");

            migrationBuilder.DropTable(
                name: "StudentType");

            migrationBuilder.DropTable(
                name: "Department");

            migrationBuilder.DropTable(
                name: "ApplicationFormSetting");

            migrationBuilder.DropTable(
                name: "Person");

            migrationBuilder.DropTable(
                name: "Faculty");

            migrationBuilder.DropTable(
                name: "FeeType");

            migrationBuilder.DropTable(
                name: "Level");

            migrationBuilder.DropTable(
                name: "Session");

            migrationBuilder.DropTable(
                name: "BloodGroup");

            migrationBuilder.DropTable(
                name: "Gender");

            migrationBuilder.DropTable(
                name: "Genotype");

            migrationBuilder.DropTable(
                name: "LocalGovernment");

            migrationBuilder.DropTable(
                name: "Religion");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "Country");
        }
    }
}
