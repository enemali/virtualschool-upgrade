﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualSchool.Persistence.Migrations
{
    public partial class modifyApplicationFormSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SemesterName",
                table: "Semester",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "SemesterId",
                table: "Semester",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "FeeName",
                table: "Fee",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "FeeDescription",
                table: "Fee",
                newName: "Description");

            migrationBuilder.AddColumn<bool>(
                name: "ApplicationActive",
                table: "Session",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Semester",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Semester",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Semester",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "Semester",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentModeId",
                table: "FeeDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProgrammeId",
                table: "FeeDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FormCode",
                table: "ApplicationFormSetting",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgrammeId",
                table: "ApplicationFormSetting",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "ApplicationFormSetting",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StopDate",
                table: "ApplicationFormSetting",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ApplicationFormSettingId",
                table: "ApplicantAppliedCourse",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Bank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BankBranch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankBranch", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralAudit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableName = table.Column<string>(nullable: true),
                    InitialValue = table.Column<string>(nullable: true),
                    CurrentValue = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    Operation = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Client = table.Column<string>(nullable: true),
                    Referer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralAudit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VW_StudentResult",
                columns: table => new
                {
                    SCRDId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MatricNumber = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Othername = table.Column<string>(nullable: true),
                    LevelName = table.Column<string>(nullable: true),
                    ProgrammeName = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    SemesterName = table.Column<string>(nullable: true),
                    SessionName = table.Column<string>(nullable: true),
                    SessionSemesterId = table.Column<int>(nullable: false),
                    CourseId = table.Column<long>(nullable: false),
                    CourseTypeId = table.Column<int>(nullable: false),
                    CourseCode = table.Column<string>(nullable: true),
                    CourseName = table.Column<string>(nullable: true),
                    Course_Unit = table.Column<int>(nullable: false),
                    TestScore = table.Column<decimal>(nullable: false),
                    ExamScore = table.Column<decimal>(nullable: false),
                    SpecialCase = table.Column<string>(nullable: true),
                    TotalScore = table.Column<decimal>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    SemesterId = table.Column<int>(nullable: false),
                    Grade = table.Column<string>(nullable: true),
                    GradePoint = table.Column<decimal>(nullable: false),
                    TotalSemesterCourseUnit = table.Column<int>(nullable: false),
                    CourseMode = table.Column<string>(nullable: true),
                    CourseModeId = table.Column<int>(nullable: false),
                    FacultyId = table.Column<int>(nullable: false),
                    FacultyName = table.Column<string>(nullable: true),
                    ContactAddress = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    LevelId = table.Column<int>(nullable: false),
                    WGP = table.Column<decimal>(nullable: false),
                    GPA = table.Column<decimal>(nullable: false),
                    CGPA = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_PaymentModeId",
                table: "FeeDetail",
                column: "PaymentModeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeDetail_ProgrammeId",
                table: "FeeDetail",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationFormSetting_ProgrammeId",
                table: "ApplicationFormSetting",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantAppliedCourse_ApplicationFormSettingId",
                table: "ApplicantAppliedCourse",
                column: "ApplicationFormSettingId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicantAppliedCourse_ApplicationFormSetting_ApplicationFormSettingId",
                table: "ApplicantAppliedCourse",
                column: "ApplicationFormSettingId",
                principalTable: "ApplicationFormSetting",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationFormSetting_Programme_ProgrammeId",
                table: "ApplicationFormSetting",
                column: "ProgrammeId",
                principalTable: "Programme",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_FeeDetail_PaymentMode_PaymentModeId",
                table: "FeeDetail",
                column: "PaymentModeId",
                principalTable: "PaymentMode",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FeeDetail_Programme_ProgrammeId",
                table: "FeeDetail",
                column: "ProgrammeId",
                principalTable: "Programme",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicantAppliedCourse_ApplicationFormSetting_ApplicationFormSettingId",
                table: "ApplicantAppliedCourse");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationFormSetting_Programme_ProgrammeId",
                table: "ApplicationFormSetting");

            migrationBuilder.DropForeignKey(
                name: "FK_FeeDetail_PaymentMode_PaymentModeId",
                table: "FeeDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_FeeDetail_Programme_ProgrammeId",
                table: "FeeDetail");

            migrationBuilder.DropTable(
                name: "Bank");

            migrationBuilder.DropTable(
                name: "BankBranch");

            migrationBuilder.DropTable(
                name: "GeneralAudit");

            migrationBuilder.DropTable(
                name: "VW_StudentResult");

            migrationBuilder.DropIndex(
                name: "IX_FeeDetail_PaymentModeId",
                table: "FeeDetail");

            migrationBuilder.DropIndex(
                name: "IX_FeeDetail_ProgrammeId",
                table: "FeeDetail");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationFormSetting_ProgrammeId",
                table: "ApplicationFormSetting");

            migrationBuilder.DropIndex(
                name: "IX_ApplicantAppliedCourse_ApplicationFormSettingId",
                table: "ApplicantAppliedCourse");

            migrationBuilder.DropColumn(
                name: "ApplicationActive",
                table: "Session");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Semester");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Semester");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Semester");

            migrationBuilder.DropColumn(
                name: "Slug",
                table: "Semester");

            migrationBuilder.DropColumn(
                name: "PaymentModeId",
                table: "FeeDetail");

            migrationBuilder.DropColumn(
                name: "ProgrammeId",
                table: "FeeDetail");

            migrationBuilder.DropColumn(
                name: "FormCode",
                table: "ApplicationFormSetting");

            migrationBuilder.DropColumn(
                name: "ProgrammeId",
                table: "ApplicationFormSetting");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "ApplicationFormSetting");

            migrationBuilder.DropColumn(
                name: "StopDate",
                table: "ApplicationFormSetting");

            migrationBuilder.DropColumn(
                name: "ApplicationFormSettingId",
                table: "ApplicantAppliedCourse");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Semester",
                newName: "SemesterName");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Semester",
                newName: "SemesterId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Fee",
                newName: "FeeName");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Fee",
                newName: "FeeDescription");
        }
    }
}
