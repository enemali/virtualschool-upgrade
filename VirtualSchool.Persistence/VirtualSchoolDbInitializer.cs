﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Persistence
{
    public class VirtualSchoolDbInitializer
    {
        private readonly Dictionary<int, Programme> Programmes = new Dictionary<int, Programme>();
        private readonly Dictionary<int, Faculty> Faculties = new Dictionary<int, Faculty>();
        private readonly Dictionary<int, Department> Departments = new Dictionary<int, Department>();
        private readonly Dictionary<int, Country> Countries = new Dictionary<int, Country>();
        private readonly Dictionary<int, State> States = new Dictionary<int, State>();
        private readonly Dictionary<int, LocalGovernment> LocalGovernments = new Dictionary<int, LocalGovernment>();
        private readonly Dictionary<int, PaymentMode> PaymentModes = new Dictionary<int, PaymentMode>();
        private readonly Dictionary<int, PaymentType> PaymentTypes = new Dictionary<int, PaymentType>();
        private readonly Dictionary<int, Religion> Religions = new Dictionary<int, Religion>();
        private readonly Dictionary<int, Session> Sessions = new Dictionary<int, Session>();
        private readonly Dictionary<int, FeeType> FeeTypes = new Dictionary<int, FeeType>();
        private readonly Dictionary<int, BloodGroup> BloodGroups = new Dictionary<int, BloodGroup>();
        private readonly Dictionary<int, Gender> Genders = new Dictionary<int, Gender>();

        public static void Initialize(VirtualSchoolDbContext context)
        {
            var initializer = new VirtualSchoolDbInitializer();
            initializer.SeeEverything(context);
        }

        public void SeeEverything(VirtualSchoolDbContext context)
        {
            if (!context.Programme.Any())
            {
                SeedProgrammes(context);
            }
            if (!context.Faculty.Any())
            {
                SeedFaculties(context);
            }
            if (!context.Department.Any())
            {
                SeedDepartments(context);
            }
            if (!context.Country.Any())
            {
                SeedCountry(context);
            }
            if (!context.State.Any())
            {
                SeedState(context);
            }
            if (!context.PaymentMode.Any())
            {
                SeedPaymentModes(context);
            }
            if (!context.PaymentType.Any())
            {
                SeedPaymentTypes(context);
            }
            if (!context.Religion.Any())
            {
                SeedReligions(context);
            }
            if (!context.Session.Any())
            {
                SeedSessions(context);
            }
            if (!context.FeeType.Any())
            {
                SeedFeeTypes(context);
            }
            if (!context.BloodGroup.Any())
            {
                SeedBloodGroups(context);
            }
            if (!context.Level.Any())
            {
                SeedLevels(context);
            }
            if (!context.LocalGovernment.Any())
            {
                SeedLocalGoverments(context);
            }
            if (!context.Gender.Any())
            {
                SeedGenders(context);
            }
            if (!context.OlevelType.Any())
            {
                SeedOlevelTypes(context);
            }
            if (!context.OlevelSubject.Any())
            {
                SeedOlevelSubjects(context);
            }
            if (!context.OlevelGrade.Any())
            {
                SeedOlevelGrades(context);
            }
            if (!context.RelationShip.Any())
            {
                SeedRealtionShips(context);
            }
            
        }

        public void SeedLevels(VirtualSchoolDbContext context)
        {
            var levels = new[]
            {
                new Level(){Name ="First Year" , Code ="100", Description ="Year One" , Slug = "first-year", Active = true},
                new Level(){Name ="Second Year" , Code ="200", Description ="Year Two" , Slug = "second-year", Active = true},
                new Level(){Name ="Third Year" , Code ="300", Description ="Year Three" , Slug = "third-year", Active = true},
                new Level(){Name ="Fourth Year" , Code ="400", Description ="Year Four" , Slug = "fourth-year", Active = true},
                new Level(){Name ="Fifth Year" , Code ="500", Description ="Year Five" , Slug = "fifth-year", Active = true},
                new Level(){Name ="Sixth Year" , Code ="600", Description ="Year Six" , Slug = "Sixth-year", Active = true},
                new Level(){Name ="Applicant" , Code ="99", Description ="Applicant" , Slug = "applicant", Active = true}
            };

            context.Level.AddRange(levels);
            context.SaveChanges();
        }

        public void SeedProgrammes(VirtualSchoolDbContext context)
        {
            var programmes = new[]
            {
                new Programme(){Name ="Undergraduate" , Code ="UND", Description ="Undegraduate Programme Jamb" , Slug = "undergraduate", Active = true},
                new Programme(){Name ="Remedial Studies" , Code ="RMD", Description ="Remedial Programme Pre Jamb" , Slug = "remedial-studies", Active = true}
            };

            context.Programme.AddRange(programmes);
            context.SaveChanges();
        }

        public void SeedGenders(VirtualSchoolDbContext context)
        {
            var genders = new[]
            {
                new Gender(){Name ="Male" , Code ="M", Description ="Male" , Slug = "male", Active = true},
                new Gender(){Name ="Female" , Code ="F", Description ="Female" , Slug = "famale", Active = true}
            };

            context.Gender.AddRange(genders);
            context.SaveChanges();
        }

        public void SeedOlevelTypes(VirtualSchoolDbContext context)
        {
            var types = new[]
            {
                new OlevelType(){Name ="Waec" , Code ="WASC", Description ="waec" , Slug = "waec", Active = true},
                new OlevelType(){Name ="Neco" , Code ="NECO", Description ="neco" , Slug = "neco", Active = true}
            };

            context.OlevelType.AddRange(types);
            context.SaveChanges();
        }

        public void SeedOlevelSubjects(VirtualSchoolDbContext context)
        {
            var types = new[]
            {
                new OlevelSubject(){Name ="English" , Code ="ENG", Description ="-" , Slug = "english", Active = true},
                new OlevelSubject(){Name ="Physics" , Code ="PHY", Description ="-" , Slug = "Physics", Active = true},
                new OlevelSubject(){Name ="Chemistry" , Code ="CHM", Description ="-" , Slug = "Chemistry", Active = true},
                new OlevelSubject(){Name ="Biology" , Code ="BIO", Description ="-" , Slug = "Biology", Active = true},
                new OlevelSubject(){Name ="Geography" , Code ="GEO", Description ="-" , Slug = "Geography", Active = true},
                new OlevelSubject(){Name ="Agricultural Science" , Code ="AGR", Description ="-" , Slug = "agricultural-science", Active = true},
                new OlevelSubject(){Name ="Home Economics" , Code ="HEC", Description ="-" , Slug = "home-economics", Active = true},
                new OlevelSubject(){Name ="Health Science" , Code ="HSC", Description ="-" , Slug = "health-science", Active = true},
                new OlevelSubject(){Name ="Mathematics" , Code ="MATH", Description ="-" , Slug = "maths", Active = true}
            };

            context.OlevelSubject.AddRange(types);
            context.SaveChanges();
        }

        public void SeedOlevelGrades(VirtualSchoolDbContext context)
        {
            var types = new[]
            {
                new OlevelGrade(){Name ="A1" , Code ="A1", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="B2" , Code ="B2", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="B3" , Code ="B3", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="C4" , Code ="C4", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="C5" , Code ="C5", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="C6" , Code ="C6", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="D7" , Code ="D7", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="E8" , Code ="E8", Description ="-" , Slug = "-", Active = true},
                new OlevelGrade(){Name ="F9" , Code ="F9", Description ="-" , Slug = "-", Active = true}
            };

            context.OlevelGrade.AddRange(types);
            context.SaveChanges();
        }

        public void SeedRealtionShips(VirtualSchoolDbContext context)
        {
            var types = new[]
            {
                new RelationShip(){Name ="Father" , Code ="F", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Mother" , Code ="m", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Brother" , Code ="b", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Sister" , Code ="s", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Husband" , Code ="h", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Wife" , Code ="w", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Uncle" , Code ="u", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Aunt" , Code ="a", Description ="-" , Slug = "-", Active = true},
                new RelationShip(){Name ="Other" , Code ="o", Description ="-" , Slug = "-", Active = true}
            };

            context.RelationShip.AddRange(types);
            context.SaveChanges();
        }
  
        public void SeedFaculties(VirtualSchoolDbContext context)
        {
            var faculties = new[]
            {
                new Faculty(){Name ="Business Administration" , Code ="BAM", Description ="Business Administration" , Slug = "business-administration", Active = true},
                new Faculty(){Name ="Engineering" , Code ="ENG", Description ="Engineering" , Slug = "engineering", Active = true}
            };

            context.Faculty.AddRange(faculties);
            context.SaveChanges();
        }

        public void SeedDepartments(VirtualSchoolDbContext context)
        {
            var department = new[]
            {
                new Department(){Name ="Accountancy" , Code ="ACC", Description ="Accountancy" , Slug = "accountancy", FacultyId = 1, Active = true}
            };

            context.Department.AddRange(department);
            context.SaveChanges();
        }
        public void SeedLocalGoverments(VirtualSchoolDbContext context)
        {
            var localGovernments = new[]
            {
                new LocalGovernment(){Name ="Aba" , Code ="ACC", Description ="Accountancy" , Slug = "accountancy", stateId = 1, Active = true}
            };

            context.LocalGovernment.AddRange(localGovernments);
            context.SaveChanges();
        }

        public void SeedCountry(VirtualSchoolDbContext context)
        {
            var country = new[]
            {
                new Country(){Name ="Nigeria" , Code ="NG", Description ="Nigeria" , Slug = "nigeria", Active = true}
            };

            context.Country.AddRange(country);
            context.SaveChanges();
        }

        public void SeedState(VirtualSchoolDbContext context)
        {
            var state = new[]
            {
                new State(){Name ="Abia" , Code ="AB", Description ="Abia" , Slug = "abia", Active = true}
            };

            context.State.AddRange(state);
            context.SaveChanges();
        }

        public void SeedPaymentModes(VirtualSchoolDbContext context)
        {
            var paymentModes = new[]
            {
                new PaymentMode(){Name ="Full Instalment" , Code ="F", Description ="Full Instalment" , Slug = "full-instalment", Active = true},
                new PaymentMode(){Name ="First Instalment" , Code ="1P", Description ="First Instalment" , Slug = "first-instalment", Active = true},
                new PaymentMode(){Name ="Second Instalment" , Code ="2P", Description ="Second Instalment" , Slug = "second-instalment", Active = true}
            };

            context.PaymentMode.AddRange(paymentModes);
            context.SaveChanges();
        }

        public void SeedPaymentTypes(VirtualSchoolDbContext context)
        {
            var paymentTypes = new[]
            {
                new PaymentType(){Name ="Bank" , Code ="BNK", Description ="Bank" , Slug = "bank", Active = true},
                new PaymentType(){Name ="Card" , Code ="CRD", Description ="Card" , Slug = "card", Active = true},
                new PaymentType(){Name ="Mobile" , Code ="MOB", Description ="Mobile" , Slug = "mobile", Active = true}
            };

            context.PaymentType.AddRange(paymentTypes);
            context.SaveChanges();
        }

        public void SeedReligions(VirtualSchoolDbContext context)
        {
            var religions = new[]
            {
                new Religion(){Name ="Christian" , Code ="CHR", Description ="Christian" , Slug = "christian", Active = true},
                new Religion(){Name ="Muslim" , Code ="MUS", Description ="Muslim" , Slug = "muslim", Active = true},
                new Religion(){Name ="Other" , Code ="OTH", Description ="Other" , Slug = "other", Active = true}
            };

            context.Religion.AddRange(religions);
            context.SaveChanges();
        }

        public void SeedSessions(VirtualSchoolDbContext context)
        {
            var sessions = new[]
            {
                new Session(){Name ="2018-2019" , Code ="1819", Description ="2018-2019" , Slug = "2018-2019", Active = true}
            };

            context.Session.AddRange(sessions);
            context.SaveChanges();
        }

        public void SeedFeeTypes(VirtualSchoolDbContext context)
        {
            var feeTypes = new[]
            {
                new FeeType(){Name ="Application Form" , Code ="APPL", Description ="Application Form" , Slug = "application-form", Active = true},
                new FeeType(){Name ="Acceptance Fee" , Code ="ACPT", Description ="Acceptance Fee" , Slug = "acceptance-fee", Active = true},
                new FeeType(){Name ="School Fee" , Code ="SCHF", Description ="School Fee" , Slug = "school-fee", Active = true}
            };

            context.FeeType.AddRange(feeTypes);
            context.SaveChanges();
        }

        public void SeedBloodGroups(VirtualSchoolDbContext context)
        {
            var bloodgroups = new[]
            {
                new BloodGroup(){Name ="O" , Code ="O", Description ="O" , Slug = "o", Active = true},
                new BloodGroup(){Name ="A" , Code ="A", Description ="A" , Slug = "a", Active = true},
                new BloodGroup(){Name ="B" , Code ="B", Description ="B" , Slug = "b", Active = true},
                new BloodGroup(){Name ="AB" , Code ="AB", Description ="AB" , Slug = "ab", Active = true},
            };

            context.BloodGroup.AddRange(bloodgroups);
            context.SaveChanges();
        }


    }
}
