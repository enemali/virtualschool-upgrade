﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Persistence.Infrastructure;

namespace VirtualSchool.Persistence
{
    public class VirtualSchoolDbContextFactory : DesignTimeDbContextFactoryBase<VirtualSchoolDbContext>
    {
        protected override VirtualSchoolDbContext CreateNewInstance(DbContextOptions<VirtualSchoolDbContext> options)
        {
            return new VirtualSchoolDbContext(options);
        }
    }
}
