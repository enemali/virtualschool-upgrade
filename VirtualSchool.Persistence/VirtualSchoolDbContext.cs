﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence.Configuration;
using VirtualSchool.Persistence.Extensions;

namespace VirtualSchool.Persistence
{
    public class VirtualSchoolDbContext: DbContext
    {
        public VirtualSchoolDbContext(DbContextOptions<VirtualSchoolDbContext> options)
            : base(options)
        {
           
        }

        public DbSet<ApplicantAppliedCourse> ApplicantAppliedCourse { get; set; }
        public DbSet<ApplicantJambDetail> ApplicantJambDetail { get; set; }
        public DbSet<ApplicationForm> ApplicationForm { get; set; }
        public DbSet<BloodGroup> BloodGroup { get; set; }
        public DbSet<Bank> Bank { get; set; }
        public DbSet<BankBranch> BankBranch { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<DepartmentOption> DepartmentOption { get; set; }
        public DbSet<Faculty> Faculty { get; set; }
        public DbSet<FeeType> FeeType { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<GeneralAudit> GeneralAudit { get; set; }
        public DbSet<Genotype> Genotype { get; set; }
        public DbSet<Level> Level { get; set; }
        public DbSet<LocalGovernment> LocalGovernment { get; set; }
        public DbSet<NextOfkin> NextOfkin { get; set; }
        public DbSet<OlevelGrade> OlevelGrade { get; set; }
        public DbSet<OlevelResult> OlevelResult { get; set; }
        public DbSet<OlevelResultDetail> OlevelResultDetail { get; set; }
        public DbSet<OlevelSubject> OlevelSubject { get; set; }
        public DbSet<OlevelType> OlevelType { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<PaymentMode> PaymentMode { get; set; }
        public DbSet<PaymentType> PaymentType { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<Programme> Programme { get; set; }
        public DbSet<ProgrammeDepartment> ProgrammeDepartment { get; set; }
        public DbSet<ProgrammeLevel> ProgrammeLevel { get; set; }
        public DbSet<RelationShip> RelationShip { get; set; }
        public DbSet<Religion> Religion { get; set; }
        public DbSet<Semester> Semester { get; set; }
        public DbSet<Session> Session { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<AdmissionList> AdmissionList { get; set; }
        public DbSet<ApplicationFormSetting> ApplicationFormSetting { get; set; }

        public DbSet<PaymentEtranzact> PaymentEtranzact { get; set; }
        public DbSet<FeeDetail> FeeDetail { get; set; }
        public DbSet<Fee> Fee { get; set; }
        public DbSet<Applicant> Applicant { get; set; }
        public DbSet<PaymentEtranzactType> PaymentEtranzactType { get; set; }
        public DbSet<PaymentTerminal> PaymentTerminal { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<StudentCourseRegistration> StudentCourseRegistration { get; set; }
        public DbSet<StudentLevel> StudentLevel { get; set; }


        public DbSet<AdmissionCriteria> AdmissionCriteria { get; set; }
        public DbSet<AdmissionCriteriaForOLevelSubject> AdmissionCriteriaForOLevelSubject { get; set; }
        public DbSet<AdmissionCriteriaForOLevelSubjectAlternative> AdmissionCriteriaForOLevelSubjectAlternative { get; set; }
        public DbSet<AdmissionCriteriaForOLevelType> AdmissionCriteriaForOLevelType { get; set; }
        public DbSet<AdmissionListBatch> AdmissionListBatch { get; set; }
        public DbSet<AdmissionListType> AdmissionListType { get; set; }
        public DbSet<ApplicantClearance> ApplicantClearance { get; set; }
        public DbSet<ApplicantStatus> ApplicantStatus { get; set; }
        public DbSet<OLevelExamSitting> OLevelExamSitting { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<CourseType> CourseType { get; set; }
        public DbSet<CourseMode> CourseMode { get; set; }
        public DbSet<StudentCourseRegistrationDetail> StudentCourseRegistrationDetail { get; set; }
        public DbQuery<StudentResultView> VW_StudentResult { get; set; }
        public DbSet<ScoreGrade> ScoreGrade { get; set; }
        public DbSet<CourseUnit> CourseUnit { get; set; }

        public DbSet<EContentType> EContentType { get; set; }
        public DbSet<ECourse> ECourse { get; set; }
        public DbSet<EAssignment> EAssignment { get; set; }
        public DbSet<EAssignmentSubmission> EAssignmentSubmission { get; set; }

        public DbSet<OnlinePayment> OnlinePayment { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();
           
        }
    }
}
