﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using VirtualSchool.Persistence;

namespace DataMigrationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();
            var connection = @"Server=.;Database=VirtualSchool;Trusted_Connection=True;MultipleActiveResultSets=True";
            services.AddDbContext<VirtualSchoolDbContext>(options => options.UseSqlServer(connection));
            var serviceProvider = services.BuildServiceProvider();
            var virtualSchoolDbContext = serviceProvider.GetService<VirtualSchoolDbContext>();
            virtualSchoolDbContext.Database.EnsureDeleted();
            virtualSchoolDbContext.Database.EnsureCreated();
            //virtualSchoolDbContext.Database.Migrate();
            MigrateBaseData baseData = new MigrateBaseData(virtualSchoolDbContext);
            baseData.MigrateData();

            Console.WriteLine("Data Retrieved!!");
           
        }
    }
}
