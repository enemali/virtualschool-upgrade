﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace DataMigrationApp
{
    public class MigrateBaseData
    {
        private readonly VirtualSchoolDbContext _context;

        DataTable _responseData;
        SqlCommand _sqlCommand;
        SqlConnection _sqlConnectionOld;

        public string _oldConnectionString;
        public MigrateBaseData(VirtualSchoolDbContext context)
        {
            _context = context;
            _oldConnectionString = "data source=.; database=VS_ABSU; Integrated Security=true";
            _sqlConnectionOld = new SqlConnection(_oldConnectionString);

        }

        private SqlCommand GenerateCommand(string sqlQuery)
        {
            _sqlCommand = new SqlCommand(sqlQuery, _sqlConnectionOld);
            _sqlCommand.CommandType = CommandType.Text;
            return _sqlCommand;
        }

        private DataTable ExecuteQuery(string sqlQuery)
        {
            _responseData = new DataTable();
            _sqlConnectionOld.Open();
            _responseData.Load(GenerateCommand(sqlQuery).ExecuteReader());
            _sqlConnectionOld.Close();
            return _responseData;

        }

        public void MigrateData()
        {
            try
            {

                var Programmes = ExecuteQuery("SELECT * FROM PROGRAMME");
                if (Programmes != null && Programmes.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Programmes.Rows)
                    {
                        var programme = _context.Programme.Where(f => f.Name == dataRow["Programme_Name"].ToString()).FirstOrDefault();
                        if (programme == null)
                        {
                            programme = new VirtualSchool.Domain.Entities.Programme
                            {
                                Name = dataRow["Programme_Name"].ToString(),
                                Description = dataRow["Programme_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Programme_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Programme_Short_Name"].ToString(),
                            };
                        }

                        _context.Add(programme);

                    }

                    _context.SaveChanges();
                }


                var FacultyDepartment = ExecuteQuery("SELECT * FROM FACULTY, DEPARTMENT WHERE DEPARTMENT.Faculty_Id = FACULTY.Faculty_Id");
                if (FacultyDepartment != null && FacultyDepartment.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in FacultyDepartment.Rows)
                    {
                        var faculty = _context.Faculty.Where(f => f.Name == dataRow["Faculty_Name"].ToString()).FirstOrDefault();
                        if (faculty == null)
                        {
                            faculty = new VirtualSchool.Domain.Entities.Faculty
                            {
                                Name = dataRow["Faculty_Name"].ToString(),
                                Description = dataRow["Faculty_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Faculty_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Faculty_Description"].ToString()
                            };
                        }

                        var department = _context.Department.Where(f => f.Name == dataRow["Department_Name"].ToString()).FirstOrDefault();
                        if (department == null)
                        {
                            department = new VirtualSchool.Domain.Entities.Department
                            {
                                Name = dataRow["Department_Name"].ToString(),
                                Description = dataRow["Department_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Department_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Department_Code"].ToString(),
                                Faculty = faculty
                            };
                        }

                        _context.Add(department);

                    }

                    _context.SaveChanges();
                }


                var DepartmentOptions = ExecuteQuery("SELECT  Department_Option_Name,Department_Name,Department_Code FROM DEPARTMENT_OPTION, DEPARTMENT WHERE DEPARTMENT.Department_Id = DEPARTMENT_OPTION.Department_Id");
                if (DepartmentOptions != null && DepartmentOptions.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in DepartmentOptions.Rows)
                    {

                        var department = _context.DepartmentOption.Where(f => f.Name == dataRow["Department_Name"].ToString()).FirstOrDefault();
                        if (department != null)
                        {
                            var option = _context.DepartmentOption.Where(f => f.Name == dataRow["Department_Option_Name"].ToString()).FirstOrDefault();
                            if (option == null)
                            {
                                option = new VirtualSchool.Domain.Entities.DepartmentOption
                                {
                                    Name = dataRow["Department_Option_Name"].ToString(),
                                    Description = dataRow["Department_Option_Name"].ToString(),
                                    Slug = Slug.GenerateSlug(dataRow["Department_Option_Name"].ToString()),
                                    Active = true,
                                    Code = dataRow["Department_Code"].ToString(),
                                    DepartmentId = department.Id
                                };
                            }

                            _context.Add(option);
                        }




                    }

                    _context.SaveChanges();
                }


                var ProgrammeDepartment = ExecuteQuery("SELECT * FROM VW_PROGRAMME_DEPARTMENT");
                if (ProgrammeDepartment != null && ProgrammeDepartment.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in ProgrammeDepartment.Rows)
                    {

                        var programme = _context.Programme.Where(f => f.Name == dataRow["Programme_Name"].ToString()).FirstOrDefault();
                        if (programme != null)
                        {
                            var department = _context.Department.Where(f => f.Name == dataRow["Department_Name"].ToString()).FirstOrDefault();
                            if (department != null)
                            {
                                var option = _context.ProgrammeDepartment.Where(f => f.Department.Name == dataRow["Department_Name"].ToString() && f.Programme.Name == dataRow["Programme_Name"].ToString()).FirstOrDefault();
                                if (option == null)
                                {
                                    option = new VirtualSchool.Domain.Entities.ProgrammeDepartment
                                    {
                                        ProgrammeId = programme.Id,
                                        DepartmentId = department.Id,
                                        Active = Convert.ToBoolean(dataRow["Activate"].ToString())
                                    };

                                    _context.Add(option);
                                    _context.SaveChanges();
                                }

                            }
                        }




                    }

                }


                var Levels = ExecuteQuery("SELECT * FROM LEVEL");
                if (Levels != null && Levels.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Levels.Rows)
                    {
                        var rowData = _context.Level.Where(f => f.Name == dataRow["Level_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Level
                            {
                                Name = dataRow["Level_Name"].ToString(),
                                Description = dataRow["Level_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Level_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Level_Name"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }


                var Sessions = ExecuteQuery("SELECT * FROM SESSION");
                if (Sessions != null && Sessions.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Sessions.Rows)
                    {
                        var rowData = _context.Session.Where(f => f.Name == dataRow["Session_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Session
                            {
                                Name = dataRow["Session_Name"].ToString(),
                                Description = dataRow["Session_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Session_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Session_Name"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var Semester = ExecuteQuery("SELECT * FROM SEMESTER");
                if (Semester != null && Semester.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Semester.Rows)
                    {
                        var rowData = _context.Semester.Where(f => f.Name == dataRow["Semester_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Semester
                            {
                                Name = dataRow["Semester_Name"].ToString(),
                                Description = dataRow["Semester_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Semester_Name"].ToString()),
                                Active = true,
                                Code = dataRow["Semester_Name"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var CourseType = ExecuteQuery("SELECT * FROM COURSE_TYPE");
                if (CourseType != null && CourseType.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in CourseType.Rows)
                    {
                        var rowData = _context.CourseType.Where(f => f.Name == dataRow["Course_Type_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.CourseType
                            {
                                Name = dataRow["Course_Type_Name"].ToString(),
                                Description = dataRow["Course_Type_Name"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var Bank = ExecuteQuery("SELECT * FROM BANK");
                if (Bank != null && Bank.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Bank.Rows)
                    {
                        var rowData = _context.Bank.Where(f => f.Name == dataRow["Bank_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Bank
                            {
                                Name = dataRow["Bank_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Bank_Name"].ToString()),
                                Code = dataRow["Bank_Code"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }


                var BloodGroup = ExecuteQuery("SELECT * FROM BLOOD_GROUP");
                if (BloodGroup != null && BloodGroup.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in BloodGroup.Rows)
                    {
                        var rowData = _context.BloodGroup.Where(f => f.Name == dataRow["Blood_Group_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.BloodGroup
                            {
                                Name = dataRow["Blood_Group_Name"].ToString(),
                                Description = dataRow["Blood_Group_Name"].ToString(),
                                Code = dataRow["Blood_Group_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Blood_Group_Name"].ToString()),
                                Active = true
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var BankBranch = ExecuteQuery("SELECT * FROM BANK_BRANCH");
                if (BankBranch != null && BankBranch.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in BankBranch.Rows)
                    {
                        var rowData = _context.BankBranch.Where(f => f.Name == dataRow["Branch_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.BankBranch
                            {
                                Name = dataRow["Branch_Name"].ToString(),
                                Code = dataRow["Branch_Code"].ToString(),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var Fee = ExecuteQuery("SELECT * FROM FEE");
                if (Fee != null && Fee.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Fee.Rows)
                    {
                        var rowData = _context.Fee.Where(f => f.Name == dataRow["Fee_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Fee
                            {
                                Name = dataRow["Fee_Name"].ToString(),
                                Amount = Convert.ToDecimal(dataRow["Amount"].ToString()),
                                Description = dataRow["Fee_Description"].ToString(),

                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var FeeType = ExecuteQuery("SELECT * FROM FEE_TYPE");
                if (FeeType != null && FeeType.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in FeeType.Rows)
                    {
                        var rowData = _context.FeeType.Where(f => f.Name == dataRow["Fee_Type_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.FeeType
                            {
                                Name = dataRow["Fee_Type_Name"].ToString(),
                                Code = dataRow["Fee_Type_Name"].ToString(),
                                Description = dataRow["Fee_Type_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Fee_Type_Name"].ToString()),
                                Active = true
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                //var FeeDetail = ExecuteQuery("SELECT  dbo.FEE_DETAIL.*, dbo.DEPARTMENT.Department_Name, dbo.FEE.Fee_Name, dbo.[LEVEL].Level_Name, dbo.PROGRAMME.Programme_Name, dbo.PAYMENT_MODE.Payment_Mode_Name,dbo.SESSION.Session_Name, dbo.FEE_TYPE.Fee_Type_Name FROM  dbo.DEPARTMENT INNER JOIN dbo.FEE_DETAIL ON dbo.DEPARTMENT.Department_Id = dbo.FEE_DETAIL.Department_Id INNER JOIN dbo.FEE ON dbo.FEE_DETAIL.Fee_Id = dbo.FEE.Fee_Id INNER JOIN dbo.[LEVEL] ON dbo.FEE_DETAIL.Level_Id = dbo.[LEVEL].Level_Id INNER JOIN dbo.PROGRAMME ON dbo.FEE_DETAIL.Programme_Id = dbo.PROGRAMME.Programme_Id INNER JOIN dbo.PAYMENT_MODE ON dbo.FEE_DETAIL.Payment_Mode_Id = dbo.PAYMENT_MODE.Payment_Mode_Id INNER JOIN dbo.SESSION ON dbo.FEE_DETAIL.Session_Id = dbo.SESSION.Session_Id  INNER JOIN dbo.FEE_TYPE ON dbo.FEE_DETAIL.Fee_Type_Id = dbo.FEE_TYPE.Fee_Type_Id");
                //if (FeeDetail != null && FeeDetail.Rows.Count > 0)
                //{
                //    foreach (DataRow dataRow in FeeDetail.Rows)
                //    {
                //        var programme = _context.Programme.Where(s => s.Name == dataRow["Programme_Name"].ToString()).FirstOrDefault();
                //        if (programme != null)
                //        {
                //            var department = _context.Department.Where(s => s.Name == dataRow["Department_Name"].ToString()).FirstOrDefault();
                //            if (department != null)
                //            {
                //                var level = _context.Level.Where(s => s.Name == dataRow["Level_Name"].ToString()).FirstOrDefault();
                //                if (level != null)
                //                {
                //                    var paymentMode = _context.PaymentMode.Where(s => s.Name == dataRow["Payment_Mode_Name"].ToString()).FirstOrDefault();
                //                    if (paymentMode != null)
                //                    {
                //                        var fee = _context.Fee.Where(s => s.Name == dataRow["Fee_Name"].ToString()).FirstOrDefault();
                //                        if (fee != null)
                //                        {
                //                            var feeType = _context.FeeType.Where(s => s.Name == dataRow["Fee_Type_Name"].ToString()).FirstOrDefault();
                //                            if (fee != null)
                //                            {
                //                                var session = _context.Session.Where(s => s.Name == dataRow["Session_Name"].ToString()).FirstOrDefault();
                //                                if (session != null)
                //                                {
                //                                    var rowData = _context.FeeDetail.Where(f => f.DepartmentId == Convert.ToInt32(dataRow["Department_Id"].ToString())
                //                                     && f.ProgrammeId == Convert.ToInt32(dataRow["Programme_Id"].ToString()) && f.LevelId == Convert.ToInt32(dataRow["Level_Id"].ToString())
                //                                     && f.SessionId == Convert.ToInt32(dataRow["Session_Id"].ToString()) && f.FeeId == Convert.ToInt32(dataRow["Fee_Id"].ToString()) && f.FeeTypeId == Convert.ToInt32(dataRow["Fee_Type_Id"].ToString())
                //                                     && f.PaymentModeId == Convert.ToInt32(dataRow["Payment_Mode_Id"].ToString())
                //                                     ).FirstOrDefault();
                //                                    if (rowData == null)
                //                                    {
                //                                        rowData = new VirtualSchool.Domain.Entities.FeeDetail
                //                                        {
                //                                            DepartmentId = department.Id,
                //                                            ProgrammeId = programme.Id,
                //                                            LevelId = level.Id,
                //                                            SessionId = session.Id,
                //                                            FeeId = fee.Id,
                //                                            FeeTypeId = feeType.Id,
                //                                            PaymentModeId = paymentMode.Id,
                //                                        };
                //                                        _context.Add(rowData);
                //                                    }


                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                      

                //    }

                //    _context.SaveChanges();
                //}

                var Genotype = ExecuteQuery("SELECT * FROM GENOTYPE");
                if (Genotype != null && Genotype.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Genotype.Rows)
                    {
                        var rowData = _context.Genotype.Where(f => f.Name == dataRow["Genotype_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Genotype
                            {
                                Name = dataRow["Genotype_Name"].ToString(),
                                Description = dataRow["Genotype_Name"].ToString(),
                                Code = dataRow["Genotype_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Genotype_Name"].ToString()),
                                Active = true
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var OlevelGrade = ExecuteQuery("SELECT * FROM O_LEVEL_GRADE");
                if (OlevelGrade != null && OlevelGrade.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in OlevelGrade.Rows)
                    {
                        var rowData = _context.OlevelGrade.Where(f => f.Name == dataRow["O_Level_Grade_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.OlevelGrade
                            {
                                Name = dataRow["O_Level_Grade_Name"].ToString(),
                                Code = dataRow["O_Level_Grade_Name"].ToString(),
                                Description = dataRow["O_Level_Grade_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["O_Level_Grade_Name"].ToString()),
                                Active = true
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var OlevelType = ExecuteQuery("SELECT * FROM O_LEVEL_TYPE");
                if (OlevelType != null && OlevelType.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in OlevelType.Rows)
                    {
                        var rowData = _context.OlevelType.Where(f => f.Name == dataRow["O_Level_Type_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.OlevelType
                            {
                                Name = dataRow["O_Level_Type_Name"].ToString(),
                                Code = dataRow["O_Level_Type_Short_Name"].ToString(),
                                Description = dataRow["O_Level_Type_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["O_Level_Type_Name"].ToString()),
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var PaymentMode = ExecuteQuery("SELECT * FROM PAYMENT_MODE");
                if (PaymentMode != null && PaymentMode.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in PaymentMode.Rows)
                    {
                        var rowData = _context.PaymentMode.Where(f => f.Name == dataRow["Payment_Mode_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.PaymentMode
                            {
                                Name = dataRow["Payment_Mode_Name"].ToString(),
                                Code = dataRow["Payment_Mode_Name"].ToString(),
                                Description = dataRow["Payment_Mode_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Payment_Mode_Description"].ToString()),
                                Active = true
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var PaymentTerminal = ExecuteQuery("SELECT  dbo.PAYMENT_TERMINAL.*, dbo.FEE_TYPE.Fee_Type_Name, dbo.SESSION.Session_Name FROM dbo.PAYMENT_TERMINAL INNER JOIN dbo.FEE_TYPE ON dbo.PAYMENT_TERMINAL.Fee_Type_Id = dbo.FEE_TYPE.Fee_Type_Id INNER JOIN dbo.SESSION ON dbo.PAYMENT_TERMINAL.Session_Id = dbo.SESSION.Session_Id");
                if (PaymentTerminal != null && PaymentTerminal.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in PaymentTerminal.Rows)
                    {
                        var feeType = _context.FeeType.Where(a => a.Name == dataRow["Fee_Type_Name"].ToString()).FirstOrDefault();
                        if (feeType != null)
                        {
                            var session = _context.Session.Where(a => a.Name == dataRow["Session_Name"].ToString()).FirstOrDefault();
                            if (session != null)
                            {
                                var rowData = _context.PaymentTerminal.Where(f => f.Terminal == dataRow["Terminal_Id"].ToString()).FirstOrDefault();
                                if (rowData == null)
                                {
                                    rowData = new VirtualSchool.Domain.Entities.PaymentTerminal
                                    {
                                        Terminal = dataRow["Terminal_Id"].ToString(),
                                        FeetypeId = feeType.Id,
                                        SessionId = session.Id,
                                    };
                                }
                                _context.Add(rowData);
                            }
                        }

                    }

                    _context.SaveChanges();
                }

                var PaymentType = ExecuteQuery("SELECT * FROM PAYMENT_TYPE");
                if (PaymentType != null && PaymentType.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in PaymentType.Rows)
                    {
                        var rowData = _context.PaymentType.Where(f => f.Name == dataRow["Payment_Type_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.PaymentType
                            {
                                Name = dataRow["Payment_Type_Name"].ToString(),
                                Code = dataRow["Payment_Type_Name"].ToString(),
                                Description = dataRow["Payment_Type_Description"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Payment_Type_Name"].ToString())
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var Religion = ExecuteQuery("SELECT * FROM RELIGION");
                if (Religion != null && Religion.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Religion.Rows)
                    {
                        var rowData = _context.Religion.Where(f => f.Name == dataRow["Religion_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.Religion
                            {
                                Name = dataRow["Religion_Name"].ToString(),
                                Code = dataRow["Religion_Name"].ToString(),
                                Description = dataRow["Religion_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Religion_Name"].ToString())
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }

                var Relationship = ExecuteQuery("SELECT * FROM RELATIONSHIP");
                if (Relationship != null && Relationship.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in Relationship.Rows)
                    {
                        var rowData = _context.RelationShip.Where(f => f.Name == dataRow["Relationship_Name"].ToString()).FirstOrDefault();
                        if (rowData == null)
                        {
                            rowData = new VirtualSchool.Domain.Entities.RelationShip
                            {
                                Name = dataRow["Relationship_Name"].ToString(),
                                Code = dataRow["Relationship_Name"].ToString(),
                                Description = dataRow["Relationship_Name"].ToString(),
                                Slug = Slug.GenerateSlug(dataRow["Relationship_Name"].ToString())
                            };
                        }

                        _context.Add(rowData);

                    }

                    _context.SaveChanges();
                }



            }
            catch (Exception ex)
            {
                _sqlConnectionOld.Close();
                _sqlConnectionOld.Dispose();
                throw ex;
            }

        }

    }
}
