﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataMigrationApp
{
    interface ISqlManager
    {
        public DataTable GetData();
        public DataTable AddData();
    }
}
