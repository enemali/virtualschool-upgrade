﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum ApplicantStatus
    {
        SubmittedApplicationForm = 1,
        OfferedAdmission = 2,
        GeneratedAcceptanceInvoice = 3,
        GeneratedAcceptanceReceipt = 4,
        OLevelResultVerified = 5,
        ClearedAndAccepted = 6,
        ClearedAndRejected = 7,
        GeneratedSchoolFeesInvoice = 8,
        GeneratedSchoolFeesReceipt = 9,
        CompeledCourseRegistrationForm = 10,
        CompletedStudentInformationForm = 11,
    }
}
