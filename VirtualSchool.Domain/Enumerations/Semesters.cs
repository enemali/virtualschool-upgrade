﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum Semesters
    {
        FirstSemester = 1,
        SecondSemester = 2
    }
}
