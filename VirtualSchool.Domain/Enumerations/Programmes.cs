﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum Programmes
    {
        Undergraduate = 1,
        Remedial_Studies = 2,
    }
}
