﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum Levels
    {
       First_Year = 1,
       Second_Year = 2,
       Third_Year = 3,
       Fourth_Year = 4,
       Fifth_Year = 5,
       Sixth_Year = 6,
       Applicant = 7
    }
}
