﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum FeeTypes
    {
        ApplicationForm = 1,
        AcceptanceFee = 2,
        SchoolFees = 3
    }
}
