﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum SchoolChoices
    {
        First_Choice = 1,
        Second_Choice = 2,
        Third_Choice = 3,
        Fourth_Choice = 4,
    }
}
