﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum PaymentModes
    {
        Full_Payment = 1,
        First_Installment = 2,
        Second_Installment = 3
    }
}
