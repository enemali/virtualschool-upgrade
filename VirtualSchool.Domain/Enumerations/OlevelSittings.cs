﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Enumerations
{
    public enum OlevelSittings
    {
        First_Sitting = 1,
        Second_Sitting = 2
    }
}
