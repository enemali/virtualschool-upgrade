﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Session
    {
        public Session()
        {
            Payments = new HashSet<Payment>();
            AdmissionLists = new HashSet<AdmissionList>();
            StudentLevels = new HashSet<StudentLevel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public bool ApplicationActive { get; set; }

        public ICollection<Payment> Payments { get; set; }
        public ICollection<AdmissionList> AdmissionLists { get; set; }
        public ICollection<StudentLevel> StudentLevels { get; set; }

    }
}
