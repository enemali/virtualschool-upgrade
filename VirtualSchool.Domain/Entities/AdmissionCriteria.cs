﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionCriteria
    {
        public AdmissionCriteria()
        {
            AdmissionCriteriaForOLevelTypes = new HashSet<AdmissionCriteriaForOLevelType>();
            AdmissionCriteriaForOLevelSubjects = new HashSet<AdmissionCriteriaForOLevelSubject>();
        }

        public int Id { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int MinimumRequiredNumberOfSubject { get; set; }
        public DateTime DateEntered { get; set; }

        public Department Department { get; set; }
        public ICollection<AdmissionCriteriaForOLevelType> AdmissionCriteriaForOLevelTypes { get; set; }
        public ICollection<AdmissionCriteriaForOLevelSubject> AdmissionCriteriaForOLevelSubjects { get; set; }
        public Programme Programme { get; set; }
    }
}
