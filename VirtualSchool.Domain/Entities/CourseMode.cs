﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class CourseMode
    {
        public int CourseModeId { get; set; }
        public string CourseModeName { get; set; }
        public string CourseModeDescription { get; set; }
    }
}
