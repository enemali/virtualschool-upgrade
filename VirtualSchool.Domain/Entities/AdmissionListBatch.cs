﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionListBatch
    {
        public AdmissionListBatch()
        {
            AdmissionLists = new HashSet<AdmissionList>();
        }

        public int Id { get; set; }
        public int AdmissionListTypeId { get; set; }
        public DateTime DateUploaded { get; set; }
        public string UploadedFilePath { get; set; }

        public ICollection<AdmissionList> AdmissionLists { get; set; }
        public AdmissionListType AdmissionListType { get; set; }
    }
}
