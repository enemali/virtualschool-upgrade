﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Person
    {
        public Person()
        {
            ApplicantAppliedCourses = new HashSet<ApplicantAppliedCourse>();
            ApplicantJambDetails = new HashSet<ApplicantJambDetail>();
            ApplicationForms = new HashSet<ApplicationForm>();
            Payments = new HashSet<Payment>();
            AdmissionLists = new HashSet<AdmissionList>();
        }
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactAddress { get; set; }
        public string Hometown { get; set; }
        public string PermanentAddress { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? GenderId { get; set; }
        public Gender Gender { get; set; }
        public int? StateId { get; set; }
        public State State { get; set; }
        public int? LocalGovernmentId { get; set; }
        public LocalGovernment LocalGovernment { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public int? BloodGroupId { get; set; }
        public BloodGroup BloodGroup { get; set; }
        public int? GenotypeId { get; set; }
        public Genotype Genotype { get; set; }
        public int? ReligionId { get; set; }
        public Religion Religion { get; set; }

        public ICollection<ApplicantAppliedCourse> ApplicantAppliedCourses { get; set; }
        public ICollection<ApplicantJambDetail> ApplicantJambDetails { get; set; }
        public ICollection<ApplicationForm> ApplicationForms { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<AdmissionList> AdmissionLists { get; set; }

    }
}
