﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class EAssignmentSubmission
    {
        public int Id { get; set; }
        public int EAssignmentId { get; set; }
        public EAssignment EAssignment { get; set; }
        public int PersonId { get; set; }
        public Student Student { get; set; }
        public string AssignmentContent { get; set; }
        public string Remarks { get; set; }
        public decimal? Score { get; set; }
        public DateTime DateSubmitted { get; set; }

    }
}
