﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class DepartmentOption
    {
        public DepartmentOption()
        {
            ApplicantAppliedCourses = new HashSet<ApplicantAppliedCourse>();
            AdmissionLists = new HashSet<AdmissionList>();
            StudentLevels = new HashSet<StudentLevel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }


        public ICollection<ApplicantAppliedCourse> ApplicantAppliedCourses { get; set; }
        public ICollection<AdmissionList> AdmissionLists { get; set; }
        public ICollection<StudentLevel> StudentLevels { get; set;}
    }
}
