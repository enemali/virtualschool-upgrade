﻿using System.Collections.Generic;

namespace VirtualSchool.Domain.Entities
{
    public class ProgrammeDepartment
    {
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public Programme Programme { get; set; }
        public Department Department { get; set; }
        public bool Active { get; set; }

        //public ICollection<Programme> Programmes { get; set; }
        //public ICollection<Department> Departments { get; set; }
    }
}
