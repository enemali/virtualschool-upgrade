﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ScoreGrade
    {
        public int Id { get; set; }
        public int ScoreFrom { get; set; }
        public int ScoreTo { get; set; }
        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public string Performance { get; set; }
        public string GradeDescription { get; set; }
        public string SessionStart { get; set; }
    }
}
