﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ELearningView
    {
        public long StudentCourseRegistrationDetailId { get; set; }
        public long? StudentCourseRegistrationId { get; set; }
        public long CourseId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
        public int EContentTypeId { get; set; }
        public int? Views { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int LevelId { get; set; }
        public long PersonId { get; set; }
        public string ProgrammeName { get; set; }
        public int ProgrammeId { get; set; }
        public string DepartmentName { get; set; }
        public int DepartmentId { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
    }
}
