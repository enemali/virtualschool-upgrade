﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OLevelExamSitting
    {
        public OLevelExamSitting()
        {
            OlevelResults = new HashSet<OlevelResult>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<OlevelResult> OlevelResults { get; set; }
    }
}
