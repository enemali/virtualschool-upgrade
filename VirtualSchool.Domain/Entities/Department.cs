﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Department
    {
        public Department()
        {
            ApplicantAppliedCourses = new HashSet<ApplicantAppliedCourse>();
            //ProgrammeDepartments = new HashSet<ProgrammeDepartment>();
            DepartmentOptions = new HashSet<DepartmentOption>();
            AdmissionLists = new HashSet<AdmissionList>();
            StudentLevels = new HashSet<StudentLevel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public int FacultyId { get; set; }
        public Faculty Faculty { get; set; }

        public ICollection<StudentLevel> StudentLevels { get; set; }
        public ICollection<DepartmentOption> DepartmentOptions { get; set; }
        public ICollection<ApplicantAppliedCourse> ApplicantAppliedCourses { get; set; }
        public ICollection <AdmissionList> AdmissionLists { get; set; }
        //public ICollection<ProgrammeDepartment> ProgrammeDepartments { get; set; }

    }
}
