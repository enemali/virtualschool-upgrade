﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OlevelResult
    {
        public OlevelResult()
        {
            OlevelResultDetails = new HashSet<OlevelResultDetail>();
        }

        public int Id { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public string ExamNumber { get; set; }
        public int ExamYear { get; set; }
        public string ScannedResultUrl { get; set; }

        public int OlevelSittingId { get; set; }
        public OLevelExamSitting OLevelExamSitting { get; set; }

        public int OlevelTypeId { get; set; }
        public OlevelType OlevelType { get; set; }

        public int? ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm { get; set; }

        public ICollection<OlevelResultDetail> OlevelResultDetails { get; set; }
    }
}
