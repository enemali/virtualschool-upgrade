﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ApplicantJambDetail
    {
        public ApplicantJambDetail()
        {

        }
        public int Id { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public string JambNumber { get; set; }
        public int? JambScore { get; set; }
        public int? SchoolChoice { get; set; }
        public OlevelSubject FirstSubject { get; set; }
        public int? FirstSubjectScore { get; set; }
        public OlevelSubject SecondSubject { get; set; }
        public int? SecondSubjectScore { get; set; }
        public OlevelSubject ThirdSubject { get; set; }
        public int? ThirdSubjectScore { get; set; }
        public OlevelSubject FourthSubject { get; set; }
        public int? FourthSubjectScore { get; set; }
        public int? ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
        

    }
}
