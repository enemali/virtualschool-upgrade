﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class GeneralAudit
    {
        public int Id { get; set; }
        public string TableName { get; set; }
        public string InitialValue { get; set; }
        public string CurrentValue { get; set; }
        public long UserId { get; set; }
        public string Operation { get; set; }
        public DateTime Time { get; set; }
        public string Client { get; set; }
        public string Referer { get; set; }
    }
}
