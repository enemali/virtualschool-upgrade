﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class FeeDetail
    {
        public int Id { get; set; }
        public int FeeId { get; set; }
        public Fee Fee { get; set; }
        public int FeeTypeId { get; set; }
        public FeeType FeeType { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int PaymentModeId { get; set; }
        public PaymentMode PaymentMode { get; set; }

    }
}
