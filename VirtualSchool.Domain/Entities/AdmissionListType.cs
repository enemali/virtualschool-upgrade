﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionListType
    {
        public AdmissionListType()
        {
            AdmissionListBatches = new HashSet<AdmissionListBatch>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<AdmissionListBatch> AdmissionListBatches { get; set; }
    }
}
