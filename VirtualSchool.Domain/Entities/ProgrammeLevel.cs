﻿namespace VirtualSchool.Domain.Entities
{
    public class ProgrammeLevel
    {
        public int ProgrammeId { get; set; }
        public int LevelId { get; set; }
        public virtual Programme Programme { get; set; }
        public virtual Level Level { get; set; }
        public bool Active { get; set; }
    }
}
