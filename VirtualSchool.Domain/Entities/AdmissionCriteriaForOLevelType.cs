﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionCriteriaForOLevelType
    {
        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public int OLevelTypeId { get; set; }

        public AdmissionCriteria AdmissionCriteria { get; set; }
        public OlevelType OlevelType { get; set; }
    }
}
