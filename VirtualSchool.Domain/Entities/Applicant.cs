﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Applicant
    {
        public int Id { get; set; }
        public string ExtraCurricularActivities { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int ApplicantStatusId { get; set; }
        public ApplicantStatus ApplicantStatus { get; set; }

        public int ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
    }
}
