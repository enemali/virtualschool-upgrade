﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class FeeType
    {
        public FeeType()
        {
            Payments = new HashSet<Payment>();
            FeeDetails = new HashSet<FeeDetail>();
            ApplicationFormSettings = new HashSet<ApplicationFormSetting>();
            PaymentEtranzactTypes = new HashSet<PaymentEtranzactType>();
            PaymentTerminals = new HashSet<PaymentTerminal>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public ICollection<Payment> Payments { get; set; }
        public ICollection<FeeDetail> FeeDetails { get; set; }
        public ICollection<ApplicationFormSetting> ApplicationFormSettings { get; set; }
        public ICollection<PaymentEtranzactType> PaymentEtranzactTypes { get; set; }
        public ICollection<PaymentTerminal> PaymentTerminals { get; set; }
    }
}
