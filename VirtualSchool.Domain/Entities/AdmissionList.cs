﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
     public class AdmissionList
     {
        public int Id { get; set; }
        
        public int AdmissionListBatchId { get; set; }
        public AdmissionListBatch AdmissionListBatch { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }

        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public int? DepartmentOptionId { get; set; }
        public DepartmentOption DepartmentOption { get; set; }

        public int? ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm { get; set; }

        public bool Activated { get; set; }

        public int SessionId { get; set; }
        public Session Session { get; set; }
    }
}
