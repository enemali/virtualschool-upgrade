﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentType
    {
        public StudentType()
        {
            Students = new HashSet<Student>();
        }
        public int StudentTypeId { get; set; }
        public string StudentTypeName { get; set; }
        public string StudentTypeDescription { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
