﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Programme
    {
        public Programme()
        {
            ApplicantAppliedCourses = new HashSet<ApplicantAppliedCourse>();
            AdmissionLists = new HashSet<AdmissionList>();
            StudentLevels = new HashSet<StudentLevel>();
            ApplicationFormSettings = new HashSet<ApplicationFormSetting>();
            //ProgrammeDepartments = new HashSet<ProgrammeDepartment>();
            //ProgrammeLevel = new HashSet<ProgrammeLevel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public ICollection<ApplicantAppliedCourse> ApplicantAppliedCourses { get; set; }
        public ICollection<AdmissionList> AdmissionLists { get; set; }
        public ICollection<StudentLevel> StudentLevels { get; set; }
        public ICollection<ApplicationFormSetting> ApplicationFormSettings { get; set; }
        //public ICollection<ProgrammeDepartment> ProgrammeDepartments { get; set; }
        //public ICollection<ProgrammeLevel> ProgrammeLevel { get; set; }

    }
}
