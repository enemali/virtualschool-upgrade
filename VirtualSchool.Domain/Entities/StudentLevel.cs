﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentLevel
    {
        public long StudentLevelId { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int DepartmentOptionId { get; set; }
        public DepartmentOption DepartmentOption { get; set; }
    }
}
