﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ApplicationForm
    {
        public ApplicationForm()
        {
            ApplicantAppliedCourses = new HashSet<ApplicantAppliedCourse>();
            ApplicantJambDetails = new HashSet<ApplicantJambDetail>();
        }
        public int Id { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int ApplicationFormSettingId { get; set; }
        public ApplicationFormSetting ApplicationFormSetting { get;set;}

        public int SerialNumber { get; set; }
        public string FormNumber { get; set; }
        public string ExamNumber { get; set; }


        public ICollection<ApplicantAppliedCourse> ApplicantAppliedCourses { get; set; }
        public ICollection<ApplicantJambDetail> ApplicantJambDetails { get; set; }
    }
}
