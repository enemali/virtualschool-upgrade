﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class CourseUnit
    {
        public int CourseUnitId { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int SemesterId { get; set; }
        public Semester Semester { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public byte MinimumUnit { get; set; }
        public byte MaximumUnit { get; set; }

    }
}
