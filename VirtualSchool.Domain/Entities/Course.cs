﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Course
    {
        public Course()
        {
            ECourses = new HashSet<ECourse>();
            EAssignments = new HashSet<EAssignment>();
            StudentCourseRegistrationDetails = new HashSet<StudentCourseRegistrationDetail>();
        }

        public int Id { get; set; }
        public int CourseTypeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int? DepartmentOptionId { get; set; }
        public int Unit { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public bool Activated { get; set; }
        
        public virtual CourseType CourseType { get; set; }
        public virtual Department Department { get; set; }
        public virtual DepartmentOption DepartmentOption { get; set; }
        public virtual Level Level { get; set; }
        public virtual Programme Programme { get; set; }
        public virtual Semester Semester { get; set; }
        public virtual ICollection<StudentCourseRegistrationDetail> StudentCourseRegistrationDetails { get; set; }

        public ICollection<ECourse> ECourses { get; set; }
        public ICollection<EAssignment> EAssignments { get; set; }
    }
}
