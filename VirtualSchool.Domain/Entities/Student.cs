﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Student
    {
        public Student()
        {
            EAssignmentSubmissions = new HashSet<EAssignmentSubmission>();
        }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public int ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm {get;set;}
        public int StudentCategoryId { get; set; }
        public StudentCategory StudentCategory { get; set; }
        public int StudentTypeId { get; set; }
        public StudentType StudentType { get; set; }
        public string MatricNumber { get; set; }
        public string PasswordHash { get; set; }
        public ICollection<EAssignmentSubmission> EAssignmentSubmissions { get; set; }
    }
}
