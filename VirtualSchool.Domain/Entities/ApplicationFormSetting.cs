﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ApplicationFormSetting
    {
        public ApplicationFormSetting()
        {
            ApplicationForms = new HashSet<ApplicationForm>();
        }
        public int Id { get; set; }
        public string ApplicationFormSettingName { get; set; }
        public int FeeTypeId { get; set; }
        public FeeType FeeType { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
        public string FormCode { get; set; }


        public ICollection<ApplicationForm> ApplicationForms { get; set; }
    }
}
