﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Payment
    {
        public Payment()
        {
            OnlinePayments = new HashSet<OnlinePayment>();
            PaymentEtranzacts = new HashSet<PaymentEtranzact>();
        }
        public int Id { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public int PaymentModeId { get; set; }
        public PaymentMode PaymentMode { get; set; }
        public int PaymentTypeId { get; set; }
        public PaymentType PaymentType { get; set; }
        public int FeeTypeId { get; set; }
        public FeeType FeeType { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public decimal Amount { get; set; }
        public long SerialNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DateGenerated { get; set; }
        public DateTime? DatePaid { get; set; }
        public ICollection<OnlinePayment> OnlinePayments { get; set; }
        public ICollection<PaymentEtranzact> PaymentEtranzacts { get; set; }
    }
}
