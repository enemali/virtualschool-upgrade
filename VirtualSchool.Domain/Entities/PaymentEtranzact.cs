﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{

    public class PaymentEtranzact
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public Payment Payment { get; set; }
        public int PaymenTerminalId { get; set; }
        public PaymentTerminal PaymentTerminal { get; set; }
        public int PaymentEtranzactTypeId { get; set; }
        public PaymentEtranzactType PaymentEtranzactType { get; set; }
        public int OnlinePaymentId { get; set; }
        public OnlinePayment OnlinePayment { get; set; }
        public string ReceiptNo { get; set; }
        public string PaymentCode { get; set; }
        public string MerchantCode { get; set; }
        public decimal? TransactionAmount { get; set; }
        public string TransactionDescription { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string ConfirmationNo { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public bool Used { get; set; }
        public Int64 UsedBy { get; set; }
       
    }



}
