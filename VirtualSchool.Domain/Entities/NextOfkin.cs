﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class NextOfkin
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public string Name { get; set; }
        public int RelationShipId { get; set; }
        public RelationShip RelationShip { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int? ApplicationFormId { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
    }
}
