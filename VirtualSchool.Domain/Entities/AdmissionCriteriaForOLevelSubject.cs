﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionCriteriaForOLevelSubject
    {
        public AdmissionCriteriaForOLevelSubject()
        {
            AdmissionCriteriaForOLevelSubjectAlternatives = new HashSet<AdmissionCriteriaForOLevelSubjectAlternative>();
        }

        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public int OLevelSubjectId { get; set; }
        public int MinimumOLevelGradeId { get; set; }
        public bool IsCompulsory { get; set; }

        public AdmissionCriteria AdmissionCriteria { get; set; }
        public ICollection<AdmissionCriteriaForOLevelSubjectAlternative> AdmissionCriteriaForOLevelSubjectAlternatives { get; set; }
        public OlevelGrade OlevelGrade { get; set; }
        public OlevelSubject OlevelSubject { get; set; }
    }
}
