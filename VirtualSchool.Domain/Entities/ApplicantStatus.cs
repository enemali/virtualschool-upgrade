﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ApplicantStatus
    {
        public ApplicantStatus()
        {
            Applicants = new HashSet<Applicant>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Applicant> Applicants { get; set; }
    }
}
