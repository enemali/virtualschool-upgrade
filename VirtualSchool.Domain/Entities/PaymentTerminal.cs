﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class PaymentTerminal
    {
        public PaymentTerminal()
        {
            PaymentEtranzacts = new HashSet<PaymentEtranzact>();
        }
        public int Id { get; set; }
        public string Terminal { get; set; }
        public FeeType FeeType { get; set; }
        public int FeetypeId { get; set; }
        public Session Session { get; set; }
        public int SessionId { get; set; }

        public ICollection<PaymentEtranzact> PaymentEtranzacts { get; set; }
    }


}
