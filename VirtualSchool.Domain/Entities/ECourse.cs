﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ECourse
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public Course Course { get; set; }
        public int EContentTypeId { get; set; }
        public EContentType EContentType { get; set; }
        public string Url { get; set; }
        public int? views { get; set; }

    }
}
