﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OlevelGrade
    {
        public OlevelGrade()
        {
            AdmissionCriteriaForOLevelSubject = new HashSet<AdmissionCriteriaForOLevelSubject>();
            OlevelResultDetails = new HashSet<OlevelResultDetail>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public ICollection<AdmissionCriteriaForOLevelSubject> AdmissionCriteriaForOLevelSubject { get; set; }
        public ICollection<OlevelResultDetail> OlevelResultDetails { get; set; }
    }
}
