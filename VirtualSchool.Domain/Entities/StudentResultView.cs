﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentResultView
    {
        public long SCRDId { get; set; }
        public string Name { get; set; }
        public string MatricNumber { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string LevelName { get; set; }
        public string ProgrammeName { get; set; }
        public string DepartmentName { get; set; }
        public string SemesterName { get; set; }
        public string SessionName { get; set; }
        public int SessionSemesterId { get; set; }
        public long CourseId { get; set; }
        public int CourseTypeId { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public int Course_Unit { get; set; }
        public decimal TestScore { get; set; }
        public decimal ExamScore { get; set; }
        public string SpecialCase { get; set; }
        public decimal TotalScore { get; set; }
        public int StudentId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public int TotalSemesterCourseUnit { get; set; }
        public string CourseMode { get; set; }
        public int CourseModeId { get; set; }
        public int FacultyId { get; set; }
        public string FacultyName { get; set; }
        public string ContactAddress { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int LevelId { get; set; }
        public decimal WGP { get; set; }
        public decimal GPA { get; set; }
        public decimal CGPA { get; set; }
    }
}
