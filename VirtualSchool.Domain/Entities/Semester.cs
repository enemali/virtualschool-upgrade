﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Semester
    {
        public Semester()
        {
            SessionSemesters = new HashSet<SessionSemester>();
            CourseUnits = new HashSet<CourseUnit>();
            Courses = new HashSet<Course>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public ICollection<SessionSemester> SessionSemesters { get; set; }
        public ICollection<CourseUnit> CourseUnits { get; set; }
        public ICollection<Course> Courses { get; set; }


    }
}
