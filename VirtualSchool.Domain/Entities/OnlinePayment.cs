﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OnlinePayment
    {
        public OnlinePayment()
        {
            //PaymentEtranzact = new HashSet<PaymentEtranzact>();
        }
        public int PaymentId { get; set; }
        public Payment Payment { get; set; }
        public int PaymentChannelId { get; set; }
        public PaymentChannel PaymentChannel { get; set; }
        public string TransactionNumber { get; set; }
        public DateTime? TransactionDate { get; set; }
        //public ICollection<PaymentEtranzact> PaymentEtranzact { get; set; }




    }



}
