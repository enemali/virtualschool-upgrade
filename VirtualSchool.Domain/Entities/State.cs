﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class State
    {
        public State()
        {
            LocalGovernments = new HashSet<LocalGovernment>();
            People = new HashSet<Person>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public Country Country { get; set; }

        public ICollection<Person> People { get; set; }
        public ICollection<LocalGovernment> LocalGovernments { get; set; }
    }
}
