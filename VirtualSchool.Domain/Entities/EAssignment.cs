﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class EAssignment
    {
        public EAssignment()
        {
            EAssignmentSubmissions = new HashSet<EAssignmentSubmission>();
        }
        public int Id { get; set; }
        public int CourseId { get; set; }
        public Course Course { get; set; }
        public string Assignment { get; set; }
        public string URL { get; set; }
        public string Instructions { get; set; }
        public DateTime DateSet { get; set; }
        public DateTime DueDate { get; set; }

        public ICollection<EAssignmentSubmission> EAssignmentSubmissions { get; set; }
    }
}
