﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OlevelType
    {
        public OlevelType()
        {
            AdmissionCriteriaForOLevelTypes = new HashSet<AdmissionCriteriaForOLevelType>();
            OlevelResults = new HashSet<OlevelResult>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public ICollection<AdmissionCriteriaForOLevelType> AdmissionCriteriaForOLevelTypes { get; set; }
        public ICollection<OlevelResult> OlevelResults { get; set; }
    }
}
