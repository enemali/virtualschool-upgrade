using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentCourseRegistrationDetail
    {
        public long Id { get; set; }
        public long StudentCourseRegistrationId { get; set; }
        public long CourseId { get; set; }
        public int CourseModeId { get; set; }
        public int CourseUnit { get; set; }
        public int SemesterId { get; set; }
        public decimal? TestScore { get; set; }
        public decimal? ExamScore { get; set; }
        public string SpecialCase { get; set; }
        public DateTime? DateUploaded { get; set; }

        public virtual Course Course { get; set; }
        public virtual CourseMode CourseMode { get; set; }
        public virtual Semester Semester { get; set; }
        public virtual StudentCourseRegistration StudentCourseRegistration { get; set; }
    }
}