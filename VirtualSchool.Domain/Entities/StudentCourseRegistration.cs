﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentCourseRegistration
    {
        public long Id { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }

        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public DateTime? DateApproved { get; set; }
    }
}
