﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class PaymentEtranzactType 
    {
        public PaymentEtranzactType()
        {
               PaymentEtranzacts = new HashSet<PaymentEtranzact>();
        }
        public int Id { get; set; }
        public string  Name { get; set; }
        public FeeType FeeType { get; set; }
        public int FeetypeId { get; set; }
        public Session Session { get; set; }
        public int SessionId { get; set; }
        public PaymentMode PaymentMode { get; set; }
        public int PaymentModeId { get; set; }
        public Programme Programme { get; set; }
        public int ProgrammeId { get; set; }

        public ICollection<PaymentEtranzact> PaymentEtranzacts { get; set; }


    }


}
