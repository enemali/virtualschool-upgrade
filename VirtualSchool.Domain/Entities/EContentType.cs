﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class EContentType
    {
        public EContentType()
        {
            ECourses = new HashSet<ECourse>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Slug { get; set; }

        public ICollection<ECourse> ECourses { get; set; }
    }
}
