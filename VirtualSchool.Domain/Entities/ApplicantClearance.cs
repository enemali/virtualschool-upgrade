﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class ApplicantClearance
    {
        public long Id { get; set; }
        public int ApplicationFormId { get; set; }
        public bool Cleared { get; set; }
        public DateTime DateCleared { get; set; }
        public bool Rejected { get; set; }
        public string RejectReason { get; set; }

        public ApplicationForm ApplicationForm { get; set; }
    }
}
