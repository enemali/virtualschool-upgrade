﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class PaymentChannel
    {
        public PaymentChannel()
        {
            OnlinePayments = new HashSet<OnlinePayment>();
        }
        public int PaymentChannnelId { get; set; }
        public string PaymentChannelName { get; set; }
        public string PaymentChannelDescription { get; set; }
        public string PaymentChannelStatus { get; set; }
        public ICollection<OnlinePayment> OnlinePayments { get; set; }
    }
}
