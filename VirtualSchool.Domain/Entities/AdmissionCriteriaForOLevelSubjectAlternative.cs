﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class AdmissionCriteriaForOLevelSubjectAlternative
    {
        public object AdmissionCriteria;

        public int Id { get; set; }
        public int AdmissionCriteriaForOLevelSubjectId { get; set; }
        public int OLevelSubjectId { get; set; }

        public AdmissionCriteriaForOLevelSubject AdmissionCriteriaForOLevelSubject { get; set; }
        public OlevelSubject OlevelSubject { get; set; }
    }
}
