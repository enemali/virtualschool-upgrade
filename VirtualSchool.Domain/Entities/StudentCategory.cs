﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class StudentCategory
    {
        public StudentCategory()
        {
            Students = new HashSet<Student>();
        }
        public int StudentCateoryId { get; set; }
        public string StudentCategoryName { get; set; }
        public string StudentCategoryDescription { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
