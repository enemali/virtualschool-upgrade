﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class Level
    {
        public  Level()
        {
            Payments = new HashSet<Payment>();
            FeeDetail = new HashSet<FeeDetail>();
            ApplicationFormSetting = new HashSet<ApplicationFormSetting>();
            StudentCourseRegistrations = new HashSet<StudentCourseRegistration>();
            CourseUnits = new HashSet<CourseUnit>();
            Courses = new HashSet<Course>();
            StudentLevels = new HashSet<StudentLevel>();
            //ProgrammeLevel = new HashSet<ProgrammeLevel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public ICollection<Payment> Payments { get; set; }
        public ICollection<FeeDetail> FeeDetail { get; set; }
        public ICollection<ApplicationFormSetting> ApplicationFormSetting { get; set; }
        public ICollection<StudentCourseRegistration> StudentCourseRegistrations { get; set; }
        public ICollection<CourseUnit> CourseUnits { get; set; }
        public ICollection<Course> Courses { get; set; }
        public ICollection<StudentLevel> StudentLevels { get; set; }
        //public ICollection<ProgrammeLevel> ProgrammeLevel { get; set; }
    }
}
