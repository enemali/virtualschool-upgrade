﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class SessionSemester
    {
        public int SessionSemesterId { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int SemesterId { get; set; }
        public Semester Semester { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
