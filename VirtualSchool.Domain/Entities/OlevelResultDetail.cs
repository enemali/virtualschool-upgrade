﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Entities
{
    public class OlevelResultDetail
    {
        public int Id { get; set; }

        public int olevelResultId { get; set; }
        public OlevelResult olevelResult { get; set; }

        public int olevelGradeId { get; set; }
        public OlevelGrade olevelGrade { get; set; }

        public int OlevelSubjectId { get; set; }
        public OlevelSubject OlevelSubject { get; set; }
    }
}
