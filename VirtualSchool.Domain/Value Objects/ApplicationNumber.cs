﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Exceptions;
using VirtualSchool.Domain.Infrastructure;

namespace VirtualSchool.Domain.Value_Objects
{
    public class ApplicationNumber : ValueObject
    {
        private ApplicationNumber()
        {
        }

        public ApplicationNumber(string value)
        {
            try
            {
                var index = value.IndexOf("\\", StringComparison.Ordinal);
                Programme = value.Substring(0, index);
                FormType = value.Substring(index + 1);
                SerialNumber = value.Substring(index + 2);
            }
            catch (Exception ex)
            {
                throw new ApplicationNumberInvalidException(value, ex);
            }
        }

        public string Programme { get; private set; }

        public string FormType { get; private set; }

        public string SerialNumber { get; private set; }

        public static implicit operator string(ApplicationNumber applicationNumber)
        {
            return applicationNumber.ToString();
        }

        public static explicit operator ApplicationNumber(string value)
        {
            return new ApplicationNumber(value);
        }

        public override string ToString()
        {
            return $"FPI\\{Programme}\\{FormType}\\{SerialNumber}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Programme;
            yield return FormType;
            yield return SerialNumber;
        }
    }


}
