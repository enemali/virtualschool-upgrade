﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Domain.Exceptions
{
    public class ApplicationNumberInvalidException:Exception
    {
        public ApplicationNumberInvalidException(string applicationNumber, Exception ex)
           : base($"Application Number \"{applicationNumber}\" is invalid.", ex)
        {
        }
    }
}
