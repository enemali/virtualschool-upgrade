﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Tests.Infrastructure
{
    public class VirtualSchoolContextFactory
    {
        public static VirtualSchoolDbContext Create()
        {
            var options = new DbContextOptionsBuilder<VirtualSchoolDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new VirtualSchoolDbContext(options);
            context.Database.EnsureCreated();
            VirtualSchoolDbInitializer.Initialize(context);
            
            context.SaveChanges();

            return context;
        }

        public static void Destroy(VirtualSchoolDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
