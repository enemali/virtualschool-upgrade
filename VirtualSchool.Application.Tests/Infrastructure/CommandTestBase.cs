﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Tests.Infrastructure
{
    public class CommandTestBase : IDisposable
    {
        protected readonly VirtualSchoolDbContext _context;

        public CommandTestBase()
        {
            _context = VirtualSchoolContextFactory.Create();
        }

        public void Dispose()
        {
            VirtualSchoolContextFactory.Destroy(_context);
        }
    }
}
