﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Persistence;
using Xunit;

namespace VirtualSchool.Application.Tests.Infrastructure
{
    public class QueryTestFixture : IDisposable
    {
        public VirtualSchoolDbContext Context { get; private set; }

        public QueryTestFixture()
        {
            Context = VirtualSchoolContextFactory.Create();
        }

        public void Dispose()
        {
            VirtualSchoolContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}
