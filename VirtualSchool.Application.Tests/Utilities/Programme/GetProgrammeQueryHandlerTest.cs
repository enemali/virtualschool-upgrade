﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Tests.Infrastructure;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Application.Utilities.Programme.Queries;
using VirtualSchool.Persistence;
using Xunit;

namespace VirtualSchool.Application.Tests.Utilities.Programme
{
    [Collection("QueryCollection")]
    public class GetProgrammeQueryHandlerTest
    {
        private readonly VirtualSchoolDbContext _context;

        public GetProgrammeQueryHandlerTest(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetSingleProgramme()
        {
            var sut = new GetProgrammeQueryHandler(_context);

            var result = await sut.Handle(new GetProgrammeQuery { Id = 1 }, CancellationToken.None);

            result.ShouldBeOfType<ProgrammeDTO>();
            result.Id.ShouldBe<int>(1);
        }

    }


}
