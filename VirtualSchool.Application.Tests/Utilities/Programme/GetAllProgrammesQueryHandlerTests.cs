﻿using System;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Tests.Infrastructure;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Application.Utilities.Programme.Queries;
using VirtualSchool.Persistence;
using Xunit;

namespace VirtualSchool.Application.Tests.Utilities.Programme
{
    [Collection("QueryCollection")]
    public class GetAllProgrammesQueryHandlerTests
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllProgrammesQueryHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetAllProgrammes()
        {
            var sut = new GetAllProgrammesQueryHandler(_context);

            var result = await sut.Handle(new GetAllProgrammesQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<ProgrammeDTO>>();
            result.FirstOrDefault().Id.ShouldBeGreaterThan<int>(0);
        }

    }

}
