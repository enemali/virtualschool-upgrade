﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Tests.Infrastructure;
using VirtualSchool.Application.Utilities.Programme.Commands;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Persistence;
using Xunit;

namespace VirtualSchool.Application.Tests.Utilities.Programme
{
    [Collection("QueryCollection")]
    public class CreateProgrammeCommandHandlerTests
    {
        private readonly VirtualSchoolDbContext _context;
        private readonly CreateProgrammeCommandHandler _commandHandler;
        private readonly CreateProgrammeCommand _command;

        public CreateProgrammeCommandHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _commandHandler = new CreateProgrammeCommandHandler(_context);
            _command = new CreateProgrammeCommand { Name = "XUNIT", Active = false, Code = "XU", Description = "Xunit Programme" };
        }

        [Fact]
        public async Task CreateSingleProgramme()
        {
            var result = await _commandHandler.Handle(_command, CancellationToken.None);
            result.ShouldBeOfType<int>();
        }

    }
}
