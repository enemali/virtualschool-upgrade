﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Common
{
    public class MailgunSetting
    {   
        public string apiKey { get; set; }
        public string Domain { get; set; }
    }
}
