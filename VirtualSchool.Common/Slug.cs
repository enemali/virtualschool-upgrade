﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace VirtualSchool.Common
{
    public static class Slug
    {
        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.ToLower();
            //Remove all accents
            var bytes = Encoding.GetEncoding("UTF-8").GetBytes(str);

            str = Encoding.ASCII.GetString(bytes);

            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

      
    }
}
