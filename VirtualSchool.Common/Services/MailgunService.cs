﻿using FluentEmail.Core;
using FluentEmail.Mailgun;
using FluentEmail.Razor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VirtualSchool.Common.Services
{
    public class MailgunService
    {
        private readonly MailgunSetting _settings;

        public MailgunService(MailgunSetting mailgunSetting)
        {
            _settings = mailgunSetting;
        }

        public async void SendMailAsync(string to,string subject, string message, string button = null,string url = null,string from = "support@lloydant.com",string template = "Default.cshtml")
        {
            try
            {
                //var sender = new MailgunSender(
                //"emails.lloydant.com", // Mailgun Domain
                //"key-06b6db8ba1fa36f7dfddcff9d7c54040" // Mailgun API Key
                //);
                var sender = new MailgunSender(_settings.Domain, _settings.apiKey);
                Email.DefaultSender = sender;
                Email.DefaultRenderer = new RazorRenderer();

                var email = Email
                .From(from, "Support")
                .To(to)
                .Subject(subject)
                .UsingTemplateFromFile($"{ Directory.GetCurrentDirectory()}/EmailTemplate/{template}", new { Subject= subject, Message = message, Button = button, Url = url });
                   
                await email.SendAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
