﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace VirtualSchool.Common.Services
{
    public class MessageServices : IEmailSender, ISmsSender
    {
        private MailgunService _mailgunService;
        public MessageServices()
        {
            _mailgunService = new MailgunService(new MailgunSetting { Domain = "emails.lloydant.com", apiKey = "key-06b6db8ba1fa36f7dfddcff9d7c54040" });
        }

        public Task SendSmsAsync(string number, string message)
        {
            throw new NotImplementedException();
        }

        public Task SendEmail(string email, string subject, string message)
        {
            _mailgunService.SendMailAsync(email, subject, message);
            return Task.CompletedTask;
        }
    }
}
