﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VirtualSchool.Common.Services
{
    public interface IEmailSender
    {
        Task SendEmail(string email, string subject, string message);
    }
}
