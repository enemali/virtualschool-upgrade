﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Exceptions
{
    public class ExistingItemException : Exception
    {
        public ExistingItemException(string name,  string message)
           : base($"Entity with value exists. Name\"{name}\" {message}")
        {
        }
    }
}
