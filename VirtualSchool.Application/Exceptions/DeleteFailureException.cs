﻿using System;

namespace VirtualSchool.Application.Exceptions
{
    public class DeleteFailureException : Exception
    {
        public DeleteFailureException(string name, object key, string message)
            : base($"Deletion of entity \"{name}\" with key ({key}) failed. {message}")
        {
        }
    }
}
