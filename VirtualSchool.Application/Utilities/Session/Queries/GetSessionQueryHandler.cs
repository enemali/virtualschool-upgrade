﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Session.Models;
using VirtualSchool.Application.Utilities.Session.Queries;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Religion.Queries
{
    public class GetSessionQueryHandler: IRequestHandler<GetSessionQuery, SessionDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetSessionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<SessionDTO> Handle(GetSessionQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Session.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Session), request.Id);
            }

            return new SessionDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
