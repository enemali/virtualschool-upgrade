﻿using MediatR;
using VirtualSchool.Application.Utilities.Session.Models;

namespace VirtualSchool.Application.Utilities.Session.Queries
{
    public class GetSessionQuery : IRequest<SessionDTO>
    {
        public int Id { get; set; }
    }
}
