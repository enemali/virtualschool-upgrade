﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Session.Models;

namespace VirtualSchool.Application.Utilities.Session.Queries
{
    public class GetAllSessionQuery : IRequest<IEnumerable<SessionDTO>>
    {
    }
}
