﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Religion.Models;
using VirtualSchool.Application.Utilities.Session.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Session.Queries
{
    public class GetAllRSessionQueryHandler : IRequestHandler<GetAllSessionQuery, IEnumerable<SessionDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllRSessionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SessionDTO>> Handle(GetAllSessionQuery request, CancellationToken cancellationToken)
        {
            var sessions = await _context.Session
                .Select(c => new SessionDTO
                {
                    Id = c.Id,
                    Name = c.Name
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return sessions;
        }
    }
}
