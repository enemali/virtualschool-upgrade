﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Session.Command
{
    public class DeleteSessionCommand:IRequest
    {
        public int Id { get; set; }
    }
}
