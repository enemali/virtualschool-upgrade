﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Session.Command
{
    public class DeleteSessionCommandHandler: IRequestHandler<DeleteSessionCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteSessionCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteSessionCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Session.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Session), request.Id);
                }

                _context.Session.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
