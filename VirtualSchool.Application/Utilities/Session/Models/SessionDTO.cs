﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Session.Models
{
    public class SessionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
