﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Programme.Commands
{
    public class DeleteProgrammeCommand : IRequest
    {
        public int Id { get; set; }
    }
}
