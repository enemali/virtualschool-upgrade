﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Programme.Commands
{
    public class DeleteProgrammeCommandHandler : IRequestHandler<DeleteProgrammeCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteProgrammeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteProgrammeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Programme.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Programme), request.Id);
            }
            
            _context.Programme.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
