﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Programme.Commands
{
    public class CreateProgrammeCommand:IRequest<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }
}
