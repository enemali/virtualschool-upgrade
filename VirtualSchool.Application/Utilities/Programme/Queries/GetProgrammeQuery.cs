﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Programme.Models;

namespace VirtualSchool.Application.Utilities.Programme.Queries
{
    public class GetProgrammeQuery:IRequest<ProgrammeDTO>
    {
        public int Id { get; set; }
    }
}
