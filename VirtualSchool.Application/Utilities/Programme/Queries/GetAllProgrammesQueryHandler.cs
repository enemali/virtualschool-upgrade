﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Programme.Queries
{
    public class GetAllProgrammesQueryHandler : IRequestHandler<GetAllProgrammesQuery, IEnumerable<ProgrammeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllProgrammesQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ProgrammeDTO>> Handle(GetAllProgrammesQuery request, CancellationToken cancellationToken)
        {
            //Select Product from table and return

            var programmes = await _context.Programme
             .Select(c => new ProgrammeDTO
             {
                 Id = c.Id,
                 Name = c.Name,
                 ShortCode = c.Code
             }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return programmes;
        }


    }


}
