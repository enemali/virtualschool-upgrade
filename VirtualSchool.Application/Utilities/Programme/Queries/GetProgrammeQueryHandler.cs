﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Programme.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Programme.Queries
{
    public class GetProgrammeQueryHandler : IRequestHandler<GetProgrammeQuery, ProgrammeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetProgrammeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<ProgrammeDTO> Handle(GetProgrammeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Programme.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Programme), request.Id);
            }

            return new ProgrammeDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortCode = entity.Code
            };
        }
    }
}
