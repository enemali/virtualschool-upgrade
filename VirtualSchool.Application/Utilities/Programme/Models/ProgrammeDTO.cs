﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Programme.Models
{
    public class ProgrammeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
    }
}
