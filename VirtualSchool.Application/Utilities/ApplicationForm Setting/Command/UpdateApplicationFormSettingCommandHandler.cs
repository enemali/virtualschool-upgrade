﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Command
{
    public class UpdateApplicationFormSettingCommandHandler :  IRequestHandler<UpdateApplicationFormSettingCommand, Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateApplicationFormSettingCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(UpdateApplicationFormSettingCommand request, CancellationToken cancellationToken)
        {
           

            var entity = await _context.ApplicationFormSetting.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.ApplicationFormSetting), request.Id);
            }

            
            entity.Id = request.Id;
            entity.ApplicationFormSettingName = request.ApplicationFormSettingName;
            entity.FeeTypeId = request.FeeTypeId;
            entity.LevelId = request.LevelId;
            entity.SessionId = request.SessionId;
            entity.FormCode = request.FormCode;
            entity.ProgrammeId = request.ProgrammeId;
            entity.StartDate = request.StartDate;
            entity.StopDate = request.StopDate;

            await _context.SaveChangesAsync(cancellationToken);


            return Unit.Value;
        }
    }
}
