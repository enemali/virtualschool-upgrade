﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Command
{
    public class DeleteApplicationFormSettingCommandHandler : IRequestHandler<DeleteApplicationFormSettingCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteApplicationFormSettingCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(DeleteApplicationFormSettingCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.ApplicationFormSetting.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicationFormSetting), request.Id);
            }

            _context.ApplicationFormSetting.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
