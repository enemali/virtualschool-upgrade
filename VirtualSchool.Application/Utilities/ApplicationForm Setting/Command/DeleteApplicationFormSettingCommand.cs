﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Command
{
   public class DeleteApplicationFormSettingCommand: IRequest
    {
        public int Id { get; set; }
    }
}
