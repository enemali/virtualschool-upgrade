﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Command
{
    public class CreateApplicationFormSettingCommandHandler : IRequestHandler<CreateApplicationFormSettingCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;
        public CreateApplicationFormSettingCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CreateApplicationFormSettingCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await _context.ApplicationFormSetting.Where(a => a.ApplicationFormSettingName == request.ApplicationFormSettingName && a.SessionId==request.SessionId && a.ProgrammeId==request.ProgrammeId).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.ApplicationFormSetting), "An entity with the code already exists! Change Application Name");
            }



            var entity = new VirtualSchool.Domain.Entities.ApplicationFormSetting
            {
                ApplicationFormSettingName = request.ApplicationFormSettingName,
                FeeTypeId = request.FeeTypeId,
                LevelId = request.LevelId,
                SessionId = request.SessionId,
                FormCode = request.FormCode,
                ProgrammeId = request.ProgrammeId,
                StartDate = request.StartDate,
                StopDate = request.StopDate

            };


            _context.ApplicationFormSetting.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
