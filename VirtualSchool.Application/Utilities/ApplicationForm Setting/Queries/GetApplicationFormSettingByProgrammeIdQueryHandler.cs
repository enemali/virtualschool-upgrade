﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.CustomErrors;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Queries
{
    public class GetApplicationFormSettingByProgrammeIdQueryHandler : IRequestHandler<GetApplicationFormSettingByProgrammeIdQuery, IEnumerable<ApplicationFormSettingDTO>>
    {
        private readonly VirtualSchoolDbContext _context;
        

        public GetApplicationFormSettingByProgrammeIdQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ApplicationFormSettingDTO>> Handle(GetApplicationFormSettingByProgrammeIdQuery request, CancellationToken cancellationToken)
        {
            List<ApplicationFormSettingDTO> programme = new List<ApplicationFormSettingDTO>();
            try
            {
                var ActiveSession = await _context.Session.Where(s => s.ApplicationActive && s.Code != null).OrderByDescending(f=>f.Id).FirstOrDefaultAsync();
                if (ActiveSession == null)
                {
                    throw NotFoundError.Response("No Session is Active for Application!");
                }
                var applicationFormSettingobject =await _context.ApplicationFormSetting.Where(a => a.SessionId == ActiveSession.Id && a.ProgrammeId == request.Id && a.StartDate<=DateTime.Now && a.StopDate>=DateTime.Now).ToListAsync();
                foreach (var item in applicationFormSettingobject)
                {
                    programme.Add(new ApplicationFormSettingDTO
                    {
                        Id = item.Id,
                        ApplicationFormSettingName = item.ApplicationFormSettingName,
                        FeeTypeId = item.FeeTypeId,
                        LevelId = item.LevelId,
                        SessionId = item.SessionId,
                        FormCode = item.FormCode,
                        ProgrammeId = item.ProgrammeId,
                        StartDate = item.StartDate,
                        StopDate = item.StopDate

                    });
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return programme;
        }

        
    }
}
