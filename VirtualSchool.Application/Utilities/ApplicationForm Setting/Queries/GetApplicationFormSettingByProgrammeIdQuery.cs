﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Queries
{
    public class GetApplicationFormSettingByProgrammeIdQuery : IRequest<IEnumerable<ApplicationFormSettingDTO>>
    {
        public int Id { get; set; }
    }
}
