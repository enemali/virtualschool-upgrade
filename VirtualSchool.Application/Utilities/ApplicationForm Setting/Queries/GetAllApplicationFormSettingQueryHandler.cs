﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Queries
{
    public class GetAllApplicationFormSettingQueryHandler : IRequestHandler<GetAllApplicationFormSettingQuery, IEnumerable<ApplicationFormSettingDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllApplicationFormSettingQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<ApplicationFormSettingDTO>> Handle(GetAllApplicationFormSettingQuery request, CancellationToken cancellationToken)
        {
            var ApplicationSetting = await _context.ApplicationFormSetting
                .Select(c => new ApplicationFormSettingDTO
                {
                    Id = c.Id,
                   ApplicationFormSettingName = c.ApplicationFormSettingName,
                   FeeTypeId = c.FeeTypeId,
                   LevelId = c.LevelId,
                   SessionId = c.SessionId,
                    FormCode = c.FormCode,
                    ProgrammeId = c.ProgrammeId,
                    StartDate = c.StartDate,
                    StopDate = c.StopDate
                }).OrderBy(p => p.ApplicationFormSettingName).ToListAsync(cancellationToken);

            return ApplicationSetting;
        }
    }
}
