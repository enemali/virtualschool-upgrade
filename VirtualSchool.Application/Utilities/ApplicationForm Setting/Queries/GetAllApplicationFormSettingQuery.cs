﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Queries
{
   public class GetAllApplicationFormSettingQuery : IRequest<IEnumerable<ApplicationFormSettingDTO>>
    {
    }
}