﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.ApplicationForm_Setting.Models
{
    public class ApplicationFormSettingDTO
    {
        public int Id { get; set; }
        public string ApplicationFormSettingName { get; set; }
        public int FeeTypeId { get; set; }
        public int LevelId { get; set; }
        public int SessionId { get; set; }
        public string FormCode { get; set; }
        public int ProgrammeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
    }
}
