﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentType.Command
{
    public class DeletePaymentTypeCommandHandler: IRequestHandler<DeletePaymentTypeCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeletePaymentTypeCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeletePaymentTypeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.PaymentType.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.PaymentType), request.Id);
                }

                _context.PaymentType.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
