﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.PaymentType.Command
{
    public class DeletePaymentTypeCommand:IRequest
    {
        public int Id { get; set; }
    }
}
