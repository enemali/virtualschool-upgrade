﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.PaymentType.Models
{
    public class PaymentTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
