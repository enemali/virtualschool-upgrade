﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.PaymentType.Models;

namespace VirtualSchool.Application.Utilities.PaymentType.Queries
{
    public class GetAllPaymentTypeQuery : IRequest<IEnumerable<PaymentTypeDTO>>
    {
    }
}
