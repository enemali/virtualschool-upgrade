﻿using MediatR;
using VirtualSchool.Application.Utilities.PaymentType.Models;

namespace VirtualSchool.Application.Utilities.PaymentType.Queries
{
    public class GetPaymentTypeQuery : IRequest<PaymentTypeDTO>
    {
        public int Id { get; set; }
    }
}
