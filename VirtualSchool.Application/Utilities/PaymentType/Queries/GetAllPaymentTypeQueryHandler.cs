﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Level.Models;
using VirtualSchool.Application.Utilities.PaymentType.Models;
using VirtualSchool.Application.Utilities.PaymentType.Queries;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Level.Queries
{
    public class GetAllPaymentTypeQueryHandler : IRequestHandler<GetAllPaymentTypeQuery, IEnumerable<PaymentTypeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllPaymentTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PaymentTypeDTO>> Handle(GetAllPaymentTypeQuery request, CancellationToken cancellationToken)
        {
            var levels = await _context.PaymentType
                .Select(c => new PaymentTypeDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return levels;
        }
    }
}
