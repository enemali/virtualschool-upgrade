﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.PaymentType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentType.Queries
{
    public class GetPaymentTypeQueryHandler : IRequestHandler<GetPaymentTypeQuery,PaymentTypeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetPaymentTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<PaymentTypeDTO> Handle(GetPaymentTypeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentType.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.PaymentType), request.Id);
            }
            return new PaymentTypeDTO
            {
                Id = entity.Id,
                Name = entity.Name,

            };
        }
    }
}
