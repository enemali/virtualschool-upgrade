﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Models
{
    public class LocalGovernmentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string StateName { get; set; }
        public int StateId { get; set; }

    }
}
