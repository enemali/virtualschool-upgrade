﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Command
{
    public class CreateLocalGovernmentCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public int StateId { get; set; }
    }
}
