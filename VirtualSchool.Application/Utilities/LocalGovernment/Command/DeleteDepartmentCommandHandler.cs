﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Command
{
    public class DeleteLocalGovernmentCommandHandler : IRequestHandler<DeleteLocalGovernmentCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteLocalGovernmentCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteLocalGovernmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.LocalGovernment.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.LocalGovernment), request.Id);
            }

            _context.LocalGovernment.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
