﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Command
{
    public class CreateLocalGovernmentCommandHandler : IRequestHandler<CreateLocalGovernmentCommand,int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateLocalGovernmentCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateLocalGovernmentCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.LocalGovernment.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.LocalGovernment), "An entity with the code already exists! Change code");
            }

            var existingEntitySlug = await _context.LocalGovernment.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntitySlug != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.LocalGovernment), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.LocalGovernment
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug,
                stateId = request.StateId
                
            };

            _context.LocalGovernment.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
