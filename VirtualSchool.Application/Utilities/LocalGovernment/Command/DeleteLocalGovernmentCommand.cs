﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Command
{
    public class DeleteLocalGovernmentCommand : IRequest
    {
        public int Id { get; set; }
    }
}
