﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Queries
{
    public class GetLocalGovernmentQueryHandler: IRequestHandler<GetLocalGovernmentQuery, LocalGovernmentDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetLocalGovernmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<LocalGovernmentDTO> Handle(GetLocalGovernmentQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.LocalGovernment.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.LocalGovernment), request.Id);
            }

            return new LocalGovernmentDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                StateId = entity.stateId,
                StateName = entity.State.Name
            };
        }
    }
}
