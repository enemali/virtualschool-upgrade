﻿using MediatR;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Queries
{
    public class GetLocalGovernmentQuery : IRequest<LocalGovernmentDTO>
    {
        public int Id { get; set; }
    }
}
