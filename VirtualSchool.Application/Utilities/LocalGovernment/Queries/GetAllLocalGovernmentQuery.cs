﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Queries
{
    public class GetAllLocalGovernmentQuery : IRequest<IEnumerable<LocalGovernmentDTO>>
    {
    }
}
