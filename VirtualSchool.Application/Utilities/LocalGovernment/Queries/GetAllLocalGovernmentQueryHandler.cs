﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Utilities.Level.Models;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.LocalGovernment.Queries
{
    public class GetAllLocalGovernmentQueryHandler : IRequestHandler<GetAllLocalGovernmentQuery, IEnumerable<LocalGovernmentDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllLocalGovernmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<LocalGovernmentDTO>> Handle(GetAllLocalGovernmentQuery request, CancellationToken cancellationToken)
        {
            var localGovernments = await _context.LocalGovernment
                .Select(c => new LocalGovernmentDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    StateName = c.State.Name,
                    StateId = c.stateId
                    
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return localGovernments;
        }
    }
}
