﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.State.Command
{
    public class DeleteStateCommandHandler: IRequestHandler<DeleteStateCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteStateCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteStateCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.State.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.State), request.Id);
                }

                _context.State.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
