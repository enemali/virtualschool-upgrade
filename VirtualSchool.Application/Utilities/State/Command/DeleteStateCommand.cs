﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.State.Command
{
    public class DeleteStateCommand:IRequest
    {
        public int Id { get; set; }
    }
}
