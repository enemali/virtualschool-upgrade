﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Religion.Models;
using VirtualSchool.Application.Utilities.State.Models;

namespace VirtualSchool.Application.Utilities.State.Queries
{
    public class GetAllStateQuery : IRequest<IEnumerable<StateDTO>>
    {
    }
}
