﻿using MediatR;
using VirtualSchool.Application.Utilities.PaymentMode.Models;

namespace VirtualSchool.Application.Utilities.State.Queries
{
    public class GetStateQuery : IRequest<PaymentModeDTO>
    {
        public int Id { get; set; }
    }
}
