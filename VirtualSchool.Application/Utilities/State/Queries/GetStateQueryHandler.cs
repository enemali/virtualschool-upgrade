﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.PaymenMode.Queries;
using VirtualSchool.Application.Utilities.PaymentMode.Models;
using VirtualSchool.Application.Utilities.Religion.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Religion.Queries
{
    public class GetLocalGovernmentQueryHandler: IRequestHandler<GetReligionQuery, ReligionDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetLocalGovernmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<ReligionDTO> Handle(GetReligionQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Religion.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Religion), request.Id);
            }

            return new ReligionDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
