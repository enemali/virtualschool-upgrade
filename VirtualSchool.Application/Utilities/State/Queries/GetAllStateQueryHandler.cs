﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Religion.Models;
using VirtualSchool.Application.Utilities.State.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.State.Queries
{
    public class GetAllStateQueryHandler : IRequestHandler<GetAllStateQuery, IEnumerable<StateDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllStateQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<StateDTO>> Handle(GetAllStateQuery request, CancellationToken cancellationToken)
        {
            var states = await _context.State
                .Select(c => new StateDTO
                {
                    Id = c.Id,
                    Name = c.Name
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return states;
        }
    }
}
