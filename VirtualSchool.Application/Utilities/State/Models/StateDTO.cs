﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.State.Models
{
    public class StateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
    }
}
