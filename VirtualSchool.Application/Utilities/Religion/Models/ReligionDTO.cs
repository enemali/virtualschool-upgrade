﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Religion.Models
{
    public class ReligionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
