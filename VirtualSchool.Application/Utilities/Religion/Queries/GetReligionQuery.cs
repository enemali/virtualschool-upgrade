﻿using MediatR;
using VirtualSchool.Application.Utilities.Religion.Models;

namespace VirtualSchool.Application.Utilities.Religion.Queries
{
    public class GetReligionQuery : IRequest<ReligionDTO>
    {
        public int Id { get; set; }
    }
}
