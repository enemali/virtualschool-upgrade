﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Religion.Models;

namespace VirtualSchool.Application.Utilities.Religion.Queries
{
    public class GetAllReglionQuery : IRequest<IEnumerable<ReligionDTO>>
    {
    }
}
