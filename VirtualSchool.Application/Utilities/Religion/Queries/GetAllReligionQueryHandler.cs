﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Religion.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Religion.Queries
{
    public class GetAllReligionQueryHandler : IRequestHandler<GetAllReglionQuery, IEnumerable<ReligionDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllReligionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ReligionDTO>> Handle(GetAllReglionQuery request, CancellationToken cancellationToken)
        {
            var religions = await _context.Religion
                .Select(c => new ReligionDTO
                {
                    Id = c.Id,
                    Name = c.Name
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return religions;
        }
    }
}
