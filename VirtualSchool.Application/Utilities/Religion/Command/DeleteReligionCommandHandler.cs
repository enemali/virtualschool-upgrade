﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Religion.Command
{
    public class DeleteDeleteReligionCommandHandler: IRequestHandler<DeleteReligionCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteDeleteReligionCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteReligionCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Religion.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Religion), request.Id);
                }

                _context.Religion.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
