﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Religion.Command
{
    public class DeleteReligionCommand:IRequest
    {
        public int Id { get; set; }

    }
}
