﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries
{
    public class GetAllProgrammeDepartmentQuery : IRequest<IEnumerable<ProgrammeDepartmentDTO>>
    {
    }
}
