﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries
{
    public class GetProgrammeDepartmentByProgrammeQuery : IRequest<List<ProgrammeDepartmentDTO>>
    {
        public int ProgrammeId { get; set; }
    }
}
