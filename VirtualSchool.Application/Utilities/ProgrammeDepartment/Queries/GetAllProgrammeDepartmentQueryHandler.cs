﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.PaymentType.Models;
using VirtualSchool.Application.Utilities.PaymentType.Queries;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Level.Queries
{
    public class GetAllProgrammeDeoartmentQueryHandler : IRequestHandler<GetAllProgrammeDepartmentQuery, IEnumerable<ProgrammeDepartmentDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllProgrammeDeoartmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ProgrammeDepartmentDTO>> Handle(GetAllProgrammeDepartmentQuery request, CancellationToken cancellationToken)
        {
            var programmeDepartments = await _context.ProgrammeDepartment
                .Select(c => new ProgrammeDepartmentDTO
                {
                    ProgrammeId = c.ProgrammeId,
                    ProgrammeName = c.Programme.Name,
                    DepartmentId = c.DepartmentId,
                    DepartmentName = c.Department.Name

                }).OrderBy(p => p.ProgrammeName).ToListAsync(cancellationToken);

            return programmeDepartments;
        }
    }
}
