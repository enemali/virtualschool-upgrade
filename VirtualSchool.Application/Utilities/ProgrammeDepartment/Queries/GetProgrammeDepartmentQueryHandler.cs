﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries
{
    public class GetProgammeDeparmentQueryHandler : IRequestHandler<GetProgrammeDepartmentQuery,ProgrammeDepartmentDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetProgammeDeparmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<ProgrammeDepartmentDTO> Handle(GetProgrammeDepartmentQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.ProgrammeDepartment.Where(p => p.ProgrammeId == request.ProgrammeId).Include(p => p.Programme).Include(p => p.Department).FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.ProgrammeDepartment), request.ProgrammeId);
            }

            return new ProgrammeDepartmentDTO
            {
                ProgrammeId = entity.ProgrammeId,
                ProgrammeName = entity.Programme.Name,
                DepartmentId = entity.DepartmentId,
                DepartmentName = entity.Department.Name
            };
        }
    }
}
