﻿using MediatR;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries
{
    public class GetProgrammeDepartmentQuery : IRequest<ProgrammeDepartmentDTO>
    {
        public int ProgrammeId { get; set; }
    }
}
