﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.ProgrammeDepartment.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Queries
{
    public class GetProgrammeDepartmentByProgrammeQueryHandler : IRequestHandler<GetProgrammeDepartmentByProgrammeQuery, List<ProgrammeDepartmentDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetProgrammeDepartmentByProgrammeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<List<ProgrammeDepartmentDTO>> Handle(GetProgrammeDepartmentByProgrammeQuery request, CancellationToken cancellationToken)
        {
         

            var programmeDepartments = await _context.ProgrammeDepartment.Where(s => s.ProgrammeId == request.ProgrammeId)
                .Select(c => new ProgrammeDepartmentDTO
                {
                    ProgrammeId = c.ProgrammeId,
                    ProgrammeName = c.Programme.Name,
                    DepartmentId = c.DepartmentId,
                    DepartmentName = c.Department.Name

                }).OrderBy(p => p.ProgrammeName).ToListAsync(cancellationToken);

            if (programmeDepartments == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.ProgrammeDepartment), request.ProgrammeId);
            }
            return programmeDepartments;
        }
        
    }
}
