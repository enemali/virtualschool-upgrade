﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Command
{
    public class UpdateProgrammeDepartmentCommandHandler : IRequestHandler<UpdateProgrammeDepartmentCommand,Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateProgrammeDepartmentCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateProgrammeDepartmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.ProgrammeDepartment.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.ProgrammeDepartment), request.Id);
            }
            
            entity.ProgrammeId = request.ProgrammeId;
            entity.DepartmentId = request.DepartmentId;
            entity.Active = request.Active;

            await _context.SaveChangesAsync(cancellationToken);


            return Unit.Value;
        }
    }
}
