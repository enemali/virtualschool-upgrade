﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Command
{
    public class DeleteProgrammeDepartmentCommandHandler: IRequestHandler<DeleteProgrammeDepartmentCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteProgrammeDepartmentCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteProgrammeDepartmentCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.ProgrammeDepartment.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ProgrammeDepartment), request.Id);
                }

                _context.ProgrammeDepartment.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
