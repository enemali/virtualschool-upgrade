﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Command
{
    public class DeleteProgrammeDepartmentCommand:IRequest
    {
        public int Id { get; set; }
    }
}
