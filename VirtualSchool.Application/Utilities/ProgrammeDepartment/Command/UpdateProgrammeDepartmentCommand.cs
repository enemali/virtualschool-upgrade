﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Command
{
    public  class UpdateProgrammeDepartmentCommand :IRequest
    {
        public int Id { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public bool Active { get; set; }
    }

}

