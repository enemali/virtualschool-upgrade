﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.ProgrammeDepartment.Command
{
    public class CreateProgrammeDepartmentCommandHandler: IRequestHandler<CreateProgrammeDepartmentCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateProgrammeDepartmentCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateProgrammeDepartmentCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await _context.ProgrammeDepartment.Where(a => a.ProgrammeId == request.ProgrammeId ).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.ProgrammeDepartment), "An entity with the Department already exists! ");
            }

            var existingEntity2 = await _context.ProgrammeDepartment.Where(a => a.DepartmentId == request.DepartmentId).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.ProgrammeDepartment), "An entity with the Programme exists!");
            }

            var entity = new VirtualSchool.Domain.Entities.ProgrammeDepartment
            {
                ProgrammeId = request.ProgrammeId,
                DepartmentId = request.DepartmentId

            };

            _context.ProgrammeDepartment.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.ProgrammeId;
        }
    }
}
