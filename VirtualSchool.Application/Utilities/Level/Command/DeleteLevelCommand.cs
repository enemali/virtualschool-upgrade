﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Level.Command
{
    public class DeleteLevelCommand:IRequest
    {
        public int Id { get; set; }
    }
}
