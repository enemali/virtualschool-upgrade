﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Level.Command
{
    public class DeleteLevelCommandHandler: IRequestHandler<DeleteLevelCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteLevelCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteLevelCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Level.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Level), request.Id);
                }

                _context.Level.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
