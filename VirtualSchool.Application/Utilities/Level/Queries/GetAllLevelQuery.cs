﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Level.Models;

namespace VirtualSchool.Application.Utilities.Level.Queries
{
    public class GetAllLevelQuery : IRequest<IEnumerable<LevelDTO>>
    {
    }
}
