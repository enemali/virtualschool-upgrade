﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Utilities.Level.Models;
using VirtualSchool.Application.Utilities.Level.Queries;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetLevelQueryHandler: IRequestHandler<GetLevelQuery,LevelDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetLevelQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<LevelDTO> Handle(GetLevelQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Level.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Level), request.Id);
            }

            return new LevelDTO
            {
                Id = entity.Id,
                Name = entity.Name
                
            };
        }
    }
}
