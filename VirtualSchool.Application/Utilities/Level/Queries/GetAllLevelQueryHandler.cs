﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Utilities.Level.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Level.Queries
{
    public class GetAllLevelQueryHandler : IRequestHandler<GetAllLevelQuery, IEnumerable<LevelDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllLevelQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<LevelDTO>> Handle(GetAllLevelQuery request, CancellationToken cancellationToken)
        {
            var levels = await _context.Level
                .Select(c => new LevelDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return levels;
        }
    }
}
