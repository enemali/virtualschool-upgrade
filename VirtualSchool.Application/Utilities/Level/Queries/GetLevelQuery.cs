﻿using MediatR;
using VirtualSchool.Application.Utilities.Level.Models;

namespace VirtualSchool.Application.Utilities.Level.Queries
{
    public class GetLevelQuery : IRequest<LevelDTO>
    {
        public int Id { get; set; }
    }
}
