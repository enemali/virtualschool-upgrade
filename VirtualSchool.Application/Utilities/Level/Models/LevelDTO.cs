﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Level.Models
{
    public class LevelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
