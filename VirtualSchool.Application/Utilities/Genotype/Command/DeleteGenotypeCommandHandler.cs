﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Genotype.Command
{
    public class DeleteGenotypeCommandHandler: IRequestHandler<DeleteGenotypeCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteGenotypeCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteGenotypeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Genotype.FindAsync(request.Id);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Genotype), request.Id);
                }

                _context.Genotype.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
