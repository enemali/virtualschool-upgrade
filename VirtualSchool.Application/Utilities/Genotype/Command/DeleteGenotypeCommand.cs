﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Genotype.Command
{
    public class DeleteGenotypeCommand:IRequest
    {
        public int Id { get; set; }
    }
}

