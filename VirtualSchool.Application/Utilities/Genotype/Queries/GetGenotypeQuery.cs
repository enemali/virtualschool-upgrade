﻿using MediatR;
using VirtualSchool.Application.Utilities.Genotype.Models;

namespace VirtualSchool.Application.Utilities.Genotype.Queries
{
    public class GetGenotypeQuery : IRequest<GenotypeDTO>
    {
        public int Id { get; set; }
    }
}
