﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.Genotype.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Genotype.Queries
{
    public class GetAllGenotypeQueryHandler : IRequestHandler<GetAllGenotypeQuery, IEnumerable<GenotypeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllGenotypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GenotypeDTO>> Handle(GetAllGenotypeQuery request, CancellationToken cancellationToken)
        {
            var genotypes = await _context.Genotype
                .Select(c => new GenotypeDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return genotypes;
        }
    }
}
