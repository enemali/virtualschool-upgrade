﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Genotype.Models;

namespace VirtualSchool.Application.Utilities.Genotype.Queries
{
    public class GetAllGenotypeQuery : IRequest<IEnumerable<GenotypeDTO>>
    {
    }
}
