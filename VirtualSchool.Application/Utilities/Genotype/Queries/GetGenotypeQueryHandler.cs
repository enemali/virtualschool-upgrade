﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Genotype.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Genotype.Queries
{
    public class GetGenotypeQueryHandler: IRequestHandler<GetGenotypeQuery, GenotypeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetGenotypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<GenotypeDTO> Handle(GetGenotypeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Genotype.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Genotype), request.Id);
            }

            return new GenotypeDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
