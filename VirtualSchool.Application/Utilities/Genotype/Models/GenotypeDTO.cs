﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Genotype.Models
{
    public class GenotypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
