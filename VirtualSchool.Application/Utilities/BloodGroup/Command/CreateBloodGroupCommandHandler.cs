﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.BloodGroup.Command
{
    public class CreateGroupCommandHandler: IRequestHandler<CreateBloodGroupCommand,int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateGroupCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateBloodGroupCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.BloodGroup.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.BloodGroup), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.BloodGroup.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.BloodGroup), "An entity with the Name already exists! Change Name");
            }

            var entity = new VirtualSchool.Domain.Entities.BloodGroup
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };

            _context.BloodGroup.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
