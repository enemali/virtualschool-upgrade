﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.BloodGroup.Command
{
    public class DeleteBloodGroupCommand:IRequest
    {
        public int Id { get; set; }
    }
}
