﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.BloodGroup.Command
{
    public class DeleteBloodGroupCommandHandler: IRequestHandler<DeleteBloodGroupCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteBloodGroupCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteBloodGroupCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.BloodGroup.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.BloodGroup), request.Id);
                }

                _context.BloodGroup.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
