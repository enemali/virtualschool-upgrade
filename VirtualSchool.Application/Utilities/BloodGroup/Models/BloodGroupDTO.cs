﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.BloodGroup.Models
{
    public class BloodGroupDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
