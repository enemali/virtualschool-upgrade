﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.BloodGroup.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.BloodGroup.Queries
{
    public class GetAllBloodGroupQueryHandler : IRequestHandler<GetAllBloodGroupQuery, IEnumerable<BloodGroupDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllBloodGroupQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<BloodGroupDTO>> Handle(GetAllBloodGroupQuery request, CancellationToken cancellationToken)
        {
            var bloodGroups = await _context.BloodGroup
                .Select(c => new BloodGroupDTO
             {
                 Id = c.Id,
                 Name = c.Name
             }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return bloodGroups;
        }

    
    }
}
