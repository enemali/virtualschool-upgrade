﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.BloodGroup.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.BloodGroup.Queries
{
    public class GetBloodGroupQueryHandler: IRequestHandler<GetBloodGroupQuery,BloodGroupDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetBloodGroupQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<BloodGroupDTO> Handle(GetBloodGroupQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.BloodGroup.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.BloodGroup), request.Id);
            }

            return new BloodGroupDTO
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
    }
}
