﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.BloodGroup.Models;

namespace VirtualSchool.Application.Utilities.BloodGroup.Queries
{
    public class GetAllBloodGroupQuery : IRequest<IEnumerable<BloodGroupDTO>>
    {

    }
}
