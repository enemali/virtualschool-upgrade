﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Gender.Models
{
    public class GenderDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
