﻿using MediatR;
using VirtualSchool.Application.Utilities.Gender.Models;

namespace VirtualSchool.Application.Utilities.Gender.Queries
{
    public class GetGenderQuery : IRequest<GenderDTO>
    {
        public int Id { get; set; }
    }
}
