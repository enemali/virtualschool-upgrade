﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Gender.Models;

namespace VirtualSchool.Application.Utilities.Gender.Queries
{
    public class GetAllGenderQuery : IRequest<IEnumerable<GenderDTO>>
    {
    }
}
