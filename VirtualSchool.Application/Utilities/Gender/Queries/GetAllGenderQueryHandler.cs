﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Gender.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Gender.Queries
{
    public class GetAllGenderQueryHandler : IRequestHandler<GetAllGenderQuery, IEnumerable<GenderDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllGenderQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GenderDTO>> Handle(GetAllGenderQuery request, CancellationToken cancellationToken)
        {
            var genders = await _context.Gender
                .Select(c => new GenderDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return genders;
        }
    }
}
