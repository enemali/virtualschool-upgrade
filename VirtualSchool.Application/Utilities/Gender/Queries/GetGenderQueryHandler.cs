﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.Gender.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Gender.Queries
{
    public class GetGenderQueryHandler: IRequestHandler<GetGenderQuery, GenderDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetGenderQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<GenderDTO> Handle(GetGenderQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Gender.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Gender), request.Id);
            }

            return new GenderDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
