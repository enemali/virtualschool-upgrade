﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Gender.Command
{
    public class DeleteGenderCommandHandler: IRequestHandler<DeleteGenderCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteGenderCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteGenderCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Gender.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Gender), request.Id);
                }

                _context.Gender.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
