﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Gender.Command
{
    public class CreateGenderCommandHandler: IRequestHandler<CreateGenderCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateGenderCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateGenderCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.Gender.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Gender), "An entity with the code already exists! Change code");
            }
            var existingEntitySlug = await _context.Gender.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntitySlug != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Gender), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.Gender
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };
            _context.Gender.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
