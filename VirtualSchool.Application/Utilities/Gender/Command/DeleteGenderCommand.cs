﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Gender.Command
{
    public class DeleteGenderCommand:IRequest
    {
        public int Id { get; set; }
    }
}
