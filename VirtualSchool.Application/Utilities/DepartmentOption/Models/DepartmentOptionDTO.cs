﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Models
{
    public class DepartmentOptionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortCode { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
