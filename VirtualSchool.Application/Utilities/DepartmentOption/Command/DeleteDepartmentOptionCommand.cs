﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Command
{
    public class DeleteDepartmentOptionCommand: IRequest
    {
        public int Id { get; set; }
    }
}
