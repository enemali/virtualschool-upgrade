﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Command;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Command
{
    public class CreateDepartmentOptionCommandHandler : IRequestHandler<CreateDepartmentOptionCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateDepartmentOptionCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateDepartmentOptionCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.DepartmentOption.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.DepartmentOption), "An entity with the code already exists! Change code");
            }

            var existingEntitySlug = await _context.DepartmentOption.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntitySlug != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.DepartmentOption), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.DepartmentOption
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug,
                DepartmentId = request.DepartmentId

            };
            _context.DepartmentOption.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
