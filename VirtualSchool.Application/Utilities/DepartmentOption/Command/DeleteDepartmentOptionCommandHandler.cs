﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Command;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Command
{
    public class DeleteDepartmentOptionCommandHandler : IRequest<DeleteDepartmentOptionCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteDepartmentOptionCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteDepartmentOptionCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.DepartmentOption.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.DepartmentOption), request.Id);
            }

            _context.DepartmentOption.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
