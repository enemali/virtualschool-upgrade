﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.DepartmentOption.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Queries
{
    public class GetAllDepartmentOptionQueryHandler : IRequestHandler<GetAllDepartmentOtionQuery, IEnumerable<DepartmentOptionDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllDepartmentOptionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<DepartmentOptionDTO>> Handle(GetAllDepartmentOtionQuery request, CancellationToken cancellationToken)
        {
            var departmentOptions = await _context.DepartmentOption
                .Select(c => new DepartmentOptionDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    ShortCode = c.Code,
                    DepartmentName = c.Department.Name,
                    DepartmentId = c.DepartmentId

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return departmentOptions;
        }
    }
}
