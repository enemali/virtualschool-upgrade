﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.DepartmentOption.Models;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Queries
{
    public class GetDepartmentOptionQuery : IRequest<DepartmentOptionDTO>
    {
        public int Id { get; set; }
    }
}
