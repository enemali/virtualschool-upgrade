﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.DepartmentOption.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.DepartmentOption.Queries
{
    public class GetDepartmenOptiontQueryHandler : IRequestHandler<GetDepartmentOptionQuery, DepartmentOptionDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetDepartmenOptiontQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<DepartmentOptionDTO> Handle(GetDepartmentOptionQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.DepartmentOption.Where(a => a.Id == request.Id).Include(a => a.Department).FirstOrDefaultAsync();


            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.DepartmentOption), request.Id);
            }

            return new DepartmentOptionDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortCode = entity.Code,
                DepartmentId = entity.DepartmentId,
                DepartmentName = entity.Department.Name

            };
        }
    }
}
