﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Grade.Command
{
    public class DeleteGradeCommandHandler: IRequestHandler<DeleteGradeCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteGradeCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteGradeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.OlevelGrade.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
                }

                _context.OlevelGrade.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
