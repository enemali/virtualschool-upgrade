﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Grade.Command
{
    public class UpdateGradeCommandHandler:IRequestHandler<UpdateGradeCommand,Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateGradeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateGradeCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);

            var entity = await _context.OlevelGrade.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
            }

            var existingEntity = await _context.OlevelGrade.Where(a => a.Code == request.Code && a.Id != request.Id).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Country), "An entity with the code already exists! Change code");
            }

            var existingEntitySlug = await _context.OlevelGrade.Where(a => a.Slug == requestSlug && a.Id != request.Id).FirstOrDefaultAsync(cancellationToken);
            if (existingEntitySlug != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Country), "An entity with the Name already exists! Change Name");
            }

            entity.Active = request.Active;
            entity.Code = request.Code;
            entity.Description = request.Description;
            entity.Name = request.Name;
            entity.Slug = requestSlug;

            await _context.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
        
    }
}
