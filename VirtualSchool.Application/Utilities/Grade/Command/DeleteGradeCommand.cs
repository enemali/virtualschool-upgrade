﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Grade.Command
{
    public class DeleteGradeCommand:IRequest
    {
        public int Id { get; set; }
    }
}
