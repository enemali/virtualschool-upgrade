﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Grade.Models;

namespace VirtualSchool.Application.Utilities.Grade.Queries
{
    public class GetAllGradeQuery : IRequest<IEnumerable<GradeDTO>>
    {

    }
}
