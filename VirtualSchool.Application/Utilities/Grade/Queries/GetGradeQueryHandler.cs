﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Grade.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Grade.Queries
{
    public class GetGradeQueryHandler: IRequestHandler<GetGradeQuery,GradeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetGradeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<GradeDTO> Handle(GetGradeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.OlevelGrade.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
            }

            return new GradeDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Slug = entity.Slug,
                Code = entity.Code
            };
        }
    }
}
