﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Grade.Models;

namespace VirtualSchool.Application.Utilities.Grade.Queries
{
    public class GetGradeQuery : IRequest<GradeDTO>
    {
        public int Id { get; set; }
    }
}
