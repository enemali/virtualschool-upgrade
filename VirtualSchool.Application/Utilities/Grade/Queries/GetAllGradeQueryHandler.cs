﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Grade.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Grade.Queries
{
    public class GetAllGradeQueryHandler : IRequestHandler<GetAllGradeQuery, IEnumerable<GradeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllGradeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GradeDTO>> Handle(GetAllGradeQuery request, CancellationToken cancellationToken)
        {
            var grades = await _context.OlevelGrade
                .Select(c => new GradeDTO
                {
                 Id = c.Id,
                 Name = c.Name,
                Description = c.Description,
                Slug = c.Slug,
                Code = c.Code
             }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return grades;
        }

    
    }
}
