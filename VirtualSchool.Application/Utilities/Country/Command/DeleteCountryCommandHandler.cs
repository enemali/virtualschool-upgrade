﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Country.Command
{
    public class DeleteCountryCommandHandler : IRequestHandler<DeleteCountryCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeleteCountryCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteCountryCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Country.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
                }

                _context.Country.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
