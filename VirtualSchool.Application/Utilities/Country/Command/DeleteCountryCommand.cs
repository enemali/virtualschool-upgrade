﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Country.Command
{
    public class DeleteCountryCommand:IRequest
    {
        public int Id { get; set; }
    }
}
