﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Country.Models;

namespace VirtualSchool.Application.Utilities.Country.Queries
{
    public class GetAllCountryQuery : IRequest<IEnumerable<CountryDTO>>
    {

    }
}
