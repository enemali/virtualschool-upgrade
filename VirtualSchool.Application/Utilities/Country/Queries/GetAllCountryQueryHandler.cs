﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Country.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Country.Queries
{
    public class GetAllCountryQueryHandler : IRequestHandler<GetAllCountryQuery, IEnumerable<CountryDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllCountryQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CountryDTO>> Handle(GetAllCountryQuery request, CancellationToken cancellationToken)
        {
            var countries = await _context.Country
                .Select(c => new CountryDTO
                {
                 Id = c.Id,
                 Name = c.Name,
                 Code = c.Code
             }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return countries;
        }

    
    }
}
