﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Country.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Country.Queries
{
    public class GetCountryQueryHandler : IRequestHandler<GetCountryQuery, CountryDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetCountryQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<CountryDTO> Handle(GetCountryQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Country.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
            }

            return new CountryDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Code = entity.Code
            };
        }
    }
}
