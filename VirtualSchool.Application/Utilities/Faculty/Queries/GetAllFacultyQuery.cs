﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.Faculty.Models;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetAllFacultyQuery : IRequest<IEnumerable<FacultyDTO>>
    {
    }
}
