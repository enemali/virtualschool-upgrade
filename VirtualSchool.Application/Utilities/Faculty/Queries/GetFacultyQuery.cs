﻿using MediatR;
using VirtualSchool.Application.Utilities.Faculty.Models;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetFacultyQuery : IRequest<FacultyDTO>
    {
        public int Id { get; set; }
    }
}
