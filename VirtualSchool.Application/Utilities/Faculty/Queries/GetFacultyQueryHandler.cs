﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetFacultyQueryHandler: IRequestHandler<GetFacultyQuery,FacultyDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetFacultyQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<FacultyDTO> Handle(GetFacultyQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Faculty.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Faculty), request.Id);
            }

            return new FacultyDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortCode = entity.Code
                
            };
        }
    }
}
