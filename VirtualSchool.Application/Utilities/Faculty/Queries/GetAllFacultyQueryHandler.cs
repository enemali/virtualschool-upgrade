﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetAllFacultyQueryHandler : IRequestHandler<GetAllFacultyQuery, IEnumerable<FacultyDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllFacultyQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<FacultyDTO>> Handle(GetAllFacultyQuery request, CancellationToken cancellationToken)
        {
            var faculties = await _context.Faculty
                .Select(c => new FacultyDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    ShortCode = c.Code,

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return faculties;
        }
    }
}
