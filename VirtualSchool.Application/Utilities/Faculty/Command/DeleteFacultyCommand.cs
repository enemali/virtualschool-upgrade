﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Faculty.Command
{
    public class DeleteFacultyCommand : IRequest
    {
        public int Id { get; set; }
    }
}
