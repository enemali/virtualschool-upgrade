﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Subject.Models;

namespace VirtualSchool.Application.Utilities.Subject.Queries
{
    public class GetAllSubjectQuery : IRequest<IEnumerable<SubjectDTO>>
    {

    }
}
