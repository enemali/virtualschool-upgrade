﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Subject.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Subject.Queries
{
    public class GetAllSubjectQueryHandler : IRequestHandler<GetAllSubjectQuery, IEnumerable<SubjectDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllSubjectQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SubjectDTO>> Handle(GetAllSubjectQuery request, CancellationToken cancellationToken)
        {
            var subjects = await _context.OlevelSubject
                .Select(c => new SubjectDTO
                {
                 Id = c.Id,
                 Name = c.Name,
                Description = c.Description,
                Slug = c.Slug,
                Code = c.Code
             }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return subjects;
        }


    }
}
