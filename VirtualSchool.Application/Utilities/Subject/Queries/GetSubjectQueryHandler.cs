﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Subject.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Subject.Queries
{
    public class GetSubjectQueryHandler: IRequestHandler<GetSubjectQuery,SubjectDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetSubjectQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<SubjectDTO> Handle(GetSubjectQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.OlevelSubject.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
            }

            return new SubjectDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Slug = entity.Slug,
                Code = entity.Code
            };
        }
    }
}
