﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Subject.Command
{
    public class DeleteSubjectCommand:IRequest
    {
        public int Id { get; set; }
    }
}
