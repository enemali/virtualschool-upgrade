﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Subject.Command
{
    public class DeleteSubjectCommandHandler: IRequestHandler<DeleteSubjectCommand>
    {
            private readonly VirtualSchoolDbContext _context;

            public DeleteSubjectCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteSubjectCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.OlevelSubject.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Country), request.Id);
                }

                _context.OlevelSubject.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }

    }
}
