﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentMode.Command
{
    public class DeletePaymetModeCommandHandler: IRequestHandler<DeletePaymentModeCommand>
    {
            private readonly VirtualSchoolDbContext _context;
     
            public DeletePaymetModeCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeletePaymentModeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.PaymentMode.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.PaymentMode), request.Id);
                }

                _context.PaymentMode.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        
    }
}
