﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentMode.Command
{
    public class CreatePaymentModeCommandHandler: IRequestHandler<CreatePaymentModeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreatePaymentModeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreatePaymentModeCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.PaymentMode.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.PaymentMode), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.PaymentMode.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.PaymentMode), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.PaymentMode
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };

            _context.PaymentMode.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
