﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.PaymentMode.Command
{
    public class DeletePaymentModeCommand:IRequest
    {
        public int Id { get; set; }
    }
}
