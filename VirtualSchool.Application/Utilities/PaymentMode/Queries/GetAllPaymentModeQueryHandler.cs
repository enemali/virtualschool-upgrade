﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.PaymentMode.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentMode.Queries
{
    public class GetAllPaymentModeQueryHandler : IRequestHandler<GetAllPaymentModeQuery, IEnumerable<PaymentModeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllPaymentModeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PaymentModeDTO>> Handle(GetAllPaymentModeQuery request, CancellationToken cancellationToken)
        {
            var paymentModes = await _context.PaymentMode
                .Select(c => new PaymentModeDTO
                {
                    Id = c.Id,
                    Name = c.Name
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return paymentModes;
        }
    }
}
