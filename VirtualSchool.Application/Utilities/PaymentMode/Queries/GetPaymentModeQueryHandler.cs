﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.LocalGovernment.Models;
using VirtualSchool.Application.Utilities.PaymenMode.Queries;
using VirtualSchool.Application.Utilities.PaymentMode.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.PaymentMode.Queries
{
    public class GetLocalGovernmentQueryHandler: IRequestHandler<GetPaymentModeQuery, PaymentModeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetLocalGovernmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<PaymentModeDTO> Handle(GetPaymentModeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentMode.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.PaymentMode), request.Id);
            }

            return new PaymentModeDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
