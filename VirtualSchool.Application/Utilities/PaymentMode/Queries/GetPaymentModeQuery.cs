﻿using MediatR;
using VirtualSchool.Application.Utilities.PaymentMode.Models;

namespace VirtualSchool.Application.Utilities.PaymenMode.Queries
{
    public class GetPaymentModeQuery : IRequest<PaymentModeDTO>
    {
        public int Id { get; set; }
    }
}
