﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.PaymentMode.Models;

namespace VirtualSchool.Application.Utilities.PaymentMode.Queries
{
    public class GetAllPaymentModeQuery : IRequest<IEnumerable<PaymentModeDTO>>
    {
    }
}
