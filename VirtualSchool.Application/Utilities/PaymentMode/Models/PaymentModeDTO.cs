﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.PaymentMode.Models
{
    public class PaymentModeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
