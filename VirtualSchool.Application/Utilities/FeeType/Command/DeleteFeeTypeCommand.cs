﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.FeeType.Command
{
    public class DeleteFeeTypeCommand:IRequest
    {
        public int Id { get; set; }
    }
}

