﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.FeeType.Command
{
    public  class UpdateFeeTypeCommand :IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

    }
}
