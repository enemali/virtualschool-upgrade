﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.FeeType.Command
{
    public class CreateFeeTypeCommandHandler: IRequestHandler<CreateFeeTypeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateFeeTypeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateFeeTypeCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.FeeType.Where(a => a.Code == request.Code).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.FeeType), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.FeeType.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.FeeType), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.FeeType
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };

            _context.FeeType.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
