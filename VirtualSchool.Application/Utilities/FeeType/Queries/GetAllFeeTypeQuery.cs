﻿using MediatR;
using System.Collections.Generic;
using VirtualSchool.Application.Utilities.FeeType.Models;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetAllFeeTypeQuery : IRequest<IEnumerable<FeeTypeDTO>>
    {

    }
}
