﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetFeeTypeQueryHandler: IRequestHandler<GetFeeTypeQuery, FeeTypeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetFeeTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<FeeTypeDTO> Handle(GetFeeTypeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.FeeType.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.FeeType), request.Id);
            }

            return new FeeTypeDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                
            };
        }
    }
}
