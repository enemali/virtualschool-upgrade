﻿using MediatR;
using VirtualSchool.Application.Utilities.FeeType.Models;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetFeeTypeQuery : IRequest<FeeTypeDTO>
    {
        public int Id { get; set; }
    }
}
