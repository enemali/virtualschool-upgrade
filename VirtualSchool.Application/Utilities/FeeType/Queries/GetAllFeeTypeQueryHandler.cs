﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetAllFeeTypeQueryHandler : IRequestHandler<GetAllFeeTypeQuery, IEnumerable<FeeTypeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllFeeTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<FeeTypeDTO>> Handle(GetAllFeeTypeQuery request, CancellationToken cancellationToken)
        {
            var feeTypes = await _context.FeeType
                .Select(c => new FeeTypeDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return feeTypes;
        }

    }
}
