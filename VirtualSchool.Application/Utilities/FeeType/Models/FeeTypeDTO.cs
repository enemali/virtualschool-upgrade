﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.FeeType.Models
{
    public class FeeTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
