﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.FeeType.Command
{
    public class DeleteDepartmentCommand : IRequest
    {
        public int Id { get; set; }
    }
}
