﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Application.Utilities.FeeType.Queries;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Faculty.Queries
{
    public class GetAllDepartmentQueryHandler : IRequestHandler<GetAllDepartmentQuery, IEnumerable<DepartmentDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllDepartmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<DepartmentDTO>> Handle(GetAllDepartmentQuery request, CancellationToken cancellationToken)
        {
            var departments = await _context.Department
                .Select(c => new DepartmentDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    ShortCode = c.Code,
                    FacultyId = c.FacultyId,
                    FacultyName = c.Faculty.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return departments;
        }
    }
}
