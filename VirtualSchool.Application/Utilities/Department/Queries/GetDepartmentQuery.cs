﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.FeeType.Models;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetDepartmentQuery : IRequest<DepartmentDTO>
    {
        public int Id { get; set; }
    }
}
