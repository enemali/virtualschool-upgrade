﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.FeeType.Queries
{
    public class GetDepartmentQueryHandler: IRequestHandler<GetDepartmentQuery,DepartmentDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetDepartmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<DepartmentDTO> Handle(GetDepartmentQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Department.Where( a => a.Id == request.Id).Include(a => a.Faculty).FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Department), request.Id);
            }


            return new DepartmentDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                ShortCode = entity.Code,
                 FacultyId = entity.Faculty.Id,
                FacultyName = entity.Faculty.Name

            };
        }
    }
}
