﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Course.Models
{
    public class CourseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ProgrammeName { get; set; }
        public string DepartmentName { get; set; }
        public string LevelName { get; set; }
        public string DepartmentOptionName { get; set; }
        public string SemesterName { get; set; }
    }
}
