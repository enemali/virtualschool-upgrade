﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Course.Command
{
    public class UpdateCourseCommandHandler : IRequestHandler<UpdateCourseCommand, Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateCourseCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateCourseCommand request, CancellationToken cancellationToken)
        {

            var entity = await _context.Course.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Course), request.Id);
            }

            var existingEntity = await _context.Course.Where(a => a.Code == request.Code && a.DepartmentId == request.DepartmentId && a.ProgrammeId == request.ProgrammeId && a.LevelId == request.LevelId && a.SemesterId == request.SemesterId && a.Id  != request.Id).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Country), "An entity with the code already exists! Change code");
            }

            entity.Activated = request.Activated;
            entity.Code = request.Code;
            entity.DepartmentId = request.DepartmentId;
            entity.Name = request.Name;
            entity.ProgrammeId = request.ProgrammeId;
            entity.CourseTypeId=request.CourseTypeId;
            entity.DepartmentOptionId = request.DepartmentOptionId;
            entity.LevelId = request.LevelId;
            entity.SemesterId = request.SemesterId;
            entity.SessionId = request.SessionId;
            entity.Unit = request.Unit;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

    }
}
