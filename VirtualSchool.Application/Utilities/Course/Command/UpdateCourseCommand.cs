﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Utilities.Course.Command
{
    public class UpdateCourseCommand:IRequest
    {
        public int Id { get; set; }
        public int CourseTypeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int? DepartmentOptionId { get; set; }
        public int Unit { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public bool Activated { get; set; }
    }

}
