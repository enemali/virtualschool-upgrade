﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Course.Command
{
    public class CreateCourseCommandHandler : IRequestHandler<CreateCourseCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateCourseCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateCourseCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await _context.Course.Where(a => a.Code == request.Code && a.DepartmentId==request.DepartmentId && a.ProgrammeId==request.ProgrammeId && a.LevelId==request.LevelId && a.SemesterId==request.SemesterId).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.Country), "An entity with the code already exists! Change code");
            }

            var entity = new Domain.Entities.Course
            {
                Name = request.Name,
                Code = request.Code,
                DepartmentId = request.DepartmentId,
                ProgrammeId = request.ProgrammeId,
                LevelId=request.LevelId,
                DepartmentOptionId=request.DepartmentOptionId,
                Activated=true,
                CourseTypeId=request.CourseTypeId,
                SemesterId=request.SemesterId,
                Unit=request.Unit,
                SessionId=request.SessionId
               
            };

            _context.Course.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
