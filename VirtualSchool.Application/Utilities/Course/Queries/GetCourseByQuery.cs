﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Course.Models;

namespace VirtualSchool.Application.Utilities.Course.Queries
{
    public class GetCourseByQuery : IRequest<IEnumerable<CourseDTO>>
    {
        public int programmeId { get; set; }
        public int departmentId { get; set; }
        public int levelId { get; set; }
        public int semesterId { get; set; }
        public int optionId { get; set; }
    }
}
