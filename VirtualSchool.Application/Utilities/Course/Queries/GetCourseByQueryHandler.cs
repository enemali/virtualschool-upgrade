﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Course.Models;
using VirtualSchool.Persistence;
using System.Linq;

namespace VirtualSchool.Application.Utilities.Course.Queries
{
    public class GetCourseByQueryHandler : IRequestHandler<GetCourseByQuery, IEnumerable<CourseDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetCourseByQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }


        public async Task<IEnumerable<CourseDTO>> Handle(GetCourseByQuery request, CancellationToken cancellationToken)
        {
            List<CourseDTO> courses=null;
            if (request.optionId <= 0)
            {
                courses = await _context.Course.Where(f => f.DepartmentId == request.departmentId && f.ProgrammeId == request.programmeId && f.SemesterId == request.semesterId && f.LevelId == request.levelId)
                    .Include(f => f.Programme)
                    .Include(f => f.Department)
                    .Include(f => f.DepartmentOption)
                    .Include(f => f.Level)
                    //.Include(f => f.Semester)
                .Select(c => new CourseDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    Code = c.Code,
                    ProgrammeName = c.Programme.Name,
                    DepartmentName = c.Department.Name,
                    //SemesterName = c.Semester.Name,
                    LevelName = c.Level.Name,
                    DepartmentOptionName = c.DepartmentOption.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);
            }
            else
            {
                courses = await _context.Course.Where(f => f.DepartmentId == request.departmentId && f.ProgrammeId == request.programmeId && f.SemesterId == request.semesterId && f.LevelId == request.levelId && f.DepartmentOptionId==request.optionId)
                    .Include(f => f.Programme)
                    .Include(f => f.Department)
                    .Include(f => f.DepartmentOption)
                    .Include(f => f.Level)
                    //.Include(f => f.Semester)
                .Select(c => new CourseDTO
                {
                    //Id = c.Id,
                    //Name = c.Name,
                    Code = c.Code,
                    ProgrammeName = c.Programme.Name,
                    DepartmentName = c.Department.Name,
                    //SemesterName = c.Semester.Name,
                    LevelName = c.Level.Name,
                    DepartmentOptionName=c.DepartmentOption.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);
            }
            

            return courses;
        }
    }
}
