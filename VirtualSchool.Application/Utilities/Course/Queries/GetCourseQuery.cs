﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Course.Models;

namespace VirtualSchool.Application.Utilities.Course.Queries
{
    public class GetCourseQuery : IRequest<CourseDTO>
    {
        public int Id { get; set; }
    }
}
