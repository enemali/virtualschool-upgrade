﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Course.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Course.Queries
{
    public class GetAllCourseQueryHandler : IRequestHandler<GetAllCourseQuery, IEnumerable<CourseDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllCourseQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CourseDTO>> Handle(GetAllCourseQuery request, CancellationToken cancellationToken)
        {
            var courses = await _context.Course
                    .Include(f => f.Programme)
                    .Include(f => f.Department)
                    .Include(f => f.DepartmentOption)
                    .Include(f => f.Level)
                .Select(c => new CourseDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    Code = c.Code,
                    DepartmentName=c.Department.Name,
                    ProgrammeName=c.Programme.Name,
                    LevelName=c.Level.Name,
                    //SemesterName=c.Semester.Name,
                    DepartmentOptionName=c.DepartmentOption!=null?c.DepartmentOption.Name:""
                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return courses;
        }


    }
}
