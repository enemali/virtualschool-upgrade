﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Course.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Course.Queries
{
    public class GetCourseQueryHandler : IRequestHandler<GetCourseQuery, CourseDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetCourseQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<CourseDTO> Handle(GetCourseQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Course.Where(a => a.Id == request.Id).Include(a => a.Programme).Include(a => a.Department).Include(f => f.Level).Include(f => f.DepartmentOption).FirstOrDefaultAsync();
           
            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Course), request.Id);
            }

            return new CourseDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Code = entity.Code,
                ProgrammeName = entity.Programme.Name,
                DepartmentName = entity.Department.Name,
                LevelName = entity.Level.Name,
                DepartmentOptionName = entity.DepartmentOption?.Id > 0 ? entity.DepartmentOption.Name : "",
                //SemesterName = entity.Semester.Name

            };
        }
    }
}
