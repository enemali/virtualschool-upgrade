﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.OlevelType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.OlevelType.Queries
{
    public class GetOlevelTypeQueryHandler : IRequestHandler<GetOlevelTypeQuery, OLevelTypeDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetOlevelTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<OLevelTypeDTO> Handle(GetOlevelTypeQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.OlevelType.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Level), request.Id);
            }

            return new OLevelTypeDTO
            {
                Id = entity.Id,
                Name = entity.Name

            };
        }
    }
}
