﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.OlevelType.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.OlevelType.Queries
{
   public class GetAllOlevelTypeQueryHandler : IRequestHandler<GetAllOlevelTypeQuery, IEnumerable<OLevelTypeDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllOlevelTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<OLevelTypeDTO>> Handle(GetAllOlevelTypeQuery request, CancellationToken cancellationToken)
        {
            var Olevels = await _context.OlevelType
                .Select(c => new OLevelTypeDTO
                {
                    Id = c.Id,
                    Name = c.Name

                }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return Olevels;
        }
    }
}
