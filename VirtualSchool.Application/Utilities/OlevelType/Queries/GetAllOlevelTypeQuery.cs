﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.OlevelType.Models;

namespace VirtualSchool.Application.Utilities.OlevelType.Queries
{
   public class GetAllOlevelTypeQuery : IRequest<IEnumerable<OLevelTypeDTO>>
    {
    }
}
