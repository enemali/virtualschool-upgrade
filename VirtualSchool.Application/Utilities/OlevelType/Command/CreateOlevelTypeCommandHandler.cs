﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.OlevelType.Command
{
 public class CreateOlevelTypeCommandHandler : IRequestHandler<CreateOlevelTypeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateOlevelTypeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CreateOlevelTypeCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.OlevelType.Where(a => a.Name == request.Name).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.OlevelType), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.OlevelType.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.OlevelType), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.OlevelType
            {
                Name = request.Name,
                Code = request.Code,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };

            _context.OlevelType.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
