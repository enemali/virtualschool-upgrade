﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.OlevelType.Command
{
  public class DeleteOlevelTypeCommand : IRequest
    {
        public int Id { get; set; }
    }
}
