﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.OlevelType.Command
{
   public class DeleteOlevelTypeCommandHandler : IRequestHandler<DeleteOlevelTypeCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteOlevelTypeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(DeleteOlevelTypeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.OlevelType.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.OlevelType), request.Id);
            }

            _context.OlevelType.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
