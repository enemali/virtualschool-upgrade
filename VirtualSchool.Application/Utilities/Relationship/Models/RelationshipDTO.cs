﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Relationship.Models
{
    public class RelationshipDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
