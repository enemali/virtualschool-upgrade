﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Utilities.Relationship.Models;

namespace VirtualSchool.Application.Utilities.Relationship.Queries
{
   public class GetAllRelationshipQuery : IRequest<IEnumerable<RelationshipDTO>>
    {
    }
}
