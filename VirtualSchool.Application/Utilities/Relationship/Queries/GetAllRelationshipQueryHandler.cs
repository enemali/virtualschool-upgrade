﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Utilities.Relationship.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Relationship.Queries
{
    class GetAllRelationshipQueryHandler : IRequestHandler<GetAllRelationshipQuery, IEnumerable<RelationshipDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllRelationshipQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RelationshipDTO>> Handle(GetAllRelationshipQuery request, CancellationToken cancellationToken)
        {
            var RelationShips = await _context.RelationShip
              .Select(c => new RelationshipDTO
              {
                  Id = c.Id,
                  Name = c.Name

              }).OrderBy(p => p.Name).ToListAsync(cancellationToken);

            return RelationShips;
        }
    }
}
