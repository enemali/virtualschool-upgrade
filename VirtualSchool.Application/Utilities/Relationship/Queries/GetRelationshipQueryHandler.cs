﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Utilities.Relationship.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Relationship.Queries
{
    class GetRelationshipQueryHandler : IRequestHandler<GetRelationshipQuery, RelationshipDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetRelationshipQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<RelationshipDTO> Handle(GetRelationshipQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.RelationShip.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.RelationShip), request.Id);
            }

            return new RelationshipDTO
            {
                Id = entity.Id,
                Name = entity.Name

            };
        }
    }
}
