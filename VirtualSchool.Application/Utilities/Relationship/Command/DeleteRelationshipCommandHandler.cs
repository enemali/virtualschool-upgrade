﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Relationship.Command
{
    class DeleteRelationshipCommandHandler : IRequestHandler<DeleteRelationshipCommand>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteRelationshipCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(DeleteRelationshipCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.RelationShip.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.RelationShip), request.Id);
            }

            _context.RelationShip.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
