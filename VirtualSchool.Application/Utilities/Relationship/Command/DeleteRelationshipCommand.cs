﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Utilities.Relationship.Command
{
   public class DeleteRelationshipCommand: IRequest
    {
        public int Id { get; set; }
    }
}
