﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Relationship.Command
{
    class UpdateRelationshipCommandHandler : IRequestHandler<UpdateRelationshipCommand, Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateRelationshipCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(UpdateRelationshipCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);

            var entity = await _context.RelationShip.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.RelationShip), request.Id);
            }

            var existingEntity = await _context.RelationShip.Where(a => a.Code == request.Code && a.Id != request.Id).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.RelationShip), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.RelationShip.Where(a => a.Slug == requestSlug && a.Id != request.Id).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.RelationShip), "An entity with the Name already exists! Change Name");
            }

            entity.Active = request.Active;
            entity.Code = request.Code;
            entity.Description = request.Description;
            entity.Name = request.Name;
            entity.Slug = requestSlug;

            await _context.SaveChangesAsync(cancellationToken);


            return Unit.Value;
        }
    }
}

