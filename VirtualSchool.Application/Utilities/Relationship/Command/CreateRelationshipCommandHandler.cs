﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Utilities.Relationship.Command
{
    public class CreateRelationshipCommandHandler : IRequestHandler<CreateRelationshipCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateRelationshipCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateRelationshipCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.RelationShip.Where(a => a.Name == request.Name).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.RelationShip), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.RelationShip.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(Domain.Entities.RelationShip), "An entity with the Name already exists! Change Name");
            }

            var entity = new Domain.Entities.RelationShip
            {
                Name = request.Name,
                Description = request.Description,
                Slug = requestSlug,
                Code = request.Code,
                Active = request.Active,
            };

            _context.RelationShip.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
    }

