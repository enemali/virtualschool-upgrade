﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Queries
{
    public class GetAlternateDepartmentQueryHandler : IRequestHandler<GetAlternateDepartmentQuery, List<AlternateDepartment>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAlternateDepartmentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<List<AlternateDepartment>> Handle(GetAlternateDepartmentQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<AlternateDepartment> alternateDepartments = new List<AlternateDepartment>();
                string rejectReason;

                var applicationForm = await _context.ApplicationForm.Where(a => a.Id == request.FormId).LastOrDefaultAsync();

                if (applicationForm == null)
                    return null;

                var applicant = await _context.Applicant.Where(a => a.ApplicationFormId == request.FormId).LastOrDefaultAsync();

                if (applicant != null && applicant.ApplicantStatusId > (int)Domain.Enumerations.ApplicantStatus.ClearedAndAccepted)
                    return null;

                var appliedCourse = await _context.ApplicantAppliedCourse.Where(a => a.PersonId == applicationForm.PersonId).Include(a => a.Person).Include(a => a.Programme).Include(a => a.Department).Include(a => a.ApplicationForm).LastOrDefaultAsync();

                if (appliedCourse == null)
                    return null;

                var programmeDepartments = await _context.ProgrammeDepartment.Where(p => p.ProgrammeId == appliedCourse.ProgrammeId).Include(a => a.Programme).Include(a => a.Department).ToListAsync();

                if (programmeDepartments == null || programmeDepartments.Count <= 0)
                    return null;

                for (int i = 0; i < programmeDepartments.Count; i++)
                {
                    var department = programmeDepartments[i].Department;
                    if (department.Id == appliedCourse.DepartmentId)
                        continue;

                    appliedCourse.Department = department;


                    rejectReason = await EvaluateApplication(appliedCourse);

                    if (string.IsNullOrWhiteSpace(rejectReason))
                        alternateDepartments.Add(new AlternateDepartment
                        {
                            DepartmentId = department.Id,
                            DepartmentName = department.Name,
                            Description = department.Description
                        });
                }

                return alternateDepartments;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> EvaluateApplication(ApplicantAppliedCourse appliedCourse)
        {
            try
            {
                Person applicant = appliedCourse.Person;
                Programme programme = appliedCourse.Programme;
                Department department = appliedCourse.Department;

                //ADMISSION CRITERIA
                //get admission criteria
                var admissionCriteria = await _context.AdmissionCriteria.Where(ac => ac.DepartmentId == department.Id && ac.ProgrammeId == programme.Id).FirstOrDefaultAsync();

                if (admissionCriteria == null || admissionCriteria.Id <= 0)
                    return $"No Admission Criteria found for {department.Name} in {programme.Name}";

                //get admission criteria o-level type
                var requiredOlevelTypes = await _context.AdmissionCriteriaForOLevelType.Where(ac => ac.AdmissionCriteriaId == admissionCriteria.Id).Include(a => a.AdmissionCriteria).Include(a => a.OlevelType).ToListAsync();

                //get admission criteria o-level subject
                var compulsorySubjects = await _context.AdmissionCriteriaForOLevelSubject.Where(s => s.AdmissionCriteriaId == admissionCriteria.Id && s.IsCompulsory == true).Include(a => a.AdmissionCriteria).Include(a => a.OlevelGrade).Include(a => a.OlevelSubject)?.ToListAsync();
                var otherSubjects = await _context.AdmissionCriteriaForOLevelSubject.Where(s => s.AdmissionCriteriaId == admissionCriteria.Id && s.IsCompulsory == false).Include(a => a.AdmissionCriteria).Include(a => a.OlevelGrade).Include(a => a.OlevelSubject)?.ToListAsync();

                //APPLICANT
                //get applicant result
                var firstSittingOlevelResult = await _context.OlevelResult.Where(f => f.PersonId == applicant.Id && f.OlevelSittingId == (int)Domain.Enumerations.OlevelSittings.First_Sitting && f.ApplicationFormId != null).Include(a => a.Person).Include(a => a.OlevelType).Include(a => a.ApplicationForm)?.LastOrDefaultAsync();
                var secondSittingOlevelResult = await _context.OlevelResult.Where(f => f.PersonId == applicant.Id && f.OlevelSittingId == (int)Domain.Enumerations.OlevelSittings.Second_Sitting && f.ApplicationFormId != null).Include(a => a.Person).Include(a => a.OlevelType).Include(a => a.ApplicationForm)?.LastOrDefaultAsync();

                //get applicant o-level subjects
                List<OlevelResultDetail> firstSittingResultDetails = null;
                List<OlevelResultDetail> secondSittingResultDetails = null;
                if (firstSittingOlevelResult != null && firstSittingOlevelResult.Id > 0)
                    firstSittingResultDetails = await _context.OlevelResultDetail.Where(fs => fs.olevelResultId == firstSittingOlevelResult.Id).Include(a => a.olevelResult).Include(a => a.olevelGrade).Include(a => a.OlevelSubject)?.ToListAsync();

                if (secondSittingOlevelResult != null && secondSittingOlevelResult.Id > 0)
                    secondSittingResultDetails = await _context.OlevelResultDetail.Where(ss => ss.olevelResultId == secondSittingOlevelResult.Id).Include(a => a.olevelResult).Include(a => a.olevelGrade).Include(a => a.OlevelSubject)?.ToListAsync();

                if ((firstSittingResultDetails == null || firstSittingResultDetails.Count <= 0) && (secondSittingResultDetails == null || secondSittingResultDetails.Count <= 0))
                    return "No O-Level result found for applicant!";

                string oLevelTypeRejectReason = CheckOlevelType(requiredOlevelTypes, firstSittingOlevelResult, secondSittingOlevelResult, appliedCourse);
                string requiredSubjectsRejectReason = CheckRequiredOlevelSubjects(compulsorySubjects, otherSubjects, firstSittingResultDetails, secondSittingResultDetails, appliedCourse);

                string rejectReason = null;
                if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = oLevelTypeRejectReason + ". " + requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason == null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason == null)
                {
                    rejectReason = oLevelTypeRejectReason;
                }

                if (!string.IsNullOrWhiteSpace(rejectReason))
                {
                    rejectReason += " Therefore did not qualify for admission.";
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string CheckOlevelType(List<AdmissionCriteriaForOLevelType> requiredOlevelTypes, OlevelResult firstSittingOlevelResult, OlevelResult secondSittingOlevelResult, ApplicantAppliedCourse applicantAppliedCourse)
        {
            try
            {
                string rejectReason = null;
                if (requiredOlevelTypes == null || requiredOlevelTypes.Count <= 0)
                    throw new Exception($"O-Level Type has not been set in Admission Criteria for {applicantAppliedCourse.Programme.Name} in {applicantAppliedCourse.Department.Name}! Please contact your system administrator.");

                string applicantOlevelTypes = null;
                if (firstSittingOlevelResult != null && firstSittingOlevelResult.Id > 0)
                {
                    applicantOlevelTypes += firstSittingOlevelResult.OlevelType.Name;

                    AdmissionCriteriaForOLevelType secondSittingAdmissionCriteriaOLevelType = null;
                    AdmissionCriteriaForOLevelType firstSittingAdmissionCriteriaOLevelType = requiredOlevelTypes.Where(ac => ac.OlevelType.Id == firstSittingOlevelResult.OlevelType.Id).SingleOrDefault();
                    if (firstSittingAdmissionCriteriaOLevelType == null)
                    {
                        if (secondSittingOlevelResult != null && secondSittingOlevelResult.Id > 0)
                        {
                            applicantOlevelTypes += $" & { secondSittingOlevelResult.OlevelType.Name}";
                            secondSittingAdmissionCriteriaOLevelType = requiredOlevelTypes.Where(ac => ac.OlevelType.Id == secondSittingOlevelResult.OlevelType.Id).SingleOrDefault();
                        }
                    }

                    if (firstSittingAdmissionCriteriaOLevelType == null && secondSittingAdmissionCriteriaOLevelType == null)
                    {
                        string validOlevelTypes = GetStringOfValidOlevelTypes(requiredOlevelTypes);
                        rejectReason = $"'{applicantOlevelTypes}' O-Level Type obtained by the applicant does not meet the O-Level Type ({validOlevelTypes}) requirement for {applicantAppliedCourse.Department.Name.ToUpper()}";
                    }
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string GetStringOfValidOlevelTypes(List<AdmissionCriteriaForOLevelType> admissionCriteriaOLevelTypes)
        {
            try
            {
                string validOlevelTypes = null;
                if (admissionCriteriaOLevelTypes != null && admissionCriteriaOLevelTypes.Count > 0)
                {
                    string[] oLevelTypes = GetDistinctOLevelTypesFrom(admissionCriteriaOLevelTypes);
                    validOlevelTypes = string.Join(", ", oLevelTypes);

                }

                return validOlevelTypes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string[] GetDistinctOLevelTypesFrom(List<AdmissionCriteriaForOLevelType> admissionCriteriaOLevelTypes)
        {
            try
            {
                string[] oLevelTypes = null;
                if (admissionCriteriaOLevelTypes != null && admissionCriteriaOLevelTypes.Count > 0)
                {
                    oLevelTypes = admissionCriteriaOLevelTypes.Select(o => o.OlevelType.Name).Distinct().ToArray();
                }

                return oLevelTypes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string CheckRequiredOlevelSubjects(List<AdmissionCriteriaForOLevelSubject> compulsorySubjects, List<AdmissionCriteriaForOLevelSubject> otherSubjects, List<OlevelResultDetail> firstSittingResultDetails, List<OlevelResultDetail> secondSittingResultDetails, ApplicantAppliedCourse applicantAppliedCourse)
        {
            try
            {
                //check for the compulsory subjects
                if ((compulsorySubjects == null || compulsorySubjects.Count <= 0) && (otherSubjects == null || otherSubjects.Count <= 0))
                    throw new Exception($"Subject has not been set in Admission Criteria for {applicantAppliedCourse.Programme.Name.ToUpper()} in {applicantAppliedCourse.Department.Name}! Please contact your system administrator");

                int minimumNoOfSubjectWithCreditOrAbove = compulsorySubjects[0].AdmissionCriteria.MinimumRequiredNumberOfSubject;

                List<OlevelResultDetail> applicantConsolidatedResultsWithCreditOrAbove = GetConsolidatedApplicantOlevelResultAboveOrEqualToE8(firstSittingResultDetails, secondSittingResultDetails);
                if (applicantConsolidatedResultsWithCreditOrAbove == null || applicantConsolidatedResultsWithCreditOrAbove.Count <= 0)
                {
                    return "Required minimum number of subjects at Credit level for " + applicantAppliedCourse.Department.Name.ToUpper() + " is " + minimumNoOfSubjectWithCreditOrAbove + " and the applicant got none, ";
                }
                else if (applicantConsolidatedResultsWithCreditOrAbove.Count < minimumNoOfSubjectWithCreditOrAbove)
                {
                    return "Required minimum number of subjects at Credit level for " + applicantAppliedCourse.Department.Name.ToUpper() + " is " + minimumNoOfSubjectWithCreditOrAbove + " the applicant got only " + applicantConsolidatedResultsWithCreditOrAbove.Count + ".";
                }

                string rejectReason = null;


                //check for main compulsory subjects
                int compulsorySubjectMatchCount = 0;
                List<string> requiredSubjects = new List<string>();
                foreach (AdmissionCriteriaForOLevelSubject compulsoryOLevelSubject in compulsorySubjects)
                {

                    string alternativeSubjects = null;
                    List<OlevelResultDetail> oLevelResultDetailAlternatives = new List<OlevelResultDetail>();
                    OlevelResultDetail oLevelResultDetail = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.OlevelSubjectId == compulsoryOLevelSubject.OLevelSubjectId && cs.olevelGradeId <= compulsoryOLevelSubject.MinimumOLevelGradeId).SingleOrDefault();

                    List<AdmissionCriteriaForOLevelSubjectAlternative> compulsoryOLevelSubjectAlternatives = _context.AdmissionCriteriaForOLevelSubjectAlternative.Where(m => m.AdmissionCriteriaForOLevelSubjectId == compulsoryOLevelSubject.Id).Include(a => a.AdmissionCriteriaForOLevelSubject).Include(a => a.OlevelSubject)?.ToList();


                    if (compulsoryOLevelSubjectAlternatives != null && compulsoryOLevelSubjectAlternatives.Count > 0)
                    {
                        foreach (AdmissionCriteriaForOLevelSubjectAlternative compulsoryOLevelSubjectAlternative in compulsoryOLevelSubjectAlternatives)
                        {
                            OlevelResultDetail oLevelResultDetailAlternative = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.OlevelSubjectId == compulsoryOLevelSubjectAlternative.OLevelSubjectId && cs.olevelGradeId <= compulsoryOLevelSubject.MinimumOLevelGradeId).SingleOrDefault();
                            if (oLevelResultDetailAlternative != null)
                            {
                                oLevelResultDetailAlternatives.Add(oLevelResultDetailAlternative);
                            }
                        }
                    }

                    if (oLevelResultDetail == null && oLevelResultDetailAlternatives.Count <= 0)
                    {
                        //get all compulsory alternative subjects
                        if (compulsoryOLevelSubjectAlternatives != null && compulsoryOLevelSubjectAlternatives.Count > 0)
                        {
                            foreach (AdmissionCriteriaForOLevelSubjectAlternative alternative in compulsoryOLevelSubjectAlternatives)
                            {
                                alternativeSubjects += "/" + alternative.OlevelSubject.Name;
                            }
                        }

                        if (string.IsNullOrEmpty(alternativeSubjects))
                        {
                            requiredSubjects.Add(compulsoryOLevelSubject.OlevelSubject.Name.ToUpper());
                            //rejectReason += compulsoryOLevelSubject.Subject.Name.ToUpper() + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but was not obtained by the applicant. Therefore did not qualify for admission.";
                        }
                        else
                        {
                            requiredSubjects.Add(" " + compulsoryOLevelSubject.OlevelSubject.Name.ToUpper() + alternativeSubjects.ToUpper());
                            //rejectReason += "Any one of " + compulsoryOLevelSubject.Subject.Name.ToUpper() + alternativeSubjects.ToUpper() + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but none of them was obtained by the applicant. Therefore did not qualify for admission.";
                        }
                    }
                    else
                    {
                        compulsorySubjectMatchCount++;
                    }
                }

                if (requiredSubjects.Count > 0)
                {
                    string[] sbjectsNotGot = requiredSubjects.ToArray();
                    string missingSubjects = string.Join(", ", sbjectsNotGot);
                    rejectReason += missingSubjects + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but was not obtained by the applicant.";
                }


                //
                int orderSubjectMatchCount = 0;
                //List<string> otherNeededSubjects = new List<string>();
                foreach (AdmissionCriteriaForOLevelSubject otherSubject in otherSubjects)
                {
                    //string alternativeSubjects = null;
                    List<OlevelResultDetail> oLevelResultDetailAlternatives = new List<OlevelResultDetail>();
                    OlevelResultDetail oLevelResultDetail = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.OlevelSubjectId == otherSubject.OLevelSubjectId && cs.olevelGradeId <= otherSubject.MinimumOLevelGradeId).SingleOrDefault();

                    List<AdmissionCriteriaForOLevelSubjectAlternative> otherSubjectAlternatives = _context.AdmissionCriteriaForOLevelSubjectAlternative.Where(m => m.AdmissionCriteriaForOLevelSubjectId == otherSubject.Id).Include(a => a.AdmissionCriteriaForOLevelSubject).Include(a => a.OlevelSubject).ToList();

                    if (otherSubjectAlternatives != null && otherSubjectAlternatives.Count > 0)
                    {
                        foreach (AdmissionCriteriaForOLevelSubjectAlternative otherSubjectAlternative in otherSubjectAlternatives)
                        {
                            OlevelResultDetail oLevelResultDetailAlternative = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.OlevelSubjectId == otherSubjectAlternative.OLevelSubjectId && cs.olevelGradeId <= otherSubject.MinimumOLevelGradeId).SingleOrDefault();
                            if (oLevelResultDetailAlternative != null)
                            {
                                //alternativeSubjects += "/" + oLevelResultDetailAlternative.Subject.Name;
                                oLevelResultDetailAlternatives.Add(oLevelResultDetailAlternative);
                            }
                        }
                    }

                    if (oLevelResultDetail != null || oLevelResultDetailAlternatives.Count > 0)
                    {
                        orderSubjectMatchCount++;
                    }
                }

                int totalSubjectsObtained = orderSubjectMatchCount + compulsorySubjectMatchCount;
                int otherSubjectCount = minimumNoOfSubjectWithCreditOrAbove - compulsorySubjects.Count;
                if (otherSubjectCount > orderSubjectMatchCount)
                {
                    if (orderSubjectMatchCount > 0)
                    {
                        rejectReason += "Applicant made a total of " + compulsorySubjectMatchCount + " compulsory subjects, and " + orderSubjectMatchCount + " other subject, which totals " + totalSubjectsObtained + " and does not meet the minimum requirement of " + minimumNoOfSubjectWithCreditOrAbove + " subjects for " + applicantAppliedCourse.Department.Name.ToUpper() + ".";
                    }
                    else
                    {
                        List<string> anyOfTheseSubjects = new List<string>();
                        foreach (AdmissionCriteriaForOLevelSubject otherOLevelSubject in otherSubjects)
                        {
                            anyOfTheseSubjects.Add(otherOLevelSubject.OlevelSubject.Name.ToUpper());
                        }

                        string[] anyOne = anyOfTheseSubjects.ToArray();
                        int totalSubjectsOutstanding = minimumNoOfSubjectWithCreditOrAbove - compulsorySubjects.Count;
                        string subjectsOutstanding = totalSubjectsOutstanding > 1 ? totalSubjectsOutstanding + " subjects" : totalSubjectsOutstanding + " subject";
                        rejectReason += "Applicant made a total of " + compulsorySubjectMatchCount + " compulsory subjects, but did not obtain any other " + subjectsOutstanding + " from (" + string.Join(", ", anyOne) + ") at credit level to complement. Which does not meet the minimum requirement of " + minimumNoOfSubjectWithCreditOrAbove + " subjects for " + applicantAppliedCourse.Department.Name.ToUpper() + ".";
                    }
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<OlevelResultDetail> GetConsolidatedApplicantOlevelResultAboveOrEqualToE8(List<OlevelResultDetail> firstSittingResultDetails, List<OlevelResultDetail> secondSittingResultDetails)
        {
            try
            {
                List<OlevelResultDetail> firstSittingOLevelResultsAboveOrEqualToE8 = GetOLevelResultsAboveOrEqualToE8(firstSittingResultDetails);
                List<OlevelResultDetail> secondSittingOLevelResultsAboveOrEqualToE8 = GetOLevelResultsAboveOrEqualToE8(secondSittingResultDetails);

                List<OlevelResultDetail> consolidatedOLevelResultsAboveOrEqualToC6 = new List<OlevelResultDetail>();
                if (firstSittingOLevelResultsAboveOrEqualToE8 != null && firstSittingOLevelResultsAboveOrEqualToE8.Count > 0)
                {
                    if (secondSittingOLevelResultsAboveOrEqualToE8 != null && secondSittingOLevelResultsAboveOrEqualToE8.Count > 0)
                    {
                        foreach (OlevelResultDetail oLevelResultDetail in firstSittingOLevelResultsAboveOrEqualToE8)
                        {
                            OlevelResultDetail oLevelResultDetailMatch = secondSittingOLevelResultsAboveOrEqualToE8.Where(ss => ss.OlevelSubjectId == oLevelResultDetail.OlevelSubjectId).SingleOrDefault();
                            if (oLevelResultDetailMatch != null)
                            {
                                if (oLevelResultDetailMatch.olevelGradeId > oLevelResultDetail.olevelGradeId)
                                {
                                    consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                                }
                                else
                                {
                                    consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetailMatch);
                                }
                            }
                            else
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }

                        foreach (OlevelResultDetail oLevelResultDetail in secondSittingOLevelResultsAboveOrEqualToE8)
                        {
                            OlevelResultDetail oLevelResultDetailMatch = firstSittingOLevelResultsAboveOrEqualToE8.Where(ss => ss.OlevelSubjectId == oLevelResultDetail.OlevelSubjectId).SingleOrDefault();
                            if (oLevelResultDetailMatch == null)
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }
                    }
                    else
                    {
                        foreach (OlevelResultDetail oLevelResultDetail in firstSittingOLevelResultsAboveOrEqualToE8)
                        {
                            if (oLevelResultDetail.olevelGradeId <= 8)
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }
                    }
                }

                return consolidatedOLevelResultsAboveOrEqualToC6;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<OlevelResultDetail> GetOLevelResultsAboveOrEqualToE8(List<OlevelResultDetail> resultDetails)
        {
            try
            {
                List<OlevelResultDetail> oLevelResultsAboveOrEqualToE8 = new List<OlevelResultDetail>();
                if (resultDetails != null && resultDetails.Count > 0)
                {
                    foreach (OlevelResultDetail firstSittingResultDetail in resultDetails)
                    {
                        if (firstSittingResultDetail.olevelGrade.Id <= 8)
                        {
                            oLevelResultsAboveOrEqualToE8.Add(firstSittingResultDetail);
                        }
                    }
                }

                return oLevelResultsAboveOrEqualToE8;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
