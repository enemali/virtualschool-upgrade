﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Queries
{
    public class GetAlternateDepartmentQuery : IRequest<List<AlternateDepartment>>
    {
        public int FormId { get; set; }
    }
}
