﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Queries
{
    public class GetOlevelResultQuery: IRequest<OlevelResultDTO>
    {
        public int PersonId { get; set; }
    }
}
