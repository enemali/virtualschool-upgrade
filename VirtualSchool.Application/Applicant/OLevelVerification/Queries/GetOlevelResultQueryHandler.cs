﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Queries
{
    public class ViewOlevelResultQueryHandler : IRequestHandler<GetOlevelResultQuery, OlevelResultDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public ViewOlevelResultQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<OlevelResultDTO> Handle(GetOlevelResultQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.OlevelResult.Where(s => s.PersonId == request.PersonId).LastOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.OlevelResult), request.PersonId);
            }

            var olevelFirstSittingEntity = await _context.OlevelResult.Where(j => j.PersonId == request.PersonId && j.OlevelSittingId == (int)OlevelSittings.First_Sitting).Include(s => s.Person).Include(s => s.OlevelType)
               .Select(f => new FirstsittingOlevelResult
               {
                   Id = f.Id,
                   ExamNumber = f.ExamNumber,
                   ExamType = f.OlevelType.Name,
                   ScannedResultUrl = f.ScannedResultUrl,
                   ExamTypeId = f.OlevelType.Id,
                   ExamYear = f.ExamYear,
                   PersonId = f.PersonId


               }).FirstOrDefaultAsync(cancellationToken);

            if (olevelFirstSittingEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.OlevelResultDetail), request.PersonId);
            }

            var olevelSecondSittingEntity = await _context.OlevelResult.Where(j => j.PersonId == request.PersonId && j.OlevelSittingId == (int)OlevelSittings.Second_Sitting).Include(s => s.OlevelType)
                .Select(f => new SecondSittingOlevelResult
                {
                    Id = f.Id,
                    ExamNumber = f.ExamNumber,
                    ExamType = f.OlevelType.Name,
                    ScannedResultUrl = f.ScannedResultUrl,
                    ExamTypeId = f.OlevelTypeId,
                    ExamYear = f.ExamYear,
                    PersonId = f.PersonId

                }).FirstOrDefaultAsync(cancellationToken);

            var olevelFirstSittingDetailEntity = await _context.OlevelResultDetail.Where(j => j.olevelResultId == olevelFirstSittingEntity.Id).Include(s => s.olevelGrade).Include(s => s.OlevelSubject)
              .Select(f => new OlevelResultDetailFirstSitting
              {
                  olevelResultId = f.olevelResultId,
                  GradeId = f.olevelGrade.Id,
                  GradeName = f.olevelGrade.Name,
                  SubjectId = f.OlevelSubject.Id,
                  SubjectName = f.OlevelSubject.Name

              }).ToListAsync(cancellationToken);

            var olevelSecondSittingDetailEntity = new List<OlevelResultDetailSecondSitting>();

            if (olevelSecondSittingEntity != null && olevelSecondSittingEntity.Id > 0)
            {

                olevelSecondSittingDetailEntity = await _context.OlevelResultDetail.Where(j => j.olevelResultId == olevelSecondSittingEntity.Id).Include(s => s.olevelGrade).Include(s => s.OlevelSubject)
                 .Select(f => new OlevelResultDetailSecondSitting
                 {
                     olevelResultId = f.olevelResultId,
                     GradeId = f.olevelGrade.Id,
                     GradeName = f.olevelGrade.Name,
                     SubjectId = f.OlevelSubject.Id,
                     SubjectName = f.OlevelSubject.Name

                 }).ToListAsync(cancellationToken);

                olevelSecondSittingEntity.OlevelResultDetailSecondSitting = olevelSecondSittingDetailEntity;
            }
            
            olevelFirstSittingEntity.OlevelResultDetailFirstSitting = olevelFirstSittingDetailEntity;
          
            var olevelResults = new OlevelResultDTO()
            {
                FirstsittingOlevelResult = olevelFirstSittingEntity,
                SecondSittingOlevelResult = olevelSecondSittingEntity,
            };

            return olevelResults;
        }
    }
}
