﻿using MediatR;
using System.Collections.Generic;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Command
{
    public class UpdateOLevelResultCommand : IRequest<int>
    {
        public int OlevelResulId { get; set; }

        public string ExamNumber { get; set; }
        public int ExamYear { get; set; }
        public int OlevelSittingId { get; set; }
        public int OlevelTypeId { get; set; }
        public int? ApplicationFormId { get; set; }
        public string ScannedResultUrl { get; set; }
        public List<OlevelResultDetailDTO> OLevelResultDetails {get;set;}
    }
    public class OlevelResultDetailDTO
    {
        public int olevelDetailResultId { get; set; }
        public int GradeId { get; set; }
        public int SubjectId { get; set; }
    }
}
