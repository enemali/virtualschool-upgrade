﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Command
{

    public class UpdateOLevelResultCommandHandler : IRequestHandler<UpdateOLevelResultCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateOLevelResultCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateOLevelResultCommand request, CancellationToken cancellationToken)
        {
            
            var olevelResultEntity = await _context.OlevelResult.FindAsync(request.OlevelResulId);

            if (olevelResultEntity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.OlevelResult), request.OlevelResulId);
            }
            
            olevelResultEntity.ApplicationFormId = request.ApplicationFormId;
            olevelResultEntity.ExamNumber = request.ExamNumber;
            olevelResultEntity.ExamYear = request.ExamYear;
            olevelResultEntity.OlevelSittingId = request.OlevelSittingId;
            olevelResultEntity.OlevelTypeId = request.OlevelTypeId;
            olevelResultEntity.ScannedResultUrl = request.ScannedResultUrl;
            
            foreach (var resultDetails in request.OLevelResultDetails)
            {
                var olevelResultDetailEntity = await _context.OlevelResultDetail.Where(s => s.Id == resultDetails.olevelDetailResultId).LastOrDefaultAsync();

                olevelResultDetailEntity.olevelGradeId = resultDetails.GradeId;
                olevelResultDetailEntity.OlevelSubjectId = resultDetails.SubjectId;
            }
            await _context.SaveChangesAsync(cancellationToken);

            return olevelResultEntity.PersonId;
        }

    }
}
