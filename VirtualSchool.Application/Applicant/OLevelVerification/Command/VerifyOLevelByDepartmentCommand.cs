﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.OLevelVerification.Models;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Command
{
    public class VerifyOLevelByDepartmentCommand : IRequest<string>
    {
        public int FormId { get; set; }
    }
}