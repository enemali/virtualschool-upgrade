﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Models
{
    public class OlevelResultDTO
    {
        public FirstsittingOlevelResult FirstsittingOlevelResult { get; set; }
        public SecondSittingOlevelResult SecondSittingOlevelResult { get; set; }

    }
    public class FirstsittingOlevelResult
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int ExamTypeId { get; set; }
        public int ExamYear { get; set; }
        public string ExamType { get; set; }
        public string ExamNumber { get; set; }
        public string ScannedResultUrl { get; set; }
        public List<OlevelResultDetailFirstSitting> OlevelResultDetailFirstSitting { get; set; }
    }
    public class SecondSittingOlevelResult
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int ExamTypeId { get; set; }
        public int ExamYear { get; set; }
        public string ExamType { get; set; }
        public string ExamNumber { get; set; }
        public string ScannedResultUrl { get; set; }
        public List<OlevelResultDetailSecondSitting> OlevelResultDetailSecondSitting { get; set; }
    }
  
    public class OlevelResultDetailFirstSitting
    {
        public int Id { get; set; }
        public int olevelResultId { get; set; }
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public string SubjectName { get; set; }
        public int SubjectId { get; set; }
    }
    public class OlevelResultDetailSecondSitting
    {
        public int Id { get; set; }
        public int olevelResultId { get; set; }
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public string SubjectName { get; set; }
        public int SubjectId { get; set; }
    }
}
