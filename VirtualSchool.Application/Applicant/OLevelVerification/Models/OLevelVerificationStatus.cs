﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.OLevelVerification.Models
{
    public class OLevelVerificationStatus
    {
        public bool Status { get; set; }
        public string Reason { get; set; }
    }
}
