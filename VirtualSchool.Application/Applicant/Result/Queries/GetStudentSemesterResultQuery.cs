﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Result.Models;

namespace VirtualSchool.Application.Applicant.Result.Queries
{
    public class GetStudentSemesterResultQuery : IRequest<IEnumerable<StudentResultDTO>>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
    }
}
