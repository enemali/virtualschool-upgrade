﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
   
        public class GetECourseQueryHandler : IRequestHandler<GetECourseQuery, IEnumerable<ECourseDTO>>
        {
            private readonly VirtualSchoolDbContext _context;

            public GetECourseQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

        public async Task<IEnumerable<ECourseDTO>> Handle(GetECourseQuery request, CancellationToken cancellationToken)
        {
            var getStudentCourseRegistration = await _context.StudentCourseRegistrationDetail.Where(s => s.StudentCourseRegistration.StudentId == request.PersonId).ToListAsync(cancellationToken);

            if(getStudentCourseRegistration.Count < 1)
            {
                throw new NotFoundException(nameof(Domain.Entities.StudentCourseRegistration), "Student Have No Course Registration History");
            }
            else
            {
                List<ECourseDTO> eCourseDTOs = new List<ECourseDTO>();
                foreach(var course in getStudentCourseRegistration)
                {
                    var eCourse = await _context.ECourse.Where(s => s.CourseId == course.CourseId && s.EContentTypeId == request.EContentTypeId).Include( s => s.Course).Include(s => s.EContentType).FirstOrDefaultAsync();
                    if(eCourse != null)
                    {
                        
                        eCourseDTOs.Add(new ECourseDTO
                        {
                            CourseId = eCourse.CourseId,
                            CourseName = eCourse.Course.Name,
                            EContentTypeId = eCourse.EContentTypeId,
                            EContentTypeName = eCourse.EContentType.Name,
                            CourseCode = eCourse.Course.Code,
                            Url = eCourse.Url,
                            views = eCourse.views

                        });
                        

                    }

                }
                return eCourseDTOs;
            }


            
        }
        }
    
}
