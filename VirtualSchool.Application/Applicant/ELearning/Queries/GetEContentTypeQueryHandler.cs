﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
    
        public class GetEContentTypeQueryHandler : IRequestHandler<GetEContentTypeQuery, EContentTypeDTO>
        {
            private readonly VirtualSchoolDbContext _context;

            public GetEContentTypeQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<EContentTypeDTO> Handle(GetEContentTypeQuery request, CancellationToken cancellationToken)
            {
                var entity = await _context.EContentType.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.EContentType), request.Id);
                }

                return new EContentTypeDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    Active = entity.Active,
                    Slug = entity.Slug


                    
                };
            }
        }
    
}
