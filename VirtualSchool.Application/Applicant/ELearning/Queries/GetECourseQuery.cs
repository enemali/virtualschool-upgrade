﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.ELearning.Models;
using MediatR;

namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
    public class GetECourseQuery : IRequest<IEnumerable<ECourseDTO>>
    {
        public int PersonId { get; set; }
        public int EContentTypeId { get; set; }
    }
}
