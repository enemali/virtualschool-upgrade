﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;
namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
    public class GetAssignmentSubmissionQueryHandler : IRequestHandler<GetAssignmentSubmissionQuery, AssignmentSubmissionDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAssignmentSubmissionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AssignmentSubmissionDTO> Handle(GetAssignmentSubmissionQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.EAssignmentSubmission.Where(a => a.PersonId == request.PersonId && a.EAssignment.CourseId == request.CourseId).Include(a => a.EAssignment).ThenInclude(s => s.Course).Include(a => a.Student).ThenInclude(a => a.Person).FirstOrDefaultAsync(cancellationToken);


            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.EAssignmentSubmission), "No assignment Submission for student in this course");
            }

            return new AssignmentSubmissionDTO
            {
                PersonId = entity.PersonId,
                Assignment = entity.EAssignment.Assignment,
                AssignmentContent = entity.AssignmentContent,
                EAssignmentId = entity.EAssignmentId,
                CourseId = entity.EAssignment.CourseId,
                CourseName = entity.EAssignment.Course.Name,
                CourseCode = entity.EAssignment.Course.Code,
                DateSet = entity.EAssignment.DateSet,
                DateSubmitted = entity.DateSubmitted,
                DueDate = entity.EAssignment.DueDate,
                Id = entity.Id,
                Instructions = entity.EAssignment.Instructions,
                MatricNumber = entity.Student.MatricNumber,
                Remarks = entity.Remarks,
                URL = entity.EAssignment.URL,
                Score = entity.Score,
                StudentName = entity.Student.Person.Firstname + entity.Student.Person.Othername + entity.Student.Person.Surname
            };

           
        }
    }

}
