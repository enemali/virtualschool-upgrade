﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.ELearning.Models;
using MediatR;

namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
        public class GetAssignmentQuery : IRequest<AssignmentDTO>
        {
            public int CourseId { get; set; }
        }
    
}
