﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;


namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
   
        public class GetAssignmentQueryHandler : IRequestHandler<GetAssignmentQuery, AssignmentDTO>
        {
            private readonly VirtualSchoolDbContext _context;

            public GetAssignmentQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

        public async Task<AssignmentDTO> Handle(GetAssignmentQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.EAssignment.Where(a => a.CourseId == request.CourseId).Include(a => a.Course).FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.EAssignment),"No Assignment in the course selected");
            }

            return new AssignmentDTO
            {
                CourseId = entity.Course.Id,
                CourseName = entity.Course.Name,
                CourseCode = entity.Course.Code,
                Assignment = entity.Assignment,
                Url = entity.URL,
                Instructions = entity.Instructions,
                DateSet = entity.DateSet,
                DueDate = entity.DueDate



            };
        }


    }
}
