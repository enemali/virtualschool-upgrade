﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.ELearning.Models;
using MediatR;


namespace VirtualSchool.Application.Applicant.ELearning.Queries
{
    public class GetEContentTypeQuery : IRequest<EContentTypeDTO>
    {
        public int Id { get; set; }
    }
}
