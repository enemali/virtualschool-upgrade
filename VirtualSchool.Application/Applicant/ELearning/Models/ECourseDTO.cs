﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.ELearning.Models
{
    public  class ECourseDTO
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public int EContentTypeId { get; set; }
        public string EContentTypeName { get; set; }
        public string Url { get; set; }
        public int? views { get; set; }
    }
}
