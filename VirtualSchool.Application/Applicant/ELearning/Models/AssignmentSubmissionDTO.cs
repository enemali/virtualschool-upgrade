﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.ELearning.Models
{
    public class AssignmentSubmissionDTO
    {
        public int Id { get; set; }
        public int EAssignmentId { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public string Assignment { get; set; }
        public string URL { get; set; }
        public string Instructions { get; set; }
        public DateTime DateSet { get; set; }
        public DateTime DueDate { get; set; }

        public int PersonId { get; set; }
        public string StudentName { get; set; }
        public string MatricNumber { get; set; }
        public string AssignmentContent { get; set; }
        public string Remarks { get; set; }
        public decimal? Score { get; set; }
        public DateTime DateSubmitted { get; set; }
    }
}
