﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.ELearning.Models
{
    public class AssignmentDTO
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public string Assignment { get; set; }
        public string Url { get; set; }
        public string Instructions { get; set; }
        public DateTime DateSet { get; set; }
        public DateTime DueDate { get; set; }

       
    }
}
