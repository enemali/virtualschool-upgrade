﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    public class CreateEAssignmentCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string Assignment { get; set; }
        public string URL { get; set; }
        public string Instructions { get; set; }
        public DateTime DateSet { get; set; }
        public DateTime DueDate { get; set; }
    }
}
