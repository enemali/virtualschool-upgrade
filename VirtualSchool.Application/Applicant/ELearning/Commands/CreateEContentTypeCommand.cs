﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    public class CreateEContentTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Slug { get; set; }
    }
}
