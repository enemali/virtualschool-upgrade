﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
   
    public class CreateAssignmentSubmissionCommandHandler : IRequestHandler<CreateAssignmentSubmissionCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateAssignmentSubmissionCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAssignmentSubmissionCommand request, CancellationToken cancellationToken)
        {

            var getStudentCourseRegistration = await _context.StudentCourseRegistrationDetail.Where(s => s.StudentCourseRegistration.StudentId == request.PersonId).ToListAsync(cancellationToken);
            if (getStudentCourseRegistration.Count < 1)
            {
                throw new NotFoundException(nameof(Domain.Entities.StudentCourseRegistration), "Student Have No Course Registration History");
            }
            else
            {
                var getAssignment = await _context.EAssignment.Where(a => a.Id == request.EAssignmentId).Include(s => s.Course).FirstOrDefaultAsync(cancellationToken);
                if(getAssignment == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.EAssignment), "Invalid Assignment");

                }
                else
                {
                    if (getStudentCourseRegistration.Any(s => s.CourseId == getAssignment.CourseId))
                    {
                        var assignmentEntity = new VirtualSchool.Domain.Entities.EAssignmentSubmission
                        {
                           PersonId = request.PersonId,
                           DateSubmitted = DateTime.Now,
                           Score = request.Score,
                           Remarks = request.Remarks,
                           AssignmentContent = request.AssignmentContent,
                           EAssignmentId = request.EAssignmentId,
                           

                        };

                        _context.EAssignmentSubmission.Add(assignmentEntity);
                        await _context.SaveChangesAsync(cancellationToken);
                        return assignmentEntity.Id;
                    }
                    else
                    {
                        throw new NotFoundException(nameof(Domain.Entities.StudentCourseRegistration), "This Course is not in Student Course Registration");

                    }
                }
            }


               

            

          
        }
    }
}
