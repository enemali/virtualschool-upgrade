﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    public  class CreateECourseCommand : IRequest<int>
    {
        public long Id { get; set; }
        public int CourseId { get; set; }
        public int EContentTypeId { get; set; }
        public string Url { get; set; }
        public int? views { get; set; }
    }
}
