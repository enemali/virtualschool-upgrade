﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    public class CreateAssignmentSubmissionCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int EAssignmentId { get; set; }
        public int PersonId { get; set; }
        public string AssignmentContent { get; set; }
        public string Remarks { get; set; }
        public decimal? Score { get; set; }
        public DateTime DateSubmitted { get; set; }
    }
}
