﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;


namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    
        public class CreateEContentTypeCommandHandler : IRequestHandler<CreateEContentTypeCommand, int>
        {
            private readonly VirtualSchoolDbContext _context;

            public CreateEContentTypeCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

        public async Task<int> Handle(CreateEContentTypeCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.Name);
            var existingEntity = await _context.EContentType.Where(a => a.Name == request.Name).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.EContentType), "An entity with the code already exists! Change code");
            }

            var existingEntity2 = await _context.EContentType.Where(a => a.Slug == requestSlug).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity2 != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.EContentType), "An entity with the Name already exists! Change Name");
            }

            var entity = new VirtualSchool.Domain.Entities.EContentType
            {
                Name = request.Name,
                Description = request.Description,
                Active = request.Active,
                Slug = requestSlug
            };

            _context.EContentType.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }

}
