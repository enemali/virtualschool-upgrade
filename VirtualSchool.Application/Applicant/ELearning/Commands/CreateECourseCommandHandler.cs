﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
    
        public class CreateECourseCommandHandler : IRequestHandler<CreateECourseCommand, int>
        {
            private readonly VirtualSchoolDbContext _context;

            public CreateECourseCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateECourseCommand request, CancellationToken cancellationToken)
            {
                

                var existingEntity2 = await _context.ECourse.Where(a => a.CourseId == request.CourseId && a.EContentTypeId == request.EContentTypeId && a.Url == request.Url).FirstOrDefaultAsync(cancellationToken);
                if (existingEntity2 != null)
                {
                    throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.ECourse), "An entity with the Course, media type and URL already exists! Change your inputs");
                }

                var entity = new VirtualSchool.Domain.Entities.ECourse
                {
                    CourseId = request.CourseId,
                    EContentTypeId = request.EContentTypeId,
                    Url = request.Url,

                };

                _context.ECourse.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
            }
        }
    
}
