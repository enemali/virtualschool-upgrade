﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.ELearning.Commands
{
   
        public class CreateEAssignmentCommandHandler : IRequestHandler< CreateEAssignmentCommand, int>
        {
            private readonly VirtualSchoolDbContext _context;

            public CreateEAssignmentCommandHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateEAssignmentCommand request, CancellationToken cancellationToken)
            {
               

                var existingEntity2 = await _context.EAssignment.Where(a => a.Assignment == request.Assignment && a.CourseId == request.CourseId).FirstOrDefaultAsync(cancellationToken);
                if (existingEntity2 != null)
                {
                    throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.EAssignment), "An entity with the Name already exists! Change Name");
                }

                var entity = new VirtualSchool.Domain.Entities.EAssignment
                {
                    Assignment = request.Assignment,
                    CourseId = request.CourseId,
                    DateSet = request.DateSet,
                    DueDate = request.DueDate,
                    URL = request.URL,
                    Instructions = request.Instructions

                };

                _context.EAssignment.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }
        }

    
}
