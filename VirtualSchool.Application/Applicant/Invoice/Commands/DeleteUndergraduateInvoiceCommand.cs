﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class DeleteUndergraduateInvoiceCommand:IRequest<Unit>
    {
        public int Id { get; set; }
    }
}
