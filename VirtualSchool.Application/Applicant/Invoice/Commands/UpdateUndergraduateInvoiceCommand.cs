﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class UpdateUndergraduateInvoiceCommand : IRequest<Unit>
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public int DepartmentId { get; set; }
        public string JambRegistrationNumber { get; set; }
    }
}
