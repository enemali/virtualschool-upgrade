﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class UpdateUndergraduateInvoiceCommandHandler : IRequestHandler<UpdateUndergraduateInvoiceCommand, Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateUndergraduateInvoiceCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateUndergraduateInvoiceCommand request, CancellationToken cancellationToken)
        {
           

            var AppliedCourseEntity = await _context.ApplicantAppliedCourse.Where(a => a.PersonId == request.PersonId).FirstOrDefaultAsync(cancellationToken);
            if (AppliedCourseEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantAppliedCourse), request.Id);
            }

            AppliedCourseEntity.DepartmentId = request.DepartmentId;


            var ApplicantJambDetailEntity = await _context.ApplicantJambDetail.Where(a => a.PersonId == request.PersonId).FirstOrDefaultAsync(cancellationToken);
            if (ApplicantJambDetailEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantJambDetail), request.Id);
            }

            ApplicantJambDetailEntity.JambNumber = request.JambRegistrationNumber;

            var PersonEntity = await _context.Person.FindAsync(request.PersonId);
            if (PersonEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Person), request.Id);
            }

            PersonEntity.Surname = request.Surname;
            PersonEntity.Firstname = request.Firstname;
            PersonEntity.Othername = request.Othername;

            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
