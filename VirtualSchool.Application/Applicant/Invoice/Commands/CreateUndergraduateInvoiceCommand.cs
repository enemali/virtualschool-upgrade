﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class CreateUndergraduateInvoiceCommand : IRequest<int>
    {
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int DepartmentId { get; set; }
        public int ProgrammeId { get; set; }
        public int? DepartmentOptionId { get; set; }
        public string JambRegistrationNumber { get; set; }
        public int SessionId { get; set; }
        public int FeeTypeId { get; set; }
        public int PaymentTypeId { get; set; }
        public int PaymentModeId { get; set; }
        public decimal Amount { get; set; }
        public int ApplicationFormSettingId { get; set; }
    }
}
