﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class DeleteUndergraduateInvoiceCommandHandler : IRequestHandler<DeleteUndergraduateInvoiceCommand, Unit>
    {
        private readonly VirtualSchoolDbContext _context;

        public DeleteUndergraduateInvoiceCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteUndergraduateInvoiceCommand request, CancellationToken cancellationToken)
        {
            var paymentEntity = await _context.Payment.FindAsync(request.Id);
            if (paymentEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Payment), request.Id);
            }

            var ApplicationFormEntity = await _context.ApplicationForm.Where(a => a.PersonId == paymentEntity.PersonId).FirstOrDefaultAsync(cancellationToken);
            if (ApplicationFormEntity != null)
            {
                throw new ExistingItemException(nameof(VirtualSchool.Domain.Entities.ApplicationForm), "Invoice Cannot be deleted because an application form already exists for the invoice.");
            }

            var AppliedCourseEntity = await _context.ApplicantAppliedCourse.Where(a => a.PersonId == paymentEntity.PersonId).FirstOrDefaultAsync(cancellationToken);
            if (AppliedCourseEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Person), request.Id);
            }
            
            var ApplicantJambDetailEntity = await _context.ApplicantJambDetail.Where(a => a.PersonId == paymentEntity.PersonId).FirstOrDefaultAsync(cancellationToken);
            if (ApplicantJambDetailEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Person), request.Id);
            }

            var PersonEntity = await _context.Person.FindAsync(paymentEntity.PersonId);
            if (PersonEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Person), request.Id);
            }

            _context.ApplicantAppliedCourse.Remove(AppliedCourseEntity);
            _context.ApplicantJambDetail.Remove(ApplicantJambDetailEntity);
            _context.Payment.Remove(paymentEntity);
            _context.Person.Remove(PersonEntity);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
