﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Persistence;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Common.Services;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Invoice.Commands
{
    public class CreateUndergraduateInvoiceCommandHandler : IRequestHandler<CreateUndergraduateInvoiceCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;
        private readonly MessageServices _messageServices;

        public CreateUndergraduateInvoiceCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
            _messageServices = new MessageServices();
        }

        public async Task<int> Handle(CreateUndergraduateInvoiceCommand request, CancellationToken cancellationToken)
        {
            try
            {

                //Create Person
                var person = new Person
                {
                    Surname = request.Surname,
                    Firstname = request.Firstname,
                    Othername = request.Othername,
                    PhoneNumber = request.PhoneNumber,
                    Email = request.Email ?? "support@lloydant.com",
                    ContactAddress = "",
                    Picture = ""
                };
                _context.Add(person);

                //Create Payment
                var payment = new Payment
                {
                    Person = person,
                    PaymentModeId =(int)PaymentModes.Full_Payment,
                    PaymentTypeId = request.PaymentTypeId,
                    FeeTypeId = (int)FeeTypes.ApplicationForm,
                    SessionId = request.SessionId,
                    LevelId = (int)Levels.Applicant,
                    SerialNumber = DateTime.Now.Ticks,
                    InvoiceNumber = "INV" + DateTime.Now.Ticks.ToString().PadLeft(10, '0'),
                    Amount = Amount((int)FeeTypes.ApplicationForm, request.DepartmentId,request.DepartmentId,request.SessionId, (int)Levels.Applicant, (int)PaymentModes.Full_Payment),
                    DateGenerated = DateTime.Now
                };
                _context.Add(payment);

                //Create Applicant Applied Course
                var appliedCourse = new ApplicantAppliedCourse
                {
                    Person = person,
                    DepartmentId = request.DepartmentId,
                    ProgrammeId = request.ProgrammeId,
                    ApplicationFormSettingId=request.ApplicationFormSettingId
                   
                };

                if (request.DepartmentOptionId > 0)
                {
                    appliedCourse.DepartmentOptionId = request.DepartmentOptionId;
                }
                _context.Add(appliedCourse);

                if (!String.IsNullOrEmpty(request.JambRegistrationNumber))
                {
                    //Create Applicant Jamb Deatil
                    var jambDetail = new ApplicantJambDetail
                    {
                        Person = person,
                        JambNumber = request.JambRegistrationNumber
                    };
                    _context.Add(jambDetail);
                }
               
             
                //Save Details


                await _context.SaveChangesAsync();

                //Send Email
                //var emailBody = $"Dear {person.Firstname} , Your invoice generation was succesfull. Here's your invoice number {payment.InvoiceNumber}";
                //await _messageServices.SendEmail(person.Email, "Invoice Generated", emailBody);

                return payment.Id;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public decimal Amount(int FeetypeId,int DepartmentId,int ProgrammeId,int SessionId, int levelId, int paymentModeId)
        {
            var Amount = 0M;
             List < FeeDetail > feeDetails = _context.FeeDetail.Where(a => a.FeeTypeId == FeetypeId && a.DepartmentId == DepartmentId && a.ProgrammeId == ProgrammeId && a.SessionId == SessionId && a.LevelId==levelId && a.PaymentModeId== paymentModeId).Include(u=>u.Fee).ToList();
            if (feeDetails != null && feeDetails.Count > 0)
            {
               Amount = feeDetails.Sum(p => p.Fee.Amount);

            }

            return Amount;
        }
    }
}
