﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Invoice.Models;

namespace VirtualSchool.Application.Applicant.Invoice.Queries
{
    public class GetInvoiceByPaymentIdOrInvoiceNumberQuery : IRequest<UndergraduateInvoice>
    {
        public string Input { get; set; }
    }
}
