﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Invoice.Models;

namespace VirtualSchool.Application.Applicant.Invoice.Queries
{
    public class GetUndergraduateInvoiceQuery: IRequest<UndergraduateInvoice>
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
