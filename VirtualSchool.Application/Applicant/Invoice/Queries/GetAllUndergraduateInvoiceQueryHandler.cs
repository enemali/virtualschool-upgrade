﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Invoice.Models;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Invoice.Queries
{
    public class GetAllUndergraduateInvoiceQueryHandler: IRequestHandler<GetAllUndergraduateInvoiceQuery, IEnumerable<UndergraduateInvoice>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllUndergraduateInvoiceQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UndergraduateInvoice>> Handle(GetAllUndergraduateInvoiceQuery request, CancellationToken cancellationToken)
        {
            var invoices = await _context.Payment
               .Select(c => new UndergraduateInvoice
               {
                   Id = c.Id,
                   InvoiceNumber = c.InvoiceNumber,
                   PersonId = c.PersonId,
                   Surname = c.Person.Surname,
                   Firstname = c.Person.Firstname,
                   Othername = c.Person.Othername,
                   DepartmentId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Id,
                   DepartmentName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Name.ToUpper(),
                   ProgrammeId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Id,
                   ProgrammeName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Name.ToUpper(),
                   JambRegistrationNumber = c.Person.ApplicantJambDetails.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantJambDetails.FirstOrDefault().JambNumber.ToUpper(),
                   SessionId = c.SessionId,
                   SessionName = c.Session.Name,
                   Paid = c.DatePaid != null ? true : false,
                   Amount = c.Amount,
                   Description = c.FeeType.Name.ToUpper(),
                   PaymentGatewayDescription = "N/A",
                   Email=c.Person.Email,
                   PhoneNumber=c.Person.PhoneNumber
               }).ToListAsync(cancellationToken);
            var undergraduateInvoices = invoices.Where(i => i.ProgrammeId == (int)Programmes.Undergraduate);
            return undergraduateInvoices;
        }

    }
}
