﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using VirtualSchool.Application.Applicant.Invoice.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Invoice.Queries
{
    public class GetUndergraduateInvoiceQueryHandler : IRequestHandler<GetUndergraduateInvoiceQuery, UndergraduateInvoice>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetUndergraduateInvoiceQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task<UndergraduateInvoice> Handle(GetUndergraduateInvoiceQuery request, CancellationToken cancellationToken)
        {
            if(request.Id > 0)
            {
                var invoice = await _context.Payment
                .Where(a => a.Id == request.Id)
                .Select(c => new UndergraduateInvoice
                {
                    Id = c.Id,
                    InvoiceNumber = c.InvoiceNumber,
                    PersonId = c.PersonId,
                    Surname = c.Person.Surname,
                    Firstname = c.Person.Firstname,
                    Othername = c.Person.Othername,
                    DepartmentId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Id,
                    DepartmentName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Name.ToUpper(),
                    ProgrammeId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Id,
                    ProgrammeName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Name.ToUpper(),
                    JambRegistrationNumber = c.Person.ApplicantJambDetails.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantJambDetails.FirstOrDefault().JambNumber.ToUpper(),
                    SessionId = c.SessionId,
                    SessionName = c.Session.Name,
                    Paid = c.DatePaid != null ? true : false,
                    Amount = c.Amount,
                    Description = c.FeeType.Name.ToUpper(),
                    PaymentGatewayDescription = "N/A",
                    Email=c.Person.Email,
                    PhoneNumber=c.Person.PhoneNumber
                }).FirstOrDefaultAsync();

                return invoice;
            }
            else if (!string.IsNullOrEmpty(request.InvoiceNumber))
            {
                var invoice = await _context.Payment
                .Where(a => a.InvoiceNumber == request.InvoiceNumber)
                .Select(c => new UndergraduateInvoice
                {
                    Id = c.Id,
                    InvoiceNumber = c.InvoiceNumber,
                    PersonId = c.PersonId,
                    Surname = c.Person.Surname,
                    Firstname = c.Person.Firstname,
                    Othername = c.Person.Othername,
                    DepartmentId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Id,
                    DepartmentName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Department.Name.ToUpper(),
                    ProgrammeId = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? 0 : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Id,
                    ProgrammeName = c.Person.ApplicantAppliedCourses.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantAppliedCourses.FirstOrDefault().Programme.Name.ToUpper(),
                    JambRegistrationNumber = c.Person.ApplicantJambDetails.FirstOrDefault() == null ? "N/A" : c.Person.ApplicantJambDetails.FirstOrDefault().JambNumber.ToUpper(),
                    SessionId = c.SessionId,
                    SessionName = c.Session.Name,
                    Paid = c.DatePaid != null ? true : false,
                    Amount = c.Amount,
                    Description = c.FeeType.Name.ToUpper(),
                    PaymentGatewayDescription = "N/A",
                    Email = c.Person.Email,
                    PhoneNumber = c.Person.PhoneNumber
                }).FirstOrDefaultAsync();

                return invoice;
            }
            return null;
        }
    }
}
