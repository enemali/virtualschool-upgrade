﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Acceptance.Queries
{
    public class GetApplicantAcceptanceStatusQueryHandler : IRequestHandler<GetApplicantAcceptanceStatusQuery, AcceptanceView>
    {
        private readonly VirtualSchoolDbContext _context;
        public GetApplicantAcceptanceStatusQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<AcceptanceView> Handle(GetApplicantAcceptanceStatusQuery request, CancellationToken cancellationToken)
        {
            var status = new AcceptanceView();
            if (request.FormNumber != null)
            {
                status = await _context.ApplicationForm
                .Where(x => x.FormNumber == request.FormNumber)
                .Select(y => new AcceptanceView
                {
                    PersonId = y.PersonId,
                    ExamNumber = y.ExamNumber,
                    SerialNumber = y.SerialNumber,
                    FormNumber = y.FormNumber,
                }).FirstOrDefaultAsync();
            }
            else if (request.PersonId > 0)
            {
                status = await _context.ApplicationForm
                .Where(x => x.PersonId == request.PersonId)
                .Select(y => new AcceptanceView
                {
                    PersonId = y.PersonId,
                    ExamNumber = y.ExamNumber,
                    SerialNumber = y.SerialNumber,
                    FormNumber = y.FormNumber,
                }).FirstOrDefaultAsync();
            }
            return status;

        }
    }
}
