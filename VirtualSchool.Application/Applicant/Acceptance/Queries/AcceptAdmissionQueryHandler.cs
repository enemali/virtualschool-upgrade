﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Domain.Entities;
using System.Linq.Expressions;

namespace VirtualSchool.Application.Applicant.Acceptance.Queries
{
    public class AcceptAdmissionQueryHandler : IRequestHandler<AcceptAdmissionQuery, AcceptAdmissionDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public AcceptAdmissionQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<AcceptAdmissionDTO> Handle(AcceptAdmissionQuery request, CancellationToken cancellationToken)
        {
            try
            {
                AcceptAdmissionDTO acceptAdmissionDTO = new AcceptAdmissionDTO();
                var applicationEntity = await _context.ApplicationForm.Where(x => x.PersonId == request.PersonId).Include(a => a.Person).FirstOrDefaultAsync();

                if (applicationEntity == null)
                {
                    throw new NotFoundException(nameof(ApplicationForm), request.PersonId);
                }
                
                var returnedAdmissionList = await CheckAdmissionList(applicationEntity.Id);
                if( returnedAdmissionList == null)
                {
                    throw new NotFoundException(nameof(ApplicationForm), request.PersonId);
                }

                var feetype = new FeeType { Id = (int)FeeTypeEnum.AcceptanceFee };
                PaymentDTO paymentDTO = new PaymentDTO
                {
                    PersonId = applicationEntity.PersonId,
                    FeeTypeId = feetype.Id,
                    SessionId = returnedAdmissionList.SessionId,
                };

                var returnedPayment = await AcceptancePaymentInvoice(paymentDTO);
                var returnedPaymentEtranzact = await CheckPaymentInEtranzact(returnedPayment);
                if (returnedPaymentEtranzact != null)
                {
                    acceptAdmissionDTO.Fullname = applicationEntity.Person.Firstname + " " + applicationEntity.Person.Surname + " " + applicationEntity.Person.Othername;
                    acceptAdmissionDTO.AcceptAdmission = true;
                    acceptAdmissionDTO.Amount = returnedPaymentEtranzact.TransactionAmount;
                    acceptAdmissionDTO.DateGenerated = returnedPayment.DateGenerated;
                    acceptAdmissionDTO.FormNumber = applicationEntity.FormNumber;
                    acceptAdmissionDTO.PaymentId = returnedPayment.Id;
                    acceptAdmissionDTO.PersonId = applicationEntity.PersonId;
                }
                else
                {
                    acceptAdmissionDTO.Fullname = applicationEntity.Person.Firstname + " " + applicationEntity.Person.Surname + " " + applicationEntity.Person.Othername;
                    acceptAdmissionDTO.AcceptAdmission = false;
                    acceptAdmissionDTO.FormNumber = applicationEntity.FormNumber;
                }

                return acceptAdmissionDTO;

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<AdmissionList> CheckAdmissionList(int ApplicationFormId)
        {
            try
            {
                Expression<Func<AdmissionList, bool>> selector = p => p.ApplicationFormId == ApplicationFormId;
                var applicationList = await _context.AdmissionList.Where(selector).FirstOrDefaultAsync();

                return applicationList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ApplicationFormSetting> GetApplicationFormSettingBy(ApplicationForm applicationForm)
        {
            try
            {
                Expression<Func<ApplicationFormSetting, bool>> selector = p => p.Id == applicationForm.ApplicationFormSettingId;
                var applicationFormSetting = await _context.ApplicationFormSetting.Where(selector).FirstOrDefaultAsync();
                return applicationFormSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Payment> AcceptancePaymentInvoice(PaymentDTO paymentDTO)
        {
            try
            {

                Expression<Func<Payment, bool>> selector =
                    p =>
                        p.FeeTypeId == paymentDTO.FeeTypeId &&
                        p.PersonId == paymentDTO.PersonId && 
                        p.SessionId==paymentDTO.SessionId;
                var paymentEntity = await _context.Payment.Where(selector).FirstOrDefaultAsync();
                return paymentEntity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PaymentEtranzact> CheckPaymentInEtranzact(Payment payment)
        {
            try
            {
                var etranzactEntity = await _context.PaymentEtranzact.Where(x => x.PaymentId == payment.Id).FirstOrDefaultAsync();
                return etranzactEntity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Person> GetPersonById(int PersonId)
        {
            try
            {
                var person = await _context.Person.FindAsync(PersonId);
                return person;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
