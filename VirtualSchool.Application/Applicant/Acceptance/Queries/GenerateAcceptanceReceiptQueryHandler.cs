﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Persistence;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Domain.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Acceptance.Queries
{
    public class GenerateAcceptanceReceiptQueryHandler : IRequestHandler<GenerateAcceptanceReceiptQuery, AcceptanceReceiptDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public GenerateAcceptanceReceiptQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AcceptanceReceiptDTO> Handle(GenerateAcceptanceReceiptQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var acceptanceReceiptDTO = new AcceptanceReceiptDTO();
                decimal shortFall = 0;
                var person = new Person();
                var etranzactPayment = await CheckPaymentInEtranzact(request.GenerateAcceptanceReceiptObject.ConfirmationNo, request.GenerateAcceptanceReceiptObject.InvoiceNumber);
                if (etranzactPayment != null)
                {
                    person = etranzactPayment.Payment.Person;
                    acceptanceReceiptDTO.FullName = person.Firstname + " " + person.Surname + " " + person.Othername;

                    if (etranzactPayment.Payment.Amount == etranzactPayment.TransactionAmount)
                    {
                        acceptanceReceiptDTO.Amount = etranzactPayment.TransactionAmount;
                        acceptanceReceiptDTO.ConfirmationNo = etranzactPayment.ConfirmationNo;
                        acceptanceReceiptDTO.FeeType = etranzactPayment.Payment.FeeType.Name;
                        acceptanceReceiptDTO.TransactionDate = etranzactPayment.TransactionDate;
                        acceptanceReceiptDTO.InvoiceGenerationDate = etranzactPayment.Payment.DateGenerated;

                    }
                    else if (etranzactPayment.Payment.Amount > etranzactPayment.TransactionAmount)
                    {
                        shortFall = (decimal)(etranzactPayment.Payment.Amount - etranzactPayment.TransactionAmount);
                        acceptanceReceiptDTO.PaymentShortFall = "You Have made a Short Payment of:" + shortFall;
                    }
                    else
                    {
                        shortFall = (decimal)(etranzactPayment.TransactionAmount - etranzactPayment.Payment.Amount);
                        acceptanceReceiptDTO.PaymentShortFall = "You Have made Surplus Payment of:" + shortFall;
                    }

                }
                return acceptanceReceiptDTO;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<PaymentEtranzact> CheckPaymentInEtranzact(string ConfirmationNo, string InvoiceNo)
        {
            try
            {
                var etranzactEntity = await _context.PaymentEtranzact.Where(x => x.ConfirmationNo == ConfirmationNo && x.CustomerID == InvoiceNo).Include(a => a.Payment).Include(a => a.Payment.Person).Include(a => a.Payment.FeeType).FirstOrDefaultAsync();
                return etranzactEntity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
