﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;

namespace VirtualSchool.Application.Applicant.Acceptance.Queries
{
    public class AcceptAdmissionQuery : IRequest<AcceptAdmissionDTO>
    {
        public string FormNumber { get; set; }
        public int PersonId { get; set; }
    }
}
