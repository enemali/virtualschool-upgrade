﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;

namespace VirtualSchool.Application.Applicant.Acceptance.Commands
{
    public class GenerateAcceptanceReceiptCommand : IRequest<AcceptanceReceiptDTO>
    {
        public GenerateAcceptanceReceiptObject GenerateAcceptanceReceiptObject { get; set; }
    }
}
