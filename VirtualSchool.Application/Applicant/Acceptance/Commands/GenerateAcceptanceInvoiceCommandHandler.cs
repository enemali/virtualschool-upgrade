﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Persistence;
using VirtualSchool.Application.Exceptions;
using System.Linq;
using VirtualSchool.Application.Applicant.Form.Models;
using Microsoft.EntityFrameworkCore;
using VirtualSchool.Application.Utilities.FeeType.Models;
using VirtualSchool.Domain.Entities;
using System.Linq.Expressions;
using System.Transactions;
using VirtualSchool.Domain.Enumerations;

namespace VirtualSchool.Application.Applicant.Acceptance.Commands
{
    public class GenerateAcceptanceInvoiceCommandHandler : IRequestHandler<GenerateAcceptanceInvoiceCommand, AcceptanceInvoiceView>
    {
        private readonly VirtualSchoolDbContext _context;
        public GenerateAcceptanceInvoiceCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<AcceptanceInvoiceView> Handle(GenerateAcceptanceInvoiceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var applicationEntity = _context.ApplicationForm.Where(x => x.FormNumber == request.GenerateAcceptanceInvoiceDTO.ApplicationFormNumber).Include(a => a.Person).Include(a => a.ApplicationFormSetting).FirstOrDefault();
                if (applicationEntity == null)
                {
                    throw new NotFoundException(nameof(ApplicationForm), request.GenerateAcceptanceInvoiceDTO.ApplicationFormNumber);
                }
                
                PaymentMode paymentMode = new PaymentMode { Id = request.GenerateAcceptanceInvoiceDTO.PaymentModeId };

                var feeType = new FeeType { Id = (int)FeeTypeEnum.AcceptanceFee };

                var status = ApplicationStatusEnum.GeneratedAcceptanceInvoice;

               
                var returnedFeeDetail= await GetFeeDetailBy(applicationEntity.ApplicationFormSetting);
                if (returnedFeeDetail == null)
                {
                    throw new NotFoundException(nameof(ApplicationForm), request.GenerateAcceptanceInvoiceDTO.ApplicationFormNumber);
                }

                var returnedFee = await GetFeeBy(returnedFeeDetail);
                returnedFeeDetail.Fee = new Fee();
                returnedFeeDetail.Fee = returnedFee ?? throw new NotFoundException(nameof(ApplicationForm), request.GenerateAcceptanceInvoiceDTO.ApplicationFormNumber);
                var invoice = await GenerateInvoiceAsync(applicationEntity, feeType, status, paymentMode, returnedFeeDetail);
                var acceptanceInvoiceView = new AcceptanceInvoiceView
                {
                    FormNumber = invoice.InvoiceNumber,
                    PersonId = invoice.PersonId,
                    FeeTypeId = invoice.FeeTypeId,
                    Amount = returnedFeeDetail.Fee.Amount,
                    DateGenerated=invoice.DateGenerated,
                    PaymentId=invoice.Id
                };
                return acceptanceInvoiceView;
            }
            catch(Exception ex)
            {
                throw ex;
            }

            
        }
        
        public async Task<Payment> GenerateInvoiceAsync(ApplicationForm form, FeeType feeType, ApplicationStatusEnum status, PaymentMode paymentMode, FeeDetail feeDetail)
        {
            try
            {
                CreateAcceptancePaymentCommand paymentRequest = new CreateAcceptancePaymentCommand
                {
                    PaymentModeId = paymentMode.Id,
                    Person = form.Person,
                    DatePaid = DateTime.Now,
                    FeeTypeId = feeType.Id,
                    PaymentTypeId = paymentMode.Id,
                    LevelId = (int)Levels.Applicant,
                    Amount=feeDetail.Fee.Amount,
                    SessionId=feeDetail.SessionId

                };


                
                //Check if student is in admission list 
                var list = new AdmissionList();
                list = await GetBy(form);
                Payment newPayment = null;
                if (list != null && list.Id > 0)
                {
                    if (await PaymentAlreadyMadeAsync(paymentRequest))
                    {
                        return await GetBy(form.Person, feeType);
                    }
                    using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                    {
                        Payment[] paymentTask = await Task.WhenAll(CreateAsync(paymentRequest));
                        newPayment = paymentTask.FirstOrDefault();
                        transaction.Complete();
                    }
                }
                return newPayment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> PaymentAlreadyMadeAsync(CreateAcceptancePaymentCommand payment)
        {
            try
            {
                
                Expression<Func<Payment, bool>> selector =
                    p =>
                        p.FeeTypeId == payment.FeeTypeId && p.PaymentModeId == payment.PaymentModeId &&
                        p.PaymentTypeId == payment.PaymentTypeId && p.PersonId == payment.Person.Id;
                var paymentEntity = await _context.Payment.Where(selector).FirstOrDefaultAsync();
                if (paymentEntity != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Payment> GetBy(Person person, FeeType feeType)
        {
            try
            {
                Expression<Func<Payment, bool>> selector = p => p.PersonId == person.Id && p.FeeTypeId == feeType.Id;
                var payment = await _context.Payment.Where(selector).FirstOrDefaultAsync();
                // SetFeeDetails(payment);

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<Person> GetByPersonId(int PersonId)
        {
            try
            {
                Expression<Func<Person, bool>> selector = p => p.Id == PersonId;
                var person = await _context.Person.Where(selector).FirstOrDefaultAsync();

                return person;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<AdmissionList> GetBy(ApplicationForm applicationForm)
        {
            try
            {
                Expression<Func<AdmissionList, bool>> selector = p => p.ApplicationFormId == applicationForm.Id && p.Activated == true;
                var admissionList = await _context.AdmissionList.Where(selector).FirstOrDefaultAsync();

                return admissionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<FeeDetail> GetFeeDetailBy(ApplicationFormSetting applicationFormSetting)
        {
            try
            {
                Expression<Func<FeeDetail, bool>> selector = p => p.LevelId == applicationFormSetting.LevelId && p.SessionId == applicationFormSetting.SessionId && p.FeeTypeId == applicationFormSetting.FeeTypeId;
                var feeDetail = await _context.FeeDetail.Where(selector).FirstOrDefaultAsync();

                return feeDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Fee> GetFeeBy(FeeDetail feeDetail)
        {
            try
            {
                var fee = await _context.Fee.FindAsync(feeDetail.FeeId);
                return fee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> UpdateStatus(ApplicationForm form, ApplicationStatusEnum status)
        {
            try
            {
                Expression<Func<Domain.Entities.Applicant, bool>> selector = a => a.ApplicationFormId == form.Id;
                var entity = await _context.Applicant.Where(selector).FirstOrDefaultAsync();

                if (entity == null)
                {
                    
                }

                entity.ApplicantStatusId = (int)status;
                _context.Add(entity);
                int modifiedRecordCount=await _context.SaveChangesAsync();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Payment> CreateAsync(CreateAcceptancePaymentCommand request)
        {
            try
            {
                //Create Payment
                var payment = new Payment
                {
                    Person = request.Person,
                    PaymentModeId = request.PaymentModeId,
                    PaymentTypeId = request.PaymentModeId,
                    FeeTypeId = request.FeeTypeId,
                    SessionId = request.SessionId,
                    LevelId = (int)Levels.Applicant,
                    Amount = request.Amount,
                    InvoiceNumber = "INV" + DateTime.Now.Ticks.ToString().PadLeft(10, '0'),
                    DateGenerated = DateTime.Now,
                    SerialNumber = DateTime.Now.Ticks,
                    DatePaid = DateTime.Now

                };
                
                //payment.FeeType = payment.FeeType;
                _context.Add(payment);
                await _context.SaveChangesAsync();
                
                return payment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Payment SetNextPaymentNumber(Payment payment)
        {
            try
            {
                payment.SerialNumber = payment.Id;
                payment.InvoiceNumber = "INV" + DateTime.Now.ToString("yy") + PaddNumber(payment.Id, 10);
                payment.DateGenerated = DateTime.Now;

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string PaddNumber(long id, int maxCount)
        {
            try
            {
                string idInString = id.ToString();
                string paddNumbers = "";
                if (idInString.Count() < maxCount)
                {
                    int zeroCount = maxCount - id.ToString().Count();
                    var builder = new StringBuilder();
                    for (int counter = 0; counter < zeroCount; counter++)
                    {
                        builder.Append("0");
                    }

                    builder.Append(id);
                    paddNumbers = builder.ToString();
                    return paddNumbers;
                }

                return paddNumbers;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<bool> SetInvoiceNumber(Payment payment)
        {
            try
            {
                Expression<Func<Payment, bool>> selector = p => p.Id == payment.Id;
                var entity = await _context.Payment.Where(selector).FirstOrDefaultAsync();
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Payment), payment.Id);
                }

                entity.SerialNumber = payment.SerialNumber;
                entity.InvoiceNumber = payment.InvoiceNumber;
                _context.Add(entity);
                int modifiedRecordCount = await _context.SaveChangesAsync();

                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
