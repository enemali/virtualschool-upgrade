﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Acceptance.Commands
{
    public class GenerateReceiptWithInvoiceNumberCommandHandler : IRequestHandler<GenerateReceiptWithInvoiceNumberCommand, AcceptanceReceiptDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public GenerateReceiptWithInvoiceNumberCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AcceptanceReceiptDTO> Handle(GenerateReceiptWithInvoiceNumberCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var acceptanceReceiptDTO = new AcceptanceReceiptDTO();
                decimal? shortFall;
                var person = new Person();
                var returnedEtranzactPayment = await CheckPaymentInEtranzact(request.InvoiceNumber);
                if (returnedEtranzactPayment == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.PaymentEtranzact), "No Matching Record is Found in Etranzact Table");
                }
                var returnedPayment = await GetPayment(returnedEtranzactPayment.PaymentId);
                if (returnedPayment != null)
                {
                    var feeType = await GetFeeTypeById(returnedPayment.FeeTypeId);
                    person = await GetPersonById(returnedPayment.PersonId);
                    if (person != null)
                    {
                        acceptanceReceiptDTO.PaymentId = returnedEtranzactPayment.PaymentId;
                        acceptanceReceiptDTO.InvoiceNumber = returnedEtranzactPayment.CustomerID;
                        acceptanceReceiptDTO.Amount = returnedEtranzactPayment.TransactionAmount;
                        acceptanceReceiptDTO.ConfirmationNo = returnedEtranzactPayment.ConfirmationNo;
                        acceptanceReceiptDTO.FeeType = feeType.Name;
                        acceptanceReceiptDTO.FullName = person.Firstname + " " + person.Surname + " " + person.Othername;
                        acceptanceReceiptDTO.TransactionDate = returnedEtranzactPayment.TransactionDate;
                        acceptanceReceiptDTO.InvoiceGenerationDate = returnedPayment.DateGenerated;

                        if (returnedPayment.Amount > returnedEtranzactPayment.TransactionAmount)
                        {
                            shortFall = (returnedPayment.Amount - returnedEtranzactPayment.TransactionAmount);
                            acceptanceReceiptDTO.PaymentShortFall = "You Have made a Short Payment of:" + shortFall;
                        }
                        else
                        {
                            shortFall = (returnedEtranzactPayment.TransactionAmount - returnedPayment.Amount);
                            acceptanceReceiptDTO.PaymentShortFall = "You Have made Surplus Payment of:" + shortFall;
                        }
                    }
                }
                return acceptanceReceiptDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    
    public async Task<PaymentEtranzact> CheckPaymentInEtranzact(string InvoiceNo)
    {
        try
        {
            var etranzactEntity = await _context.PaymentEtranzact.Where(x => x.CustomerID == InvoiceNo).FirstOrDefaultAsync();
            return etranzactEntity;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public async Task<Payment> GetPayment(int PaymentId)
    {
        try
        {
            var payment = await _context.Payment.FindAsync(PaymentId);
            return payment;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public async Task<Person> GetPersonById(int PersonId)
    {
        try
        {
            var person = await _context.Person.FindAsync(PersonId);

            return person;
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task<FeeType> GetFeeTypeById(int feeTypeId)
    {
        try
        {
            var feeType = await _context.FeeType.FindAsync(feeTypeId);

            return feeType;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
}
