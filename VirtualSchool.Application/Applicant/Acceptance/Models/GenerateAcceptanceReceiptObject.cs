﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class GenerateAcceptanceReceiptObject
    {
        public string ConfirmationNo { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
