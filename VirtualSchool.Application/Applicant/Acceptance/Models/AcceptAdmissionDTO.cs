﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class AcceptAdmissionDTO
    {
        public bool AcceptAdmission { get; set; }
        public int PersonId { get; set; }
        public string FormNumber { get; set; }
        public int FeeTypeId { get; set; }
        public decimal? Amount { get; set; }
        public DateTime DateGenerated { get; set; }
        public int PaymentId { get; set; }
        public string Fullname { get; set; }

    }
}
