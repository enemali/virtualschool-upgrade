﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public enum PaymentModeEnum
    {
        FullInstallment = 1,
        FirstInstallment = 2,
        SecondInstallment = 3
    }
}
