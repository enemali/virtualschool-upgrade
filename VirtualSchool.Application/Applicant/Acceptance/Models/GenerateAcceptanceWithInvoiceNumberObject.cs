﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class GenerateAcceptanceWithInvoiceNumberObject: IRequest<AcceptanceReceiptDTO>
    {
        public string InvoiceNumber { get; set; }
    }
}
