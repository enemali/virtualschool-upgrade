﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class GenerateAcceptanceInvoiceDTO
    {
        public string ApplicationFormNumber { get; set; }
        public int PersonId { get; set; }
        public int PaymentModeId { get; set; }
        public int PaymentTypeId { get; set; }
        
    }
}
