﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public enum FeeTypeEnum
    {
        ApplicationForm = 1,
        AcceptanceFee = 2,
        SchoolFees=3,
    }
}
