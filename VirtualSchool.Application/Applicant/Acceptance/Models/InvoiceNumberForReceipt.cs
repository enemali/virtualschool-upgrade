﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class InvoiceNumberForReceipt
    {
        public string InvoiceNumber { get; set; }
    }
}
