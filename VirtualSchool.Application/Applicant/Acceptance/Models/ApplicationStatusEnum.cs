﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public enum ApplicationStatusEnum
    {
        GeneratedAcceptanceInvoice = 3,
        GeneratedAcceptanceReceipt = 4,
    }
}
