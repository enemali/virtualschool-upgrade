﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class AcceptanceReceiptDTO
    {
        public int PaymentId { get; set; }
        public string InvoiceNumber { get; set; }
        public string ConfirmationNo { get; set; }
        public string FullName { get; set; }
        public string FeeType { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime? InvoiceGenerationDate { get; set; }
        public string PaymentShortFall { get; set; }
    }
}
