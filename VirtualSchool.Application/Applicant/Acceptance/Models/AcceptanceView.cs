﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class AcceptanceView
    {
        public long PersonId { get; set; }
        public string Name { get; set; }
        public string ProgrammeName { get; set; }

        public string FormNumber { get; set; }
        public string ExamNumber { get; set; }
        public int SerialNumber { get; set; }

        public string Status { get; set; }
    }
}
