﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Acceptance.Models
{
    public class PaymentDTO
    {
        public int PersonId { get; set; }
        public int DepartmentId { get; set; }
        public int ProgrammeId { get; set; }
        public int? DepartmentOptionId { get; set; }
        public string JambRegistrationNumber { get; set; }
        public int SessionId { get; set; }
        public int FeeTypeId { get; set; }
        public int PaymentTypeId { get; set; }
        public int PaymentModeId { get; set; }
        public int LevelId { get; set; }
        public decimal Amount { get; set; }
        public DateTime DatePaid { get; set; }
        public DateTime? DateGenerator { get; set; }
    }
}
