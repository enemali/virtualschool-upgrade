﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Commands
{
    public class ReturningStudentCourseRegistrationCommand : IRequest<ReturnedStudentCourseRegistration>
    {
        public int PersonId { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public bool Completed { get; set; }
        public List<RegisteredCoursesDTO> RegisteredCoursesDTO { get; set; }
    }
}
