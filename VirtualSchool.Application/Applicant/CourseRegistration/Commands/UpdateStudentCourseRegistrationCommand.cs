﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Commands
{
    public class UpdateStudentCourseRegistrationCommand : IRequest<UpdateCourseRegistrationDTO>
    {
        public int PersonId { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public long StudentCourseRegistrationId { get; set; }
        public UpdateCourseRegistrationDTO UpdateCourseRegistrationDTO { get; set; }
    }
}
