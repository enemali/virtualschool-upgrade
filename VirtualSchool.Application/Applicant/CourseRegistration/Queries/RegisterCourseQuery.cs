﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Queries
{
    public class RegisterCourseQuery : IRequest<PullCoursesForRegistrationDTO>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
    }
}
