﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Domain.Entities;
using System.Linq.Expressions;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Queries
{
    class GetRegisteredCourseQueryHandler : IRequestHandler<GetRegisteredCourseQuery, CourseRegistrationDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public GetRegisteredCourseQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<CourseRegistrationDTO> Handle(GetRegisteredCourseQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var listOfCoursesRegistered = await GetRegisteredCourses(request);
                return listOfCoursesRegistered;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<CourseRegistrationDTO> GetRegisteredCourses(GetRegisteredCourseQuery request)
        {
            CourseRegistrationDTO courseRegistrationDTO = new CourseRegistrationDTO();
            courseRegistrationDTO.CoursesDTO = new List<CoursesDTO>();
            try
            {
                Expression<Func<StudentCourseRegistration, bool>> selector = p => p.StudentId == request.PersonId && p.SessionId == request.SessionId;
                var courseRegistered = await _context.StudentCourseRegistration.Where(selector).FirstOrDefaultAsync();
                if (courseRegistered != null)
                {
                    var returnedRegisteredCourses = await GetRegisteredCourses(courseRegistered, request);
                    foreach(var returnedRegisteredCourse in returnedRegisteredCourses)
                    {
                        CoursesDTO course = new CoursesDTO();
                        course.CourseId = returnedRegisteredCourse.Id;
                        course.CourseCode = returnedRegisteredCourse.Code;
                        course.CourseName = returnedRegisteredCourse.Name;
                        course.CourseTypeId = returnedRegisteredCourse.CourseTypeId;
                        course.CourseUnit = returnedRegisteredCourse.Unit;
                        course.IsRegistered = true;
                        courseRegistrationDTO.CoursesDTO.Add(course);
                    }
                    courseRegistrationDTO.DepartmentId = courseRegistered.DepartmentId;
                    courseRegistrationDTO.LevelId = courseRegistered.LevelId;
                    courseRegistrationDTO.ProgrammeId = courseRegistered.ProgrammeId;
                    courseRegistrationDTO.SessionId = courseRegistered.SessionId;
                    courseRegistrationDTO.PersonId = request.PersonId;
                }

                return courseRegistrationDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Course>> GetRegisteredCourses(StudentCourseRegistration studentCourseRegistration, GetRegisteredCourseQuery request)
        {
            List<StudentCourseRegistrationDetail> studentCourseRegistrationDetailList = new List<StudentCourseRegistrationDetail>();
            try
            {
                Expression<Func<StudentCourseRegistrationDetail, bool>> selector = p => p.StudentCourseRegistrationId == studentCourseRegistration.Id;
                var courseRegisteredDetailList = await _context.StudentCourseRegistrationDetail.Where(selector).ToListAsync();
                var returnedregisteredCoursesList = await GetRegisteredCoursesByCourseId(courseRegisteredDetailList);

                return returnedregisteredCoursesList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Course>> GetRegisteredCoursesByCourseId(List<StudentCourseRegistrationDetail> studentCourseRegistrationDetail)
        {
            List<Course> registeredCoursesList = new List<Course>();
            try
            {
                foreach (var courseDetail in studentCourseRegistrationDetail)
                {
                    var registeredCourses = await _context.Course.FindAsync(courseDetail.CourseId);
                    registeredCoursesList.Add(registeredCourses);
                };


                return registeredCoursesList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
