﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Application.Applicant.CourseRegistration.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Queries
{
    public class ReturningStudentRegisterCourseQueryHandler : IRequestHandler<ReturningStudentRegisterCourseQuery, PullCoursesForRegistrationDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public ReturningStudentRegisterCourseQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<PullCoursesForRegistrationDTO> Handle(ReturningStudentRegisterCourseQuery request, CancellationToken cancellationToken)
        {
            PullCoursesForRegistrationDTO pullCoursesForRegistrationDTO = new PullCoursesForRegistrationDTO();
            pullCoursesForRegistrationDTO.CoursesDTO = new List<CoursesDTO>();
            CoursesDTO coursesDTO = new CoursesDTO();
            try
            {
                //Get the department the student is admitted to from the AdmissionList
                var departmentId = await GetStudentDepartment(request.PersonId);
                //Ensure the student has paid Required SchoolFees For the Session
                var StudentHasPaid = await HasStudentPaidCompleteSchoolFeesBy(request.SessionId, request.PersonId, departmentId);
                if (StudentHasPaid)
                {
                    //Get Student Current Level
                    var StudentLevel = await GetStudentCurrentLevel(request.PersonId);

                    if (StudentLevel != null)
                    {
                        //Check max and min Course unit for the Session and Semester
                        var returnedCourseUnit = await GetMaxandMinCourseUnit(request, StudentLevel.LevelId, departmentId);
                        //Get list of Courses to Register
                        var courseList = await GetCourseBy(request, StudentLevel.LevelId, departmentId);
                        //Ensure the student has not registered course for the Selected Session and Semester
                        var isRegistered = await HasRegisteredCourse(request);
                        if (!isRegistered)
                        {


                            if (returnedCourseUnit != null)
                            {

                                if (courseList != null && courseList.Count() > 0)
                                {
                                    foreach (var course in courseList)
                                    {
                                        coursesDTO = new CoursesDTO();
                                        coursesDTO.CourseId = course.Id;
                                        coursesDTO.CourseCode = course.Code;
                                        coursesDTO.CourseName = course.Name;
                                        coursesDTO.CourseTypeId = course.CourseTypeId;
                                        coursesDTO.CourseUnit = course.Unit;
                                        pullCoursesForRegistrationDTO.CoursesDTO.Add(coursesDTO);
                                    }
                                    pullCoursesForRegistrationDTO.DepartmentId = departmentId;
                                    pullCoursesForRegistrationDTO.LevelId = StudentLevel.LevelId;
                                    pullCoursesForRegistrationDTO.MaxCourseUnit = returnedCourseUnit.MaximumUnit;
                                    pullCoursesForRegistrationDTO.MinCourseUnit = returnedCourseUnit.MinimumUnit;
                                    pullCoursesForRegistrationDTO.PersonId = request.PersonId;
                                    pullCoursesForRegistrationDTO.SessionId = request.SessionId;
                                    pullCoursesForRegistrationDTO.SemesterId = request.SemesterId;
                                }
                            }

                        }
                        else
                        {
                            var returnRegisteredCourses = await AlreadyRegisteredCoursesAsync(request);
                            if (courseList.Count() > 0 && courseList != null)
                            {
                                foreach (var course in courseList)
                                {
                                    coursesDTO = new CoursesDTO();
                                    var existId = returnRegisteredCourses.Where(x => x.Id == course.Id).FirstOrDefault();
                                    if (existId == null)
                                    {
                                        coursesDTO.IsRegistered = false;
                                    }
                                    else
                                    {
                                        coursesDTO.IsRegistered = true;
                                    }

                                    coursesDTO.CourseId = course.Id;
                                    coursesDTO.CourseCode = course.Code;
                                    coursesDTO.CourseName = course.Name;
                                    coursesDTO.CourseTypeId = course.CourseTypeId;
                                    coursesDTO.CourseUnit = course.Unit;
                                    pullCoursesForRegistrationDTO.CoursesDTO.Add(coursesDTO);
                                }
                                pullCoursesForRegistrationDTO.DepartmentId = departmentId;
                                pullCoursesForRegistrationDTO.LevelId = StudentLevel.LevelId;
                                pullCoursesForRegistrationDTO.MaxCourseUnit = returnedCourseUnit.MaximumUnit;
                                pullCoursesForRegistrationDTO.MinCourseUnit = returnedCourseUnit.MinimumUnit;
                                pullCoursesForRegistrationDTO.PersonId = request.PersonId;
                                pullCoursesForRegistrationDTO.SessionId = request.SessionId;
                                pullCoursesForRegistrationDTO.SemesterId = request.SemesterId;
                            }
                        }
                    }
                }
                else
                {

                }
                return pullCoursesForRegistrationDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> GetStudentDepartment(int personId)
        {
            try
            {
                var departmentId = 0;
                var applicationForm = await _context.ApplicationForm.Where(x => x.PersonId == personId).FirstOrDefaultAsync();
                if (applicationForm != null)
                {
                    var isAdmitted = await _context.AdmissionList.Where(x => x.ApplicationFormId == applicationForm.Id).FirstOrDefaultAsync();
                    if (isAdmitted != null)
                    {
                        departmentId = isAdmitted.DepartmentId;
                    }
                }


                return departmentId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> HasStudentPaidCompleteSchoolFeesBy(int SessionId, int PersonId, int DepartmenId)
        {
            try
            {
                var feeType = new FeeType { Id = (int)FeeTypeEnum.SchoolFees };
                var paymentModeFirst = new PaymentMode { Id = (int)PaymentModeEnum.FirstInstallment };
                var paymentModeFull = new PaymentMode { Id = (int)PaymentModeEnum.FullInstallment };
                Expression<Func<PaymentEtranzact, bool>> selector = p => (p.OnlinePayment.Payment.PersonId == PersonId && p.OnlinePayment.Payment.SessionId == SessionId && p.OnlinePayment.Payment.FeeType.Id == feeType.Id && p.OnlinePayment.Payment.PaymentModeId == paymentModeFull.Id)
                || (p.OnlinePayment.Payment.PersonId == PersonId && p.OnlinePayment.Payment.SessionId == SessionId && p.OnlinePayment.Payment.FeeTypeId == feeType.Id && p.OnlinePayment.Payment.PaymentModeId == paymentModeFirst.Id);
                var paymentEtranzact = await _context.PaymentEtranzact.Where(selector).FirstOrDefaultAsync();
                if (paymentEtranzact != null || paymentEtranzact.ConfirmationNo != null)
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }
        public async Task<StudentLevel> GetStudentCurrentLevel(int personId)
        {
            try
            {
                var currenrLevel = await _context.StudentLevel.Where(x => x.StudentId == personId).FirstOrDefaultAsync();



                return currenrLevel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> HasRegisteredCourse(ReturningStudentRegisterCourseQuery request)
        {
            try
            {
                List<StudentCourseRegistrationDetail> studentCourseRegistrationDetailList = new List<StudentCourseRegistrationDetail>();
                Expression<Func<StudentCourseRegistration, bool>> selector = p => p.StudentId == request.PersonId && p.SessionId == request.SessionId;
                List<StudentCourseRegistration> courseRegisteredList = await _context.StudentCourseRegistration.Where(selector).ToListAsync();
                if (courseRegisteredList != null && courseRegisteredList.Count > 0)
                {
                    foreach (var courseRegistered in courseRegisteredList)
                    {
                        var studentCourseRegistrationDetail = await _context.StudentCourseRegistrationDetail.Where(x => x.StudentCourseRegistrationId == courseRegistered.Id).FirstOrDefaultAsync();
                        if (studentCourseRegistrationDetail.SemesterId == request.SemesterId)
                        {
                            studentCourseRegistrationDetailList.Add(studentCourseRegistrationDetail);
                        }
                    }
                    if (studentCourseRegistrationDetailList.Count < 0 || studentCourseRegistrationDetailList == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Course>> AlreadyRegisteredCoursesAsync(ReturningStudentRegisterCourseQuery request)
        {
            List<Course> courses = new List<Course>();
            try
            {

                Expression<Func<StudentCourseRegistration, bool>> selector = p => p.StudentId == request.PersonId && p.SessionId == request.SessionId;
                StudentCourseRegistration courseRegistered = await _context.StudentCourseRegistration.Where(selector).FirstOrDefaultAsync();
                if (courseRegistered != null)
                {
                    var studentCourseRegistrationDetail = await _context.StudentCourseRegistrationDetail.Where(x => x.StudentCourseRegistrationId == courseRegistered.Id && x.SemesterId == request.SemesterId).ToListAsync();
                    if (studentCourseRegistrationDetail != null && studentCourseRegistrationDetail.Count > 0)
                    {
                        foreach (var courseDetail in studentCourseRegistrationDetail)
                        {
                            var registeredCourses = await _context.Course.FindAsync(courseDetail.CourseId);
                            courses.Add(registeredCourses);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return courses;
        }
        public async Task<CourseUnit> GetMaxandMinCourseUnit(ReturningStudentRegisterCourseQuery request, int levelId, int departmentId)
        {
            try
            {
                Expression<Func<CourseUnit, bool>> selector = p => p.LevelId == levelId && p.DepartmentId == departmentId && p.SemesterId == request.SemesterId;
                var maxMinCourseUnit = await _context.CourseUnit.Where(selector).FirstOrDefaultAsync();
                return maxMinCourseUnit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Course>> GetCourseBy(ReturningStudentRegisterCourseQuery request, int levelId, int departmentId)
        {
            try
            {
                Expression<Func<Course, bool>> selector = p => p.LevelId == levelId && p.DepartmentId == departmentId && p.SemesterId == request.SemesterId && p.Activated == true;
                var courseList = await _context.Course.Where(selector).ToListAsync();

                return courseList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
