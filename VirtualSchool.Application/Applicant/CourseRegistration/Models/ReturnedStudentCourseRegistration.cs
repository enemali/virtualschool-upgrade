﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Models
{
    public class ReturnedStudentCourseRegistration
    {
        public int PersonId { get; set; }
        public int DepartmentId { get; set; }
        public int ProgrammeId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
        public List<ReturnedStudentCourseRegistrationDetail> ReturnedStudentCourseRegistrationDetail { get; set; }
    }
}
