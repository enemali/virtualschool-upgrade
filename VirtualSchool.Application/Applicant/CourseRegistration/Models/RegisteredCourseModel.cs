﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Models
{
    public class RegisteredCourseModel
    {
        public int PersonId { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public List<RegisteredCoursesDTO> RegisteredCoursesDTO { get; set; }
    }
}
