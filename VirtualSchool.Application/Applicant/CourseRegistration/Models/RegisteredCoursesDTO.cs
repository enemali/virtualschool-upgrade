﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Models
{
    public class RegisteredCoursesDTO
    {
        public long CourseId { get; set; }
        public bool IsRegistered { get; set; }
        public int CourseModeId { get; set; }
    }
}
