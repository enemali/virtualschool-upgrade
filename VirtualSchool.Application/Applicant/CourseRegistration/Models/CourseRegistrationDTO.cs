﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.CourseRegistration.Models
{
    public class CourseRegistrationDTO
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public int SemesterId { get; set; }
        public List<CoursesDTO> CoursesDTO { get; set; }

    }
}
