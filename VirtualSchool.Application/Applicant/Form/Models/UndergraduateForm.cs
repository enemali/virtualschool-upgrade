﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Form.Models
{
    public class UndergraduateForm
    {
        public int Id { get; set; }
        public string FormNumber { get; set; }
        public string ExamNumber { get; set; }
        public int PersonId { get; set; }

        public PersonDetails Person { get; set; }
        public JambDetails JambDetails { get; set; }
        public NextOfkinDetails NextOfkinDetails { get; set; }
        public ProgrammeDetails ProgrammeDetails { get; set; }
        
        public OLevel FirstSittingOlevel { get; set; }
        public OLevel SecondSittingOlevel { get; set; }

    }

    public class PersonDetails
    {
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string State { get; set; }
        public string LocalGovernment { get; set; }
        public string Hometown { get; set; }
        public string ContactAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Religion { get; set; }
        public string PassportImage { get; set; }
    }

    public class JambDetails
    {
        public string JambRegistrationNumber { get; set; }
        public int? JambScore { get; set; }
        public string SchoolChoice { get; set; }
        public string FirstSubject { get; set; }
        public int? FirstSubjectScore { get; set; }
        public string SecondSubject { get; set; }
        public int? SecondSubjectScore { get; set; }
        public string ThirdSubject { get; set; }
        public int? ThirdSubjectScore { get; set; }
        public string FourthSubject { get; set; }
        public int? FourthSubjectScore { get; set; }
    }

    public class NextOfkinDetails
    {
        public string NextOfKin { get; set; }
        public string NextOfKinRelation { get; set; }
        public string NextOfKinAddress { get; set; }
        public string NextOfKinPhoneNumber { get; set; }
    }

    public class ProgrammeDetails
    {
        public string FacultyName { get; set; }
        public string DepartmentName { get; set; }
        public string ProgrammeName { get; set; }

    }
}
