﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Form.Models
{
    public class OLevel
    {
        public OLevel()
        {
            Subjects = new List<OlevelSubject>();
        }

        public int ExamTypeId { get; set; }
        public int ExamYear { get; set; }
        public string ExamType { get; set; }
        public string ExamNumber { get; set; }
        public string ScannedResultUrl { get; set; }
        public List<OlevelSubject> Subjects { get; set; }
    }
    
}
