﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Form.Models
{
    public class JambSubjects
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
