﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Form.Models
{
    public class UndergraduateApplicant
    {
        public long Id { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string ApplicationNumber { get; set; }
        public string JambNumber { get; set; }


    }
}
