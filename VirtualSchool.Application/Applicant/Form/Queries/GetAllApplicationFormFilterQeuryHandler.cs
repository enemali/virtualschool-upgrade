﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Form.Queries
{
    public class GetAllApplicationFormFilterQeuryHandler : IRequestHandler<GetAllApplicationFormFilterQeury, IEnumerable<UndergraduateApplicant>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllApplicationFormFilterQeuryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UndergraduateApplicant>> Handle(GetAllApplicationFormFilterQeury request, CancellationToken cancellationToken)
        {
            List<UndergraduateApplicant> applicants = new List<UndergraduateApplicant>(); 
            //Get Form
            var ApplicantsEntity = await _context.ApplicantAppliedCourse.Where( a =>  a.DepartmentId == request.departmentId
                                    && a.ProgrammeId == request.programmeId 
                                    && a.ApplicationForm.ApplicationFormSetting.SessionId == request.sessionId)
                                    .Include(p => p.ApplicationForm)
                                    .Include(p => p.Person)
                                    .Include(p => p.ApplicationForm.ApplicantJambDetails)
                                    .ToListAsync();
            if (ApplicantsEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantAppliedCourse), request.departmentId);
            }

            foreach (var item in ApplicantsEntity)
            {
                applicants.Add(new UndergraduateApplicant
                {
                    Id = item.PersonId,
                    Surname = item.Person.Surname,
                    Firstname = item.Person.Firstname,
                    Othername = item.Person.Othername,
                    ApplicationNumber = item.ApplicationForm.FormNumber,
                    JambNumber = item.ApplicationForm.ApplicantJambDetails.Count > 0 ? item.ApplicationForm.ApplicantJambDetails.FirstOrDefault().JambNumber : "N/A"
                });
            }

            return applicants;
          
        }
    }
}
