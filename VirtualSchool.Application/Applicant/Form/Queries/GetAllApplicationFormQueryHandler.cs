﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Form.Queries
{
    public class GetAllApplicationFormQueryHandler : IRequestHandler<GetAllApplicationFormQuery, IEnumerable<UndergraduateForm>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllApplicationFormQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UndergraduateForm>> Handle(GetAllApplicationFormQuery request, CancellationToken cancellationToken)
        {

            throw new NotImplementedException();
        }
    }
}
