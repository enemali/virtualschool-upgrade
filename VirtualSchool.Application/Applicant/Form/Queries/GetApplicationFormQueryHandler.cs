﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Form.Queries
{
    public class GetApplicationFormQueryHandler : IRequestHandler<GetApplicationFormQuery, UndergraduateForm>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetApplicationFormQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<UndergraduateForm> Handle(GetApplicationFormQuery request, CancellationToken cancellationToken)
        {
            //Get Form
            var formEntity = await _context.ApplicationForm.FindAsync(request.Id);
            if (formEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicationForm), request.Id);
            }

            //Get Person
            var personEntity = await _context.Person.Where(a => a.Id == formEntity.PersonId)
                .Select(p => new PersonDetails
                {
                    Surname = p.Surname,
                    Firstname = p.Firstname,
                    Othername = p.Othername,
                    Gender = p.Gender.Name,
                    Religion = p.Religion.Name,
                    State = p.State.Name,
                    LocalGovernment = p.LocalGovernment.Name,
                    DateOfBirth = p.DateOfBirth.ToString(),
                    ContactAddress = p.ContactAddress,
                    PassportImage = p.Picture,
                    Hometown = p.Hometown,
                    PermanentAddress = p.PermanentAddress,
                    PhoneNumber = p.PhoneNumber,

                }).FirstOrDefaultAsync(cancellationToken);
            if (personEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.Person), formEntity.PersonId);
            }


            var jambDetailEntity = await _context.ApplicantJambDetail.Where(j => j.PersonId == formEntity.PersonId)
                .Select(j => new JambDetails
                {
                    JambRegistrationNumber = j.JambNumber,
                    JambScore = j.JambScore,
                    SchoolChoice = j.SchoolChoice.ToString(),
                    FirstSubject = j.FirstSubject.Name,
                    FirstSubjectScore = j.FirstSubjectScore,
                    SecondSubject = j.SecondSubject.Name,
                    SecondSubjectScore = j.SecondSubjectScore,
                    ThirdSubject = j.ThirdSubject.Name,
                    ThirdSubjectScore = j.ThirdSubjectScore,
                    FourthSubject = j.FourthSubject.Name,
                    FourthSubjectScore = j.FourthSubjectScore,
                }).FirstOrDefaultAsync(cancellationToken);
            if (jambDetailEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantJambDetail), formEntity.PersonId);
            }

            var appliedCourseEntity = await _context.ApplicantAppliedCourse.Where(j => j.PersonId == formEntity.PersonId)
                .Select(a => new ProgrammeDetails
                {
                    DepartmentName = a.Department.Name,
                    FacultyName = a.Department.Faculty.Name,
                    ProgrammeName = a.Programme.Name
                }).FirstOrDefaultAsync(cancellationToken);
            if (appliedCourseEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantAppliedCourse), formEntity.PersonId);
            }

            var nextOfKinEntity = await _context.NextOfkin.Where(j => j.PersonId == formEntity.PersonId)
                .Select(n => new NextOfkinDetails
                {
                    NextOfKin = n.Name,
                    NextOfKinAddress = n.Address,
                    NextOfKinPhoneNumber = n.PhoneNumber,
                    NextOfKinRelation = n.RelationShip.Name
                }).FirstOrDefaultAsync(cancellationToken);
            if (nextOfKinEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.NextOfkin), formEntity.PersonId);
            }
            var olevelFirstSittingEntity = await _context.OlevelResult.Where(j => j.PersonId == formEntity.PersonId && j.OlevelSittingId == (int)OlevelSittings.First_Sitting).Include(j => j.OlevelResultDetails)
                                            .ThenInclude(b => b.OlevelSubject)
                                            .Include(b => b.OlevelResultDetails)
                                            .ThenInclude(b => b.olevelGrade)
                                            .Select(f => new OLevel
                                            {
                                                ExamNumber = f.ExamNumber,
                                                ExamType = f.OlevelType.Name,
                                                ScannedResultUrl = f.ScannedResultUrl,
                                                ExamYear = f.ExamYear,
                                                ExamTypeId = f.OlevelTypeId,
                                                Subjects = f.OlevelResultDetails.Select(r => new OlevelSubject
                                                {
                                                    GradeId = r.olevelGradeId,
                                                    GradeName = r.olevelGrade.Name,
                                                    SubjectId = r.OlevelSubjectId,
                                                    SubjectName = r.OlevelSubject.Name,
                                                }).ToList()
                                            })
                                            .FirstOrDefaultAsync(cancellationToken);

            if (olevelFirstSittingEntity == null)
            {
                throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.OlevelResultDetail), formEntity.PersonId);
            }

            var olevelSecondSittingEntity = await _context.OlevelResult.Where(j => j.PersonId == formEntity.PersonId && j.OlevelSittingId == (int)OlevelSittings.Second_Sitting).Include(j => j.OlevelResultDetails)
                                            .ThenInclude(b => b.OlevelSubject)
                                            .Include(b => b.OlevelResultDetails)
                                            .ThenInclude(b => b.olevelGrade)
                                            .Select(f => new OLevel
                                                {
                                                    ExamNumber = f.ExamNumber,
                                                    ExamType = f.OlevelType.Name,
                                                    ScannedResultUrl = f.ScannedResultUrl,
                                                    ExamYear = f.ExamYear,
                                                    ExamTypeId = f.OlevelTypeId,
                                                    Subjects = f.OlevelResultDetails.Select(r => new OlevelSubject
                                                    {
                                                        GradeId = r.olevelGradeId,
                                                        GradeName = r.olevelGrade.Name,
                                                        SubjectId = r.OlevelSubjectId,
                                                        SubjectName = r.OlevelSubject.Name,
                                                    }).ToList()
                                                 })
                                            .FirstOrDefaultAsync(cancellationToken);
            //if (olevelSecondSittingEntity == null)
            //{
            //    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.OlevelResultDetail), formEntity.PersonId);
            //}

            var Form = new UndergraduateForm()
            {
                Id = formEntity.Id,
                FormNumber = formEntity.FormNumber,
                ExamNumber = formEntity.ExamNumber,
                PersonId = formEntity.PersonId,

                Person = personEntity,
                JambDetails = jambDetailEntity,
                ProgrammeDetails = appliedCourseEntity,
                NextOfkinDetails = nextOfKinEntity,
                FirstSittingOlevel = olevelFirstSittingEntity,
                SecondSittingOlevel = olevelSecondSittingEntity

            };

            return Form;

        }
    }
}
