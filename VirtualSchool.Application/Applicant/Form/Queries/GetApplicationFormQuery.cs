﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Form.Models;

namespace VirtualSchool.Application.Applicant.Form.Queries
{
    public class GetApplicationFormQuery: IRequest<UndergraduateForm>
    {
        public int Id { get; set; }
    }
}
