﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Form.Models;

namespace VirtualSchool.Application.Applicant.Form.Queries
{
    public class GetAllApplicationFormFilterQeury : IRequest<IEnumerable<UndergraduateApplicant>>
    {
        public int departmentId { get; set; }
        public int programmeId { get; set; }
        public int sessionId { get; set; }
    }
}
