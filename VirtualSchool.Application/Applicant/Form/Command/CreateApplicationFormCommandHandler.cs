﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.CustomErrors;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Form.Command
{
    public class CreateApplicationFormCommandHandler : IRequestHandler<CreateApplicationFormCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateApplicationFormCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateApplicationFormCommand request, CancellationToken cancellationToken)
        {
            try
            {


                //Get Person
                var personEntity = await _context.Person.FindAsync(request.PersonId);
                if (personEntity == null)
                {
                    throw NotFoundError.Response("Person not Found");
                    
                }

                //Update Person
                personEntity.GenderId = request.GenderId;
                personEntity.BloodGroupId = request.BloodGroupId;
                personEntity.DateOfBirth = Convert.ToDateTime(request.DateOfBirth);
                personEntity.BloodGroupId = request.BloodGroupId;
                personEntity.CountryId = request.CountryId;
                personEntity.StateId = request.StateId;
                personEntity.LocalGovernmentId = request.LocalGovernmentId;
                personEntity.ReligionId = request.ReligionId;
                personEntity.Hometown = request.Hometown;
                personEntity.ContactAddress = request.ContactAddress;
                personEntity.PermanentAddress = request.PermanentAddress;
                personEntity.Picture = request.PassportImage;

                //Create Application Form
                var applicantApplied=await _context.ApplicantAppliedCourse.Where(f => f.PersonId == request.PersonId).Include(f => f.ApplicationFormSetting).ThenInclude(f => f.Programme).Include(f => f.Department).Include(f=>f.ApplicationFormSetting.Session).FirstOrDefaultAsync();
                var applicationFormNum=await GenerateApplicationNumber(applicantApplied);
                var form = new ApplicationForm()
                {
                    PersonId = request.PersonId,
                    ApplicationFormSettingId=applicantApplied.ApplicationFormSettingId,
                    ExamNumber=applicationFormNum.ExamNumber,
                    FormNumber=applicationFormNum.FormNumber,
                    SerialNumber=applicationFormNum.SerialNumber,
                    
                };
                _context.ApplicationForm.Add(form);

               


                //Update Jamb Details
                var jambDetailEntity = await _context.ApplicantJambDetail.Where(j => j.PersonId == request.PersonId).FirstOrDefaultAsync(cancellationToken);
                if (jambDetailEntity == null)
                {
                    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantJambDetail), request.PersonId);
                }

                jambDetailEntity.ApplicationForm = form;
                jambDetailEntity.JambScore = request.JambScore;
                jambDetailEntity.SchoolChoice = request.SchoolChoice;
                jambDetailEntity.FirstSubject = await _context.OlevelSubject.FindAsync(request.FirstSubject.Id);

                jambDetailEntity.FirstSubjectScore = request.FirstSubjectScore;
                jambDetailEntity.SecondSubject = await _context.OlevelSubject.FindAsync(request.SecondSubject.Id); ;
                jambDetailEntity.SecondSubjectScore = request.SecondSubjectScore;
                jambDetailEntity.ThirdSubject = await _context.OlevelSubject.FindAsync(request.ThirdSubject.Id); ;
                jambDetailEntity.ThirdSubjectScore = request.ThirdSubjectScore;
                jambDetailEntity.FourthSubject = await _context.OlevelSubject.FindAsync(request.FourthSubject.Id);
                jambDetailEntity.FourthSubjectScore = request.FourthSubjectScore;


                //update programme details
                var appliedCourseEntity = await _context.ApplicantAppliedCourse.Where(j => j.PersonId == request.PersonId).FirstOrDefaultAsync(cancellationToken);
               
                if (appliedCourseEntity == null)
                {
                    throw new NotFoundException(nameof(VirtualSchool.Domain.Entities.ApplicantAppliedCourse), request.PersonId);
                }

                appliedCourseEntity.ApplicationForm = form;


                //Create Applicant 
                var applicant = new Domain.Entities.Applicant
                {
                    Person = personEntity,
                    ApplicantStatusId = (int)Domain.Enumerations.ApplicantStatus.SubmittedApplicationForm,
                    ExtraCurricularActivities = request.ExtracurriculumActivities,
                    ApplicationForm = form

                };
                _context.Applicant.Add(applicant);

                //Create Olevel
                if (request.FirstSittingOlevel != null && !string.IsNullOrEmpty(request.FirstSittingOlevel.ExamNumber) && request.FirstSittingOlevel.Subjects.Count > 0)
                {
                    var OlevelResult = new OlevelResult()
                    {
                        ApplicationForm = form,
                        PersonId = request.PersonId,
                        ExamNumber = request.FirstSittingOlevel.ExamNumber,
                        ExamYear = request.FirstSittingOlevel.ExamYear,
                        OlevelSittingId = (int)OlevelSittings.First_Sitting,
                        OlevelTypeId = request.FirstSittingOlevel.ExamTypeId,
                        ScannedResultUrl = request.FirstSittingOlevel.ScannedResultUrl,
                    };

                    _context.OlevelResult.Add(OlevelResult);

                    foreach (var pair in request.FirstSittingOlevel.Subjects)
                    {
                        var resultDetail = new OlevelResultDetail()
                        {
                            olevelResult = OlevelResult,
                            olevelGradeId = pair.GradeId,
                            OlevelSubjectId = pair.SubjectId
                        };

                        _context.OlevelResultDetail.Add(resultDetail);
                    }
                }

                if (request.SecondSittingOlevel != null && !string.IsNullOrEmpty(request.SecondSittingOlevel.ExamNumber) && request.SecondSittingOlevel.Subjects.Count > 0)
                {
                    var OlevelResult = new OlevelResult()
                    {
                        ApplicationForm = form,
                        PersonId = request.PersonId,
                        ExamNumber = request.SecondSittingOlevel.ExamNumber,
                        ExamYear = request.SecondSittingOlevel.ExamYear,
                        OlevelSittingId = (int)OlevelSittings.Second_Sitting,
                        OlevelTypeId = request.SecondSittingOlevel.ExamTypeId,
                        ScannedResultUrl = request.SecondSittingOlevel.ScannedResultUrl,
                    };

                    _context.OlevelResult.Add(OlevelResult);

                    foreach (var pair in request.SecondSittingOlevel.Subjects)
                    {
                        var resultDetail = new OlevelResultDetail()
                        {
                            olevelResult = OlevelResult,
                            olevelGradeId = pair.GradeId,
                            OlevelSubjectId = pair.SubjectId
                        };

                        _context.OlevelResultDetail.Add(resultDetail);
                    }
                }


                //create next of kin
                var NextOfkin = new NextOfkin()
                {
                    Person = personEntity,
                    ApplicationForm = form,
                    Name = request.NextOfKin,
                    Address = request.NextOfKinAddress,
                    RelationShipId = request.NextOfKinRelationShipId,
                    PhoneNumber = request.NextOfKinPhoneNumber
                };

                _context.NextOfkin.Add(NextOfkin);


                await _context.SaveChangesAsync(cancellationToken);

                return form.Id;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<ApplicationForm> GenerateApplicationNumber(ApplicantAppliedCourse applicantAppliedCourse)
        {
            ApplicationForm applicationForm = new ApplicationForm();
            try
            {
                var lastApplication=await _context.ApplicationForm.Where(f => f.ApplicationFormSettingId == applicantAppliedCourse.ApplicationFormSetting.Id).ToListAsync();
                if (lastApplication?.Count > 0)
                {
                    var SessionName = applicantAppliedCourse.ApplicationFormSetting.Session.Code + "0000";
                    var SerialNumber = lastApplication.LastOrDefault().SerialNumber + 1;

                    var concatenateSerialNumber = SessionName + SerialNumber.ToString();
                    var FormType = applicantAppliedCourse.ApplicationFormSetting.FormCode;
                    applicationForm.FormNumber = "ABSU/" + applicantAppliedCourse.ApplicationFormSetting.Programme.Code + "/" + FormType + "/" + concatenateSerialNumber;
                    applicationForm.SerialNumber = SerialNumber;
                    var paddedSerialNumber = SerialNumber.ToString().PadLeft(6, '0');
                    applicationForm.ExamNumber = applicantAppliedCourse.Department.Code + "/" + applicantAppliedCourse.ApplicationFormSetting.Session.Code + "/" + paddedSerialNumber;

                }
                else
                {
                    var SessionName = applicantAppliedCourse.ApplicationFormSetting.Session.Code + "0000";
                    var SerialNumber = 1;
                    var concatenateSerialNumber = SessionName + SerialNumber.ToString();
                    var FormType = applicantAppliedCourse.ApplicationFormSetting.FormCode;
                    applicationForm.FormNumber = "ABSU/" + applicantAppliedCourse.ApplicationFormSetting.Programme.Code + "/" + FormType + "/" + concatenateSerialNumber;
                    applicationForm.SerialNumber = SerialNumber;
                    var paddedSerialNumber= SerialNumber.ToString().PadLeft(6, '0');
                    applicationForm.ExamNumber = applicantAppliedCourse.Department.Code + "/" + applicantAppliedCourse.ApplicationFormSetting.Session.Code + "/" + paddedSerialNumber;
                    
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return applicationForm;
        }
    }
}
