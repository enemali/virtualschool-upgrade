﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Form.Models;

namespace VirtualSchool.Application.Applicant.Form.Command
{
    public class CreateApplicationFormCommand: IRequest<int>
    {
        public int PersonId { get; set; }
        public int GenderId { get; set; }
        public string DateOfBirth { get; set; }
        public int BloodGroupId { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int LocalGovernmentId { get; set; }
        public int ReligionId { get; set; }
        public string Hometown { get; set; }
        public string ContactAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PassportImage { get; set; }
        
        public int JambScore { get; set; }
        public int SchoolChoice { get; set; }
        public JambSubjects FirstSubject { get; set; }
        public int FirstSubjectScore { get; set; }
        public JambSubjects SecondSubject { get; set; }
        public int SecondSubjectScore { get; set; }
        public JambSubjects ThirdSubject { get; set; }
        public int ThirdSubjectScore { get; set; }
        public JambSubjects FourthSubject { get; set; }
        public int FourthSubjectScore { get; set; }

        public OLevel FirstSittingOlevel { get; set; }
        public OLevel SecondSittingOlevel { get; set; }

        public string NextOfKin { get; set; }
        public int NextOfKinRelationShipId { get; set; }
        public string NextOfKinAddress { get; set; }
        public string NextOfKinPhoneNumber { get; set; }
        public string ExtracurriculumActivities { get; set; }

    }
}
