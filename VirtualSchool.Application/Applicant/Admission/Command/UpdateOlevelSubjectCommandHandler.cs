﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;


namespace VirtualSchool.Application.Applicant.Admission.Command
{
    
    public class UpdateOlevelSubjectCommandHandler : IRequestHandler<UpdateOlevelSubjectCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateOlevelSubjectCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateOlevelSubjectCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _context.AdmissionCriteriaForOLevelSubject.Where(s => s.AdmissionCriteriaId == request.AdmissionCriteriaId && s.OLevelSubjectId == request.OLevelSubjectId).FirstOrDefaultAsync(cancellationToken);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubject), "Admission Criteria Not Found");
                }
                else
                {


                    entity.OLevelSubjectId = request.OLevelSubjectId;
                    entity.MinimumOLevelGradeId = request.MinimumOLevelGradeId;
                    entity.IsCompulsory = request.IsCompulsory;
                    entity.AdmissionCriteriaId = request.AdmissionCriteriaId;


                    await _context.SaveChangesAsync(cancellationToken);


                    return entity.Id;

                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubject), ex.Message);

            }








        }
    }

}
