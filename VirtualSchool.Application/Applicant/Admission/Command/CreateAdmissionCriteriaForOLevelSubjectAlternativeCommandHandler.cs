﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
   
    public class CreateAdmissionCriteriaForOLevelSubjectAlternativeCommandHandler : IRequestHandler<CreateAdmissionCriteriaForOLevelSubjectAlternativeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateAdmissionCriteriaForOLevelSubjectAlternativeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAdmissionCriteriaForOLevelSubjectAlternativeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIFCriteriaExists = await _context.AdmissionCriteriaForOLevelSubjectAlternative.Where(s => s.AdmissionCriteriaForOLevelSubjectId == request.AdmissionCriteriaForOLevelSubjectId && s.OLevelSubjectId == request.OLevelSubjectId).ToListAsync(cancellationToken);
                if (checkIFCriteriaExists.Count > 0)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), "Criteria Already Exists");
                }
                else
                {
                    var admisssionCriteria = new VirtualSchool.Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative
                    {
                        OLevelSubjectId = request.OLevelSubjectId,
                        AdmissionCriteriaForOLevelSubjectId = request.AdmissionCriteriaForOLevelSubjectId

                    };

                    _context.AdmissionCriteriaForOLevelSubjectAlternative.Add(admisssionCriteria);
                    await _context.SaveChangesAsync(cancellationToken);
                    return admisssionCriteria.Id;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), ex.Message);

            }








        }
    }
}
