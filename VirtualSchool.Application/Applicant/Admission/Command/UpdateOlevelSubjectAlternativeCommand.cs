﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class UpdateOlevelSubjectAlternativeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int AdmissionCriteriaForOLevelSubjectId { get; set; }
        public int OLevelSubjectId { get; set; }
    }
}
