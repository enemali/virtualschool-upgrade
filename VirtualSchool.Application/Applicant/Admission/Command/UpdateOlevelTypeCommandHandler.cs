﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;


namespace VirtualSchool.Application.Applicant.Admission.Command
{
     
     public class UpdateOlevelTypeCommandHandler : IRequestHandler<UpdateOlevelTypeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateOlevelTypeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateOlevelTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _context.AdmissionCriteriaForOLevelType.Where(s => s.AdmissionCriteriaId == request.AdmissionCriteriaId && s.OLevelTypeId == request.OLevelTypeId).FirstOrDefaultAsync(cancellationToken);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelType), "Admission Criteria Not Found");
                }
                else
                {


                    entity.AdmissionCriteriaId = request.AdmissionCriteriaId;
                    entity.OLevelTypeId = request.OLevelTypeId;


                    await _context.SaveChangesAsync(cancellationToken);


                    return entity.Id;

                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), ex.Message);

            }








        }
    }
}
