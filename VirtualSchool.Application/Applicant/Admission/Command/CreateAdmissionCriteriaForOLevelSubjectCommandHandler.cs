﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;
namespace VirtualSchool.Application.Applicant.Admission.Command
{
    
    public class CreateAdmissionCriteriaForOLevelSubjectCommandHandler : IRequestHandler<CreateAdmissionCriteriaForOLevelSubjectCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateAdmissionCriteriaForOLevelSubjectCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAdmissionCriteriaForOLevelSubjectCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIFCriteriaExists = await _context.AdmissionCriteriaForOLevelSubject.Where(s => s.AdmissionCriteriaId == request.AdmissionCriteriaId && s.OLevelSubjectId == request.OLevelSubjectId).ToListAsync(cancellationToken);
                if (checkIFCriteriaExists.Count > 0)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubject), "Criteria Already Exists");
                }
                else
                {
                    var admisssionCriteria = new VirtualSchool.Domain.Entities.AdmissionCriteriaForOLevelSubject
                    {
                        OLevelSubjectId = request.OLevelSubjectId,
                        MinimumOLevelGradeId = request.MinimumOLevelGradeId,
                        AdmissionCriteriaId = request.AdmissionCriteriaId,
                        IsCompulsory = request.IsCompulsory,
                        
                    };

                    _context.AdmissionCriteriaForOLevelSubject.Add(admisssionCriteria);
                    await _context.SaveChangesAsync(cancellationToken);
                    return admisssionCriteria.Id;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);

            }








        }
    }
}
