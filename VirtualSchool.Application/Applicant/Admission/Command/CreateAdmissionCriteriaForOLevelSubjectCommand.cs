﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class CreateAdmissionCriteriaForOLevelSubjectCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public int OLevelSubjectId { get; set; }
        public int MinimumOLevelGradeId { get; set; }
        public bool IsCompulsory { get; set; }
    }
}
