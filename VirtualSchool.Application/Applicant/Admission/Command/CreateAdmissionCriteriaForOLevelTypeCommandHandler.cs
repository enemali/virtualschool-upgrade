﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
   

        public class CreateAdmissionCriteriaForOLevelTypeCommandHandler : IRequestHandler<CreateAdmissionCriteriaForOLevelTypeCommand, int>
        {
        private readonly VirtualSchoolDbContext _context;

        public CreateAdmissionCriteriaForOLevelTypeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAdmissionCriteriaForOLevelTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIFCriteriaExists = await _context.AdmissionCriteriaForOLevelType.Where(s => s.AdmissionCriteriaId == request.AdmissionCriteriaId && s.OLevelTypeId == request.OLevelTypeId).ToListAsync(cancellationToken);
                if (checkIFCriteriaExists.Count > 0)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelType), "Admission Criteria Already Exists");
                }
                else
                {


                    var admisssionCriteria = new VirtualSchool.Domain.Entities.AdmissionCriteriaForOLevelType
                    {
                        AdmissionCriteriaId = request.AdmissionCriteriaId,
                        OLevelTypeId = request.OLevelTypeId
                        


                    };

                    _context.AdmissionCriteriaForOLevelType.Add(admisssionCriteria);
                    await _context.SaveChangesAsync(cancellationToken);
                    return admisssionCriteria.Id;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelType), ex.Message);

            }








        }
    }
}

