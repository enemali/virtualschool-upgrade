﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class CreateAdmissionCriteriaForOLevelTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public int OLevelTypeId { get; set; }
    }
}
