﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
    
  public class UpdateAdmissionCriteriaCommandHandler : IRequestHandler<UpdateAdmissionCriteriaCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateAdmissionCriteriaCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateAdmissionCriteriaCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _context.AdmissionCriteria.Where(s => s.ProgrammeId == request.ProgrammeId && s.DepartmentId == request.DepartmentId).FirstOrDefaultAsync(cancellationToken);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), "Admission Criteria Not Found");
                }
                else
                {


                    entity.DateEntered = request.DateEntered;
                    entity.DepartmentId = request.DepartmentId;
                    entity.ProgrammeId = request.ProgrammeId;
                    entity.MinimumRequiredNumberOfSubject = request.MinimumRequiredNumberOfSubject;


                    await _context.SaveChangesAsync(cancellationToken);


                    return entity.Id;

                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);

            }








        }
    }
}
