﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;


namespace VirtualSchool.Application.Applicant.Admission.Command
{
   
           public class UpdateOlevelSubjectAlternativeCommandHandler : IRequestHandler<UpdateOlevelSubjectAlternativeCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public UpdateOlevelSubjectAlternativeCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateOlevelSubjectAlternativeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _context.AdmissionCriteriaForOLevelSubjectAlternative.Where(s => s.AdmissionCriteriaForOLevelSubjectId == request.AdmissionCriteriaForOLevelSubjectId && s.OLevelSubjectId == request.OLevelSubjectId).FirstOrDefaultAsync(cancellationToken);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), "Admission Criteria Not Found");
                }
                else
                {


                    entity.OLevelSubjectId = request.OLevelSubjectId;
                    entity.AdmissionCriteriaForOLevelSubjectId = request.AdmissionCriteriaForOLevelSubjectId;
                    

                    await _context.SaveChangesAsync(cancellationToken);


                    return entity.Id;

                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), ex.Message);

            }








        }
    }

}
