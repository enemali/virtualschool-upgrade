﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class UpdateAdmissionCriteriaCommand : IRequest<int>
    {
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int MinimumRequiredNumberOfSubject { get; set; }
        public DateTime DateEntered { get; set; }
    }
}
