﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Acceptance.Models;
using VirtualSchool.Domain.Entities;
namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class CreateAdmissionCommand : IRequest<int>
    {
        public int Id { get; set; }

        public int AdmissionListBatchId { get; set; }
      
        public int PersonId { get; set; }
        

        public int ProgrammeId { get; set; }
       

        public int DepartmentId { get; set; }
      

        public int? DepartmentOptionId { get; set; }
      

       // public int? ApplicationFormId { get; set; }
       
        public bool Activated { get; set; }

      //  public int SessionId { get; set; }
       
    }
}
