﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Admission.Command
{
    public class CreateAdmissionCriteriaCommandHandler : IRequestHandler<CreateAdmissionCriteriaCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateAdmissionCriteriaCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAdmissionCriteriaCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIFCriteriaExists = await _context.AdmissionCriteria.Where(s => s.ProgrammeId == request.ProgrammeId && s.DepartmentId == request.DepartmentId).ToListAsync(cancellationToken);
                if (checkIFCriteriaExists.Count > 0)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), "Admission Criteria Already Exists");
                }
                else
                {


                    var admisssionCriteria = new VirtualSchool.Domain.Entities.AdmissionCriteria
                    {
                        ProgrammeId = request.ProgrammeId,
                        DateEntered = DateTime.Now,
                        MinimumRequiredNumberOfSubject = request.MinimumRequiredNumberOfSubject,
                        DepartmentId = request.DepartmentId,



                    };

                    _context.AdmissionCriteria.Add(admisssionCriteria);
                    await _context.SaveChangesAsync(cancellationToken);
                    return admisssionCriteria.Id;
                }
            }
            catch(Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);

            }








        }
    }
}
