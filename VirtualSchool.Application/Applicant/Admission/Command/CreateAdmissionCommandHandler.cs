﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Persistence;
using VirtualSchool.Domain.Entities;
namespace VirtualSchool.Application.Applicant.Admission.Command
{
 
    public class CreateAdmissionCommandHandler : IRequestHandler<CreateAdmissionCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateAdmissionCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateAdmissionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIfStudentIsAdmitted = await _context.AdmissionList.Where(s => s.PersonId == request.PersonId).ToListAsync(cancellationToken);
                if (checkIfStudentIsAdmitted.Count > 0)
                {

                    throw new NotFoundException(nameof(Domain.Entities.AdmissionList), "Student Already Admitted");
                }
                else
                {
                    var getForm = await _context.ApplicationForm.Where(s => s.PersonId == request.PersonId).Include(a => a.ApplicationFormSetting).FirstOrDefaultAsync(cancellationToken);
                    if(getForm == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.ApplicationForm), "Student application form not found");
                    }
                    var admisssionList = new AdmissionList();

                    admisssionList.ProgrammeId = request.ProgrammeId;
                    admisssionList.ApplicationFormId = getForm.Id;
                    admisssionList.PersonId = request.PersonId;
                    admisssionList.Activated = true;
                    admisssionList.AdmissionListBatchId = request.AdmissionListBatchId;
                    admisssionList.SessionId = getForm.ApplicationFormSetting.SessionId;
                    admisssionList.DepartmentId = request.DepartmentId;
                    if(request.DepartmentOptionId > 0)
                    {
                       admisssionList.DepartmentOptionId = request.DepartmentOptionId;
                    }
                 
                        
                    _context.AdmissionList.Add(admisssionList);
                    await _context.SaveChangesAsync(cancellationToken);
                    return admisssionList.Id;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionList), ex.Message);

            }








        }
    }
}
