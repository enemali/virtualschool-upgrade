﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionCriteriaForOLevelSubjectDTO
    {
        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public string AdmissionCriteriaDepartment { get; set; }
        public string AdmissionCriteriaProgramme { get; set; }
        public int OLevelSubjectId { get; set; }
        public string OLevelSubjectName { get; set; }
        public int MinimumOLevelGradeId { get; set; }
        public string MinimumOLevelGradeName { get; set; }
        public bool IsCompulsory { get; set; }
    }
}
