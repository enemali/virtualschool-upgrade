﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionCriteriaForOLevelSubjectAlternativeDTO
    {
        public int Id { get; set; }
        public int AdmissionCriteriaForOLevelSubjectId { get; set; }
        public int OLevelSubjectId { get; set; }
        public string OLevelSubjectName { get; set; }
       
    }
}
