﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionSlipDTO
    {
        public int Id { get; set; }
        public string ApplicationFormNumber { get; set; }
        public int PersonId { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        private string _fullname()
        {
            return Firstname + " " + Surname + " " + Othername;
        }
        public string Fullaname
        {
            get { return _fullname(); }
            set { _fullname(); }
        }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int ProgrammeId { get; set; }
        public string ProgrammeName { get; set; }
        public string JambRegistrationNumber { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        
    }
}
