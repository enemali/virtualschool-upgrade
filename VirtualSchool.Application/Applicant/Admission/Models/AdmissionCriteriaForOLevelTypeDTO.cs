﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionCriteriaForOLevelTypeDTO
    {
        public int Id { get; set; }
        public int AdmissionCriteriaId { get; set; }
        public int OLevelTypeId { get; set; }
        public int AdmissionCriteriaProgrammeId { get; set; }
        public string AdmissionCriteriaProgrammeName { get; set; }
        public int AdmissionCriteriaDepartmentId { get; set; }
        public string AdmissionCriteriaDepartmentName { get; set; }
        public int AdmissionCriteriaMinimumRequiredNumberOfSubject { get; set; }
        public DateTime DateEntered { get; set; }
    }
}
