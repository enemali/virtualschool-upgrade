﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionCriteriaDTO
    {
        public int Id { get; set; }
        public int ProgrammeId { get; set; }
        public string ProgrammeName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int MinimumRequiredNumberOfSubject { get; set; }
        public DateTime DateEntered { get; set; }
    }
}
