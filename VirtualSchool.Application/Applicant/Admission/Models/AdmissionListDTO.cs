﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.Admission.Models
{
    public class AdmissionListDTO
    {
        public long AdmissionListId { get; set; }
        public long PersonId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int? DepartmentOptionId { get; set; }
        public int SessionId { get; set; }
        public bool Admitted { get; set; }

        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        private string _fullname()
        {
            return Firstname + " " + Surname + " " + Othername;
        }
        public string Fullaname
        {
            get { return _fullname(); }
            set { _fullname(); }
        }
        public string DepartmentName { get; set; }
        public string ProgrammeName { get; set; }
        public string ApplicationNumber { get; set; }
        public string SessionName { get; set; }
        public string DepartmentOptionName { get; set; }
        public string ImageUrl { get; set; }


    }
}
