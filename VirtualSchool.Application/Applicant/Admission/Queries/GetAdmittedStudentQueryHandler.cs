﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
  public class GetAdmittedStudentQueryHandler : IRequestHandler<GetAdmittedStudentQuery, IEnumerable<AdmissionListDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAdmittedStudentQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<AdmissionListDTO>> Handle(GetAdmittedStudentQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var admittedStudents = await _context.AdmissionList.Where(a => a.DepartmentId == request.departmentId && a.ProgrammeId == request.programmeId && a.SessionId == request.sessionId)
                .Select(entity => new AdmissionListDTO
                {
                    ProgrammeId = entity.ProgrammeId,
                    AdmissionListId = entity.Id,
                    PersonId = entity.PersonId,
                    SessionId = entity.SessionId,
                    DepartmentId = entity.DepartmentId,
                    DepartmentOptionId = entity.DepartmentOptionId,
                    Admitted = entity.Activated,
                    ApplicationNumber= entity.ApplicationForm != null && entity.ApplicationForm.FormNumber != null ? entity.ApplicationForm.FormNumber.ToUpper() : "N/A",
                    DepartmentName= entity.Department != null && entity.Department.Name != null ? entity.Department.Name.ToUpper() : "N/A",
                    Firstname= entity.Person != null && entity.Person.Firstname != null ? entity.Person.Firstname.ToUpper() : "N/A",
                    Othername= entity.Person!= null && entity.Person.Othername != null ? entity.Person.Othername.ToUpper() : "N/A",
                    ProgrammeName= entity.Programme != null && entity.Programme.Name != null ? entity.Programme.Name.ToUpper() : "N/A",
                    SessionName= entity.Session!=null && entity.Session.Name!=null? entity.Session.Name.ToUpper():"N/A",

                    Surname= entity.Person != null && entity.Person.Surname != null ? entity.Person.Surname.ToUpper() : "N/A",
                    DepartmentOptionName = entity.DepartmentOption.Name != null ? entity.DepartmentOption.Name.ToUpper() : "N/A",
                    ImageUrl=entity.Person.Picture
            }).ToListAsync(cancellationToken);

                return admittedStudents;
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionList), ex.Message);

            }
            


        }
    }
}
