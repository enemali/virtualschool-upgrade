﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
namespace VirtualSchool.Application.Applicant.Admission.Queries
{
   
     public class GetAdmissionCriteriaForOLevelSubjectAlternativeQueryHandler : IRequestHandler<GetAdmissionCriteriaForOLevelSubjectAlternativeQuery, AdmissionCriteriaForOLevelSubjectAlternativeDTO>
    {

        private readonly VirtualSchoolDbContext _context;

        public GetAdmissionCriteriaForOLevelSubjectAlternativeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AdmissionCriteriaForOLevelSubjectAlternativeDTO> Handle(GetAdmissionCriteriaForOLevelSubjectAlternativeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var getCriteria = await _context.AdmissionCriteriaForOLevelSubjectAlternative.Where(a => a.AdmissionCriteriaForOLevelSubjectId == request.AdmissionCriteriaForOLevelSubjectId && a.OLevelSubjectId == request.OLevelSubjectId).Include(a => a.OlevelSubject).FirstOrDefaultAsync();
                AdmissionCriteriaForOLevelSubjectAlternativeDTO criteria = new AdmissionCriteriaForOLevelSubjectAlternativeDTO();

                if (getCriteria == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubjectAlternative), "Subject criteria not found");
                }
                else
                {
                    criteria.AdmissionCriteriaForOLevelSubjectId = getCriteria.AdmissionCriteriaForOLevelSubjectId;
                    criteria.Id = getCriteria.Id;
                    criteria.OLevelSubjectName = getCriteria.OlevelSubject.Name;
                    criteria.OLevelSubjectId = getCriteria.OLevelSubjectId;
                   

                    return criteria;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);
            }
        }




    }
}
