﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmittedStudentQuery : IRequest<IEnumerable<AdmissionListDTO>>
    {
        public int departmentId { get; set; }
        public int programmeId { get; set; }
        public int sessionId { get; set; }
    }
}
