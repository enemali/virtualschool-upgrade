﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.Admission.Models;



namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class CheckAdmissionStatusQuery: IRequest<CheckAdmissionDTO>
    {
        public int PersonId { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
