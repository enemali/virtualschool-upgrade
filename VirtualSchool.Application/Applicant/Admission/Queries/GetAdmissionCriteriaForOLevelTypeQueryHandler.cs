﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
     public class GetAdmissionCriteriaForOLevelTypeQueryHandler : IRequestHandler<GetAdmissionCriteriaForOLevelTypeQuery, AdmissionCriteriaForOLevelTypeDTO>
     {

            private readonly VirtualSchoolDbContext _context;

        public GetAdmissionCriteriaForOLevelTypeQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AdmissionCriteriaForOLevelTypeDTO> Handle(GetAdmissionCriteriaForOLevelTypeQuery request, CancellationToken cancellationToken)
            {
                try
                {
                    var getCriteria = await _context.AdmissionCriteriaForOLevelType.Where(a => a.AdmissionCriteria.DepartmentId == request.DepartmentId && a.AdmissionCriteria.ProgrammeId == request.ProgrammeId && a.OLevelTypeId == request.OlevelTypeId).Include(s => s.AdmissionCriteria).ThenInclude( p => p.Programme).Include(d => d.AdmissionCriteria.Department).FirstOrDefaultAsync();
                    AdmissionCriteriaForOLevelTypeDTO criteria = new AdmissionCriteriaForOLevelTypeDTO();

                    if (getCriteria == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelType), "Criteria not found");
                    }
                    else
                    {
                        criteria.OLevelTypeId = getCriteria.OLevelTypeId;
                        criteria.AdmissionCriteriaId = getCriteria.AdmissionCriteriaId;
                        criteria.AdmissionCriteriaMinimumRequiredNumberOfSubject = getCriteria.AdmissionCriteria.MinimumRequiredNumberOfSubject;
                        criteria.AdmissionCriteriaProgrammeId = getCriteria.AdmissionCriteria.ProgrammeId;
                        criteria.AdmissionCriteriaProgrammeName = getCriteria.AdmissionCriteria.Programme.Name;
                        criteria.DateEntered = getCriteria.AdmissionCriteria.DateEntered;
                        criteria.AdmissionCriteriaDepartmentId = getCriteria.AdmissionCriteria.DepartmentId;
                        criteria.AdmissionCriteriaDepartmentName = getCriteria.AdmissionCriteria.Department.Name;

                        return criteria;
                    }
                }
                catch (Exception ex)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelType), ex.Message);
                }
            }




    }
}
