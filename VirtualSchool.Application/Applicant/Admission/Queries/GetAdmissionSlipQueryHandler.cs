﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
   
        public class GetAdmissionSlipQueryHandler : IRequestHandler<GetAdmissionSlipQuery, AdmissionSlipDTO>
        {
            private readonly VirtualSchoolDbContext _context;

            public GetAdmissionSlipQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<AdmissionSlipDTO> Handle(GetAdmissionSlipQuery request, CancellationToken cancellationToken)
            {
            var entity = await _context.AdmissionList.Where(a => a.PersonId == request.Id).Include(a => a.Person).Include(a => a.Session).Include(a=> a.Programme).Include( a => a.Department).Include(a => a.ApplicationForm).FirstOrDefaultAsync();

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionList), request.Id);
                }

                return new AdmissionSlipDTO
                {
                    Id = entity.Id,
                    Firstname = entity.Person.Firstname,
                    Surname = entity.Person.Surname,
                    Othername = entity.Person.Othername,
                    PersonId = entity.PersonId,
                    DepartmentId = entity.DepartmentId,
                    DepartmentName = entity.Department.Name,
                    ProgrammeId = entity.ProgrammeId,
                    ProgrammeName = entity.Programme.Name,
                    SessionId = entity.SessionId,
                    SessionName = entity.Session.Name,
                    ApplicationFormNumber = entity.ApplicationForm.FormNumber


                };
            }
        }
    
}
