﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
   
    public class GetAllAdmissionCriteriaQueryHandler : IRequestHandler<GetAllAdmissionCriteriaQuery, IEnumerable<AdmissionCriteriaDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAllAdmissionCriteriaQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<AdmissionCriteriaDTO>> Handle(GetAllAdmissionCriteriaQuery request, CancellationToken cancellationToken)
        {
            try
            {


               

              
                var criterias = await _context.AdmissionCriteria
                .Select(entity => new AdmissionCriteriaDTO
                {
                    ProgrammeId = entity.ProgrammeId,
                    ProgrammeName = entity.Programme.Name,
                    DepartmentName = entity.Department.Name,
                    DepartmentId = entity.DepartmentId,
                    MinimumRequiredNumberOfSubject = entity.MinimumRequiredNumberOfSubject,
                    DateEntered = entity.DateEntered,
                    Id = entity.Id
                }).OrderBy(p => p.DateEntered).ToListAsync(cancellationToken);

                return criterias;
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);

            }
        }



    }
}
