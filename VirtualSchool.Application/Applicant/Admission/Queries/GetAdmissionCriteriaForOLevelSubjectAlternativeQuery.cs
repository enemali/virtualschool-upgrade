﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmissionCriteriaForOLevelSubjectAlternativeQuery : IRequest<AdmissionCriteriaForOLevelSubjectAlternativeDTO>
    {
        public int Id { get; set; }
        public int AdmissionCriteriaForOLevelSubjectId { get; set; }
        public int OLevelSubjectId { get; set; }
    }
}
