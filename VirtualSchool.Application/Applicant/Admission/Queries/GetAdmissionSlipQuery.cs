﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmissionSlipQuery: IRequest<AdmissionSlipDTO>
    {
        public int Id { get; set; }
    }
}

