﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmissionCriteriaForOLevelSubjectQueryHandler : IRequestHandler<GetAdmissionCriteriaForOLevelSubjectQuery, AdmissionCriteriaForOLevelSubjectDTO>
    {

        private readonly VirtualSchoolDbContext _context;

        public GetAdmissionCriteriaForOLevelSubjectQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AdmissionCriteriaForOLevelSubjectDTO> Handle(GetAdmissionCriteriaForOLevelSubjectQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var getCriteria = await _context.AdmissionCriteriaForOLevelSubject.Where(a => a.AdmissionCriteriaId == request.AdmissionCriteriaId && a.OLevelSubjectId == request.OLevelSubjectId).Include(a => a.OlevelSubject).Include(a => a.AdmissionCriteria).Include(s => s.AdmissionCriteria.Department).Include(s => s.AdmissionCriteria.Programme).Include(s => s.OlevelGrade).FirstOrDefaultAsync();
                AdmissionCriteriaForOLevelSubjectDTO criteria = new AdmissionCriteriaForOLevelSubjectDTO();

                if (getCriteria == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteriaForOLevelSubject), "Subject criteria not found");
                }
                else
                {
                    criteria.AdmissionCriteriaDepartment = getCriteria.AdmissionCriteria.Department.Name;
                    criteria.AdmissionCriteriaProgramme = getCriteria.AdmissionCriteria.Programme.Name;
                    criteria.AdmissionCriteriaId = getCriteria.AdmissionCriteriaId;
                    criteria.MinimumOLevelGradeId = getCriteria.MinimumOLevelGradeId;
                    criteria.MinimumOLevelGradeName = getCriteria.OlevelGrade.Name;
                    criteria.OLevelSubjectId = getCriteria.OLevelSubjectId;
                    criteria.OLevelSubjectName = getCriteria.OlevelSubject.Name;
                    criteria.IsCompulsory = getCriteria.IsCompulsory;
                    criteria.Id = getCriteria.Id;

                    return criteria;
                }
            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);
            }
        }




    }
}
    

