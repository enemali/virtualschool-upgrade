﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.ELearning.Models;
using VirtualSchool.Persistence;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
   
    public class GetAdmissionCriteriaQueryHandler : IRequestHandler<GetAdmissionCriteriaQuery, AdmissionCriteriaDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public GetAdmissionCriteriaQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<AdmissionCriteriaDTO> Handle(GetAdmissionCriteriaQuery request, CancellationToken cancellationToken)
        {
            try
            {


                var entity = await _context.AdmissionCriteria.Where(a => a.DepartmentId == request.DepartmentId && a.ProgrammeId == request.ProgrammeId).Include(a => a.Programme).Include(a => a.Department).FirstOrDefaultAsync();

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), "No AdmissioncCriteria found");
                }

                return new AdmissionCriteriaDTO
                {
                    ProgrammeId = entity.ProgrammeId,
                    ProgrammeName = entity.Programme.Name,
                    DepartmentName = entity.Department.Name,
                    DepartmentId = entity.DepartmentId,
                    MinimumRequiredNumberOfSubject = entity.MinimumRequiredNumberOfSubject,
                    DateEntered = entity.DateEntered,
                    Id = entity.Id
                   



                };
            }
            catch(Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.AdmissionCriteria), ex.Message);

            }
        }
        


    }
}
