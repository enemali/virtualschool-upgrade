﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmissionCriteriaForOLevelSubjectQuery : IRequest<AdmissionCriteriaForOLevelSubjectDTO>
    {
        public int AdmissionCriteriaId { get; set; }
        public int OLevelSubjectId { get; set; }

    }
}
