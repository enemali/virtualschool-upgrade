﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.Admission.Models;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{
    public class GetAdmissionCriteriaForOLevelTypeQuery : IRequest<AdmissionCriteriaForOLevelTypeDTO>
    {
        public int DepartmentId { get; set; }
        public int ProgrammeId { get; set; }
        public int OlevelTypeId {get;set;}
    }
}
