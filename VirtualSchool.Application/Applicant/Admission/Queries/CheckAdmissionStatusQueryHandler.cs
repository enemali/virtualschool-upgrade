﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.Admission.Models;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.Admission.Queries
{

    public class CheckAdmissionStatusQueryHandler : IRequestHandler<CheckAdmissionStatusQuery, CheckAdmissionDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public CheckAdmissionStatusQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<CheckAdmissionDTO> Handle(CheckAdmissionStatusQuery request, CancellationToken cancellationToken)
        {
            AdmissionList checkAdmission = new AdmissionList();
            if (request.PersonId > 0)
            {
                checkAdmission = await _context.AdmissionList.Where(a => a.PersonId == request.PersonId)
                    .Include(a => a.DepartmentOption)
                    .Include(a => a.Department)
                    .Include(a => a.ApplicationForm)
                    .Include(a => a.AdmissionListBatch)
                    .Include(a => a.Programme)
                    .Include(a => a.Session)
                    .Include(a => a.Person)
                    .FirstOrDefaultAsync();
            }
            else if (!String.IsNullOrEmpty(request.ApplicationNumber))
            {
                checkAdmission = await _context.AdmissionList.Include(a=>a.ApplicationForm).Where(a => a.ApplicationForm.FormNumber == request.ApplicationNumber)
                    .Include(a=>a.DepartmentOption)
                    .Include(a=>a.Department)
                    .Include(a=>a.AdmissionListBatch)
                    .Include(a=>a.Programme)
                    .Include(a=>a.Session)
                    .Include(a=>a.Person)
                    .FirstOrDefaultAsync();
            }

            CheckAdmissionDTO admissionStatus = new CheckAdmissionDTO();

            if (checkAdmission == null)
            {
                var getPerson = new Person();
                if (request.PersonId > 0)
                {
                    getPerson = await _context.Person.FindAsync(request.PersonId);
                }
                else
                {
                    getPerson=await _context.ApplicationForm.Where(f => f.FormNumber == request.ApplicationNumber).Include(f=>f.Person).Select(f=>f.Person).FirstOrDefaultAsync();
                }
                
                if (getPerson == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Person), request.PersonId);
                }

                admissionStatus.PersonId = getPerson.Id;
                admissionStatus.Surname = getPerson.Surname;
                admissionStatus.Firstname = getPerson.Firstname;
                admissionStatus.Othername = getPerson.Othername;
                admissionStatus.Admitted = false;
                admissionStatus.AdmittedDescription = "Not yet admitted.";
                admissionStatus.ImageUrl = getPerson.Picture;

                return admissionStatus;

            }
            else
            {
                admissionStatus.Admitted = true;
                admissionStatus.Firstname = checkAdmission.Person.Firstname;
                admissionStatus.Othername = checkAdmission.Person.Othername;
                admissionStatus.Surname = checkAdmission.Person.Surname;
                admissionStatus.AdmittedDescription = "You have been admitted.";
                admissionStatus.ApplicationNumber = checkAdmission.ApplicationForm != null && checkAdmission.ApplicationForm.FormNumber != null ? checkAdmission.ApplicationForm.FormNumber.ToUpper() : "N/A";
                admissionStatus.ProgrammeName = checkAdmission.Programme != null && checkAdmission.Programme.Name != null ? checkAdmission.Programme.Name.ToUpper() : "N/A";
                admissionStatus.DepartmentName = checkAdmission.Department != null && checkAdmission.Department.Name != null ? checkAdmission.Department.Name.ToUpper() : "N/A";
                admissionStatus.SessionName = checkAdmission.Session != null && checkAdmission.Session.Name != null ? checkAdmission.Session.Name.ToUpper() : "N/A";
                admissionStatus.DepartmentOptionId = checkAdmission.DepartmentOptionId;
                admissionStatus.DepartmentOptionName = checkAdmission.DepartmentOption?.Name != null ? checkAdmission.DepartmentOption.Name.ToUpper() : "N/A";
                admissionStatus.Admitted = true;
                admissionStatus.SessionId = checkAdmission.SessionId;
                admissionStatus.PersonId = checkAdmission.PersonId;
                admissionStatus.ProgrammeId = checkAdmission.ProgrammeId;
                admissionStatus.DepartmentId = checkAdmission.DepartmentId;
                admissionStatus.AdmissionListId = checkAdmission.Id;
                admissionStatus.ImageUrl = checkAdmission.Person.Picture;


                return admissionStatus;
            }

        }


    }
}
