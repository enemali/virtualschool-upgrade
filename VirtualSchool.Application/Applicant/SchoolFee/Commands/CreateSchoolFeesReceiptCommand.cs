﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace VirtualSchool.Application.Applicant.SchoolFee.Commands
{
    public class CreateSchoolFeesReceiptCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }       
        public int PaymenTerminalId { get; set; }
        public int PaymentEtranzactTypeId { get; set; }       
        public int OnlinePaymentId { get; set; }
        public string ReceiptNo { get; set; }
        public string PaymentCode { get; set; }
        public string MerchantCode { get; set; }
        public decimal? TransactionAmount { get; set; }
        public string TransactionDescription { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string ConfirmationNo { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public bool Used { get; set; }
        public Int64 UsedBy { get; set; }

    }
}
