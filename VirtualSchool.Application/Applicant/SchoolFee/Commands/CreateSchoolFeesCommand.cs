﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;


namespace VirtualSchool.Application.Applicant.SchoolFee.Commands
{
    public class CreateSchoolFeesCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int PaymentModeId { get; set; }      
        public int PaymentTypeId { get; set; }
        public int FeeTypeId { get; set; }     
        public int SessionId { get; set; }
        public int LevelId { get; set; }     
        public decimal Amount { get; set; }
        public long SerialNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DateGenerated { get; set; }
        public DateTime? DatePaid { get; set; }
    }
}
