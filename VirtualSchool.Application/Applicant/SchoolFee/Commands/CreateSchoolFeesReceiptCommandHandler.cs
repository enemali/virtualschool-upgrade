﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.SchoolFee.Commands
{
 
    public class CreateSchoolFeesReceiptCommandHandler : IRequestHandler<CreateSchoolFeesReceiptCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateSchoolFeesReceiptCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateSchoolFeesReceiptCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIfpaymentExists = await _context.PaymentEtranzact.FindAsync(request.PaymentId);
                if (checkIfpaymentExists != null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.PaymentEtranzact), "Payment Already Exists in Etranzact Payments");
                }
                var getPayment = await _context.Payment.FindAsync(request.PaymentId);
                if(getPayment == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Payment), "Payment Not Found");

                }
                using (TransactionScope transaction = new TransactionScope())
                {
                    var etranzactEntity = new PaymentEtranzact
                    {
                        //Id = request.PaymentId,
                        PaymentId = request.PaymentId,
                        PaymenTerminalId = request.PaymenTerminalId,
                        PaymentEtranzactTypeId = request.PaymentEtranzactTypeId,
                        ReceiptNo = request.ReceiptNo,
                        OnlinePaymentId = request.PaymentId,
                        CustomerID = request.CustomerID,
                        ConfirmationNo = request.ConfirmationNo,
                        BankCode = request.BankCode,
                        MerchantCode = request.MerchantCode,
                        CustomerAddress = request.CustomerAddress,
                        CustomerName = request.CustomerName,
                        BranchCode = request.BranchCode,
                        PaymentCode = request.PaymentCode,
                        TransactionAmount = request.TransactionAmount,
                        TransactionDate = request.TransactionDate,
                        TransactionDescription = request.TransactionDescription,
                        
                    };


                    _context.PaymentEtranzact.Add(etranzactEntity);

                    //Updating Date Paid on Payment Table
                    getPayment.DatePaid = etranzactEntity.TransactionDate;
                    _context.Payment.Update(getPayment);
                    _context.SaveChanges();

                     transaction.Complete();
                    return etranzactEntity.Id;
                }

            }
            catch (Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.PaymentEtranzact), ex);

            }





        }   

    }
}
