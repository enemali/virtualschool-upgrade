﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Common;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;
using VirtualSchool.Domain.Entities;

namespace VirtualSchool.Application.Applicant.SchoolFee.Commands
{

    public class CreateSchoolFeesCommandHandler : IRequestHandler<CreateSchoolFeesCommand, int>
    {
        private readonly VirtualSchoolDbContext _context;

        public CreateSchoolFeesCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateSchoolFeesCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var checkIfAlreadGenerated = await _context.Payment.Where(s => s.PersonId == request.PersonId && s.FeeTypeId == request.FeeTypeId && s.SessionId == request.SessionId && s.PaymentModeId == request.PaymentModeId).ToListAsync(cancellationToken);
                if (checkIfAlreadGenerated.Count > 0)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Payment), "Student have already generated invoice for the selected feetype, session and paymentmode");
                }
                //To check if student paid first installment
                if(request.PaymentModeId == (int)PaymentModes.Second_Installment)
                {
                    var getFirstInstallment = await _context.Payment.Where(s => s.PersonId == request.PersonId && s.FeeTypeId == request.FeeTypeId && s.SessionId == request.SessionId && s.PaymentModeId == (int)PaymentModes.First_Installment && s.DatePaid != null).ToListAsync(cancellationToken);
                    if(getFirstInstallment.Count < 1)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.Payment), "Student have not genearted first installment in selected session and fee");

                    }
                    if(getFirstInstallment.Count == 1)
                    {
                        var checkPaid = await _context.PaymentEtranzact.Where(s => s.Id == getFirstInstallment.FirstOrDefault().Id).ToListAsync(cancellationToken);
                        if(checkPaid.Count < 1)
                        {
                            throw new NotFoundException(nameof(Domain.Entities.PaymentEtranzact), "Student have generated but have not paid first installment in selected session and fee");

                        }
                    }
                }
                using (TransactionScope transaction = new TransactionScope())
                {
                    var paymentEntity = new Payment
                    {
                        PersonId = request.PersonId,
                        DateGenerated = DateTime.Now,
                        FeeTypeId = request.FeeTypeId,
                        Amount = request.Amount,
                        SessionId = request.SessionId,
                        PaymentModeId = request.PaymentModeId,
                        PaymentTypeId = request.PaymentTypeId,
                        LevelId = request.LevelId,
                        SerialNumber = 0,
                        InvoiceNumber = "ABSU",
                    };


                    _context.Payment.Add(paymentEntity);
                   _context.SaveChanges();


                    paymentEntity.SerialNumber = paymentEntity.Id;
                    paymentEntity.InvoiceNumber = "ABSU" + DateTime.Now.ToString("yy") + PaddNumber(paymentEntity.Id, 10);
                    _context.Payment.Update(paymentEntity);


                    //Create OnlinePayment
                    PaymentChannel paymentChannel = new PaymentChannel() { PaymentChannnelId = (int)PaymentChannels.etranzact };
                    OnlinePayment onlinePayment = new OnlinePayment();
                    onlinePayment.PaymentId = paymentEntity.Id;
                    onlinePayment.TransactionDate = DateTime.Now;
                    onlinePayment.PaymentChannelId = (int)PaymentChannels.etranzact;
                    _context.OnlinePayment.Add(onlinePayment);

                    _context.SaveChanges();
                    transaction.Complete();
                    return paymentEntity.Id;
                }

            }
            catch(Exception ex)
            {
                throw new NotFoundException(nameof(Domain.Entities.Payment), ex);

            }





        }






        public static string PaddNumber(long id, int maxCount)
        {
            try
            {
                string idInString = id.ToString();
                string paddNumbers = "";
                if (idInString.Count() < maxCount)
                {
                    int zeroCount = maxCount - id.ToString().Count();
                    StringBuilder builder = new StringBuilder();
                    for (int counter = 0; counter < zeroCount; counter++)
                    {
                        builder.Append("0");
                    }

                    builder.Append(id);
                    paddNumbers = builder.ToString();
                    return paddNumbers;
                }
                else
                {
                    paddNumbers = id.ToString();
                }

                return paddNumbers;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}

