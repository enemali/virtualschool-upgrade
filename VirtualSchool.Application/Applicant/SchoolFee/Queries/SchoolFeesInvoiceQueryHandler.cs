﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Applicant.SchoolFee.Models;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Application.Utilities.Faculty.Models;
using VirtualSchool.Application.Applicant.Form.Models;
using VirtualSchool.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VirtualSchool.Application.Applicant.SchoolFee.Queries
{
   
        public class SchoolFeesInvoiceQueryHandler : IRequestHandler<SchoolFeesInvoiceQuery, SchoolFeesInvoiceDTO>
        {
            private readonly VirtualSchoolDbContext _context;

            public SchoolFeesInvoiceQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }

            public async Task<SchoolFeesInvoiceDTO> Handle(SchoolFeesInvoiceQuery request, CancellationToken cancellationToken)
            {
                var getPayment = await _context.Payment.Where(a => a.PersonId == request.PersonId && a.SessionId == request.SessionId && a.FeeTypeId == (int)FeeTypes.SchoolFees && a.PaymentModeId == request.PaymentModeId).Include(a => a.Person).Include(s => s.Session).FirstOrDefaultAsync();

                SchoolFeesInvoiceDTO schoolFees = new SchoolFeesInvoiceDTO();

                if (getPayment == null)
                {
                    var getPerson = await _context.Person.FindAsync(request.PersonId);
                    if (getPerson == null)
                    {

                        throw new NotFoundException(nameof(Domain.Entities.Person), request.PersonId);

                    }
                schoolFees.PersonId = getPerson.Id;
                schoolFees.Surname = getPerson.Surname;
                schoolFees.Firstname = getPerson.Firstname;
                schoolFees.Othername = getPerson.Othername;
                schoolFees.SessionId = request.SessionId;
                schoolFees.GeneratedInvoice = false;
                schoolFees.Description = "No Payment Record Found ";



                }
             else
            {
                var getStudent = await _context.StudentLevel.Where(s => s.StudentId == request.PersonId && s.SessionId == request.SessionId).Include(s => s.Student).ThenInclude(p => p.Person).Include(p => p.Department).Include(p => p.Programme).Include(p => p.Level).FirstAsync();
                if (getStudent == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.StudentLevel), request.PersonId);

                }
                else
                {
                    schoolFees.Firstname = getStudent.Student.Person.Firstname;
                    schoolFees.Surname = getStudent.Student.Person.Surname;
                    schoolFees.Othername = getStudent.Student.Person.Othername;
                    schoolFees.InvoiceNumber = getPayment.InvoiceNumber;
                    schoolFees.PersonId = getPayment.PersonId;
                    schoolFees.SessionId = getPayment.SessionId;
                    schoolFees.SessionName = getPayment.Session.Name;
                    schoolFees.DepartmentId = getStudent.DepartmentId;
                    schoolFees.DepartmentName = getStudent.Department.Name;
                    schoolFees.GeneratedInvoice = true;
                    schoolFees.ProgrammeId = getStudent.ProgrammeId;
                    schoolFees.ProgrammeName = getStudent.Programme.Name;
                    schoolFees.LevelId = getStudent.LevelId;
                    schoolFees.LevelName = getStudent.Level.Name;
                }
                    


               }
            return schoolFees;
            }


        }
}

