﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.SchoolFee.Models;
namespace VirtualSchool.Application.Applicant.SchoolFee.Queries
{
   
        public class SchoolFeesInvoiceQuery : IRequest<SchoolFeesInvoiceDTO>
        {
            public int PersonId { get; set; }
            public int SessionId { get; set; }
            public int PaymentModeId { get; set; }

        }
    
}
