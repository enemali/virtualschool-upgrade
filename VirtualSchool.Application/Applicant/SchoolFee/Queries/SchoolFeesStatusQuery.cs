﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using VirtualSchool.Application.Applicant.SchoolFee.Models;

namespace VirtualSchool.Application.Applicant.SchoolFee.Queries
{ 
        public class SchoolFeesStatusQuery : IRequest<SchoolFeesStatusDTO>
        {
            public int PersonId { get; set; }
            public int SessionId { get; set; }
            
        }
    }

