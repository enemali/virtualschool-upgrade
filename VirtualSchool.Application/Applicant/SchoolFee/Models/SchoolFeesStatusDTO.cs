﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Applicant.SchoolFee.Models
{
    public class SchoolFeesStatusDTO
    {
        public int PersonId { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        private string _fullname()
        {
            return Firstname + " " + Surname + " " + Othername;
        }
        public string Fullaname
        {
            get { return _fullname(); }
            set { _fullname(); }
        }
        public bool Paid { get; set; }
        public string SessionName { get; set; }
        public int SessionId { get; set; }
        public String Description { get; set; }

    }

}

