﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw MultipleChioceError.Response();
    /// </usage>
    /// </summary>
    public class MultipleChioceError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.MultipleChoices.ToString();

        private MultipleChioceError()
           : base(300, HttpStatusCode.MultipleChoices.ToString())
        {
            base.HttpErrorType = TYPE;
        }

        private MultipleChioceError(string message)
            : base(300, HttpStatusCode.MultipleChoices.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            MultipleChioceError multipleChioceError = null;
            if (Message == null)
            {
                multipleChioceError = new MultipleChioceError();
            }
            else
            {
                multipleChioceError = new MultipleChioceError(Message);
            }

            return new Exception(multipleChioceError.ToString());
        }

    }
}
