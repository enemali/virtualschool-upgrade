﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw AmbigiousError.Response();
    /// </usage>
    /// </summary>
    public class AmbigiousError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.Ambiguous.ToString();

        public AmbigiousError()
            : base(300, HttpStatusCode.Ambiguous.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        public AmbigiousError(string message)
            : base(300, HttpStatusCode.Ambiguous.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            AmbigiousError ambigiousError = null;
            if (Message == null)
            {
                ambigiousError = new AmbigiousError();
            }
            else
            {
                ambigiousError = new AmbigiousError(Message);
            }

            return new Exception(ambigiousError.ToString());
        }
    }
}
