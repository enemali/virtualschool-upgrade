﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw UnauthorizedError.Response();
    /// </usage>
    /// </summary>
    public class UnauthorizedError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.Unauthorized.ToString();

        private UnauthorizedError()
           : base(401, HttpStatusCode.Unauthorized.ToString())
        {
            base.HttpErrorType = TYPE;
        }

        private UnauthorizedError(string message)
            : base(401, HttpStatusCode.Unauthorized.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            UnauthorizedError unauthorizedError = null;
            if (Message == null)
            {
                unauthorizedError = new UnauthorizedError();
            }
            else
            {
                unauthorizedError = new UnauthorizedError(Message);
            }

            return new Exception(unauthorizedError.ToString());
        }
    }
}
