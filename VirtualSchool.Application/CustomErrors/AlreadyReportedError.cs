﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw AlreadyReportedError.Response();
    /// </usage>
    /// </summary>
    public class AlreadyReportedError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.AlreadyReported.ToString();

        private AlreadyReportedError()
           : base(208, HttpStatusCode.AlreadyReported.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private AlreadyReportedError(string message)
            : base(208, HttpStatusCode.AlreadyReported.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            AlreadyReportedError alreadyReported = null;
            if (Message == null)
            {
                alreadyReported = new AlreadyReportedError();
            }
            else
            {
                alreadyReported = new AlreadyReportedError(Message);
            }

            return new Exception(alreadyReported.ToString());
        }
    }
}
