﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw NoContentError.Response();
    /// </usage>
    /// </summary>
    public class NoContentError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.NoContent.ToString();

        private NoContentError()
           : base(204, HttpStatusCode.NoContent.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private NoContentError(string message)
            : base(204, HttpStatusCode.NoContent.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            NoContentError noContentError = null;
            if (Message == null)
            {
                noContentError = new NoContentError();
            }
            else
            {
                noContentError = new NoContentError(Message);
            }

            return new Exception(noContentError.ToString());
        }
    }
}
