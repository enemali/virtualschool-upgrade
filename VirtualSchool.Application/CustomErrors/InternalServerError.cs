﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw InternalServerError.Response();
    /// </usage>
    /// </summary>
    public class InternalServerError : ApiError
    {
        private readonly string TYPE = HttpStatusCode.GatewayTimeout.ToString();

        private InternalServerError()
            : base(500, HttpStatusCode.InternalServerError.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private InternalServerError(string message)
            : base(500, HttpStatusCode.InternalServerError.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            InternalServerError internalServerError = null;
            if (Message == null)
            {
                internalServerError = new InternalServerError();
            }
            else
            {
                internalServerError = new InternalServerError(Message);
            }

            return new Exception(internalServerError.ToString());
        }
    }
}
