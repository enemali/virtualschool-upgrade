﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// Holds a List a parameters that are used to create a Mock exception
    /// <field> StatusCode: This Takes an int which evaluates to a http exception
    /// <field> StatusDescription: This describes the Http error that was thrown I.E BadRequest
    /// <field> HttpErrorType: This is the name of the Http Error that was thrown I.E BadRequest
    /// <field> Message: This is a readable message that is set by the user and displayed
    /// </summary>
    public abstract class ApiError
    {
        public int StatusCode { get; private set; }

        public string StatusDescription { get; private set; }

        protected string HttpErrorType { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Message { get; private set; }

        public ApiError(int statusCode, string statusDescription)
        {
            this.StatusCode = statusCode;
            this.StatusDescription = statusDescription;
        }

        public ApiError(int statusCode, string statusDescription, string message)
            : this(statusCode, statusDescription)
        {
            this.Message = message;
        }

        public override string ToString()
        {
            string message = (!string.IsNullOrEmpty(Message) ?
                string.Format("Oops...an error occured.\nStatus Code: {0}\nType: {1}\nDescription: {2}\nMessage: {3}\n", StatusCode, HttpErrorType, StatusDescription, Message)
                : string.Format("Oops...an error occured.\nStatus Code: {0}\nType: {1}\nDescription: {2}\n", StatusCode, HttpErrorType, StatusDescription));
            return message;
        }
    }
}
