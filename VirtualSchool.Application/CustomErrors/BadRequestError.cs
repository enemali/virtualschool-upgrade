﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw BadRequestError.Response();
    /// </usage>
    /// </summary>
    public class BadRequestError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.BadRequest.ToString();

        private BadRequestError()
            : base(400, HttpStatusCode.BadRequest.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private BadRequestError(string message)
            : base(400, HttpStatusCode.BadRequest.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            BadRequestError badRequest = null;
            if (Message == null)
            {
                badRequest = new BadRequestError();
            }
            else
            {
                badRequest = new BadRequestError(Message);
            }
           
            return new Exception(badRequest.ToString());
        }
    }
}
