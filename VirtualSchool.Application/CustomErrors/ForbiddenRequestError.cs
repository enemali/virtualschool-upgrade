﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw ForbiddenRequestError.Response();
    /// </usage>
    /// </summary>
    public class ForbiddenRequestError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.Forbidden.ToString();

        private ForbiddenRequestError()
           : base(403, HttpStatusCode.Forbidden.ToString())
        {
            base.HttpErrorType = TYPE;
        }

        private ForbiddenRequestError(string message)
            : base(403, HttpStatusCode.Forbidden.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            ForbiddenRequestError forbiddenRequest = null;
            if (Message == null)
            {
                forbiddenRequest = new ForbiddenRequestError();
            }
            else
            {
                forbiddenRequest = new ForbiddenRequestError(Message);
            }

            return new Exception(forbiddenRequest.ToString());
        }
    }
}
