﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw GatewayTimeoutError.Response();
    /// </usage>
    /// </summary>
    public class GatewayTimeoutError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.GatewayTimeout.ToString();

        private GatewayTimeoutError()
            : base(504, HttpStatusCode.GatewayTimeout.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private GatewayTimeoutError(string message)
            : base(504, HttpStatusCode.GatewayTimeout.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            GatewayTimeoutError gatewayTimeout = null;
            if (Message == null)
            {
                gatewayTimeout = new GatewayTimeoutError();
            }
            else
            {
                gatewayTimeout = new GatewayTimeoutError(Message);
            }

            return new Exception(gatewayTimeout.ToString());
        }
    }
}
