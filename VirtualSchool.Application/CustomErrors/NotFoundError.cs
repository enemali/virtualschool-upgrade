﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw NotFoundError.Response();
    /// </usage>
    /// </summary>
    public class NotFoundError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.NotFound.ToString();

        private NotFoundError()
          : base(404, HttpStatusCode.NotFound.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private NotFoundError(string message)
            : base(404, HttpStatusCode.NotFound.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            NotFoundError notFoundError = null;
            if (Message == null)
            {
                notFoundError = new NotFoundError();
            }
            else
            {
                notFoundError = new NotFoundError(Message);
            }

            return new Exception(notFoundError.ToString());
        }
    }
}
