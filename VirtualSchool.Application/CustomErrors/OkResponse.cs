﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an exception instance through method: Response()
    /// The Response() takes an optional Message
    /// It returns an Object of OkResponse through the method: OkResponseObject()
    /// OkResponseObject() takes an optional parameter of Message
    /// <usage> 
    /// throw OkResponse.Response();
    /// </usage>
    /// <usage>
    /// OkResponse.OkResponseObject()
    /// </usage>
    /// </summary>
    public class OkResponse: ApiError
    {
        private readonly string TYPE = HttpStatusCode.OK.ToString();

        private OkResponse()
           : base(200, HttpStatusCode.OK.ToString())
        {
            base.HttpErrorType = TYPE;
        }

        private OkResponse(string message)
            : base(200, HttpStatusCode.OK.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static OkResponse OkResponseObject(string Message = null)
        {
            OkResponse ok = null;
            if (Message == null)
            {
                ok = new OkResponse();
            }
            else
            {
                ok = new OkResponse(Message);
            }

            return ok;
        }

        public static Exception Response(string Message = null)
        {
            OkResponse ok = null;
            if (Message == null)
            {
                ok = new OkResponse();
            }
            else
            {
                ok = new OkResponse(Message);
            }

            return new Exception(ok.ToString());
        }
    }
}
