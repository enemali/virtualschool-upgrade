﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace VirtualSchool.Application.CustomErrors
{
    /// <summary>
    /// This is Singleton class, Which returns an instance through method: Response()
    /// The Response() takes an optional Message
    /// <usage> 
    /// throw BadGatewayError.Response();
    /// </usage>
    /// </summary>
    public class BadGatewayError: ApiError
    {
        private readonly string TYPE = HttpStatusCode.BadGateway.ToString();

        private BadGatewayError()
            : base(502, HttpStatusCode.BadGateway.ToString())
        {
            base.HttpErrorType = TYPE;
        }


        private BadGatewayError(string message)
            : base(502, HttpStatusCode.BadGateway.ToString(), message)
        {
            base.HttpErrorType = TYPE;
        }

        public static Exception Response(string Message = null)
        {
            BadGatewayError badGateway = null;
            if (Message == null)
            {
                badGateway = new BadGatewayError();
            }
            else
            {
                badGateway = new BadGatewayError(Message);
            }

            return new Exception(badGateway.ToString());
        }
    }
}
