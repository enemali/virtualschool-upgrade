﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Student.StudentSchoolFee.Models;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.StudentSchoolFee.Queries
{
    public class StudentSchoolFeesReceiptQueryHandler : IRequestHandler<StudentSchoolFeesReceiptQuery, StudentSchoolFeesReceiptDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public StudentSchoolFeesReceiptQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<StudentSchoolFeesReceiptDTO> Handle(StudentSchoolFeesReceiptQuery request, CancellationToken cancellationToken)
        {
            var getPayment = await _context.Payment.Where(a => a.PersonId == request.PersonId && a.SessionId == request.SessionId && a.FeeTypeId == (int)FeeTypes.SchoolFees).Include(a => a.Person).Include(s => s.Session).FirstOrDefaultAsync();

            StudentSchoolFeesReceiptDTO schoolFees = new StudentSchoolFeesReceiptDTO();

            if (getPayment == null)
            {
                var getPerson = await _context.Person.FindAsync(request.PersonId);
                if (getPerson == null)
                {

                    throw new NotFoundException(nameof(Domain.Entities.Person), request.PersonId);

                }
                schoolFees.PersonId = getPerson.Id;
                schoolFees.Surname = getPerson.Surname;
                schoolFees.Firstname = getPerson.Firstname;
                schoolFees.Othername = getPerson.Othername;
                schoolFees.SessionId = request.SessionId;
                schoolFees.SchoolFeesPaid = false;
                schoolFees.Description = "No Payment Record Found";

            }
            else
            {

                var getPaymentEtranzact = await _context.PaymentEtranzact.Where(p => p.PaymentId == getPayment.Id).FirstOrDefaultAsync();
                if (getPaymentEtranzact != null)
                {
                    var getStudent = await _context.StudentLevel.Where(s => s.StudentId == request.PersonId && s.SessionId == request.SessionId).Include(s => s.Student).ThenInclude(p => p.Person).Include(p => p.Department).Include(p => p.Programme).Include(p => p.Level).FirstAsync();

                    if (getStudent == null)
                    {
                        throw new NotFoundException(nameof(Domain.Entities.StudentLevel), request.PersonId);

                    }
                    else
                    {
                        schoolFees.Firstname = getStudent.Student.Person.Firstname;
                        schoolFees.Surname = getStudent.Student.Person.Surname;
                        schoolFees.Othername = getStudent.Student.Person.Othername;
                        schoolFees.InvoiceNumber = getPayment.InvoiceNumber;
                        schoolFees.PersonId = getPayment.PersonId;
                        schoolFees.SessionId = getPayment.SessionId;
                        schoolFees.SessionName = getPayment.Session.Name;
                        schoolFees.DepartmentId = getStudent.DepartmentId;
                        schoolFees.DepartmentName = getStudent.Department.Name;
                        schoolFees.SchoolFeesPaid = true;
                        schoolFees.ProgrammeId = getStudent.ProgrammeId;
                        schoolFees.ProgrammeName = getStudent.Programme.Name;
                        schoolFees.LevelId = getStudent.LevelId;
                        schoolFees.LevelName = getStudent.Level.Name;
                        schoolFees.Amount = getPaymentEtranzact.TransactionAmount;
                        schoolFees.Description = "School Fees Paid For Session";

                    }



                }
                else
                {
                    schoolFees.Firstname = getPayment.Person.Firstname;
                    schoolFees.Surname = getPayment.Person.Surname;
                    schoolFees.Othername = getPayment.Person.Othername;
                    schoolFees.InvoiceNumber = getPayment.InvoiceNumber;
                    schoolFees.SchoolFeesPaid = false;
                    schoolFees.Description = "No School Fees Receipt/Payment Found";
                }


            }


            return schoolFees;

        }



    }
}
