﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Student.StudentSchoolFee.Models;
using VirtualSchool.Domain.Enumerations;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.StudentSchoolFee.Queries
{
    public class StudentSchoolFeesStatusQueryHandler : IRequestHandler<StudentSchoolFeesStatusQuery, StudentSchoolFeesStatusDTO>
    {
        private readonly VirtualSchoolDbContext _context;

        public StudentSchoolFeesStatusQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<StudentSchoolFeesStatusDTO> Handle(StudentSchoolFeesStatusQuery request, CancellationToken cancellationToken)
        {
            var getPayment = await _context.Payment.Where(a => a.PersonId == request.PersonId && a.SessionId == request.SessionId && a.FeeTypeId == (int)FeeTypes.SchoolFees).Include(a => a.Person).Include(s => s.Session).FirstOrDefaultAsync();

            StudentSchoolFeesStatusDTO schoolFeesStatusDTO = new StudentSchoolFeesStatusDTO();

            if (getPayment == null)
            {
                var getPerson = await _context.Person.FindAsync(request.PersonId);
                if (getPerson == null)
                {

                    throw new NotFoundException(nameof(Domain.Entities.Person), request.PersonId);

                }
                schoolFeesStatusDTO.PersonId = getPerson.Id;
                schoolFeesStatusDTO.Surname = getPerson.Surname;
                schoolFeesStatusDTO.Firstname = getPerson.Firstname;
                schoolFeesStatusDTO.Othername = getPerson.Othername;
                schoolFeesStatusDTO.SessionId = request.SessionId;
                schoolFeesStatusDTO.Paid = false;
                schoolFeesStatusDTO.Description = "No Payment Record Found";



            }
            else
            {
                var getPaymentEtranzact = await _context.PaymentEtranzact.Where(p => p.PaymentId == getPayment.Id).FirstOrDefaultAsync();
                if (getPaymentEtranzact != null)
                {
                    schoolFeesStatusDTO.PersonId = getPayment.Person.Id;
                    schoolFeesStatusDTO.Surname = getPayment.Person.Surname;
                    schoolFeesStatusDTO.Firstname = getPayment.Person.Firstname;
                    schoolFeesStatusDTO.Othername = getPayment.Person.Othername;
                    schoolFeesStatusDTO.Paid = true;
                    schoolFeesStatusDTO.Description = "Student Has Paid School Fees of N" + getPaymentEtranzact.TransactionAmount;
                    schoolFeesStatusDTO.SessionId = getPayment.SessionId;
                    schoolFeesStatusDTO.SessionName = getPayment.Session.Name;
                }
                else
                {
                    schoolFeesStatusDTO.PersonId = getPayment.Person.Id;
                    schoolFeesStatusDTO.Surname = getPayment.Person.Surname;
                    schoolFeesStatusDTO.Firstname = getPayment.Person.Firstname;
                    schoolFeesStatusDTO.Othername = getPayment.Person.Othername;
                    schoolFeesStatusDTO.Paid = false;
                    schoolFeesStatusDTO.Description = "Generated Invoice but no payment yet";
                    schoolFeesStatusDTO.SessionId = getPayment.SessionId;
                    schoolFeesStatusDTO.SessionName = getPayment.Session.Name;

                }


            }
            return schoolFeesStatusDTO;
        }


    }
}
