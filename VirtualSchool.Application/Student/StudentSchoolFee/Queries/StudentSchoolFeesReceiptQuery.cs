﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.StudentSchoolFee.Models;

namespace VirtualSchool.Application.Student.StudentSchoolFee.Queries
{
    public class StudentSchoolFeesReceiptQuery : IRequest<StudentSchoolFeesReceiptDTO>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
    }
}
