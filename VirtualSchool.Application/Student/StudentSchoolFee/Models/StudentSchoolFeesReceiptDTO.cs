﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.StudentSchoolFee.Models
{
    public class StudentSchoolFeesReceiptDTO
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public int PersonId { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        private string _fullname()
        {
            return Firstname + " " + Surname + " " + Othername;
        }
        public string Fullaname
        {
            get { return _fullname(); }
            set { _fullname(); }
        }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int ProgrammeId { get; set; }
        public string ProgrammeName { get; set; }
        public string JambRegistrationNumber { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public bool SchoolFeesPaid { get; set; }
        public int LevelId { get; set; }
        public string LevelName { get; set; }
        public string Description { get; set; }
        public string PaymentGatewayDescription { get; set; }
        public decimal? Amount { get; set; }
    }
}
