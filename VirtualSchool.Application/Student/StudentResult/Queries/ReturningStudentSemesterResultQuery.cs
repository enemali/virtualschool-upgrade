﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.StudentResult.Models;

namespace VirtualSchool.Application.Student.StudentResult.Queries
{
    public class ReturningStudentSemesterResultQuery:IRequest<IEnumerable<ReturningStudentResultDTO>>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
    }
}
