﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.StudentResult.Models;

namespace VirtualSchool.Application.Student.StudentResult.Queries
{
    public class ReturningStudentTranscriptQuery : IRequest<IEnumerable<ReturningStudentResultDTO>>
    {
        public int PersonId { get; set; }
    }
}
