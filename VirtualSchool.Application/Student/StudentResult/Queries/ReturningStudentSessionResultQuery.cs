﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.StudentResult.Models;

namespace VirtualSchool.Application.Student.StudentResult.Queries
{
    public class ReturningStudentSessionResultQuery : IRequest<IEnumerable<ReturningStudentResultDTO>>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
    }
}
