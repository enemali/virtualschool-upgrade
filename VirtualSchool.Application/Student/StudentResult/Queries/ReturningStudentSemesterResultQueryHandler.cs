﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.StudentResult.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.StudentResult.Queries
{
    public class ReturningStudentSemesterResultQueryHandler : IRequestHandler<ReturningStudentSemesterResultQuery, IEnumerable<ReturningStudentResultDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public ReturningStudentSemesterResultQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ReturningStudentResultDTO>> Handle(ReturningStudentSemesterResultQuery request, CancellationToken cancellationToken)
        {
            var results = await _context.VW_StudentResult.Where(s => s.SessionId == request.SessionId && s.SemesterId == request.SemesterId && s.StudentId == request.PersonId).ToListAsync();

            if (results == null || results.Count <= 0)
                return null;

            var masterResult = results.Select(c => new ReturningStudentResultDTO
            {
                Name = c.Name,
                MatricNumber = c.MatricNumber,
                LevelName = c.LevelName,
                ProgrammeName = c.ProgrammeName,
                DepartmentName = c.DepartmentName,
                SemesterName = c.SemesterName,
                SessionName = c.SessionName,
                CourseCode = c.CourseCode,
                CourseName = c.CourseName,
                CourseUnit = c.Course_Unit,
                TestScore = c.TestScore,
                ExamScore = c.ExamScore,
                SpecialCase = c.SpecialCase,
                TotalScore = c.TotalScore,
                TotalSemesterCourseUnit = c.TotalSemesterCourseUnit,
                CourseMode = c.CourseMode,
                FacultyName = c.FacultyName,
                ContactAddress = c.ContactAddress,
                Email = c.Email,
                PhoneNumber = c.PhoneNumber,
                Grade = c.Grade,
                GradePoint = c.GradePoint,
                WGP = c.WGP,
                GPA = results.Sum(s => s.WGP) / c.TotalSemesterCourseUnit,
                CGPA = results.Sum(s => s.WGP) / c.TotalSemesterCourseUnit,
            }).ToList();

            return masterResult;
        }

    }
}
