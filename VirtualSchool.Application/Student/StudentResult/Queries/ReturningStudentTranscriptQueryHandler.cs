﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.StudentResult.Models;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.StudentResult.Queries
{
    public class ReturningStudentTranscriptQueryHandler : IRequestHandler<ReturningStudentTranscriptQuery, IEnumerable<ReturningStudentResultDTO>>
    {
        private readonly VirtualSchoolDbContext _context;

        public ReturningStudentTranscriptQueryHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<ReturningStudentResultDTO>> Handle(ReturningStudentTranscriptQuery request, CancellationToken cancellationToken)
        {
            var results = await _context.VW_StudentResult.Where(s => s.StudentId == request.PersonId).ToListAsync();

            if (results == null || results.Count <= 0)
                return null;

            var sessions = await _context.Session.ToListAsync();
            for (int i = 0; i < sessions.Count; i++)
            {
                int sessionId = sessions[i].Id;
                int firstSemesterId = (int)Domain.Enumerations.Semesters.FirstSemester;
                int secondSemesterId = (int)Domain.Enumerations.Semesters.SecondSemester;

                //calculate GPA for first semester
                results.Where(r => r.SessionId == sessionId && r.SemesterId == firstSemesterId)?.ToList().ForEach(r => {
                    r.GPA = results.Where(c => c.SessionId == sessionId && c.SemesterId == firstSemesterId).Sum(c => c.WGP) /
                            results.Where(c => c.SessionId == sessionId && c.SemesterId == firstSemesterId).LastOrDefault().TotalSemesterCourseUnit;
                    r.CGPA = results.Sum(c => c.WGP) / results.Sum(c => c.Course_Unit);
                });

                //calculate GPA for second semester
                results.Where(r => r.SessionId == sessionId && r.SemesterId == secondSemesterId)?.ToList().ForEach(r => {
                    r.GPA = results.Where(c => c.SessionId == sessionId && c.SemesterId == secondSemesterId).Sum(c => c.WGP) /
                            results.Where(c => c.SessionId == sessionId && c.SemesterId == secondSemesterId).LastOrDefault().TotalSemesterCourseUnit;
                    r.CGPA = results.Sum(c => c.WGP) / results.Sum(c => c.Course_Unit);
                });
            }

            var masterResult = results.Select(c => new ReturningStudentResultDTO
            {
                Name = c.Name,
                MatricNumber = c.MatricNumber,
                LevelName = c.LevelName,
                ProgrammeName = c.ProgrammeName,
                DepartmentName = c.DepartmentName,
                SemesterName = c.SemesterName,
                SessionName = c.SessionName,
                CourseCode = c.CourseCode,
                CourseName = c.CourseName,
                CourseUnit = c.Course_Unit,
                TestScore = c.TestScore,
                ExamScore = c.ExamScore,
                SpecialCase = c.SpecialCase,
                TotalScore = c.TotalScore,
                TotalSemesterCourseUnit = c.TotalSemesterCourseUnit,
                CourseMode = c.CourseMode,
                FacultyName = c.FacultyName,
                ContactAddress = c.ContactAddress,
                Email = c.Email,
                PhoneNumber = c.PhoneNumber,
                Grade = c.Grade,
                GradePoint = c.GradePoint,
                WGP = c.WGP,
                GPA = c.GPA,
                CGPA = c.CGPA,
                GraduationStatus = GetGraduationStatus(c.CGPA)
            }).ToList();


            return masterResult;
        }

        private string GetGraduationStatus(decimal? CGPA)
        {
            CGPA = CGPA != null ? Math.Round(Convert.ToDecimal(CGPA), 2) : CGPA;
            string title = null;
            try
            {
                if (CGPA >= 3.5M && CGPA <= 4.0M)
                {
                    title = "DISTINCTION";
                }
                else if (CGPA >= 3.0M && CGPA <= 3.49M)
                {
                    title = "UPPER CREDIT";
                }
                else if (CGPA >= 2.5M && CGPA <= 2.99M)
                {
                    title = "LOWER CREDIT";
                }
                else if (CGPA >= 2.0M && CGPA <= 2.49M)
                {
                    title = "PASS";
                }
                else if (CGPA < 2.0M)
                {
                    title = "POOR";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return title;
        }
    }
}
