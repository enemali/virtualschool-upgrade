﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.StudentResult.Models
{
    public class ReturningStudentResultDTO
    {
        public string Name { get; set; }
        public string MatricNumber { get; set; }
        public string LevelName { get; set; }
        public string ProgrammeName { get; set; }
        public string DepartmentName { get; set; }
        public string SemesterName { get; set; }
        public string SessionName { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public int CourseUnit { get; set; }
        public decimal TestScore { get; set; }
        public decimal ExamScore { get; set; }
        public string SpecialCase { get; set; }
        public decimal TotalScore { get; set; }
        public int TotalSemesterCourseUnit { get; set; }
        public string CourseMode { get; set; }
        public string FacultyName { get; set; }
        public string ContactAddress { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Grade { get; set; }
        public decimal GradePoint { get; set; }
        public decimal WGP { get; set; }
        public decimal GPA { get; set; }
        public decimal CGPA { get; set; }
        public string GraduationStatus { get; set; }
    }
}
