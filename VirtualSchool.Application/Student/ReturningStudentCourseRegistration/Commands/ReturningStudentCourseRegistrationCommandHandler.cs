﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Commands
{
    public class ReturningStudentCourseRegistrationCommandHandler : IRequestHandler<ReturningStudentCourseRegistrationCommand, StudentCourseRegistrationList>
    {
        private readonly VirtualSchoolDbContext _context;

        public ReturningStudentCourseRegistrationCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<StudentCourseRegistrationList> Handle(ReturningStudentCourseRegistrationCommand request, CancellationToken cancellationToken)
        {
            StudentCourseRegistrationList returnedStudentCourseRegistration = new StudentCourseRegistrationList();
            returnedStudentCourseRegistration.StudentCourseRegistrationDetail = new List<StudentCourseRegistrationDetailDTO>();

            try
            {

                var returnedCourseRegistrationId = await CreateReturningStudentCourseRegistrationAsync(request);
                var coursesAlreadyRegistered = await RecentlyCreatedReturningStudentCourseRegistrationDetailAsync(request, returnedCourseRegistrationId);
                if (coursesAlreadyRegistered != null && coursesAlreadyRegistered.Count > 0)
                {
                    returnedStudentCourseRegistration.DepartmentId = request.DepartmentId;
                    returnedStudentCourseRegistration.PersonId = request.PersonId;
                    returnedStudentCourseRegistration.ProgrammeId = request.ProgrammeId;
                    returnedStudentCourseRegistration.SemesterId = request.SemesterId;
                    returnedStudentCourseRegistration.SessionId = request.SessionId;
                    foreach (var courseRegistrationDetailId in coursesAlreadyRegistered)
                    {
                        StudentCourseRegistrationDetailDTO detailId = new StudentCourseRegistrationDetailDTO();
                        detailId.StudentCourseRegistrationId = courseRegistrationDetailId.Id;
                        returnedStudentCourseRegistration.StudentCourseRegistrationDetail.Add(detailId);
                    }
                }
                else
                {
                    if (returnedCourseRegistrationId > 0)
                    {
                        var isCompleted = await CreateReturningStudentCourseRegistrationDetailAsync(request, returnedCourseRegistrationId);
                        if (isCompleted)
                        {
                            var returnedStudentCourseRegistrationDetail = await RecentlyCreatedReturningStudentCourseRegistrationDetailAsync(request, returnedCourseRegistrationId);
                            returnedStudentCourseRegistration.DepartmentId = request.DepartmentId;
                            returnedStudentCourseRegistration.PersonId = request.PersonId;
                            returnedStudentCourseRegistration.ProgrammeId = request.ProgrammeId;
                            returnedStudentCourseRegistration.SemesterId = request.SemesterId;
                            returnedStudentCourseRegistration.SessionId = request.SessionId;
                            foreach (var courseRegistrationDetailId in returnedStudentCourseRegistrationDetail)
                            {
                                StudentCourseRegistrationDetailDTO detailId = new StudentCourseRegistrationDetailDTO();
                                detailId.StudentCourseRegistrationId = courseRegistrationDetailId.Id;
                                returnedStudentCourseRegistration.StudentCourseRegistrationDetail.Add(detailId);
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnedStudentCourseRegistration;
        }
        public async Task<long> CreateReturningStudentCourseRegistrationAsync(ReturningStudentCourseRegistrationCommand request)
        {
            var existingCourseRegistration = new StudentCourseRegistration();
            try
            {
                Expression<Func<StudentCourseRegistration, bool>> selector = x => x.DepartmentId == request.DepartmentId && x.LevelId == request.LevelId && x.StudentId == request.PersonId
                && x.ProgrammeId == request.ProgrammeId && x.SessionId == request.SessionId;
                existingCourseRegistration = await _context.StudentCourseRegistration.Where(selector).FirstOrDefaultAsync();
                if (existingCourseRegistration == null)
                {
                    StudentCourseRegistration studentCourseRegistrationEntity = new StudentCourseRegistration()
                    {
                        StudentId = request.PersonId,
                        LevelId = request.LevelId,
                        SessionId = request.SessionId,
                        ProgrammeId = request.ProgrammeId,
                        DepartmentId = request.DepartmentId
                    };
                    _context.StudentCourseRegistration.Add(studentCourseRegistrationEntity);
                    await _context.SaveChangesAsync();
                    return studentCourseRegistrationEntity.Id;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return existingCourseRegistration.Id;
        }
        public async Task<bool> CreateReturningStudentCourseRegistrationDetailAsync(ReturningStudentCourseRegistrationCommand request, long studentCourseRegistrationId)
        {
            List<long> studentCourseRegistrationDetailId = new List<long>();
            try
            {
                var courses = request.StudentRegisteredCoursesDTO;
                int count = 0;
                foreach (var course in courses)
                {
                    //Only courses that are checked are added to the StudentCourseRegistrationDetail Table
                    if (course.IsRegistered == true)
                    {
                        count += 1;
                        StudentCourseRegistrationDetail studentCourseRegistrationDetailEntity = new StudentCourseRegistrationDetail()
                        {
                            StudentCourseRegistrationId = studentCourseRegistrationId,
                            CourseId = course.CourseId,
                            CourseModeId = course.CourseModeId,
                            SemesterId = request.SemesterId

                        };
                        var createdDetail = _context.StudentCourseRegistrationDetail.Add(studentCourseRegistrationDetailEntity);

                        studentCourseRegistrationDetailId.Add(createdDetail.Entity.Id);
                    }


                }
                await _context.SaveChangesAsync();
                if (studentCourseRegistrationDetailId.Count == count)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }
        public async Task<List<StudentCourseRegistrationDetail>> RecentlyCreatedReturningStudentCourseRegistrationDetailAsync(ReturningStudentCourseRegistrationCommand request, long studentCourseRegistrationId)
        {
            try
            {
                Expression<Func<StudentCourseRegistrationDetail, bool>> selector =
                    p =>
                        p.StudentCourseRegistrationId == studentCourseRegistrationId && p.SemesterId == request.SemesterId;
                var studentCourseRegistrationDetailEntity = await _context.StudentCourseRegistrationDetail.Where(selector).ToListAsync();
                return studentCourseRegistrationDetailEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
