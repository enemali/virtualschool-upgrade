﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Commands
{
    public class UpdateReturningStudentCourseRegistrationCommandHandler : IRequestHandler<UpdateReturningStudentCourseRegistrationCommand, StudentUpdateCourseRegistrationDTO>
    {
        private readonly VirtualSchoolDbContext _context;
        public UpdateReturningStudentCourseRegistrationCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
        }
        public async Task<StudentUpdateCourseRegistrationDTO> Handle(UpdateReturningStudentCourseRegistrationCommand request, CancellationToken cancellationToken)
        {
            StudentUpdateCourseRegistrationDTO updateCourseRegistrationDTO = new StudentUpdateCourseRegistrationDTO();
            updateCourseRegistrationDTO.StudentCourseUpdateDTO = new List<StudentCourseUpdateDTO>();
            try
            {
                //Delete existing RegistrationDetail
                var isDeleted = await DeleteExistingRecord(request);
                if (isDeleted)
                {
                    //Check max and min Course unit for the Session and Semester
                    var returnedCourseUnit = await GetMaxandMinCourseUnit(request);
                    //Get list of Courses to Register
                    var courseList = await GetCourseBy(request);
                    var isUpdated = await RecreateStudentCourseRegistrationDetailAsync(request);
                    if (isUpdated)
                    {
                        var registeredCourses = await DetailCourseRegistrationUpdate(request);
                        if (registeredCourses != null)
                        {

                            foreach (var course in courseList)
                            {
                                var courseUpdateDTO = new StudentCourseUpdateDTO();
                                var existId = registeredCourses.Where(x => x.Id == course.Id).FirstOrDefault();
                                if (existId == null)
                                {
                                    courseUpdateDTO.IsRegistered = false;
                                }
                                else
                                {
                                    courseUpdateDTO.IsRegistered = true;
                                }

                                courseUpdateDTO.CourseId = course.Id;
                                courseUpdateDTO.CourseCode = course.Code;
                                courseUpdateDTO.CourseName = course.Name;
                                courseUpdateDTO.CourseTypeId = course.CourseTypeId;
                                courseUpdateDTO.CourseUnit = course.Unit;
                                updateCourseRegistrationDTO.StudentCourseUpdateDTO.Add(courseUpdateDTO);
                            }
                            updateCourseRegistrationDTO.DepartmentId = request.DepartmentId;
                            updateCourseRegistrationDTO.LevelId = request.LevelId;
                            updateCourseRegistrationDTO.MaxCourseUnit = returnedCourseUnit.MaximumUnit;
                            updateCourseRegistrationDTO.MinCourseUnit = returnedCourseUnit.MinimumUnit;
                            updateCourseRegistrationDTO.PersonId = request.PersonId;
                            updateCourseRegistrationDTO.SessionId = request.SessionId;
                            updateCourseRegistrationDTO.SemesterId = request.SemesterId;
                            updateCourseRegistrationDTO.StudentCourseRegistrationId = request.StudentCourseRegistrationId;
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return updateCourseRegistrationDTO;
        }
        public async Task<bool> DeleteExistingRecord(UpdateReturningStudentCourseRegistrationCommand request)
        {
            try
            {
                var existingList = await _context.StudentCourseRegistrationDetail.Where(x => x.StudentCourseRegistrationId == request.StudentCourseRegistrationId).ToListAsync();
                if (existingList != null)
                {
                    _context.RemoveRange(existingList);
                    await _context.SaveChangesAsync();
                    return true;
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }
        public async Task<bool> RecreateStudentCourseRegistrationDetailAsync(UpdateReturningStudentCourseRegistrationCommand request)
        {
            List<long> studentCourseRegistrationDetailId = new List<long>();
            try
            {
                var courses = request.StudentUpdateCourseRegistrationDTO.StudentCourseUpdateDTO;
                int count = 0;
                foreach (var course in courses)
                {
                    //Only courses that are checked are added to the StudentCourseRegistrationDetail Table
                    if (course.IsRegistered == true)
                    {
                        count += 1;
                        StudentCourseRegistrationDetail studentCourseRegistrationDetailEntity = new StudentCourseRegistrationDetail()
                        {
                            StudentCourseRegistrationId = request.StudentCourseRegistrationId,
                            CourseId = course.CourseId,
                            CourseModeId = course.CourseModeId,
                            SemesterId = request.SemesterId

                        };
                        var createdDetail = _context.StudentCourseRegistrationDetail.Add(studentCourseRegistrationDetailEntity);

                        studentCourseRegistrationDetailId.Add(createdDetail.Entity.Id);
                    }


                }
                await _context.SaveChangesAsync();
                if (studentCourseRegistrationDetailId.Count == count)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }
        public async Task<List<Course>> DetailCourseRegistrationUpdate(UpdateReturningStudentCourseRegistrationCommand request)
        {
            List<Course> courseList = new List<Course>();
            try
            {
                var registeredCourses = await _context.StudentCourseRegistrationDetail.Where(x => x.StudentCourseRegistrationId == request.StudentCourseRegistrationId && x.SemesterId == request.SemesterId).ToListAsync();
                if (registeredCourses != null)
                {
                    foreach (var registeredCourse in registeredCourses)
                    {
                        var course = await _context.Course.FindAsync(registeredCourse.CourseId);
                        if (course != null)
                        {
                            courseList.Add(course);
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return courseList;
        }
        public async Task<CourseUnit> GetMaxandMinCourseUnit(UpdateReturningStudentCourseRegistrationCommand request)
        {
            try
            {
                Expression<Func<CourseUnit, bool>> selector = p => p.LevelId == request.LevelId && p.DepartmentId == request.DepartmentId && p.SemesterId == request.SemesterId;
                var maxMinCourseUnit = await _context.CourseUnit.Where(selector).FirstOrDefaultAsync();
                return maxMinCourseUnit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Course>> GetCourseBy(UpdateReturningStudentCourseRegistrationCommand request)
        {
            try
            {
                Expression<Func<Course, bool>> selector = p => p.LevelId == request.LevelId && p.DepartmentId == request.DepartmentId && p.SemesterId == request.SemesterId && p.Activated == true;
                var courseList = await _context.Course.Where(selector).ToListAsync();

                return courseList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
