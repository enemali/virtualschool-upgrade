﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Commands
{
    public class UpdateReturningStudentCourseRegistrationCommand : IRequest<StudentUpdateCourseRegistrationDTO>
    {
        public int PersonId { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public long StudentCourseRegistrationId { get; set; }
        public StudentUpdateCourseRegistrationDTO StudentUpdateCourseRegistrationDTO { get; set; }
    }
}
