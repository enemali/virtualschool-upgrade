﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Queries
{
    public class GetReturningStudentRegisteredCourseQueryHandler : IRequestHandler<GetReturningStudentRegisteredCourseQuery, StudentCourseRegistrationDTO>
    {
            private readonly VirtualSchoolDbContext _context;
            public GetReturningStudentRegisteredCourseQueryHandler(VirtualSchoolDbContext context)
            {
                _context = context;
            }
            public async Task<StudentCourseRegistrationDTO> Handle(GetReturningStudentRegisteredCourseQuery request, CancellationToken cancellationToken)
            {
                try
                {
                    var listOfCoursesRegistered = await GetRegisteredCourses(request);
                    return listOfCoursesRegistered;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public async Task<StudentCourseRegistrationDTO> GetRegisteredCourses(GetReturningStudentRegisteredCourseQuery request)
            {
                StudentCourseRegistrationDTO courseRegistrationDTO = new StudentCourseRegistrationDTO();
                courseRegistrationDTO.StudentCoursesDTO = new List<StudentCoursesDTO>();
                try
                {
                    Expression<Func<StudentCourseRegistration, bool>> selector = p => p.StudentId == request.PersonId && p.SessionId == request.SessionId;
                    var courseRegistered = await _context.StudentCourseRegistration.Where(selector).FirstOrDefaultAsync();
                    if (courseRegistered != null)
                    {
                        var returnedRegisteredCourses = await GetRegisteredCourses(courseRegistered, request);
                        foreach (var returnedRegisteredCourse in returnedRegisteredCourses)
                        {
                            StudentCoursesDTO course = new StudentCoursesDTO();
                            course.CourseId = returnedRegisteredCourse.Id;
                            course.CourseCode = returnedRegisteredCourse.Code;
                            course.CourseName = returnedRegisteredCourse.Name;
                            course.CourseTypeId = returnedRegisteredCourse.CourseTypeId;
                            course.CourseUnit = returnedRegisteredCourse.Unit;
                            course.IsRegistered = true;
                            courseRegistrationDTO.StudentCoursesDTO.Add(course);
                        }
                        courseRegistrationDTO.DepartmentId = courseRegistered.DepartmentId;
                        courseRegistrationDTO.LevelId = courseRegistered.LevelId;
                        courseRegistrationDTO.ProgrammeId = courseRegistered.ProgrammeId;
                        courseRegistrationDTO.SessionId = courseRegistered.SessionId;
                        courseRegistrationDTO.PersonId = request.PersonId;
                    }

                    return courseRegistrationDTO;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public async Task<List<Course>> GetRegisteredCourses(StudentCourseRegistration studentCourseRegistration, GetReturningStudentRegisteredCourseQuery request)
            {
                List<StudentCourseRegistrationDetail> studentCourseRegistrationDetailList = new List<StudentCourseRegistrationDetail>();
                try
                {
                    Expression<Func<StudentCourseRegistrationDetail, bool>> selector = p => p.StudentCourseRegistrationId == studentCourseRegistration.Id;
                    var courseRegisteredDetailList = await _context.StudentCourseRegistrationDetail.Where(selector).ToListAsync();
                    var returnedregisteredCoursesList = await GetRegisteredCoursesByCourseId(courseRegisteredDetailList);

                    return returnedregisteredCoursesList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public async Task<List<Course>> GetRegisteredCoursesByCourseId(List<StudentCourseRegistrationDetail> studentCourseRegistrationDetail)
            {
                List<Course> registeredCoursesList = new List<Course>();
                try
                {
                    foreach (var courseDetail in studentCourseRegistrationDetail)
                    {
                        var registeredCourses = await _context.Course.FindAsync(courseDetail.CourseId);
                        registeredCoursesList.Add(registeredCourses);
                    };


                    return registeredCoursesList;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }

