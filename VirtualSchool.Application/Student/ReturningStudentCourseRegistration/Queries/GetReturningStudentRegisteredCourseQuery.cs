﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Queries
{
   public class GetReturningStudentRegisteredCourseQuery : IRequest<StudentCourseRegistrationDTO>
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
    }
}
