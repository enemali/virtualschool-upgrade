﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models
{
    public class StudentUpdateCourseRegistrationDTO
    {
        public int PersonId { get; set; }
        public int SemesterId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public long StudentCourseRegistrationId { get; set; }
        public int MaxCourseUnit { get; set; }
        public int MinCourseUnit { get; set; }
        public List<StudentCourseUpdateDTO> StudentCourseUpdateDTO { get; set; }
    }
}
