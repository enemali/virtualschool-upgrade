﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models
{
    public class StudentCoursesDTO
    {
        public long CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public int CourseTypeId { get; set; }
        public int CourseUnit { get; set; }
        public bool IsRegistered { get; set; }
    }
}
