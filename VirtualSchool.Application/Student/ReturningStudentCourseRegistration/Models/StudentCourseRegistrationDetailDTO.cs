﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models
{
    public class StudentCourseRegistrationDetailDTO
    {
        public long StudentCourseRegistrationId { get; set; }
    }
}
