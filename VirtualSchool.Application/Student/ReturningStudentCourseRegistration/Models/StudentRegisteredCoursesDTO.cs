﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models
{
    public class StudentRegisteredCoursesDTO
    {
        public long CourseId { get; set; }
        public bool IsRegistered { get; set; }
        public int CourseModeId { get; set; }
    }
}
