﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.ReturningStudentCourseRegistration.Models
{
    public class StudentCourseRegistrationDTO
    {
        public int PersonId { get; set; }
        public int SessionId { get; set; }
        public int DepartmentId { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public int SemesterId { get; set; }
        public List<StudentCoursesDTO> StudentCoursesDTO { get; set; }
    }
}
