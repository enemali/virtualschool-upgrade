﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualSchool.Application.Exceptions;
using VirtualSchool.Application.Student.StudentLogin.Model;
using VirtualSchool.Application.Utilities.Level;
using VirtualSchool.Common;
using VirtualSchool.Domain.Entities;
using VirtualSchool.Persistence;

namespace VirtualSchool.Application.Student.StudentLogin.Command
{
    public class StudentLoginCommandHandler : IRequestHandler<StudentLoginCommand, StudentLoginReturnDTO>
    {
        private readonly VirtualSchoolDbContext _context;
       //private readonly AppSettings _appSettings;
        public StudentLoginCommandHandler(VirtualSchoolDbContext context)
        {
            _context = context;
           // _appSettings = appSettings;
        }
        public async Task<StudentLoginReturnDTO> Handle(StudentLoginCommand request, CancellationToken cancellationToken)
        {
            string requestSlug = Slug.GenerateSlug(request.MatricNumber);
            // AppSettings _appSettings = new AppSettings();
            
            var existingEntity = await _context.Student.Where(a => a.MatricNumber == request.MatricNumber && a.PasswordHash == request.Password).FirstOrDefaultAsync(cancellationToken);
            if (existingEntity != null)
            {
                var mySecret = "asdv234234^&%&^%&^hjsdfb2%%%";
                var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));

               
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
            new Claim(ClaimTypes.NameIdentifier, existingEntity.PersonId.ToString()),
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                   
                    SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
               
                var tokenString = tokenHandler.WriteToken(token);
                var StudentDetails =  _context.StudentLevel.Where(x => x.StudentId == existingEntity.PersonId).AsEnumerable().LastOrDefault();
                StudentLoginReturnDTO studentLoginReturnDTO = new StudentLoginReturnDTO()
                {
                    MatricNumber = existingEntity.MatricNumber,
                    PersonId = existingEntity.PersonId,
                    Token = tokenString,
                    CurrentLevelId = StudentDetails.LevelId,
                    ProgrammeId = StudentDetails.ProgrammeId,
                    DepartmentId = StudentDetails.DepartmentId,
                     SessionId = StudentDetails.SessionId
                };
                return studentLoginReturnDTO;

            }
            else
            {
                throw new ExistingItemException(nameof(Domain.Entities.Student), "Student Record Not Found");
            }










        }
    }
}
