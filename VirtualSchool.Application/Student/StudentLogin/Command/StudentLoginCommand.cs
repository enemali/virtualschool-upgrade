﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Student.StudentLogin.Model;

namespace VirtualSchool.Application.Student.StudentLogin.Command
{
    public class StudentLoginCommand : IRequest<StudentLoginReturnDTO>
    {
        public string MatricNumber { get; set; }
        public string Password { get; set; }
    }
}
