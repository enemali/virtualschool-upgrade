﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualSchool.Application.Student.StudentLogin.Model
{
   public class StudentLoginReturnDTO
    {
        public string MatricNumber { get; set; }
        public int PersonId { get; set; }
        public string Token { get; set; }
        public int DepartmentId { get; set; }
        public int ProgrammeId { get; set; }
        public int CurrentLevelId { get; set; }
        public int SessionId { get; set; }
    }
}

