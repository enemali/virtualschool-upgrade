﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSchool.Application.Notifications.Models;

namespace VirtualSchool.Application.Interface
{
    public interface INotificationService
    {
        void Send(Message message);
    }
}
